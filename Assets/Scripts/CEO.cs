﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;

public class CEO : MonoBehaviour
{

    public GameObject panelMain;
    public GameObject panelHome;
    public GameObject panelNetworkServer;
    public GameObject panelNetworkClient;
    public GameObject panelSettings;

    public Text ipText;
    public Text statusText;

    private bool areButtonsInteractable = false;

    private int currentLevel = 0;

    private bool showReceivedTexture = false;

    private bool isSide = false;
    private bool isFloor = false;
    private bool isPlayerLeft = true;
    private bool isSingleTest = false;



    protected static CEO instance = null;

    public static CEO Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        // set the singleton instance
        instance = this;
    }

    void Start()
    {
        DontDestroyOnLoad(transform.gameObject);
        SetIPText();
        //ToggleMain();
        SaveLoad.Load();

        SetIsSide(SaveLoad.savedSettings.isSide);
        SetIsPlayerLeft(SaveLoad.savedSettings.isPlayerLeft);
        Invoke("AutoNetwork", 1f);
        ClearResources();

    }


    public void SetNewParent(GameObject parentObject)
    {
        gameObject.transform.parent = parentObject.transform;
    }

    private void AutoNetwork()
    {
        if (isSide)
        {
            LNetworkManager.Instance.StartLITServer();
        }

        else
        {
            LNetworkManager.Instance.StartLITClient();
        }
    }


    public void ToggleTest()
    {
        isSingleTest = !isSingleTest;
    }

    public bool GetIsSingleTest()
    {
        return isSingleTest;
    }

    public void SetIsSide(bool is_side)
    {
        isSide = is_side;
        isFloor = !isSide;
        SaveLoad.savedSettings.isSide = is_side;
    }

    public bool GetIsSide()
    {
        return isSide;
    }

    public bool GetIsFloor()
    {
        return isFloor;
    }

    public void SetIsPlayerLeft(bool is_plLeft)
    {
        isPlayerLeft = is_plLeft;
        SaveLoad.savedSettings.isPlayerLeft = is_plLeft;
    }

    public bool GetIsPlayerLeft()
    {
        return isPlayerLeft;
    }

    public void SetIPText()
    {
        ipText.text = "This IP: " + GetIP();
    }

    public void SetNetworkStatusText(string st)
    {
        statusText.text = "Status: " + st;
    }

    public string GetIP()
    {
        //string ipAddress = NetworkManager.singleton.networkAddress;
        string ipAddress = Network.player.ipAddress;
        //string ipAddress = LocalIPAddress();
        return ipAddress;
    }

    public string LocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }

    //GUI Methods
    void CrossFadeAlpha(GameObject gObject, float target, float animationTime)
    {

        //Debug.Log("CrossFadeAlpha "+ gObject + " target: "+ target + " animationTime: "+ animationTime);
        areButtonsInteractable = false;
        Invoke("ReactivateInteraction", animationTime + 1f);
        bool justSetActive = false;
        if (target == 0f)
        {
            StartCoroutine(DeactivateDelayed(gObject, animationTime));
        }
        else
        {
            gObject.SetActive(true);
            justSetActive = true;
            areButtonsInteractable = true;
        }

        foreach (Graphic graphic in gObject.GetComponentsInChildren<Graphic>())
        {
            if (justSetActive)
            {
                graphic.canvasRenderer.SetAlpha(0.0f);
            }
            graphic.CrossFadeAlpha(target, animationTime, true);

        }
    }


    IEnumerator DeactivateDelayed(GameObject go, float timeSeconds)
    {
        yield return new WaitForSeconds(timeSeconds);
        go.SetActive(false);
    }

    private void ReactivateInteraction()
    {
        areButtonsInteractable = true;
    }

    public bool AreButtonsInteractable()
    {
        return areButtonsInteractable;
    }

    private void ChangePanelStates(float home, float network, float settings, float time = 0.5f)
    {
        CrossFadeAlpha(panelHome, home, time);

        if (isSide)
        {
            CrossFadeAlpha(panelNetworkServer, network, time);
        }
        else
        {
            CrossFadeAlpha(panelNetworkClient, network, time);
        }

        CrossFadeAlpha(panelSettings, settings, time);
    }

    public void SetMainActive(bool act)
    {
        float tm = 0.5f;
        if (act && panelMain.active == false)
        {
            CrossFadeAlpha(panelMain, 1f, tm);
            ChangePanelStates(1, 0, 0, 0.01f);
        }
        else
        {
            CrossFadeAlpha(panelMain, 0f, tm);
        }
    }

    public void ToggleMain()
    {
        SetMainActive(!panelMain.active);
    }

    public void MenuButton(int btn)
    {
        switch (btn)
        {
            case 0: //Home
                ChangePanelStates(1, 0, 0);
                break;
            case 1: // network
                ChangePanelStates(0, 1, 0);
                break;
            case 3: // settings
                ChangePanelStates(0, 0, 1);
                break;
            case 4: // Proton Football
                if (isSingleTest)
                {
                    LoadLevel(2);
                }
                else
                {
                    SetLoadingLevel(2);
                }
                break;
            case 5: // Level MoviePlayer
                if (isSingleTest)
                {
                    LoadLevel(3);
                }
                else
                {
                    SetLoadingLevel(3);
                }
                break;
            case 12: // Start LIT Client
                LNetworkManager.Instance.StartLITClient();
                break;
            case 13: // Start LIT Server
                LNetworkManager.Instance.StartLITServer();
                break;
            default:
                break;
        }
    }


    public void SetLoadingLevel(int level)
    {
        if (LPlayer.Instance != null && isFloor)
        {
            LPlayer.Instance.CmdSetLevelSynchronize(level);
        }
        else
        {
            LPlayer.Instance.levelSynchronize = level;
        }

    }


    public void LoadLevel(int level)
    {
        Debug.Log("LoadLevel " + level);
        if (currentLevel != level)
        {
            switch (level)
            {
                case -1: //Reset
                    Reset();
                    break;
                case 0: //Home
                    LoadIntro();
                    break;
                case 1: // Higgnite
                        //ChangePanelStates(0, 1);
                    break;
                case 2: // Proton Football
                    LoadProtonFootball();
                    SetMainActive(false);
                    break;
                case 3: // Level MoviePlayer
                    LoadMoviePlayer();
                    SetMainActive(false);
                    break;
                case 4: // Level HEAL
                    LoadHeal();
                    SetMainActive(false);
                    break;
                case 5: // Level Discovery Wall
                    LoadDW();
                    SetMainActive(false);
                    break;
                case 10: // Level Calibration
                    LoadCalibration();
                    SetMainActive(false);
                    break;
                default:
                    break;

            }

            currentLevel = level;
        }
    }

    public void LoadProtonFootball()
    {
        if (GetIsFloor())
        {
            SceneManager.LoadScene("PF_Floor", LoadSceneMode.Single);
        }
        else
        {
            SceneManager.LoadScene("PF_Side", LoadSceneMode.Single);
        }
    }

    public void LoadHeal()
    {
        if (GetIsFloor())
        {
            SceneManager.LoadScene("HEAL_Floor", LoadSceneMode.Single);
        }
        else
        {
            SceneManager.LoadScene("HEAL_Side", LoadSceneMode.Single);
        }
    }

    public void LoadDW()
    {
        if (GetIsFloor())
        {
            SceneManager.LoadScene("Discovery_Wall_Floor", LoadSceneMode.Single);
        }
        else
        {
            SceneManager.LoadScene("Discovery_Wall_Side", LoadSceneMode.Single);
        }
    }

    public void LoadCalibration()
    {
        SetMainActive(false);
        if (GetIsFloor())
        {
            SceneManager.LoadScene("Calibration_Floor", LoadSceneMode.Single);
        }
        else
        {
            SceneManager.LoadScene("Calibration_Floor", LoadSceneMode.Single);
        }
    }

    public void LoadMoviePlayer()
    {
        if (GetIsFloor())
        {
            SceneManager.LoadScene("Blank", LoadSceneMode.Single);
        }
        else
        {
            SceneManager.LoadScene("MoviePlayer", LoadSceneMode.Single);
        }
    }

    public void LoadIntro()
    {
        SceneManager.LoadScene("Intro", LoadSceneMode.Single);
    }

    void OnGUI()
    {
        if (showReceivedTexture)
            GUI.DrawTexture(new Rect(30, 30, 120, 80), KinectChassis.Instance.GetReceivedTex());
    }

    private void DisplayBodyData()
    {

        string bodyData = KinOps.Instance.GetSkeletons2();

        if (bodyData != null)
        {
            //UDPSend.Instance.sendString(bodyData);

            if (TestText.Instance != null)
                TestText.Instance.SetTestText(bodyData);

        }

    }

    private void ClearResources()
    {
        Resources.UnloadUnusedAssets();
        Invoke("ClearResources", 60f);
    }

    public void Reset()
    {
        SceneManager.LoadScene("Reset", LoadSceneMode.Single);
    }

    public void Kill()
    {
        Destroy(gameObject);
    }

    public void DetectPressedKeyOrButton()
    {
        foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(kcode))
                Debug.Log("KeyCode down: " + kcode);
        }
    }

    void OnApplicationQuit()
    {
        StopAllCoroutines();
    }



    void Update()
    {

        //DisplayBodyData();
        if (Input.GetKeyDown(KeyCode.F1))
        {
            ToggleMain();
        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.S))
        {
            SaveLoad.Save();
        }

        //if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.R))
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.R))
        {
            //Invoke("Reset", 0.5f);
            if (LPlayer.Instance != null)
            {
                if (GetIsFloor())
                {
                    LPlayer.Instance.CmdReset();
                }
                else
                {
                    LPlayer.Instance.RpcReset();
                }

            }

            SetLoadingLevel(-1);

        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.C))
        {
            LoadCalibration();

        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.H))
        {
            if (isSingleTest)
            {
                LoadLevel(4);
            }
            else
            {
                SetLoadingLevel(4);
            }

        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.D))
        {
            LoadLevel(5);
        }


        if (LPlayer.Instance != null && LPlayer.Instance.levelSynchronize != currentLevel)
        {
            LoadLevel(LPlayer.Instance.levelSynchronize);
        }

        //DetectPressedKeyOrButton();

    }


}
