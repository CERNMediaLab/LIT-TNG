﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LPlayerHEAL : NetworkBehaviour
{
	
	protected static LPlayerHEAL instance = null;

	[SyncVar]
	public Vector3 playerFloorPosition = Vector3.zero;

	public static LPlayerHEAL Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		// set the singleton instance
		instance = this;
	}

	void Start ()
	{

	}

	[Command]
	public void CmdStartSendingBeamSide()
	{
		HealManager.Instance.ActionS_StartSendingBeamSide();
	}

	[ClientRpc]
	public void RpcSendprotonFloor()
	{
		HealManager.Instance.ActionF_SendprotonFloor();
    }

    [ClientRpc]
    public void RpcAuthorizeProtons()
    {
        HealManager.Instance.ActionF_AuthorizeProtonsFloor();
    }

    [ClientRpc]
	public void RpcSetIsP1InTheZone(bool ip1itzSet)
	{
		HealManager.Instance.SetIsP1InTheZone(ip1itzSet);
		//Debug.Log("RpcSetIsP1InTheZone " + ip1itzSet);
	}

	[ClientRpc]
	public void RpcSetIsP2InTheZone(bool ip2itzSet)
	{
		HealManager.Instance.SetIsP2InTheZone(ip2itzSet);
		//Debug.Log("RpcSetIsP2InTheZone " + ip2itzSet);
	}

	[ClientRpc]
	public void RpcSetIsP3InTheZone(bool ip3itzSet)
	{
		HealManager.Instance.SetIsP3InTheZone(ip3itzSet);
		//Debug.Log("RpcSetIsP3InTheZone " + ip3itzSet);
	}

	[ClientRpc]
	public void RpcReset()
	{
		HealManager.Instance.ResetfromRemote();
	}

	[Command]
	public void CmdReset()
	{
		HealManager.Instance.ResetfromRemote();
	}


	void Update ()
	{

	}
}
