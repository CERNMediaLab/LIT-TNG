﻿using System;
using UnityEngine;
using UnityEngine.Networking;


public class llapi_INetworkManager : MonoBehaviour
{

	// The singleton instance of llapi_INetworkManager
	protected static llapi_INetworkManager instance = null;

	private bool _isStarted = false;
	private bool _isServer = false;
	private bool _isClientConnected = false;
	string ip  = "127.0.0.1";
	int   port = 7075;
	private int _messageIdx = 0;

	private int m_ConnectionId = 0;
	//private int m_WebSocketHostId = 0;
	private int m_GenericHostId = 0;

	private string m_SendString = "";
	private string m_RecString  = "";
	private ConnectionConfig m_Config = null;
	private byte m_CommunicationChannel = 0;


	void Start()
	{		
		instance = this;														//singleton
		m_Config = new ConnectionConfig();                                      //create configuration containing one reliable channel
		m_CommunicationChannel = m_Config.AddChannel(QosType.Reliable);
	}

	public static llapi_INetworkManager Instance
	{
		get
		{
			return instance;
		}
	}

	void OnGUI () {
		GUI.Box(new Rect(5, 5, 450, 450), "window");
		if ( !_isStarted )
		{
			ip = GUI.TextField(new Rect(10, 10, 250, 30), ip, 25);
			port = Convert.ToInt32( GUI.TextField(new Rect(10, 40, 250, 30), port.ToString(), 25) );

			if ( GUI.Button( new Rect(10, 70, 250, 30), "start server" ) )
			{
				StartServer();
			}

			if (GUI.Button(new Rect(10, 100, 250, 30), "start client"))
			{
				StartClient();
			}
		}
		else
		{
			GUI.Label(new Rect(10, 20, 250, 500), "Sent: " + m_SendString);
			GUI.Label(new Rect(10, 70, 250, 50), "Recv: " + m_RecString);
			if (GUI.Button(new Rect(10, 120, 250, 50), "stop"))
			{
				Stop();
			}
		}
	}

	public void StartServer()
	{
		_isStarted = true;
		_isServer = true;
		NetworkTransport.Init();

		HostTopology topology = new HostTopology(m_Config, 12);
		//m_WebSocketHostId = NetworkTransport.AddWebsocketHost(topology, port, null);     //Websocket just in case
		m_GenericHostId = NetworkTransport.AddHost(topology, port, null);
	}

	public void StartClient()
	{
		_isStarted = true;
		_isServer = false;
		NetworkTransport.Init();

		HostTopology topology = new HostTopology(m_Config, 12);
		m_GenericHostId = NetworkTransport.AddHost(topology, 0); //any port for udp client, for websocket second parameter is ignored, as webgl based game can be client only
		byte error;
		m_ConnectionId = NetworkTransport.Connect(m_GenericHostId, ip, port, 0, out error);
	}

	public void Stop()
	{
		_isStarted = false;
		NetworkTransport.Shutdown();
	}

	string GenerateRandomString()
	{
		string myString = "";
		int minCharAmount = 8;
		int maxCharAmount = 16;
		const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";
		int charAmount = UnityEngine.Random.Range(minCharAmount, maxCharAmount); //set those to the minimum and maximum length of your string
		for (int i = 0; i < charAmount; i++)
		{
			myString += glyphs[UnityEngine.Random.Range(0, glyphs.Length)];
		}
		return myString;
	}

	public void SendStringToServer(string message)
	{
		if (!_isStarted || _isServer)
			return;

		SendStringToConnectionID( message, m_ConnectionId);
	}

	public void SendStringToConnectionID(string message, int connectionID)
	{
		byte error;
		byte[] bytes = new byte[message.Length * sizeof(char)];
		System.Buffer.BlockCopy(message.ToCharArray(), 0, bytes, 0, bytes.Length);
		NetworkTransport.Send(m_GenericHostId, connectionID, m_CommunicationChannel, bytes, bytes.Length, out error);
	}

	void Update()
	{
		if (!_isStarted)
			return;
		int recHostId;
		int connectionId;
		int channelId;
		byte[] recBuffer = new byte[1024];
		int bufferSize = 1024;
		int dataSize;
		byte error;
		NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
		switch (recData)
		{

		case NetworkEventType.Nothing:
			break;

		case NetworkEventType.ConnectEvent:
		{
			if (!_isServer)
			{
				_isClientConnected = true;
			}
			Debug.Log(String.Format("Connect from host {0} connection {1}", recHostId, connectionId));
			break;
		}

		case NetworkEventType.DataEvent:  //if server will receive echo it will send it back to client, when client will receive echo from serve wit will send other message
		{

			//Debug.Log(String.Format("Received event host {0} connection {1} channel {2} message length {3}", recHostId, connectionId, channelId, dataSize));
			char[] chars = new char[dataSize / sizeof(char)];
			System.Buffer.BlockCopy(recBuffer, 0, chars, 0, dataSize);
			m_RecString = new string(chars);

		}
		break;
		case NetworkEventType.DisconnectEvent:
		{
			if (!_isServer)
			{
				_isClientConnected = false;
				Debug.Log(String.Format("DisConnect from host {0} connection {1}", recHostId, connectionId));
			}
			break;
		}
		}

		if (Input.GetKeyUp ("h"))
		{
			SendStringToServer(GenerateRandomString());
		}
	}
}
