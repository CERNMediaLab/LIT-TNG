﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class INetworkManager : NetworkManager
{
	// The singleton instance of NetworkManager
	protected static INetworkManager instance = null;

	public bool isAtStartup = true;

	NetworkClient myClient;


	public static INetworkManager Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		// set the singleton instance
		instance = this;
		Initialize();
	}

	private void Initialize()
	{
		DontDestroyOnLoad(gameObject);
	}

	public void Shout()
	{
		Debug.Log("potato");
	}

	public void SetupServer()
    {
        NetworkServer.Listen(4444);
        isAtStartup = false;
    }

    public void SetupClient()
    {
        myClient = new NetworkClient();
        myClient.RegisterHandler(MsgType.Connect, OnConnected);     
        myClient.Connect("127.0.0.1", 4444);
        isAtStartup = false;
    }

    public void SetupLocalClient()
    {
        myClient = ClientScene.ConnectLocalServer();
        myClient.RegisterHandler(MsgType.Connect, OnConnected);     
        isAtStartup = false;
    }

    // client function
    public void OnConnected(NetworkMessage netMsg)
    {
        Debug.Log("Connected to server");
    }

    string GenerateRandomString()
	{
		string myString = "";
		int minCharAmount = 8;
		int maxCharAmount = 16;
		const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";
		int charAmount = UnityEngine.Random.Range(minCharAmount, maxCharAmount); //set those to the minimum and maximum length of your string
		for (int i = 0; i < charAmount; i++)
		{
			myString += glyphs[UnityEngine.Random.Range(0, glyphs.Length)];
		}
		return myString;
	}

	public void SendStringToConnectionID(string message, int connectionID)
	{
		byte error;
		byte[] bytes = new byte[message.Length * sizeof(char)];
		System.Buffer.BlockCopy(message.ToCharArray(), 0, bytes, 0, bytes.Length);
		//NetworkTransport.Send(myClient.hostId, NetworkConnection.connectionId, myClient.hostTopology, bytes, bytes.Length, out error);
	}


	void Start ()
	{

	}


	void Update ()
	{
		if (isAtStartup)
		{
			if (Input.GetKeyDown(KeyCode.S))
			{
				SetupServer();
			}

			if (Input.GetKeyDown(KeyCode.C))
			{
				SetupClient();
			}

			if (Input.GetKeyDown(KeyCode.B))
			{
				SetupServer();
				SetupLocalClient();
			}
		}

	}

	void OnGUI()
	{
		if (isAtStartup)
		{
			GUI.Label(new Rect(2, 10, 150, 100), "Press S for server");
			GUI.Label(new Rect(2, 30, 150, 100), "Press B for both");
			GUI.Label(new Rect(2, 50, 150, 100), "Press C for client");
		}
	}
}
