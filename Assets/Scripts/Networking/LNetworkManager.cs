using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Net;

public class LNetworkManager : NetworkManager
{

	private bool isServer = true;
	protected static LNetworkManager instance = null;

	private bool _isStarted = false;
	private bool isClientConnected = false;
	private string ServerIp  = "192.168.1.101";
	private string FloorIP  = "192.168.1.102";
	private int port = 6666;
	private string status = "Not Started";
	private NetworkConnection connection = null;


	private string m_SendString = "";
	private string m_RecString  = "";

	private int connectionStatus = 0; //0 = disconnected, 1 = server, 2 = client

	public static LNetworkManager Instance
	{
		get
		{
			return instance;
		}
	}

	void Start ()
	{
		Time.timeScale = 1.0f;
		Debug.Log("LNetworkManager START");
		instance = this;
		NetworkManager.singleton.StopClient();
		NetworkManager.singleton.StopHost();
		NetworkManager.singleton.StopMatchMaker();
		Network.Disconnect();
		NetworkTransport.Shutdown();
		NetworkTransport.Init();
		NetworkServer.Reset();
		//StartHost();
	}



	public void startup()
	{
		if (isServer) {
			Debug.Log("Starting LIT server...");
			StartHost();
		} else {
			Debug.Log("Starting LIT client...");
			StartClient();
		}
	}

	public string GetStatus()
	{
		return status;
	}

	public int GetConnectionStatus()
	{
		return connectionStatus;
	}

	public NetworkConnection GetNetworkConnection()
	{
		return connection;
	}

	public override void OnStartHost()
	{
		status = "Host started: " + networkAddress + ":" + networkPort;
		_isStarted = true;
		CEO.Instance.SetNetworkStatusText(status);
	}

	public override void OnStartServer()
	{
		status = "Server started: " + networkAddress + ":" + networkPort;
		_isStarted = true;
		CEO.Instance.SetNetworkStatusText(status);
	}

	public override void OnStartClient(NetworkClient client)
	{
		status = "Client started: " + networkAddress + ":" + networkPort;
		_isStarted = true;
		connectionStatus = 2;
		CEO.Instance.SetNetworkStatusText(status);
	}

	public override void OnClientDisconnect(NetworkConnection conn)
	{
		status = "Client lost connection, retrying...";
		connection = null;
		CEO.Instance.SetNetworkStatusText(status);

		//Invoke("startup", 5);
	}

	public override void OnClientConnect(NetworkConnection conn)
	{
		status = "Client connected to a server";
		ClientScene.Ready(conn);
		ClientScene.AddPlayer(0);
		connection = conn;
		connectionStatus = 2;
		CEO.Instance.SetNetworkStatusText(status);

	}

	public override void OnServerConnect(NetworkConnection conn)
	{
		status = "A client with IP " + IPAddress.Parse(conn.address) + " connected to this server";
		CEO.Instance.SetNetworkStatusText(status);
		//Invoke("startup", 5);
	}


	public override void OnServerDisconnect(NetworkConnection conn)
	{
		status = "A client disconnected from this server";
		NetworkServer.DestroyPlayersForConnection(conn);
		CEO.Instance.SetNetworkStatusText(status);
		if(UDPSend.Instance != null) UDPSend.Instance.Kill();
		//Invoke("startup", 5);
	}

	public void SetFloorIP(string fip)
	{
		FloorIP = fip;
		UDPSend.Instance.Setup(FloorIP, 11000);
	}



	public void SendStringToServer(string message)
	{
		if (isServer || connection == null) return;

		m_SendString = message;
		int m_GenericHostId = connection.hostId;
		int connectionID = connection.connectionId;
		int m_CommunicationChannel = 1;
		byte error;
		byte[] bytes = new byte[message.Length * sizeof(char)];
		System.Buffer.BlockCopy(message.ToCharArray(), 0, bytes, 0, bytes.Length);
		NetworkTransport.Send(m_GenericHostId, connectionID, m_CommunicationChannel, bytes, bytes.Length, out error);

	}

	string GenerateRandomString()
	{
		string myString = "";
		int minCharAmount = 8;
		int maxCharAmount = 16;
		const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";
		int charAmount = UnityEngine.Random.Range(minCharAmount, maxCharAmount); //set those to the minimum and maximum length of your string
		for (int i = 0; i < charAmount; i++)
		{
			myString += glyphs[UnityEngine.Random.Range(0, glyphs.Length)];
		}
		return myString;
	}

	public void StartLITServer()
	{
		NetworkServer.Reset();
		isServer = true;
		_isStarted = true;
		NetworkManager.singleton.networkPort = port;
		//NetworkManager.singleton.StartHost();
		StartServer();

	}

	public void StartLITClient()
	{
		NetworkServer.Reset();
		isServer = false;
		_isStarted = true;
		NetworkManager.singleton.networkAddress = ServerIp;
		NetworkManager.singleton.networkPort = port;
		StartClient();
	}


	public void Stop()
	{
		if (isServer)
		{
			StopServer();
		}
		else
		{
			StopClient();
		}
		_isStarted = false;
		status = "Stopped";
		connectionStatus = 0;
	}

	public void SetIP(string newIP)
	{
		ServerIp = newIP;
		//KinectChassis.Instance.dataManager.SetKaptIP(newIP);
	}

	void OnGUITest ()
	{
		GUI.Box(new Rect(5, 5, 450, 450), "window");
		if ( !_isStarted )
		{
			ServerIp = GUI.TextField(new Rect(10, 10, 250, 30), ServerIp, 25);
			port = Convert.ToInt32( GUI.TextField(new Rect(10, 40, 250, 30), port.ToString(), 25) );

			if ( GUI.Button( new Rect(10, 70, 250, 30), "start server" ) )
			{
				StartLITServer();
			}

			if (GUI.Button(new Rect(10, 100, 250, 30), "start client"))
			{
				StartLITClient();
			}
		}
		else
		{

			if (GUI.Button(new Rect(10, 120, 250, 50), "stop"))
			{
				Stop();
			}
		}

		GUI.Label(new Rect(10, 170, 600, 20), status);
		GUI.Label(new Rect(270, 20, 250, 500), "Sent: " + m_SendString);
		GUI.Label(new Rect(270, 70, 250, 50), "Recv: " + m_RecString);
	}

	void OnApplicationQuit()
	{
		NetworkManager.singleton.StopClient();
		NetworkManager.singleton.StopHost();
		NetworkManager.singleton.StopMatchMaker();
		Network.Disconnect();
	}

	void Update()
	{

	}

	void UpdateLLN()
	{
		if (!_isStarted)
			return;
		int recHostId;
		int connectionId;
		int channelId;
		byte[] recBuffer = new byte[1024];
		int bufferSize = 1024;
		int dataSize;
		byte error;
		NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
		switch (recData)
		{

		case NetworkEventType.Nothing:
			break;

		/*case NetworkEventType.ConnectEvent:
		{
			if (!isServer)
			{
				isClientConnected = true;
			}
			Debug.Log(String.Format("Connect from host {0} connection {1}", recHostId, connectionId));
			break;
		}*/

		case NetworkEventType.DataEvent:  //if server will receive echo it will send it back to client, when client will receive echo from serve wit will send other message
		{

			//Debug.Log(String.Format("Received event host {0} connection {1} channel {2} message length {3}", recHostId, connectionId, channelId, dataSize));
			char[] chars = new char[dataSize / sizeof(char)];
			System.Buffer.BlockCopy(recBuffer, 0, chars, 0, dataSize);
			m_RecString = new string(chars);

		}
		break;
		case NetworkEventType.DisconnectEvent:
		{
			if (!isServer)
			{
				isClientConnected = false;
				Debug.Log(String.Format("DisConnect from host {0} connection {1}", recHostId, connectionId));
			}
			break;
		}
		}

		if (Input.GetKeyUp ("h"))
		{
			SendStringToServer(GenerateRandomString());
		}
	}

}
