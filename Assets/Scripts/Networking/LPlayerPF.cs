﻿
﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Text;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class LPlayerPF : NetworkBehaviour
{

	// The singleton instance of LPlayerPF
	protected static LPlayerPF instance = null;

	public IList<string> structData;

	[SyncVar]
	public float leftKickEnergy = 0f;

	[SyncVar]
	public float rightKickEnergy = 0f;

	//allocate and initialize variables. To avoid memory leaks...
	private TStructTeleportEvent tste;
	private IList<string> tempDataString;
	private string dataString;
	private string data;
	private MemoryStream ms;
	private BinaryFormatter formatter;
	private Stream stream ;
	private StringBuilder builder;

	public static LPlayerPF Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		// set the singleton instance
		instance = this;
	}

	void Start()
	{
		tempDataString = new List<string>();
		ms = new MemoryStream();
		formatter = new BinaryFormatter();
		builder = new StringBuilder();
	}

	[Command]
	public void CmdCalibrateRemoteAsLocal(Vector3 leftFootPos, Vector3 rightFootPos, float _sideSwap, float _protonDistance)
	{
		KinectChassis.Instance.CalibrateRemoteAsLocal(leftFootPos, rightFootPos, _sideSwap);
		SaveLoad.savedSettings.protonDistance = _protonDistance;
		Debug.Log("CmdCalibrateRemoteAsLocal " + leftFootPos + "  " + rightFootPos + "  " + _protonDistance + " " + _sideSwap);
	}

	[Command]
	public void CmdCollision(float energyLeft, float energyRight, float devX, float devY)
	{
		ProtonFootballManager.Instance.DoCommandCollision(energyLeft, energyRight, devX, devY);
	}


	[Command]
	public void CmdLeftKick(float energy)
	{
		ProtonFootballManager.Instance.DoCommandKickLeft(energy);
	}


	[Command]
	public void CmdRightKick(float energy)
	{
		ProtonFootballManager.Instance.DoCommandKickRight(energy);
	}


	[Command]
	public void CmdStartSendingStructData(int numOfData)
	{
		Debug.Log("Start receiving...");
		structData = new List<string>(numOfData);
	}


	[Command]
	public void CmdGetStructData(string data, int id)
	{
		structData.Insert(id, data);
	}


	[Command]
	public void CmdEndOfSendingStructData()
	{
		Debug.Log("End of sending...");
		TStructEvent tsEvent = DeserializeStructData();
		VisualizeEvent(tsEvent);

		ClearStructData();
	}

	[Command]
	public void CmdSendKickImageJpg(byte[] jpg, int index)
	{
		ProtonFootballManager.Instance.SetLastRemoteTexture(jpg, index);
	}

	[Command]
	public void CmdSendKickImageString(string imgStr, int index)
	{
		ProtonFootballManager.Instance.SetLastRemoteTexture(imgStr, index);
	}

	[Command]
	public void CmdSetTopSpeedRemote(float tsr)
	{
		ProtonFootballManager.Instance.SetTopSpeedRemote(tsr);
	}

	[Command]
	public void CmdReloadPackman()
	{
		ProtonFootballManager.Instance.DoCommandReloadPackman();
	}

	[Command]
	public void CmdReloadSpiral()
	{
		ProtonFootballManager.Instance.DoCommandReloadSpiral();
	}

	[ClientRpc]
	public void RpcKickfromRemote(Vector3 kickVector, float side)
	{
		if (side == 1)
		{
			ProtonFootballManager.Instance.KickProtonRight(kickVector);
		}
		else
		{
			ProtonFootballManager.Instance.KickProtonLeft(kickVector);
		}
	}



	public void Teleport(TStructEvent tsEvent)
	{
		Debug.Log("Teleport");

		tste = new TStructTeleportEvent(tsEvent);
		dataString = ConvertStructToStringOfBytes(tste);

		SplitIntoChunks(dataString, 16000); //4095

		//COMMAND to initialize list
		Debug.Log("Sending data...");
		CmdStartSendingStructData(tempDataString.Count);

		for (int i = 0; i < tempDataString.Count ; i ++)
		{
			CmdGetStructData( tempDataString[i], i);
			Debug.Log(i);
		}

		Debug.Log("Deserialize...");
		CmdEndOfSendingStructData();
	}

	public string ConvertStructToStringOfBytes(TStructTeleportEvent obj)
	{
		ms = new MemoryStream();		
		formatter.Serialize(ms, obj);
		return Convert.ToBase64String(ms.ToArray());
	}

	private void SplitIntoChunks(string text, int chunkSize)
	{
		tempDataString.Clear();
		int offset = 0;
		while (offset < text.Length)
		{
			int size = Math.Min(chunkSize, text.Length - offset);
			tempDataString.Add(text.Substring(offset, size));
			offset += size;
		}
	}

	void Update()
	{


	}

	private TStructEvent DeserializeStructData()
	{
		data = ConvertIListToString(structData);	

		// Now deserialize from byte array into a new struct instance.
		stream = new MemoryStream(Convert.FromBase64String(data));
		tste = (TStructTeleportEvent)formatter.Deserialize(stream);
		Debug.Log("Deserialized size...");
		return tste.convertToTStructEvent();
	}

	private void VisualizeEvent(TStructEvent tsEvent)
	{
		if (TEventManager.Instance != null)
		{
			TEventManager.Instance.SetAutoTeleport(false);
			TEventManager.Instance.SetAutoKill(false);
			TEventManager.Instance.KillLastEvent();
			TEventManager.Instance.HireAgent(tsEvent, ProtonFootballManager.Instance.GetEventDisplacement());

		}
	}

	public string ConvertIListToString(IList<string> list)
	{
		//builder.Clear();
		builder.Length = 0;
		builder.Capacity = 0;
		foreach (string s in list) {
			builder.Append(s);
		}
		return builder.ToString();
	}

	public void ClearStructData()
	{
		structData.Clear();
	}

}
