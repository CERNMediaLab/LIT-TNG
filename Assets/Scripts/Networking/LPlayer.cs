﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class LPlayer : NetworkBehaviour
{

	public GameObject LPlayerPF;
	public GameObject LPlayerHEAL;

	private GameObject player = null;

	[SyncVar]
	public int levelSynchronize = 0;

	[SyncVar]
	public int levelFloor = 0;

	[SyncVar]
	public int levelSide = 0;

	// The singleton instance of LPlayer
	protected static LPlayer instance = null;

	public static LPlayer Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		// set the singleton instance
		instance = this;
		DontDestroyOnLoad(transform.gameObject);
	}

	void Start()
	{
		if (CEO.Instance.GetIsFloor())
		{
			CmdSetFloorIP(CEO.Instance.GetIP());
		}
	}

	[Command]
	public void CmdSetFloorLevel(int lvl)
	{
		levelFloor = lvl;
	}

	[Command]
	public void CmdSetLevelSynchronize(int lvl)
	{
		levelSynchronize = lvl;
	}

	[Command]
	public void CmdSetFloorIP(string fip)
	{
		LNetworkManager.Instance.SetFloorIP(fip);
	}


	[Command]
	public void CmdSpawnPF()
	{
		ClearPlayer();
		Invoke("SpawnPF", 0.5f);	
	}

	public void SpawnPF()
	{
		player = Instantiate(LPlayerPF, Vector3.zero, Quaternion.identity) as GameObject;
		NetworkServer.SpawnWithClientAuthority(player, base.connectionToClient);
	}

	[Command]
	public void CmdSpawnHEAL()
	{
		ClearPlayer();
		Invoke("SpawnHEAL", 0.5f);		
	}

	public void SpawnHEAL()
	{
		player = Instantiate(LPlayerHEAL, Vector3.zero, Quaternion.identity) as GameObject;
		NetworkServer.SpawnWithClientAuthority(player, base.connectionToClient);
	}


	[Command]
	public void CmdReset()
	{
		CEO.Instance.Reset();
	}

	[ClientRpc]
	public void RpcReset()
	{
		CEO.Instance.Reset();
	}


	public void ClearPlayer()
	{
		if (player != null)
		{
			Destroy(player);
		}

		player = null;
	}

	public void CreateLevelPlayer(int lvl)
	{
		if (CEO.Instance.GetIsFloor())
		{
			switch (lvl)
			{
			case 1 : //Higgnite
				break;
			case 2 : // Proton Football
				CmdSpawnPF();
				break;
			case 3 : //MoviePlayer
				break;
			case 4 : // HEAL
				CmdSpawnHEAL();
				break;
			default :
				break;
			}
		}

	}

	public void Kill()
	{
		Destroy(gameObject);
	}


	void Update()
	{
		/*if (Input.GetKeyUp ("p") && isLocalPlayer)
		{
			CmdSpawnPF();
		}*/
	}


}
