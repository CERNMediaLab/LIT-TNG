﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.Networking;

public class CalibrationManager : MonoBehaviour
{


	

	private bool spawnedPlayer = false;

	private int thisSide = 1;
	protected static CalibrationManager instance = null;

	private int axisMode = 2;
	
	public static CalibrationManager Instance
	{
		get
		{
			return instance;
		}
	}

	void Start ()
	{
		instance = this;



		if (CEO.Instance != null && !CEO.Instance.GetIsSide())
		{
			KinectChassis.Instance.SetActive(true);
			KinectChassis.Instance.ShowFeetMarkers(true);
			KinectChassis.Instance.SetCalibrationIndicatorPosition(SaveLoad.savedSettings.protonDistance / 2f + 22f);
			KinectChassis.Instance.dataManager.RequestJoints();

		}
		else
		{
			KinectChassis.Instance.SetActive(true);
			KinectChassis.Instance.ShowFeetMarkers(true);
			KinectChassis.Instance.SetFeetVisibility(true);

		}


		if (LPlayer.Instance != null)
		{
			/*if (CEO.Instance.GetIsFloor())
			{
				LPlayer.Instance.CmdSetFloorLevel(2);
			}
			else
			{
				LPlayer.Instance.levelSide = 2;
			}*/
		}

		

	}

		public void SpawnPlayer()
	{
		if (LPlayer.Instance != null)
		{
			LPlayer.Instance.CreateLevelPlayer(2);
			spawnedPlayer = true;
		}
	}






	void Update ()
	{


		



		if (CEO.Instance != null && CEO.Instance.GetIsSide())
		{
			thisSide = 1;
			if (CEO.Instance.GetIsPlayerLeft())
			{
				thisSide = -1;
			}


		}

		if (!spawnedPlayer && LPlayer.Instance != null && !CEO.Instance.GetIsSide())
		{
			if (LPlayer.Instance != null && LPlayer.Instance.levelSide == 2)
			{
				SpawnPlayer();
			}
		}



		//SendBodyData();



	}
}

