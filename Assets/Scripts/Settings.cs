﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class Settings
{

    //LOCAL SETTINGS
    public bool isSide = true;
    public bool isPlayerLeft = true;


    // PROTON FOOTBALL
    public float protonDistance = 180f;
    public float collisionRadius = 4f;
    public float radiusKick = 4f;
    public float protonScale = 15f;

    // PF KUNT
    public float zeroPosX = 0f;
    public float zeroPosY = 0f;
    public float zeroPosZ = 0f;
    public float displacementX = 0f;
    public float displacementY = 0f;
    public float displacementZ = 0f;
    public float worldScale = 50f;
    public float alpha = 0f;
    public float phi = 0f;
    public float side = 1f;

    public float zSide = 1f;
    public float rotate180 = 1f;

    // SIDE KUNT
    public float zeroPosXS = 0f;
    public float zeroPosYS = 0f;
    public float zeroPosZS = 0f;
    public float displacementXS = 0f;
    public float displacementYS = 0f;
    public float displacementZS = 0f;
    public float worldScaleS = 60f;
    public float alphaS = 0f;
    public float phiS = 0f;
    public float sideS = 1f;

    public float zSideS = 1f;
    public float rotate180S = 1f;

    // HEAL
    public float pos1X = 0f;
    public float pos1Y = 0f;
    public float pos1Z = 0f;

    public float pos2X = 0f;
    public float pos2Y = 0f;
    public float pos2Z = 0f;

    public float pos3X = 0f;
    public float pos3Y = 0f;
    public float pos3Z = 0f;

    public float sideScale = 1f;

    public float zeroPointHandsX = 0f;
    public float zeroPointHandsY = 0f;
    public float zeroPointHandsZ = 0f;

    public float minEnergyPosition = 0f;
    public float maxEnergyPosition = 20f;

    //DW
    public float pos0X = 0f;
    public float pos0Y = 0f;
    public float pos0Z = 0f;




}

public static class SaveLoad
{
    public static bool isReady = false;

    public static Settings savedSettings = new Settings();
    public static string SavePath = "c:/Temp";

    //it's static so we can call it from anywhere
    public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        if (!File.Exists(SavePath))
        {
            Directory.CreateDirectory(SavePath);
        }
        FileStream file = File.Create (SavePath + "/LIT_savedSettings.gd");
        bf.Serialize(file, SaveLoad.savedSettings);
        file.Close();

        Debug.Log("Values Saved!");
    }

    public static void Load()
    {
        if (File.Exists(SavePath + "/LIT_savedSettings.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(SavePath + "/LIT_savedSettings.gd", FileMode.Open);
            SaveLoad.savedSettings = (Settings)bf.Deserialize(file);
            file.Close();

            isReady = true;

            Debug.Log("Values Loaded!");
        }
    }
}
