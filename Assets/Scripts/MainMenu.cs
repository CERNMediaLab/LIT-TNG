﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour 
{

	protected static MainMenu instance = null;
	
	public static MainMenu Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		// set the singleton instance
		instance = this;		
	}

	void Start () 
	{
		
	}

	public void LoadProtonFootball()
	{
		SceneManager.LoadScene("PF_Floor", LoadSceneMode.Single);
	}
	
	
	void Update () 
	{
		
	}
}
