using UnityEngine;
using System.Collections;

public class Circle 
{


	public float x = 0.0f;
	public float y = 0.0f;
	public float d = 0.0f; //radius

	public Circle(float _x, float _y, float _d)
	{
		x = _x;
		y = _y;
		d = _d;
	}

	public bool Contains(Vector2 point)
	{
		float a = Vector2.Distance(getCenter(), point);
		bool ret = false;
		if (a <= d) ret = true;
		return ret;
	}

	public Vector2 getCenter()
	{
		return new Vector2(x, y);
	}

	public void setCenter(Vector2 center)
	{
		x = center.x;
		y = center.y;
	}

	public void setCenter(float _x, float _y)
	{
		x = _x;
		y = _y;
	}

	public bool isColliding(Circle c)
	{
		//Debug.Log(c.getCenter() + "   " +  getCenter() + "     " + (c.d + d));
		bool ret = false;
		if (Vector2.Distance(c.getCenter(), getCenter()) <= (c.d + d)) ret =  true;
		return ret;
	}
}
