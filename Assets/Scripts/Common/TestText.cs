﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestText : MonoBehaviour {

	protected static TestText instance = null;

	private Text txtStatus = null;


	public static TestText Instance
	{
		get
		{
			return instance;
		}
	}

	void Start ()
	{

	}


	private void Awake()
	{
		txtStatus = GetComponent<Text>();
		instance = this;
	}

	public void SetTestText(string tt)
	{
		txtStatus.text = tt;
	}


	
}
