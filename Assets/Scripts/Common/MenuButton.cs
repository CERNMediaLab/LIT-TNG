﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MenuButton : MonoBehaviour 
{
	public int btn = 1;
	private Button button = null;

	void Start ()
	{
		button = gameObject.GetComponent<Button>();
		button.onClick.AddListener(ClickAction);
	}

	void ClickAction ()
	{
		CEO.Instance.MenuButton(btn);		
	}

	void Update()
	{
		//button.interactable = CEO.Instance.AreButtonsInteractable();
	}
}
