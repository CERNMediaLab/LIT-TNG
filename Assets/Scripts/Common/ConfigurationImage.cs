﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigurationImage : MonoBehaviour
{

	public Material imageMaterial;

	void Start ()
	{

	}

	private void SetBlend(float bl)
	{
		imageMaterial.SetFloat ("_Blend", bl );
	}

	public void SetBlendSoft(float bl)
	{
		float time = 0.5f;
		StartCoroutine(SoftBlendTo(bl, time));
	}

	IEnumerator SoftBlendTo(float or , float aTime)
	{

		float currentBlend = imageMaterial.GetFloat("_Blend");
		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
		{
			float blend = Mathf.Lerp(currentBlend , or , t);
			imageMaterial.SetFloat ("_Blend", blend);
			yield return null;
		}

		imageMaterial.SetFloat ("_Blend", or );
	}



	void Update ()
	{

	}
}
