﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Windows.Kinect;

public class KinectImage : MonoBehaviour
{
	private RawImage img;
	private byte[] jpegPeople = null;
	private Texture2D tx;

	Mode _mode = Mode.Color;

	public enum Mode
	{
		Blank,
		Color,
		Depth,
		Infrared,
		People
	}

	void Start()
	{
		img = (RawImage)gameObject.GetComponent<RawImage>();
		tx = new Texture2D(2, 2);

		StartColor();
	}

	public void StartIR()
	{
		_mode = Mode.Infrared;
	}

	public void StartDepth()
	{

		_mode = Mode.Depth;
	}

	public void StartPeople()
	{
		_mode = Mode.People;
	}

	public void StartColor()
	{

		_mode = Mode.Color;
	}



	void Update ()
	{
		switch (_mode)
		{
		case Mode.Color:
			UpdateColor();
			break;
		case Mode.Infrared:
			UpdateIR();
			break;
		case Mode.Depth:
			UpdateDepth();
			break;
		case Mode.People:
			UpdatePeople();
			break;
		}
		//img.texture = KinectManager.Instance.GetUsersLblTex();

	}

	void UpdateIR()
	{
		tx = KinectChassis.Instance.GetTexture_IR();
		if (tx != null)
			img.texture = tx;

	}

	void UpdateColor()
	{
		tx = KinectChassis.Instance.GetTexture_Color();
		if (tx != null)
			img.texture = tx;
	}


	void UpdateDepth()
	{
		tx = KinectChassis.Instance.GetTexture_Depth();
		if (tx != null)
			img.texture = tx;
	}

	void UpdatePeople()
	{
		tx = KinectChassis.Instance.GetTexture_People(true);
		//tx = new Texture2D(2, 2);
		//jpegPeople = KinectChassis.Instance.GetJpegPeople();
		//tx.LoadImage(jpegPeople);

		if (tx != null)
			img.texture = tx;
	}



}
