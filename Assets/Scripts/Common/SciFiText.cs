﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;


[RequireComponent(typeof(AudioSource))]
public class SciFiText : MonoBehaviour
{

	public AudioClip typeSound;
	private bool playSound = false;
	private Text txtStatus;
	private bool isFadingIn = false;
	private bool isFadingOut = false;
	private bool isSettingNewText = false;

	private bool isIn = true;
	private bool isOut = false;
	private bool _useTypewriter = false;
	private string nextText = "";
	private Color nextColor = Color.white;
	private bool isColorSet = true;

	private float fadeSpeed = 5f;
	private float alpha = 1f;
	private float typeTimer = 0.005f;
	private float carriageReturnTimer = 0.2f;
	private int multiplier = 4;

	private RectTransform m_RectTransform;
	private int fontSize = 12;
	private float waitTime = 0f;
	private bool didWaitTime = false;


	public Color fontColor;




	void Start()
	{
		txtStatus = GetComponent<Text>();
		txtStatus.text = nextText;
		txtStatus.color = fontColor;
		txtStatus.fontSize = fontSize;
	}

	public void SetSize(Vector2 _size)
	{
		m_RectTransform = GetComponent<RectTransform>();
		m_RectTransform.anchoredPosition = Vector2.zero;
		m_RectTransform.sizeDelta = _size * 0.9f;

	}

	public void DidWaitTime()
	{
		didWaitTime = true;
	}

	public void FadeOut()
	{
		StopAllCoroutines();
		isFadingOut = true;
		isFadingIn = false;
		isIn = false;
	}

	private void FadeIn()
	{
		isFadingIn = true;
		isFadingOut = false;
		isOut = false;
		if (!isColorSet)
		{
			SetColor(nextColor);
			isColorSet = true;
		}
	}

	private void FadeOutCycle()
	{
		if (isFadingOut)
		{
			float alpha = txtStatus.color.a;
			float newAlpha = alpha -= Time.deltaTime * fadeSpeed;

			if (newAlpha <= 0f)
			{
				newAlpha = 0f;
				isFadingOut = false;
				isOut = true;
			}

			txtStatus.color = new Color(txtStatus.color.r, txtStatus.color.g, txtStatus.color.b, newAlpha);
		}
	}

	private void FadeInCycle()
	{
		if (isFadingIn)
		{
			alpha = txtStatus.color.a;
			float newAlpha = alpha += Time.deltaTime * fadeSpeed;

			if (newAlpha >= 1f)
			{
				newAlpha = 1f;
				isFadingIn = false;
				isIn = true;
			}

			txtStatus.color = new Color(txtStatus.color.r, txtStatus.color.g, txtStatus.color.b, newAlpha);
		}
	}

	public void SetFadeSpeed(float newFadeSpeed)
	{
		fadeSpeed = newFadeSpeed;
	}

	public void SetTypeTimer(float newTypeTimer)
	{
		typeTimer = newTypeTimer;
	}

	public void SetNewText(string newText, bool useTypewriter, float waitTime)
	{
		if (waitTime == 0f)
		{
			didWaitTime = true;
		} else
		{
			didWaitTime = false;
			Invoke("DidWaitTime", waitTime);
		}
		isSettingNewText = true;
		_useTypewriter = useTypewriter;
		nextText = newText;
		FadeOut();
	}

	public void SetNewFontSize(int _fontSize)
	{
		fontSize = _fontSize;
		if (txtStatus != null)
		{
			txtStatus.fontSize = fontSize;
			//NewTextCycle();
			FadeOut();
			//nextText = nextText.fontSize = size;
		}
	}


	public void SetColor(Color newColor)
	{
		if (!isFadingOut && txtStatus != null) txtStatus.color = new Color(newColor.r, newColor.g, newColor.b, alpha);
		else
		{
			nextColor = newColor;
			isColorSet = false;
		}
	}

	private void NewTextCycle()
	{
		if (isOut)
		{
			if (!_useTypewriter)
			{
				txtStatus.text = nextText;
			}
			else
			{
				txtStatus.text = "";
				StartCoroutine(AnimateText());
			}

			FadeIn();
			isSettingNewText = false;
		}

	}

	/*IEnumerator AnimateText()
	{
	    if (nextText != null)
	    {
	        foreach (char c in nextText)
	        {
	            txtStatus.text += c;
	            if (playSound && typeSound)
	                GetComponent<AudioSource>().PlayOneShot(typeSound);
	            float timer = typeTimer;
	            if (Char.IsControl(c))
	            {
	                timer = carriageReturnTimer;
	            }
	            yield return new WaitForSeconds(timer);
	        }

	    }

	}*/

	IEnumerator AnimateText()
	{
		if (nextText != null)
		{
			int numChar = 0;
			for (int i = 0; i < nextText.Length; i++)
			{
				if (numChar > nextText.Length - 1)
				{
					break;
				}
				float timer = typeTimer;
				for (int m = 0; m < multiplier; m++)
				{
					txtStatus.text += nextText[i * multiplier + m];



					if (Char.IsControl(nextText[m]))
					{
						timer = carriageReturnTimer;
					}
					numChar++;
					if (numChar > nextText.Length - 1)
					{
						break;
					}
				}
				if (playSound && typeSound)
					GetComponent<AudioSource>().PlayOneShot(typeSound);

				yield return new WaitForSeconds(timer);
			}

		}

	}


	void Update()
	{
		if (didWaitTime)
		{

			if (isFadingOut) FadeOutCycle();
			if (isFadingIn) FadeInCycle();
			if (isSettingNewText) NewTextCycle();
		}

	}
}
