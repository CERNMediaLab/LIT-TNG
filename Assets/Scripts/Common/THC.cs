using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Linq;

public static class THC {

	public static Dictionary<string, List<string>> EnDict = new Dictionary<string, List<string>>();
	public static Dictionary<string, List<string>> FrDict = new Dictionary<string, List<string>>();
	public static Dictionary<string, List<string>> DeDict = new Dictionary<string, List<string>>();
	public static Dictionary<string, List<string>> EsDict = new Dictionary<string, List<string>>();
	public static Dictionary<string, List<string>> ItDict = new Dictionary<string, List<string>>();

	public static Dictionary<string, List<string>> ActiveDict = new Dictionary<string, List<string>>();

	public static string languagePath = "D:/videos/";
	public static int rfDemoCount = 0;
	public static int dpDemoCount = 0;
	public static int qdpDemoCount = 0;

	private static string key,title,duration,language,description;
	private static List<string> dictData;
	private static Vector3 posData;
	private static Vector2 maskPosData,maskScaleData;
	private static Quaternion rotData;
	private static float X,Y,Z;
	private static int count;
	private static int _fontSize;

	public static void LoadLanguages()
	{
			LoadLanguageXML("videosDescription.xml", EnDict);
			/*LoadLanguageXML("french.xml", FrDict);
			LoadLanguageXML("german.xml", DeDict);
			LoadLanguageXML("spanish.xml", EsDict);
			LoadLanguageXML("italian.xml", ItDict); */
	}


	public static void LoadLanguageXML(string filename, Dictionary<string,List<string>> languageDict)
	{
		//reset dictionary
		languageDict.Clear();
		//insert data to dictionary from xml language file
		using (XmlReader reader = XmlReader.Create(languagePath + filename ))
		{
			while (reader.Read())
			{
					switch (reader.Name)
					{
						case "id" :
							key = reader.ReadElementContentAsString();
							break;
						case "title" :
							title = reader.ReadElementContentAsString();
							break;
						case "duration" :
							duration = reader.ReadElementContentAsString();
							break;
						case "language" :
							language = reader.ReadElementContentAsString();
							break;
						case "description" :
							description = reader.ReadElementContentAsString();
							break;
					}

					if(reader.NodeType == XmlNodeType.EndElement && reader.Name == "Info")
							languageDict.Add(key,new List<string> {title,duration,language,description});					
		  }

		  reader.Close();
		}

		Debug.Log(filename + " loaded.");

	}

	public static string GetTitle(string key)
	{
			if (ActiveDict.ContainsKey(key))
			{
					dictData = ActiveDict[key];
					return dictData[0];
			}
			else
			{
					Debug.Log("The key has not been found in the dictionary");
					return null;
			}
	}

	public static string GetDuration(string key)
	{
			if (ActiveDict.ContainsKey(key))
			{
					dictData = ActiveDict[key];
					return dictData[1];
			}
			else
			{
					Debug.Log("The key has not been found in the dictionary");
					return null;
			}
	}

	public static string GetLanguage(string key)
	{
			if (ActiveDict.ContainsKey(key))
			{
					dictData = ActiveDict[key];
					return dictData[2];
			}
			else
			{
					Debug.Log("The key has not been found in the dictionary");
					return null;
			}
	}

	public static string GetDescription(string key)
	{
			if (ActiveDict.ContainsKey(key))
			{
					dictData = ActiveDict[key];
					return dictData[3];
			}
			else
			{
					Debug.Log("The key has not been found in the dictionary");
					return null;
			}
	}


	public static void ChangeLanguage(int value)
	{
		  switch (value)
			{
					case 1:
						ActiveDict = EnDict;
						break;
					case 2:
						ActiveDict = FrDict;
						break;
					case 3:
						ActiveDict = DeDict;
						break;
					case 4:
						ActiveDict = EsDict;
						break;
					case 5:
						ActiveDict = ItDict;
						break;
					default :
						ActiveDict = EnDict;
						break;
			}
	}


}
