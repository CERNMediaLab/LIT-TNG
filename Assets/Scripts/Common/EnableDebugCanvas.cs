﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDebugCanvas : MonoBehaviour {

	public bool enabled = false;
	public GameObject debugCanvas;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.D))
		{
				enabled = !enabled;
				debugCanvas.SetActive(enabled);
		}

	}
}
