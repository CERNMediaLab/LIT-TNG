﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkText : MonoBehaviour {

	// Use this for initialization
  public float time = 1f;
	public float delayStartTime = 0f;
	Color tempColor;

	void Start ()
  {		
			Invoke("BlinkOn",delayStartTime);
	}

	void BlinkOn()
	{
    if(this.gameObject.active)
		  StartCoroutine(FadeAlpha(1f, 1f));

		Invoke("BlinkOff",time);
	}

	void BlinkOff()
	{
      if(this.gameObject.active)
		     StartCoroutine(FadeAlpha(0f, 1f));

		  Invoke("BlinkOn",time + 2f);
	}


	IEnumerator FadeAlpha(float aValue, float aTime)
	{
  		Color txtColor = this.GetComponent<Text>().color;
  		float alpha = txtColor.a;
  		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
  		{
  			Color newColor = new Color(txtColor.r, txtColor.g, txtColor.b, Mathf.Lerp(alpha, aValue, t));
  			this.GetComponent<Text>().color = newColor;
  			yield return null;
  		}
  		this.GetComponent<Text>().color = new Color(txtColor.r, txtColor.g, txtColor.b, aValue);
	}
}
