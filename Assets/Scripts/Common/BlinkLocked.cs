﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkLocked : MonoBehaviour {

	// Use this for initialization
  public float time = 1f;
  public bool isBlinking = true;
	float newTime;
  bool newIsBlinking;

	void Start () {

		newTime = time;
    newIsBlinking = isBlinking;

		if (isBlinking)
			blinkOn();
	}

	void Update ()
  {

    if (time != newTime)
      newTime = time;

    if(isBlinking != newIsBlinking)
    {
      newIsBlinking = isBlinking;
      blinkOn();
    }

	}

	void blinkOn()
	{
			this.GetComponent<Image>().CrossFadeAlpha(1.0f,1.0f,true);
      Invoke("blinkOff",newTime);
	}

	void blinkOff()
	{
		  this.GetComponent<Image>().CrossFadeAlpha(0.0f,1.0f,true);
			if (newIsBlinking)
				Invoke("blinkOn",newTime);
	}
}
