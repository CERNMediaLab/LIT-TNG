﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwapLanguageTexts : MonoBehaviour {

	public GameObject enText,frText;
	public float time = 1f;
	public float delayStartTime = 3.5f;
	public bool isBlinking = false;
	public bool previousIsBlinking = false;
	Color tempColor;

	void Update()
	{
			if(isBlinking!=previousIsBlinking)
			{
				previousIsBlinking = isBlinking;
				if(isBlinking)
					Blink();
			}
	}

	public bool GetIsBlinking()
	{
		return isBlinking;
	}

	public void SetIsBlinking(bool blink)
	{
		isBlinking = blink;
	}

	public void SetPreviousIsBlinking(bool blink)
	{
		previousIsBlinking = blink;
	}

	void Blink()
	{
		enText.GetComponent<Text>().color = new Color(1f, 1f, 1f, 0f);
		frText.GetComponent<Text>().color = new Color(1f, 1f, 1f, 0f);
		CancelInvoke();
		Invoke("BlinkOnEnText",0.3f);
		//Invoke("BlinkOnFrText",delayStartTime);
	}

	void BlinkOnEnText()
	{
    if(enText.gameObject.active)
		  StartCoroutine(FadeAlpha(enText,1f, 1f));

		Invoke("BlinkOffEnText",time);
	}

	void BlinkOffEnText()
	{
      if(enText.gameObject.active)
		     StartCoroutine(FadeAlpha(enText,0f, 1f));

		  Invoke("BlinkOnFrText",time);
	}

	void BlinkOnFrText()
	{
    if(frText.gameObject.active)
		  StartCoroutine(FadeAlpha(frText,1f, 1f));

		Invoke("BlinkOffFrText",time);
	}

	void BlinkOffFrText()
	{
      if(frText.gameObject.active)
		     StartCoroutine(FadeAlpha(frText,0f, 1f));

		  Invoke("BlinkOnEnText",time);
	}


	IEnumerator FadeAlpha(GameObject obj,float aValue, float aTime)
	{
  		Color txtColor = obj.GetComponent<Text>().color;
  		float alpha = txtColor.a;
  		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
  		{
  			Color newColor = new Color(txtColor.r, txtColor.g, txtColor.b, Mathf.Lerp(alpha, aValue, t));
  			obj.GetComponent<Text>().color = newColor;
  			yield return null;
  		}
  		obj.GetComponent<Text>().color = new Color(txtColor.r, txtColor.g, txtColor.b, aValue);
	}
}
