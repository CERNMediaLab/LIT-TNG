﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeviationDot : MonoBehaviour
{

	private float maxX = 55f;
	private float maxY = 21f;
	private float maxDevX = 100f;
	private float maxDevY = 45f;

	private float previousDevX = 0f;
	private float previousDevY = 0f;
	private float DevX = 0f;
	private float DevY = 0f;
	private Vector2 pos;

	private Image marker;


	void Start ()
	{
		marker = gameObject.GetComponent<Image>();
		marker.canvasRenderer.SetAlpha (0f);
		Blink();

	}

	public void SetNewPosition()
	{
		pos =  new Vector2(DevX * maxX / maxDevX, -DevY * maxY / maxDevY);
		gameObject.GetComponent<RectTransform>().anchoredPosition = pos;
	}

	void Blink()
	{
		StartCoroutine(BlinkSequence(0.2f));
		Invoke("Blink", 0.8f);
	}


	public IEnumerator BlinkImage(Image im, float blinkTime)
	{
		im.CrossFadeAlpha(1f, blinkTime, true);
		yield return new WaitForSeconds(blinkTime);
		im.CrossFadeAlpha(0f, blinkTime, true);
	}

	public IEnumerator BlinkSequence (float blinkTime)
	{
		StartCoroutine(BlinkImage(marker, blinkTime));
		yield return new WaitForSeconds(blinkTime);
	}


	void Update ()
	{
		DevX = ProtonFootballManager.Instance.GetCollisionDeviationX();
		DevY = ProtonFootballManager.Instance.GetCollisionDeviationY();

		if (DevX != previousDevX || DevY != previousDevY)
		{
			SetNewPosition();
		}

		DevX = previousDevX;
		DevY = previousDevY;

	}
}
