﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Text;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class NetworkFootball : NetworkBehaviour
{

	public TEventManager evManager;
	public IList<string> structData;

	

	[Command]
	public void CmdStartSendingStructData(int numOfData)
	{
		Debug.Log("Start receiving...");
		structData = new List<string>(numOfData);
	}


	[Command]
	public void CmdGetStructData(string data, int id)
	{
		structData.Insert(id, data);
	}


	[Command]
	public void CmdEndOfSendingStructData()
	{
		Debug.Log("End of sending...");
		TStructEvent tsEvent = DeserializeStructData();
		VisualizeEvent(tsEvent);

		ClearStructData();
	}

	[Command]
	public void CmdKickLeft(float energy)
	{
		
	}

	[Command]
	public void CmdKickRight(float energy)
	{
		
	}

	public void Teleport(TStructEvent tsEvent)
	{
		Debug.Log("Teleport");

		//Start event convertion.
		// - From struct to string
		// - From string to multiple strings - RPC limit 4096
		TStructTeleportEvent tste = new TStructTeleportEvent(tsEvent);
		string dataString = ConvertStructToStringOfBytes(tste);

		IList<string> tempDataString = SplitIntoChunks(dataString, 4095);

		//COMMAND to initialize list
		Debug.Log("Sending data...");		
		CmdStartSendingStructData(tempDataString.Count);

		for (int i = 0; i < tempDataString.Count ; i ++)
		{			
			CmdGetStructData( tempDataString[i], i);
		}

		Debug.Log("Deserialize...");		
		CmdEndOfSendingStructData();
	}

	public string ConvertStructToStringOfBytes(TStructTeleportEvent obj)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            new BinaryFormatter().Serialize(ms, obj);
            return Convert.ToBase64String(ms.ToArray());
        }
    }

    private IList<string> SplitIntoChunks(string text, int chunkSize)
    {
        List<string> chunks = new List<string>();
        int offset = 0;
        while (offset < text.Length)
        {
            int size = Math.Min(chunkSize, text.Length - offset);
            chunks.Add(text.Substring(offset, size));
            offset += size;
        }
        return chunks;
    }

	void Update()
	{
		
	}

	private TStructEvent DeserializeStructData()
    {
        string data = ConvertIListToString(structData);

        IFormatter formatter = new BinaryFormatter();
        // Now deserialize from byte array into a new struct instance.
        Stream stream = new MemoryStream(Convert.FromBase64String(data));
        TStructTeleportEvent tste = (TStructTeleportEvent)formatter.Deserialize(stream);
        Debug.Log("Deserialized size...");
        return tste.convertToTStructEvent();
    }

    private void VisualizeEvent(TStructEvent tsEvent)
    {
        if (evManager != null)
        {
            evManager.SetAutoTeleport(false);
            evManager.SetAutoKill(false);
            evManager.KillLastEvent();
            evManager.HireAgent(tsEvent);
        }
    }

    public string ConvertIListToString(IList<string> list)
    {
        StringBuilder builder = new StringBuilder();
        foreach (string s in list) {
            builder.Append(s);
        }
        return builder.ToString();
    }

    public void ClearStructData() 
    {
        structData.Clear();
    }
}
