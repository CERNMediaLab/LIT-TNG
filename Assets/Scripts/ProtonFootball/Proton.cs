﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Proton : MonoBehaviour
{

	public ParticleSystem trail;
	public VideoPlayer vPlayer;
	//public VideoClip clip;
	private ProtonFootballManager manager;
	private Vector3 speedVector = Vector3.zero;
	private bool wasKicked = false;
	private int side = 1;
	private float scale = 2f;
	private float speed = 0f;
	private float energy = 0f;
	private bool askedRespawn = false;



	void Start ()
	{
		Invoke("ResetEnergy", 1f);
		//vPlayer.clip = clip;
		/*vPlayer.source = VideoSource.VideoClip;
		vPlayer.renderMode = VideoRenderMode.RenderTexture;
		vPlayer.isLooping = true;
		vPlayer.Play();*/
		MovieTexture mt = (MovieTexture)GetComponent<Renderer>().material.mainTexture;
		mt.loop = true;
		AudioSource myAudioSource = gameObject.AddComponent<AudioSource>();
		myAudioSource.clip = mt.audioClip;
		myAudioSource.pitch = 4f;
		mt.Play();
		myAudioSource.Play();
	}

	private void ResetEnergy()
	{
		if (!wasKicked)
		{
			manager.SetEnergyProton(side, 0f);
		}
	}

	public void SetManager(ProtonFootballManager mgr)
	{
		manager = mgr;
	}

	public void SetSide(int sd)
	{
		side = sd;
	}

	public void Kill()
	{
		//vPlayer.enabled = false;
		if (!wasKicked && !askedRespawn) manager.SpawnInSeconds(side, 2f);
		askedRespawn = true;
		//Invoke ("KillPart2", 0.3f);
		KillPart2();
		
	}

	public void KillPart2()
	{
		Destroy(this.gameObject);
	}

	public float MagnitudeToEnergy(float mag)
	{
		float speedBase = (mag * mag) / 280f;
		speed = Mathf.Clamp(speedBase, 85f, 550);
		energy =  (speedBase * speedBase) / 55f;
		return energy;
	}

	public float EnergyToMagnitude(float en)
	{
		float speedBase = Mathf.Sqrt(en * 55f);
		speed = Mathf.Clamp(speedBase, 85f, 550);
		float magnitude = Mathf.Sqrt(speedBase * 280);
		return magnitude;
	}

	public void Kick(Vector3 speedVectorRaw)
	{
		if (!wasKicked)
		{
			wasKicked = true;
			float magnitude = speedVectorRaw.magnitude;
			MagnitudeToEnergy(magnitude);

			Invoke("Kill", 3f);
			manager.SpawnInSeconds(side, 2f);
			askedRespawn = true;
			manager.SetEnergyProton(side, energy);
			speedVector = speed * speedVectorRaw.normalized;
		}
	}

	public void Kick(Vector3 directionNonNormalized, float kickEnergy)
	{
		if (!wasKicked)
		{
			wasKicked = true;
			float magnitude = EnergyToMagnitude(kickEnergy);

			Invoke("Kill", 3f);
			manager.SpawnInSeconds(side, 2f);
			askedRespawn = true;
			manager.SetEnergyProton(side, kickEnergy);
			speedVector = speed * directionNonNormalized.normalized;
		}
	}

	public void SetScale(float sc)
	{
		scale = sc;
		this.transform.localScale = new Vector3(sc, 0.001f, sc);
	}


	void Update ()
	{
		if (KinectChassis.Instance != null && !wasKicked)
		{
			Vector3 speedVectorRaw = KinectChassis.Instance.CheckKick(this.transform.position, manager.GetRadiusKick());
			speedVector = new Vector3(speedVectorRaw.x, 0, speedVectorRaw.y);
		}

		if (speedVector != Vector3.zero && !wasKicked)
		{
			Kick(speedVector);
		}

		if (wasKicked)
		{
			this.transform.position += speedVector * Time.deltaTime;
		}
		else
		{
			this.transform.position = new Vector3(0f, 0f, (float)side * manager.GetProtonDistance() / 2f);
		}

		/*if (Input.GetKeyDown("m"))
		{

			if (side == 1)
			{
				Kick(new Vector3(Random.Range(-0.15f, 0.15f), 0, -1f), Random.Range(0.1f, 7000f));
			}
		}

		if (Input.GetKeyDown("n"))
		{

			if (side == -1)
			{
				Kick(new Vector3(0, 0, 0.85f));
			}
		}*/

		if (Input.GetKeyDown("l"))
		{
			Destroy(this.gameObject);
		}

	}
}
