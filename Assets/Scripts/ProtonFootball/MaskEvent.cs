﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaskEvent : MonoBehaviour
{
	private float currentScale = 0f;
	private float currentAlpha = 1f;
	private Image image;
	private bool isReady = false;
	private bool isDisappearing = false;
	private float maxAlpha = 0.85f;

	void Start ()
	{
		image = GetComponent<Image>();
		gameObject.transform.localScale = new Vector3(0, 0, 0);
		SetAlphaByEnergy();
		StartCoroutine(GrowTo(1f, 1f));
		Invoke("IsReady", 0.3f);
	}

	void SetAlphaByEnergy()
	{
		Color c = image.color;

		currentAlpha =  0.2f + maxAlpha * ProtonFootballManager.Instance.GetEnergyCollision()/14000f;
		currentAlpha = Mathf.Clamp(currentAlpha, 0.3f, maxAlpha);
		c.a = currentAlpha;
		image.color = c;
	}

	void IsReady()
	{
		isReady = true;
	}

	IEnumerator GrowTo(float sc , float aTime)
	{
		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
		{
			currentScale = Mathf.SmoothStep(0f , sc , t);
			gameObject.transform.localScale = new Vector3(currentScale, currentScale, currentScale);
			yield return null;
		}
		currentScale = sc;
		gameObject.transform.localScale = new Vector3(sc, sc, sc);
	}

	IEnumerator FadeOut(float alpha, float aTime)
	{		
		Color c = image.color;
		float startAlpha = c.a;

		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
		{
			currentAlpha = Mathf.SmoothStep(startAlpha , alpha , t);
			c.a = currentAlpha;
			image.color = c;
			yield return null;
		}
		currentAlpha = alpha;
		c.a = currentAlpha;
		image.color = c;
	}

	void Kill()
	{
		Destroy(this.gameObject);
	}

	void FadeOutAndKill()
	{
		isDisappearing = true;
		StartCoroutine(FadeOut(0f, 0.3f));
		Invoke("Kill", 0.4f);
	}


	void Update ()
	{
		if(isReady && !isDisappearing)
		{
			if(ProtonFootballManager.Instance.JustGotAnEvent())
			{
				FadeOutAndKill();
			}
		}

	}
}
