﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KickImage : MonoBehaviour
{
	public bool isLeft = true;
	private RawImage kickImg;
	private bool isFrozen = false;
	private Texture2D tx;
	private Texture2D tx2;
	private Texture2D tex;
	private Color[] TexturePixels;

	private Texture2D[] sequence;


	private float timeCapture = 0.08f;
	private float timeWait = 4f;
	private float playSlowdownFactor = 1.5f;
	private int iCapture = 0;
	private int iShow = 0;



	void Start ()
	{
		kickImg = (RawImage)gameObject.GetComponent<RawImage>();
		UpdatePeopleLocal();
		InitializeRemoteTexture();

		sequence = new Texture2D[24];

		iCapture = 0;
		foreach (Texture2D t in sequence)
		{
			sequence[iCapture] = tex;
			iCapture++;
		}			

		//SequenceImages();
		ShowSequence();

	}

	private void InitializeRemoteTexture()
	{
		tex = new Texture2D(2, 2);
		Color grayBlack = new Color(0.0745f, 0.0667f, 0.0627f, 1f);


		for (int y = 0; y < tex.height; y++)
		{
			for (int x = 0; x < tex.width; x++)
			{
				tex.SetPixel(x, y, grayBlack);
			}
		}
		tex.Apply();
	}

	

	void UpdatePeopleLocal()
	{
		StartCoroutine(UpdatePeopleLocalActions());

	}


	IEnumerator UpdatePeopleLocalActions()
	{
		tx = KinectChassis.Instance.GetTexture_People(false);

		TexturePixels = tx.GetPixels();

		tx2 = new Texture2D(tx.width, tx.height, TextureFormat.RGBA32, false);

		tx2.SetPixels(TexturePixels);
		tx2.Apply(false);

		if (tx2 != null)
			kickImg.texture = tx2;

		yield return null;
	}

	

	void UpdateSequenceRemote()
	{
		if (ProtonFootballManager.Instance.GetLastRemoteSequence() != null)
		{
			sequence = ProtonFootballManager.Instance.GetLastRemoteSequence();
		}
	}

	

	void UpdatePeopleRemote()
	{
		//tex = new Texture2D(2, 2);
		if (ProtonFootballManager.Instance.GetLastRemoteJpg() != null)
		{
			tex.LoadImage(ProtonFootballManager.Instance.GetLastRemoteJpg());
		}


		if (tex != null)
			kickImg.texture = tex;
	}

	void CaptureSequenceLocal()
	{
		//StartCoroutine(CaptureSequenceCR());
		ProtonFootballManager.Instance.StartCapture();
		Invoke("AssignLocalSequenceTextures", 2.5f);

	}

	void AssignLocalSequenceTextures()
	{
		sequence = ProtonFootballManager.Instance.GetLastLocalSequence();
	}

	


	void CaptureSequenceRemote()
	{		
		sequence = ProtonFootballManager.Instance.GetLastRemoteSequence();
	}

	

	private void ShowSequence()
	{
		StartCoroutine(SequenceImages());
		Invoke("ShowSequence", timeWait);
	}

	IEnumerator SequenceImages()
	{
		iShow = 0;
		foreach (Texture2D t in sequence)
		{
			kickImg.texture = sequence[iShow];
			//kickImg.texture = ProtonFootballManager.Instance.GetLastSequence()[iShow];
			//Debug.Log("Showed "+ iShow);
			yield return new WaitForSeconds(timeCapture*playSlowdownFactor);
			iShow++;
		}		
	}

	

	private void Freeze(float time)
	{
		isFrozen = true;
		UpdatePeopleLocal();
		Invoke("Unfreeze", time);
	}

	private void Unfreeze()
	{
		isFrozen = false;
		UpdatePeopleLocal();
	}

	void Update()
	{
		if (!isLeft)
		{
			if (ProtonFootballManager.Instance.GetCaptureKickLeft())
			{
				//UpdatePeopleLocal();
				CaptureSequenceLocal();
				ProtonFootballManager.Instance.SetCaptureKickLeft(false);
				//Freeze(2f);
			}
		}
		else
		{
			//CaptureSequenceRemote();
			Invoke("CaptureSequenceRemote", 0.3f);
		}

		if (Input.GetKeyDown("6"))
		{

			CaptureSequenceLocal();
		}


	}


}
