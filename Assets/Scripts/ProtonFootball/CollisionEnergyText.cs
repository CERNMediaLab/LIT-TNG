﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisionEnergyText : MonoBehaviour
{
	public Text energyCenterTx = null;
	public Text energyLeftTx = null;
	public Text energyRightTx = null;

	private float latestCollisionEnergy = 0f;
	private float MaxEnergy = 7000f;

	private AudioSource audio;
	public AudioClip explosion1;
	public AudioClip explosion2;
	public AudioClip explosion3;
	public AudioClip explosion4;

	void Start()
	{
		audio = GetComponent<AudioSource>();
	}

	void Update ()
	{
		float energy = ProtonFootballManager.Instance.GetEnergyCollision();
		float energyLeft = ProtonFootballManager.Instance.GetEnergyCollisionLeft();
		float energyRight = ProtonFootballManager.Instance.GetEnergyCollisionRight();

		energyCenterTx.text = ProtonFootballManager.Instance.GetDialEnergyText(energy, 0);
		energyLeftTx.text = ProtonFootballManager.Instance.GetDialEnergyText(energyLeft, 0);
		energyRightTx.text = ProtonFootballManager.Instance.GetDialEnergyText(energyRight, 0);

		if (ProtonFootballManager.Instance.JustGotAnEvent())
		{
			//audio.pitch = 1.2f;
			if (energy == 14000f)
			{
				audio.PlayOneShot(explosion4, 6f);
			}
			else if (energy > 1000)
			{
				audio.PlayOneShot(explosion3, 2.2f);
			}
			else if (energy > 1f)
			{
				audio.PlayOneShot(explosion2,0.6f);
			}
			else
			{
				audio.PlayOneShot(explosion1, 0.4f);
			}

		}

		latestCollisionEnergy = energy;
	}
}
