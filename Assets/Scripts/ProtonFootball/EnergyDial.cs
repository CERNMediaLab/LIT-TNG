﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class EnergyDial : MonoBehaviour
{

	public bool isLeft = true;
	public Text dialText = null;
	public Text dialTextUnits = null;
	private float MaxEnergy = 7000f;
	public Image dialImg;
	public Image instaSpeedImage;
	private float lastEnergy = -1f;
	public AudioSource audio;
	public AudioSource audio2;
	public AudioClip kick;
	public AudioClip beep;

	private float instaSpeed = 0f;
	private float instaSpeedSmooth = 0f;

	private void Awake()
	{
		//dialText = GetComponent<Text>();
	}

	void Start()
	{
		dialText.font.material.mainTexture.filterMode = FilterMode.Trilinear;
		//audio = GetComponent<AudioSource>();
		//audio2 = gameObject.AddComponent<AudioSource>();
	}

	public float MagnitudeToEnergy(float mag)
	{
		float speedBase = (mag * mag) / 280f;
		float speed = Mathf.Clamp(speedBase, 85f, 550);
		float energy =  (speedBase * speedBase) / 55f;
		return energy;
	}

	void SetInstaSpeed(float instS)
	{
		instaSpeed = instS;
		StartCoroutine(SetInstaSpeedSmooth(instaSpeed, 0.3f));
		
	}

	IEnumerator SetInstaSpeedSmooth(float sp , float aTime)
	{
		if ( sp != instaSpeedSmooth)
		{
			float ispeed = instaSpeedSmooth;
			for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
			{
				instaSpeedSmooth = Mathf.SmoothStep(ispeed , sp , t);
				yield return null;
			}
			instaSpeedSmooth = sp;			
		}

	}

	
	void Update ()
	{
		int side = -1;
		if (isLeft)
		{
			side = 1;
			float en = ProtonFootballManager.Instance.GetKickEnergyLeft();
			if (en != 0f)
			{
				//audio.pitch = 1.2f;
				audio.PlayOneShot(kick, 0.3f + (en / MaxEnergy) * 0.7f);
				ProtonFootballManager.Instance.SetKickEnergyLeft(0f);
			}

			if(instaSpeed != ProtonFootballManager.Instance.GetTopSpeedRemote())
			{
				SetInstaSpeed(ProtonFootballManager.Instance.GetTopSpeedRemote());
			}
		}
		else
		{
			float en = ProtonFootballManager.Instance.GetKickEnergyRight();
			if (en != 0f)
			{
				//audio.pitch = 1.2f;
				audio.PlayOneShot(kick, 0.3f + (en / MaxEnergy) * 0.7f);
				ProtonFootballManager.Instance.SetKickEnergyRight(0f);
			}

			if(instaSpeed != ProtonFootballManager.Instance.GetTopSpeed())
			{
				SetInstaSpeed(ProtonFootballManager.Instance.GetTopSpeed());
			}
		}

		instaSpeedImage.fillAmount = MagnitudeToEnergy(instaSpeedSmooth) / MaxEnergy;

		float energy = ProtonFootballManager.Instance.GetEnergySmooth(side);
		if (energy != lastEnergy)
		{
			dialText.text = ProtonFootballManager.Instance.GetDialEnergyText(energy, 1);
			dialTextUnits.text = ProtonFootballManager.Instance.GetDialEnergyText(energy, 2);
			dialImg.fillAmount = energy / MaxEnergy;
			//Debug.Log("Dial Energy: " + energy.ToString());
			if (!audio2.isPlaying && energy > 0f)
			{
				audio2.pitch = 1f + (energy / MaxEnergy) * 0.5f;
				//audio2.pitch = 1f;
				audio2.PlayOneShot(beep, 1f);

			}

		}

		lastEnergy = energy;




	}

}
