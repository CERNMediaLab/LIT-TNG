﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TextDate : MonoBehaviour 
{

	private Text dateText = null;
	
	void Start () 
	{
		dateText = GetComponent<Text>();
	}
	
	
	void Update () 
	{
		dateText.text = System.DateTime.Now.ToString("dd MMMMM yyyy").ToUpper();
	}
}
