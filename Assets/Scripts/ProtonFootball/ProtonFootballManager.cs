﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.Networking;

public class ProtonFootballManager : MonoBehaviour
{
	public GameObject protonprefab;
	public Text textEnergy1;
	public Text textEnergy2;
	public GameObject Base01;
	public GameObject Base02;

	public GameObject MaskEvent;
	public GameObject glitch;

	private float eventScale = 0.15f;
	private float eventScaleSide = 1.2f;
	private Proton proton1 = null;
	private Proton proton2 = null;
	private float collisionRadius = 4f;
	private float radiusKick = 4f;
	private float protonScale = 20f;
	private float energyProton1 = 0f;
	private float energyProton2 = 0f;

	private float energyCollision = 0f;
	private float energyCollisionLeft = 0f;
	private float energyCollisionRight = 0f;

	private float collisionDeviationX = 0f;
	private float collisionDeviationY = 0f;

	private float energySmoothLeft = 0f;
	private float energySmoothRight = 0f;


	private string stringEnergy1 = "";
	private string stringEnergy2 = "";
	private bool spawnedPlayer = false;
	private bool canSendData = true;
	private string lastBodyData = "";

	private bool captureKickLeft = false;
	private bool captureKickRight = false;

	private bool reloadLeft = false;
	private bool reloadRight = false;

	private float kickEnergyLeft = 0f;
	private float kickEnergyRight = 0f;

	private float topSpeed = 0f;
	private float topSpeedRemote = 0f;

	private bool showArrow = false;
	private Vector3 eventVDisplacement = new Vector3(-25f, -475f, 7f);

	private bool justGotAnEvent = false;

	private bool kickRpcCooldownDone = true;

	private byte[] lastRemoteJpg = null;
	private Texture2D tx;
	private Texture2D tx2;
	private Texture2D[] lastRemoteSequence;
	private Texture2D[] lastLocalSequence;
	private List<Texture2D> capturedSequence;
	private Color[] TexturePixels;
	private float timeCapture = 0.08f;
	private int iCapture = 0;
	private bool isCapturing = false;



	private int thisSide = 1;

	private AudioSource audio;
	public AudioClip reloadPackman;
	public AudioClip reloadSpiral;
	private Vector3 _speedVectorRaw;

	protected static ProtonFootballManager instance = null;




	public static ProtonFootballManager Instance
	{
		get
		{
			return instance;
		}
	}

	void Start()
	{
		Invoke("DelayedStart", 0.5f);
	}

	void DelayedStart ()
	{
		instance = this;
		audio = gameObject.AddComponent<AudioSource>();


		InitializeSequence();

		if (CEO.Instance != null)
		{
			if (!CEO.Instance.GetIsSide())
			{
				KinectChassis.Instance.SetActive(true);
				KinectChassis.Instance.ShowFeetMarkers(true);
				KinectChassis.Instance.ShowHandMarkers(false);
				KinectChassis.Instance.SetCalibrationIndicatorPosition(SaveLoad.savedSettings.protonDistance / 2f + 22f);
				KinectChassis.Instance.dataManager.RequestJoints();
				KinectChassis.Instance.SetStreamSelection(new int[] {0, 1, 0, 1, 1});
				TEventManager.Instance.SetScale(eventScale);
				Invoke("SpawnProtons", 1f);
			}
			else
			{
				KinectChassis.Instance.SetActive(true);
				KinectChassis.Instance.ShowFeetMarkers(true);
				KinectChassis.Instance.SetFeetVisibility(false);
				TEventManager.Instance.SetScale(eventScaleSide);
				KinectChassis.Instance.dataManager.RequestJoints();
				KinectChassis.Instance.SetStreamSelection(new int[] {0, 1, 0, 1, 1});
			}
		}

		TEventManager.Instance.Initialize();

		TEventManager.Instance.SetDisplayCollidingProtons(false);

		if (LPlayer.Instance != null)
		{
			if (CEO.Instance.GetIsFloor())
			{
				LPlayer.Instance.CmdSetFloorLevel(2);
			}
			else
			{
				LPlayer.Instance.levelSide = 2;
			}
		}

	}

	public void InitializeSequence()
	{
		capturedSequence = new List<Texture2D>();

		lastRemoteSequence = new Texture2D[24];
		lastLocalSequence = new Texture2D[24];

		tx = new Texture2D(2, 2);
		tx2 = new Texture2D(2, 2);
		Color grayBlack = new Color(0.0745f, 0.0667f, 0.0627f, 1f);


		for (int y = 0; y < tx.height; y++)
		{
			for (int x = 0; x < tx.width; x++)
			{
				tx.SetPixel(x, y, grayBlack);
			}
		}
		tx.Apply();

		iCapture = 0;
		foreach (Texture2D t in lastRemoteSequence)
		{
			lastRemoteSequence[iCapture] = tx;
			lastLocalSequence[iCapture] = tx;
			iCapture++;
		}

		CaptureSequence();
	}

	public void StartCapture()
	{
		isCapturing = true;
	}

	private void CaptureSequence()
	{
		Invoke("CaptureSequence", timeCapture);
		StartCoroutine(CaptureSequenceActions());
	}

	IEnumerator CaptureSequenceActions()
	{
		tx = KinectChassis.Instance.GetTexture_People(false);

		TexturePixels = tx.GetPixels();


		tx2 = new Texture2D(tx.width, tx.height, TextureFormat.RGBA32, false);
		//Graphics.CopyTexture(tx, tx2);

		tx2.SetPixels(TexturePixels);
		tx2.Apply(false);

		capturedSequence.Add(tx2);
		if (!isCapturing)
		{
			while (capturedSequence.Count > 8)
			{
				capturedSequence.RemoveAt(0);
			}
		}
		else
		{
			if (capturedSequence.Count >= lastRemoteSequence.Length)
			{
				SetFinalSequence();
				isCapturing = false;
				if (CEO.Instance.GetIsFloor())
				{
					StartCoroutine(SendSequence());
				}
			}
		}

		yield return null;
	}

	private void SetFinalSequence()
	{
		iCapture = 0;
		foreach (Texture2D t in lastLocalSequence)
		{
			//Texture2D.DestroyImmediate(lastLocalSequence[iCapture]);
			lastLocalSequence[iCapture] = capturedSequence[iCapture];
			iCapture++;
		}
	}

	IEnumerator CreateFakeProtonForTest()
	{
		
		GameObject pr = Instantiate(protonprefab, new Vector3(0f, 0f, SaveLoad.savedSettings.protonDistance / 2f), Quaternion.identity);
        
        yield return new WaitForSeconds(2f);
        Destroy(pr);
	}

	public Vector3 GetEventDisplacement()
	{
		return eventVDisplacement;
	}

	public Vector3 CheckKickProton(int side)
	{
		Vector3 speedVector = Vector3.zero;
		if (KinectChassis.Instance != null)
		{
			_speedVectorRaw = KinectChassis.Instance.CheckKick(new Vector3(0f, 0f, side * SaveLoad.savedSettings.protonDistance / 2f), GetRadiusKick());
			speedVector = new Vector3(_speedVectorRaw.x, 0, _speedVectorRaw.y);
		}

		return speedVector;
	}

	public void SetTopSpeed(float ts)
	{
		topSpeed = ts;
		if (CEO.Instance != null && CEO.Instance.GetIsFloor() && LPlayerPF.Instance != null)
		{
			LPlayerPF.Instance.CmdSetTopSpeedRemote(ts);
		}
	}

	public void SetTopSpeedRemote(float tsr)
	{
		topSpeedRemote = tsr;
	}

	public float GetTopSpeedRemote()
	{
		return topSpeedRemote;
	}

	public float GetTopSpeed()
	{
		return topSpeed;
	}

	public Vector3 GetProtonPosition(int side)
	{
		return new Vector3(0f, 0f, side * SaveLoad.savedSettings.protonDistance / 2f);
	}

	public void CalibrateLeft()
	{
		if (CEO.Instance != null)
		{
			if (CEO.Instance.GetIsPlayerLeft())
			{
				KinectChassis.Instance.CalibrateLocalFeet(3f);
			}
			else
			{
				KinectChassis.Instance.CalibrateRemoteFeet(3f);
			}
		}
		else
		{
			KinectChassis.Instance.CalibrateLocalFeet(3f);
		}

		KinectChassis.Instance.FadeLeftFeetTo(1.0f, 0.7f);
	}

	public void CalibrateRight()
	{
		if (CEO.Instance != null)
		{
			if (!CEO.Instance.GetIsPlayerLeft())
			{
				KinectChassis.Instance.CalibrateLocalFeet(3f);
			}
			else
			{
				KinectChassis.Instance.CalibrateRemoteFeet(3f);
			}
		}
		else
		{
			KinectChassis.Instance.CalibrateLocalFeet(3f);
		}

		KinectChassis.Instance.FadeRightFeetTo(1.0f, 0.7f);
	}

	/*public void CalibrateRemoteAsLocal()
	{
		if (CEO.Instance != null)
		{
			Vector3 leftFootPos = Vector3.zero;
			Vector3 rightFootPos = Vector3.zero;
			if (CEO.Instance.GetIsPlayerLeft()) //Remote is Right
			{
				leftFootPos = KinectChassis.Instance.GetCalibrationIndicatorPosition(1);
				rightFootPos = KinectChassis.Instance.GetCalibrationIndicatorPosition(2);
			}
			else
			{
				leftFootPos = KinectChassis.Instance.GetCalibrationIndicatorPosition(3);
				rightFootPos = KinectChassis.Instance.GetCalibrationIndicatorPosition(4);
			}

			LPlayerPF.Instance.CmdCalibrateRemoteAsLocal(leftFootPos, rightFootPos, KinectChassis.Instance.GetSideSwap());
		}
	}*/

	public void SideSwap()
	{
		KinectChassis.Instance.SideSwap();
	}

	public void Rotate180()
	{
		KinectChassis.Instance.Rotate180();
	}

	public void SpawnPlayer()
	{
		if (LPlayer.Instance != null)
		{
			LPlayer.Instance.CreateLevelPlayer(2);
			spawnedPlayer = true;
		}
	}

	public float GetProtonDistance()
	{
		return SaveLoad.savedSettings.protonDistance;
	}

	public void SetProtonDistance(float pd)
	{
		SaveLoad.savedSettings.protonDistance = pd;
	}

	private Proton SpawnProton(Vector3 position)
	{
		Proton pr = Instantiate(protonprefab, position, Quaternion.identity).GetComponent( typeof(Proton) ) as Proton;
		pr.SetManager(this);
		return pr;
	}

	private Proton SpawnProton()
	{
		return SpawnProton(Vector3.zero);
	}

	public void ReloadSpawnLeft()
	{
		reloadLeft = true;
	}

	public bool GetReloadLeft()
	{
		return reloadLeft;
	}

	public void ResetReloadLeft()
	{
		reloadLeft = false;
	}

	public void ReloadSpawnRight()
	{
		reloadRight = true;
	}

	public bool GetReloadRight()
	{
		return reloadRight;
	}

	public void ResetReloadRight()
	{
		reloadRight = false;
	}

	private void SpawnProtons()
	{
		SpawnProton1();
		SpawnProton2();
	}

	private void SpawnProtonsAction()
	{
		SpawnProton1Action();
		SpawnProton2Action();
	}

	public void SpawnProton1()
	{
		ReloadSpawnRight();
	}

	public void SpawnProton1Action()
	{
		proton1 = SpawnProton(new Vector3(0f, 0f, SaveLoad.savedSettings.protonDistance / 2f));
		proton1.SetSide(1);
		proton1.SetScale(protonScale);
	}

	public void SpawnProton2()
	{
		ReloadSpawnLeft();
	}

	public void SpawnProton2Action()
	{
		proton2 = SpawnProton(new Vector3(0f, 0f, -SaveLoad.savedSettings.protonDistance / 2f));
		proton2.SetSide(-1);
		proton2.SetScale(protonScale);
	}


	public void SpawnInSeconds(int side, float seconds)
	{
		if (side == 1)
		{
			Invoke("SpawnProton1", seconds);
		}
		else
		{
			Invoke("SpawnProton2", seconds);
		}
	}

	public float GetRadiusKick()
	{
		collisionRadius = protonScale;
		radiusKick = protonScale;
		return radiusKick;
	}

	private bool CheckCollision()
	{
		bool collision = false;
		if (proton1 != null && proton2 != null)
		{
			collision = (proton1.transform.position - proton2.transform.position).magnitude <= collisionRadius;

		}
		return collision;
	}

	public void SetEnergySmooth(int side, float en)
	{
		float aTime = side == 1 ? 1.2f * Mathf.Abs(en - energySmoothLeft) / 7000f :  1.2f * Mathf.Abs(en - energySmoothRight) / 7000f ;
		aTime = Mathf.Clamp(aTime, 0.3f, 1.2f);
		if (side == 1)
		{
			StartCoroutine(ProtonEnergyGoToLeft(side, en, aTime));
		}
		else
		{
			StartCoroutine(ProtonEnergyGoToRight(side, en , aTime));
		}
	}

	public float GetEnergySmooth(int side)
	{
		if (side == 1)
		{
			return energySmoothLeft;
		}
		else
		{
			return energySmoothRight;
		}
	}

	IEnumerator ProtonEnergyGoToLeft(int side, float en , float aTime)
	{
		if ( en != energySmoothLeft)
		{
			float currentEnergy = energySmoothLeft;
			for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
			{
				energySmoothLeft = Mathf.SmoothStep(currentEnergy , en , t);
				yield return null;
			}
			energySmoothLeft = en;
			//currentEnergy = energySmoothLeft;
		}
	}

	IEnumerator ProtonEnergyGoToRight(int side, float en , float aTime)
	{
		if ( en != energySmoothRight)
		{
			float currentEnergy = energySmoothRight;
			for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
			{
				energySmoothRight = Mathf.SmoothStep(currentEnergy , en , t);
				yield return null;
			}
			energySmoothRight = en;
		}
	}

	public float GetKickEnergyLeft()
	{
		return kickEnergyLeft;
	}

	public void SetKickEnergyLeft(float en)
	{
		kickEnergyLeft = en;
	}

	public float GetKickEnergyRight()
	{
		return kickEnergyRight;
	}

	public void SetKickEnergyRight(float en)
	{
		kickEnergyRight = en;
	}


	public void SetEnergyProton(int side, float en)
	{

		if (LPlayerPF.Instance != null && CEO.Instance.GetIsFloor() )
		{
			if (side == 1)
			{
				LPlayerPF.Instance.CmdLeftKick(en);
				if ( !CEO.Instance.GetIsPlayerLeft() && en > 0f)	SendLastPeopleSequence();
			}
			else
			{
				LPlayerPF.Instance.CmdRightKick(en);
				if ( CEO.Instance.GetIsPlayerLeft() && en > 0f) SendLastPeopleSequence();
			}

		}

		if (CEO.Instance != null && CEO.Instance.GetIsFloor())
		{
			SetEnergyText(side, en);
		}
		else
		{
			SetEnergySmooth(side, en);
		}

	}

	public string GetEnergyText(float en)
	{
		float energy = Mathf.Clamp(en, 0f, 14000f);
		float energyInUnits = energy;
		string energyText = "";


		string units = " GeV";
		if (energy < 1f)
		{
			units = " MeV";
			energyInUnits = energy * 1000f;
		}
		else if (energy < 1000f)
		{
			units = " GeV";
			energyInUnits = energy;
		}
		else if (energy > 1000f)
		{
			units = " TeV";
			energyInUnits = energy / 1000f;
		}

		energyText = "Energy: " + energyInUnits.ToString("n2") + units;
		return energyText;
	}

	public string GetDialEnergyText(float en, int mode)
	{
		float clampEnergy = mode == 0 ? 14000 : 7000;
		float energy = Mathf.Clamp(en, 0f, clampEnergy);
		float energyInUnits = energy;
		string energyText = "";
		string energyInUnitsText = "";



		string units = " MeV";
		if (energy < 1f)
		{
			units = " MeV";
			energyInUnits = energy * 1000f;
			energyInUnitsText = energyInUnits.ToString("n0");
		}
		else if (energy < 1000f)
		{
			units = " GeV";
			energyInUnits = energy;
			energyInUnitsText = energyInUnits.ToString("n1");
		}
		else if (energy > 1000f)
		{
			units = " TeV";
			energyInUnits = energy / 1000f;
			energyInUnitsText = energyInUnits.ToString("n2");
		}

		switch (mode)
		{
		case 0:
			energyText = energyInUnitsText + units;
			break;
		case 1:
			energyText = energyInUnitsText;
			break;
		case 2:
			energyText = units;
			break;
		}


		return energyText;
	}

	public void SetEnergyText(int side, float en)
	{
		float energy = Mathf.Clamp(en, 0f, 7000f);


		if (side == 1)
		{
			energyProton1 = energy;
			stringEnergy1 = GetEnergyText(en);
		}
		else
		{
			energyProton2 = energy;
			stringEnergy2 = GetEnergyText(en);
		}

	}

	public float GetEnergyCollision()
	{
		return energyCollision;
	}

	public float GetEnergyCollisionRight()
	{
		return energyCollisionRight;
	}

	public float GetEnergyCollisionLeft()
	{
		return energyCollisionLeft;
	}

	public float GetCollisionDeviationX()
	{
		return collisionDeviationX;
	}

	public float GetCollisionDeviationY()
	{
		return collisionDeviationY;
	}

	public void DoCommandCollision(float left, float right, float devX, float devY)
	{
		energyCollisionLeft = left;
		energyCollision = left + right;
		energyCollisionRight = right;
		collisionDeviationX = devX;
		collisionDeviationY = devY;
		JustGotATeleportedEvent();
	}

	public void DoCommandKickLeft(float energy)
	{
		energy = Mathf.Clamp(energy, 0f, 7000f);
		SetEnergyProton(-1, energy);

		if (energy > 0f) SetCaptureKickLeft(true);
		SetKickEnergyLeft(energy);
	}

	public void DoCommandKickRight(float energy)
	{
		energy = Mathf.Clamp(energy, 0f, 7000f);
		SetEnergyProton(1, energy);
		if (energy > 0f) SetCaptureKickRight(true);
		SetKickEnergyRight(energy);
	}

	public void DoCommandReloadPackman()
	{
		if (CEO.Instance.GetIsPlayerLeft())
		{
			audio.pitch = 1.4f;
		}
		else
		{
			audio.pitch = 1.0f;
		}

		audio.PlayOneShot(reloadPackman, 0.6f);
	}

	public void DoCommandReloadSpiral()
	{
		if (CEO.Instance.GetIsPlayerLeft())
		{
			audio.pitch = 1.4f;
		}
		else
		{
			audio.pitch = 1.0f;
		}

		audio.PlayOneShot(reloadSpiral, 1f);
	}

	public void SetCaptureKickLeft(bool cl)
	{
		captureKickLeft = cl;
		//Debug.Log("Setting Capture Left: " + cl);
	}

	public bool GetCaptureKickLeft()
	{
		return captureKickLeft;
	}

	public void SetCaptureKickRight(bool cr)
	{
		captureKickRight = cr;
		//Debug.Log("Setting Capture Right: " + cr);
	}

	public void KickProtonLeft(Vector3 speedVectorRaw)
	{
		if (proton2 != null)
			proton2.Kick(speedVectorRaw);
	}

	public void KickProtonRight(Vector3 speedVectorRaw)
	{
		if (proton1 != null)
			proton1.Kick(speedVectorRaw);
	}

	public bool GetCaptureKickRight()
	{
		return captureKickRight;
	}

	public void Teleport(TStructEvent tsEvent)
	{
		//Debug.Log("Teleport");
		if (LPlayerPF.Instance != null)
		{
			LPlayerPF.Instance.Teleport(tsEvent);
		}
	}

	/*public string ConvertStructToStringOfBytes(TStructTeleportEvent obj)
	{
		using (MemoryStream ms = new MemoryStream())
		{
			new BinaryFormatter().Serialize(ms, obj);
			return Convert.ToBase64String(ms.ToArray());
		}
	}*/

	private IList<string> SplitIntoChunks(string text, int chunkSize)
	{
		List<string> chunks = new List<string>();
		int offset = 0;
		while (offset < text.Length)
		{
			int size = Math.Min(chunkSize, text.Length - offset);
			chunks.Add(text.Substring(offset, size));
			offset += size;
		}
		return chunks;
	}

	private void SendBodyData()
	{
		if (CEO.Instance != null && CEO.Instance.GetIsSide() && canSendData)
		{
			string bodyData = KinOps.Instance.GetSkeletons2();
			if (bodyData == null) bodyData = "[]";


			if (bodyData != lastBodyData)
			{
				UDPSend.Instance.sendString(bodyData);
			}

			lastBodyData = bodyData;

			if (TestText.Instance != null)
				TestText.Instance.SetTestText(bodyData);
			//log = "Sending Joints\nBytes: " + numBytes.ToString();

		}
	}





	public bool GetShowArrow()
	{
		return showArrow;
	}

	public void ToggleShowArrow()
	{
		showArrow = !showArrow;
	}

	public void JustGotATeleportedEvent()
	{
		justGotAnEvent = true;
		StartCoroutine(NextFrameAfterTeleport());
		GameObject mask = Instantiate(MaskEvent, Vector3.zero, Quaternion.identity);
		mask.transform.SetParent(MaskEvent.transform.parent);
		mask.transform.position = MaskEvent.transform.position;
		mask.transform.rotation = MaskEvent.transform.rotation;
		mask.SetActive(true);
	}

	private IEnumerator NextFrameAfterTeleport()
	{
		yield return null;
		justGotAnEvent = false;
	}

	public bool JustGotAnEvent()
	{
		return justGotAnEvent;
	}

	public void ResetCooldown()
	{
		kickRpcCooldownDone = true;
	}

	/*public void SendLastPeopleImage()
	{
		byte[] peopleJPG = KinectChassis.Instance.GetJpegPeople();
		string peopleString = Convert.ToBase64String(peopleJPG);

		if (lastRemoteJpg != peopleJPG)
			LPlayerPF.Instance.CmdSendKickImageString(peopleString);

		lastRemoteJpg = peopleJPG;
	}*/


	public void SendLastPeopleSequence()
	{
		//StartCoroutine(CaptureAndSendSequence());
		isCapturing = true;
	}

	IEnumerator SendSequence()
	{
		iCapture = 0;
		foreach (Texture2D t in lastLocalSequence)
		{
			LPlayerPF.Instance.CmdSendKickImageJpg(lastLocalSequence[iCapture].EncodeToJPG(20), iCapture);
			//Debug.Log ("Sent " + iCapture);
			yield return new WaitForSeconds(timeCapture * 0.5f);
			iCapture++;
		}
	}

	IEnumerator CaptureAndSendSequenceString()
	{
		iCapture = 0;
		foreach (Texture2D t in lastLocalSequence)
		{

			lastRemoteJpg = KinectChassis.Instance.GetJpegPeople();
			string peopleString = Convert.ToBase64String(lastRemoteJpg);

			LPlayerPF.Instance.CmdSendKickImageString(peopleString, iCapture);

			yield return new WaitForSeconds(timeCapture);
			iCapture++;
		}
	}

	IEnumerator CaptureAndSendSequence()
	{
		iCapture = 0;
		foreach (Texture2D t in lastLocalSequence)
		{
			lastRemoteJpg = KinectChassis.Instance.GetJpegPeople();

			LPlayerPF.Instance.CmdSendKickImageJpg(lastRemoteJpg, iCapture);
			//Debug.Log ("Sent " + iCapture);
			yield return new WaitForSeconds(timeCapture);
			iCapture++;
		}
	}


	public void SetLastRemoteTexture(string imageString, int index)
	{
		//Debug.Log(index + "   " + imageString);
		byte[] lrj = Convert.FromBase64String(imageString);
		if (lrj != null)
			try
			{
				lastRemoteSequence[index].LoadImage(lrj);
				lastRemoteSequence[index].Apply();
			}
			catch (Exception e)
			{
				Debug.Log("LoadImage error: " + e);
			}
	}

	public void SetLastRemoteTexture(byte[] lrj, int index)
	{

		if (lrj != null)
			try
			{
				tx = new Texture2D(2, 2);
				tx.LoadImage(lrj);
				tx.Apply();
				lastRemoteSequence[index] = tx;
				//Debug.Log("lastRemoteSequence " + index);
			}
			catch (Exception e)
			{
				Debug.Log("LoadImage error: " + e);
			}
	}

	public byte[] GetLastRemoteJpg()
	{
		return lastRemoteJpg;
	}

	public Texture2D[] GetLastRemoteSequence()
	{
		return lastRemoteSequence;
	}

	public Texture2D[] GetLastLocalSequence()
	{
		return lastLocalSequence;
	}



	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.S) && !Input.GetKey(KeyCode.LeftShift))
		{
			//SpawnProton(Vector3.zero);
			SpawnProtonsAction();
		}

		if (Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.LeftShift))
		{			
			glitch.SetActive(!glitch.active);
		}

		if (Input.GetKeyDown("a"))
		{
			//SpawnProton(Vector3.zero);
			ToggleShowArrow();
		}


		/*if (Input.GetKeyDown("d"))
		{
			canSendData = !canSendData;
		}*/

		if (Input.GetKeyDown("w"))
		{
			float lr = 0f;
			if (CEO.Instance.GetIsSide())
			{
				TStructEvent gEvent = TEventManager.Instance.GenerateEvent(7f, 5f, eventVDisplacement, eventScaleSide, lr);

			}
			else
			{
				TStructEvent gEvent = TEventManager.Instance.GenerateEvent(7f, 5f, Vector3.zero, eventScale, lr);

				Teleport(gEvent);
			}

		}

		if (Input.GetKeyDown("e"))
		{
			SendLastPeopleSequence();
			//DoCommandKickLeft(7000f);
		}

		if (Input.GetKeyDown("f"))
		{
			KinectChassis.Instance.ToggleFeetVisibility();
		}

		if (Input.GetKeyDown("q"))
		{
			StartCoroutine(CreateFakeProtonForTest());			
		}

		if (Input.GetKeyDown("m") || Input.GetKeyDown(KeyCode.Mouse1))
		{
			KickProtonRight(new Vector3(UnityEngine.Random.Range(-0.15f, 0.15f), 0, -1f)*UnityEngine.Random.Range(0.1f, 7000f));
		}

		if (Input.GetKeyDown("n") || Input.GetKeyDown(KeyCode.Mouse0))
		{
			KickProtonLeft(new Vector3(UnityEngine.Random.Range(-0.15f, 0.15f), 0, 1f)* UnityEngine.Random.Range(0.1f, 7000f));
		}

		/*if (Input.GetKeyDown("0"))
		{
			Debug.Log("ProtonDistance: " + SaveLoad.savedSettings.protonDistance + " FirstFootPos: " + KinectChassis.Instance.GetFirstFootPosition());
		}*/



		/*if (Input.GetKeyDown("g"))
		{
			DoCommandKickLeft(3000f);
		}*/


		if (KinectChassis.Instance.GetTopSpeed() != topSpeed) SetTopSpeed(KinectChassis.Instance.GetTopSpeed());

		if (CEO.Instance != null && !CEO.Instance.GetIsSide() && CheckCollision())
		{
			Vector3 collisionPoint = (proton1.transform.position + proton2.transform.position) / 2f;
			proton1.Kill();
			proton2.Kill();
			proton1 = null;
			proton2 = null;
			if (LPlayerPF.Instance != null)
			{
				LPlayerPF.Instance.CmdCollision(energyProton2, energyProton1, collisionPoint.z, collisionPoint.x);
			}
			float lrBalance = (energyProton1 - energyProton2)/(energyProton2 + energyProton1);
			TStructEvent gEvent = TEventManager.Instance.GenerateEvent((energyProton1 + energyProton2) / 1000f, 3f, collisionPoint, eventScale, lrBalance);
			Teleport(gEvent);
		}

		if (CEO.Instance != null && CEO.Instance.GetIsSide())
		{
			thisSide = 1;
			if (CEO.Instance.GetIsPlayerLeft())
			{
				thisSide = -1;
			}

			if (kickRpcCooldownDone)
			{
				Vector3 kickVctr = CheckKickProton(thisSide);
				if (kickVctr != Vector3.zero)
				{
					LPlayerPF.Instance.RpcKickfromRemote(kickVctr, 1f);
					kickRpcCooldownDone = false;
					Invoke("ResetCooldown", 0.4f);
				}
			}
		}

		if (!spawnedPlayer && LPlayer.Instance != null && !CEO.Instance.GetIsSide())
		{
			if (LPlayer.Instance != null && LPlayer.Instance.levelSide == 2)
			{
				SpawnPlayer();
			}
		}



		//SendBodyData();

		textEnergy1.text = stringEnergy1;
		textEnergy2.text = stringEnergy2;

	}
}

