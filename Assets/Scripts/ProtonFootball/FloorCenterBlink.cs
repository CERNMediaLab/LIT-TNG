﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorCenterBlink : MonoBehaviour
{
	public Image p1;
	public Image p2;
	public Image p3;
	public Image p4;

	public Image p1b;
	public Image p2b;
	public Image p3b;
	public Image p4b;


	void Start ()
	{
		p1.canvasRenderer.SetAlpha (0f);
		p2.canvasRenderer.SetAlpha (0f);
		p3.canvasRenderer.SetAlpha (0f);
		p4.canvasRenderer.SetAlpha (0f);

		p1b.canvasRenderer.SetAlpha (0f);
		p2b.canvasRenderer.SetAlpha (0f);
		p3b.canvasRenderer.SetAlpha (0f);
		p4b.canvasRenderer.SetAlpha (0f);
		Blink();
	}

	void Blink()
	{
		StartCoroutine(BlinkSequence(0.1f, 0.25f));
		Invoke("Blink", 2f);
	}


	public IEnumerator BlinkImage(Image im, float blinkTime)
	{
		im.CrossFadeAlpha(0.5f, blinkTime, true);
		yield return new WaitForSeconds(blinkTime);
		im.CrossFadeAlpha(0f, blinkTime, true);
	}

	public IEnumerator BlinkSequence (float timeBetween, float blinkTime) 
	{
		
		StartCoroutine(BlinkImage(p4,blinkTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(BlinkImage(p3,blinkTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(BlinkImage(p2,blinkTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(BlinkImage(p1,blinkTime));
		yield return new WaitForSeconds(timeBetween*3f);

		StartCoroutine(BlinkImage(p4b,blinkTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(BlinkImage(p3b,blinkTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(BlinkImage(p2b,blinkTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(BlinkImage(p1b,blinkTime));
		yield return new WaitForSeconds(timeBetween);
	}






	void Update ()
	{

	}
}
