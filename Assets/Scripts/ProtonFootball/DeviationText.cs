﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DeviationText : MonoBehaviour
{

	public Text devTextX = null;
	public Text devTextY = null;

	private float realDistanceProtons = 105f;
	private float unityDistanceprotons = 90f;

	void Start ()
	{

	}


	void Update ()
	{
		if (devTextX != null && devTextY != null)
		{
			devTextX.text = (ProtonFootballManager.Instance.GetCollisionDeviationX()*realDistanceProtons/unityDistanceprotons).ToString("n2") + " cm";
			devTextY.text = (-ProtonFootballManager.Instance.GetCollisionDeviationY()*realDistanceProtons/unityDistanceprotons).ToString("n2") + " cm";
		}

	}
}
