﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProtonReloader : MonoBehaviour
{
	public Image relIm1;
	public Image relIm2;
	public Image relIm3;

	public Image relIm1b;
	public Image relIm2b;
	public Image relIm3b;

	public bool isLeft = true;

	private float fill_1 = 0f;
	private float fill_2 = 0f;
	private float fill_3 = 0f;


	private float fillTime = 0.4f;
	private float timeBetween = 0.2f;

	private float maxFillAmount = 1f;

	private bool isPackman = false;

	void Start ()
	{

	}

	public void Reload()
	{
		if (LPlayerPF.Instance != null)
		{
			isPackman = !isPackman;
			SetPackmanSpiral();
			StartCoroutine(ReloadSequence());
		}
	}

	public void SetPackmanSpiral()
	{
		if (isPackman)
		{
			LPlayerPF.Instance.CmdReloadPackman();
			maxFillAmount = 0.5f;
			fillTime = 0.4f;
			timeBetween = 0.2f;
		}
		else
		{
			LPlayerPF.Instance.CmdReloadSpiral();
			maxFillAmount = 1f;
			fillTime = 0.2f;
			timeBetween = 0.2f;
		}
	}

	IEnumerator Fill_1_GoTo(float f1 , float aTime)
	{
		if ( f1 != fill_1)
		{
			float orig = fill_1;
			for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
			{
				fill_1 = Mathf.SmoothStep(orig , f1 , t);
				yield return null;
			}
			fill_1 = f1;
		}
	}

	IEnumerator Fill_2_GoTo(float f2 , float aTime)
	{
		if ( f2 != fill_2)
		{
			float orig = fill_2;
			for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
			{
				fill_2 = Mathf.SmoothStep(orig , f2 , t);
				yield return null;
			}
			fill_2 = f2;
		}
	}

	IEnumerator Fill_3_GoTo(float f3 , float aTime)
	{
		if ( f3 != fill_3)
		{
			float orig = fill_3;
			for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
			{
				fill_3 = Mathf.SmoothStep(orig , f3 , t);
				yield return null;
			}
			fill_3 = f3;
		}
	}

	public IEnumerator ReloadSequence()
	{
		StartCoroutine(Fill_1_GoTo(maxFillAmount, fillTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(Fill_2_GoTo(maxFillAmount, fillTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(Fill_3_GoTo(maxFillAmount, fillTime));
		yield return new WaitForSeconds(fillTime + timeBetween);

		if (isLeft)
		{
			ProtonFootballManager.Instance.SpawnProton2Action();
		}
		else
		{
			ProtonFootballManager.Instance.SpawnProton1Action();
		}

		StartCoroutine(Fill_3_GoTo(0f, fillTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(Fill_2_GoTo(0f, fillTime));
		yield return new WaitForSeconds(timeBetween);
		StartCoroutine(Fill_1_GoTo(0f, fillTime));
	}


	void Update ()
	{
		relIm1.fillAmount = fill_1;
		relIm2.fillAmount = fill_2;
		relIm3.fillAmount = fill_3;

		if (relIm1b && relIm2b && relIm3b && isPackman)
		{
			relIm1b.fillAmount = fill_1;
			relIm2b.fillAmount = fill_2;
			relIm3b.fillAmount = fill_3;
		}

		if (Input.GetKeyDown("r"))
		{
			Reload();
		}

		if (isLeft && ProtonFootballManager.Instance.GetReloadLeft())
		{
			ProtonFootballManager.Instance.ResetReloadLeft();
			Reload();
		}

		if (!isLeft && ProtonFootballManager.Instance.GetReloadRight())
		{
			ProtonFootballManager.Instance.ResetReloadRight();
			Reload();
		}
	}
}
