﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RemoteFeetTest : MonoBehaviour
{
	private Text feetText = null;

	void Start()
	{
		feetText = GetComponent<Text>();
	}

	void Update ()
	{
		int thisSide = 1;
		if (CEO.Instance.GetIsPlayerLeft())
		{
			thisSide = -1;
		}

		feetText.text = "Feet: " + KinectChassis.Instance.GetFeetString() + "  ProtonPos: " + ProtonFootballManager.Instance.GetProtonPosition(thisSide).ToString("F0");
	}
}
