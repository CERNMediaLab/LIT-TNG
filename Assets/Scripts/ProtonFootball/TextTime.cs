﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TextTime : MonoBehaviour 
{

	private Text timeText = null;
	
	void Start () 
	{
		timeText = GetComponent<Text>();
	}
	
	
	void Update () 
	{
		timeText.text = System.DateTime.Now.ToString("HH:mm:ss");
	}
}
