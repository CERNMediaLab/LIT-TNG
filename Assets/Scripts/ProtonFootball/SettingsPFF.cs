﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPFF : MonoBehaviour {

	public Text txtFlip;
	public Text txtReverse;
	
	void Start () 
	{
		
	}
	
	
	void Update () 
	{
		txtFlip.text = "SideSwap: " + KinectChassis.Instance.GetSideSwap();
		txtReverse.text = "Rotate180: " + KinectChassis.Instance.GetRotate180();;
	}
}
