using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class MoviePlayerManager : MonoBehaviour
{

	protected static MoviePlayerManager instance = null;

	public static int fileIndex = 0;
	public static MovieButton selectedButton = null;
	public static string videoTitle;

	public GameObject movieListPanel;
	public GameObject screenMenuBar;
	public GameObject moviePlayerCanvas;
	public Text videoTitleText;
	public Slider volumeBar1,volumeBar2;
	public bool isPaused = false;
	public bool isMuted = false;
	public bool isShuffle = false;
	public bool isFirstPlayed = true;
	public float initialVolume = 0.4f;
	public bool videoChanged = false;
	public Text titleText;
	public Text durationText;
	public Text languageText;
	public Text descriptionText;


	private int previousFileIndex;
	private bool isVisible = false;
	private bool isScreenActive = false;
	private bool isScreeMenuBarActive = false;
	private AudioSource audioSrc;
	private string _videoTitle = "";

	public static MoviePlayerManager Instance
	{
		get
		{
			return instance;
		}
	}

	// Use this for initialization
	void Start ()
	{
			instance = this;
			THC.LoadLanguages();
			THC.ChangeLanguage(1);
			//ShowMovieList();
			Invoke("SetInitialVolume",1f);
			//Invoke("SetNormalIndexesList",0.5f);
	}

	// Update is called once per frame
	void Update ()
	{
			//check if the selected video file changed
			if(fileIndex!=previousFileIndex)
			{
				previousFileIndex = fileIndex;
				isFirstPlayed = true;
			}
			//show screen menu bar
			if (Input.GetKeyDown(KeyCode.Tab) && !isScreeMenuBarActive)
			{
				ShowScreenMenuBar();
				isScreeMenuBarActive = true;
			}//hide screen menu bar
			else if (Input.GetKeyDown(KeyCode.Tab) && isScreeMenuBarActive)
			{
				HideScreenMenuBar();
				isScreeMenuBarActive = false;
			}

			if(Input.GetKeyDown(KeyCode.Space))
			{
					PlayPauseVideo();

			}

			if(_videoTitle!=videoTitle)
			{
					_videoTitle = videoTitle;
					videoTitleText.text = _videoTitle;
			}
	}

	//----------------------BASIC VIDEO PLAYER FUNCTIONALITY----------------------

	public void playVideoFromMainMenu()
	{
			PlayPauseVideo();
			moviePlayerCanvas.SetActive(false);
	}

	public void PlayPauseVideo()
	{
		if (isFirstPlayed) //play video for first time
		{
			LVideoPlayer.Instance.PlayVideo(fileIndex);
			isFirstPlayed = false;
			isPaused = false;
		}
		else if(!isPaused)  //pause video
		{
			LVideoPlayer.Instance.PauseVideo(fileIndex);
			isPaused = !isPaused;
		}
		else if(isPaused) //resume video
		{
			LVideoPlayer.Instance.ResumeVideo();
			isPaused = !isPaused;
		}
	}

	public void StopVideo()
	{
		LVideoPlayer.Instance.StopVideo();
		isFirstPlayed = true;
		GetBackToVideoPlayer();
	}

	public void SetVolume(Slider volumeBar)
	{
			audioSrc = GetComponent<AudioSource>();
			audioSrc.volume = volumeBar.value;
			//synchronize volume bars
			volumeBar1.value = volumeBar.value;
			volumeBar2.value = volumeBar.value;

			if(audioSrc.volume==0)
				isMuted = true;
			else
				isMuted = false;

	}

	void SetInitialVolume()
	{
		volumeBar1.value = initialVolume;
		volumeBar2.value = initialVolume;
		audioSrc = GetComponent<AudioSource>();
		audioSrc.volume = initialVolume;
	}

	public void MuteSound(bool muted)
	{
			if(muted)
				audioSrc.mute = true;
			else
				audioSrc.mute = false;

			isMuted = muted;
	}

	public void GoToPreviousNextVideo(bool next)
	{
			if(isShuffle)
			{
					fileIndex = Random.Range(0,LVideoPlayer.Instance.fileNames.Length);
					while(fileIndex == previousFileIndex)
					{
						fileIndex = Random.Range(0,LVideoPlayer.Instance.fileNames.Length);
					}
			}
			else
			{
				if(!next && fileIndex!=0)
					fileIndex--;
				else if(next && fileIndex < LVideoPlayer.Instance.fileNames.Length-1 )
					fileIndex++;
			}


			previousFileIndex = fileIndex;

			isFirstPlayed = true;
			videoChanged = true;
			isPaused = false;
			PlayPauseVideo();
	}

	public void GetBackToVideoPlayer()
	{
			moviePlayerCanvas.SetActive(true);
			isScreeMenuBarActive = false;
			isPaused = false;
			HideScreenMenuBar();
	}

	public void GetBackToMainMenu()
	{
			//CEO.Instance.SetNewParent(this.gameObject);
			CEO.Instance.SetLoadingLevel(0);
	}

	public void ShufflePlaylist()
	{
			isShuffle = !isShuffle;
	}


	//-----------------------SHOW/HIDE PANELS ETC METHODS-------------------------
	public void SetVideoInfoText(string key)
	{
			videoTitle = LVideoPlayer.Instance.fileNames[fileIndex];

			titleText.text = THC.GetTitle("videoInfo" + key);
			durationText.text = THC.GetDuration("videoInfo" + key);
			languageText.text = THC.GetLanguage("videoInfo" + key);
			descriptionText.text = "                            " +THC.GetDescription("videoInfo" + key);
	}

	void ShowScreenMenuBar()
	{
		CrossFadeAlpha(screenMenuBar,1f,0.5f);
	}

	void HideScreenMenuBar()
	{
		CrossFadeAlpha(screenMenuBar,0f,0.5f);
	}

	//-------------------------------MISCELLANEOUS METHODS------------------------
	void CrossFadeAlpha (GameObject gObject, float target, float animationTime)
	{
		bool justSetActive = false;
		if (target == 0f)
		{
			StartCoroutine(DeactivateDelayed(gObject, animationTime));
		}
		else
		{
			gObject.SetActive(true);
			justSetActive = true;
		}

		foreach (Graphic graphic in gObject.GetComponentsInChildren<Graphic>())
		{
			if (justSetActive)
			{
				graphic.canvasRenderer.SetAlpha(0.0f);
			}
			graphic.CrossFadeAlpha(target, animationTime, false);
		}
	}

	IEnumerator DeactivateDelayed(GameObject go, float timeSeconds)
	{
		yield return new WaitForSeconds(timeSeconds);
		go.SetActive(false);
	}
}
