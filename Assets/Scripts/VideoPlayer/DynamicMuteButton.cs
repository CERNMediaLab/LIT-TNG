﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DynamicMuteButton : MonoBehaviour {

	private bool isMuted = false;
	private Image buttonIdleImage,buttonPressedImage;
	private SpriteState tempState;
	private Button muteButton;

	public MoviePlayerManager mpMgr;
	public Sprite notMuteIdle,notMutePressed,muteIdle,mutePressed;


	// Use this for initialization
	void Start () {

		muteButton = GetComponent<Button>();
		muteButton.onClick.AddListener(ChangeMuteButtonState);

		buttonIdleImage = this.GetComponent<Image>();

		tempState = new SpriteState();
		tempState = muteButton.spriteState;		
	}

	void Update()
	{
		if(isMuted!=mpMgr.isMuted)
		{
			ChangeMuteButtonState();
			isMuted = mpMgr.isMuted;
		}

	}

	void ChangeMuteButtonState()
	{
			if(!isMuted)
			{
					buttonIdleImage.sprite = muteIdle;
					tempState.pressedSprite = mutePressed;
					muteButton.spriteState = tempState;

					mpMgr.MuteSound(true);
					isMuted = true;
			}
			else
			{
					buttonIdleImage.sprite = notMuteIdle;
					tempState.pressedSprite = notMutePressed;
					muteButton.spriteState = tempState;

					mpMgr.MuteSound(false);
					isMuted = false;
			}
	}
}
