using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DynamicPlayPauseButton : MonoBehaviour {


	public Sprite playIdle,playPressed,pauseIdle,pausePressed;
	public MoviePlayerManager mpMgr;
	private Image buttonIdleImage,buttonPressedImage;
	private Button playPauseButton;
	private SpriteState tempState;

	// Use this for initialization
	void Start () {

		playPauseButton = GetComponent<Button>();
		playPauseButton.onClick.AddListener(PlayPauseVideo);

		buttonIdleImage = this.GetComponent<Image>();
		buttonIdleImage.sprite = pauseIdle;

		tempState = new SpriteState();
		tempState = playPauseButton.spriteState;

		tempState.pressedSprite = pausePressed;
		playPauseButton.spriteState = tempState;

	}

	void PlayPauseVideo()
	{
		mpMgr.isFirstPlayed = false;
		mpMgr.PlayPauseVideo();
		//ChangePlayPauseButtonState();
	}


	void ChangePlayPauseButtonState()
	{

		//mpMgr.PlayPauseVideo();

		if(!mpMgr.isPaused)
		{
				buttonIdleImage.sprite = pauseIdle;
				tempState.pressedSprite = pausePressed;
				playPauseButton.spriteState = tempState;
		}
		else
		{
				buttonIdleImage.sprite = playIdle;
				tempState.pressedSprite = playPressed;
				playPauseButton.spriteState = tempState;

		}

	}

	// Update is called once per frame
	void Update () {

		if(mpMgr.videoChanged)
		{
			mpMgr.videoChanged=false;
			ChangePlayPauseButtonState();
		}
		else
			ChangePlayPauseButtonState();

	}
}
