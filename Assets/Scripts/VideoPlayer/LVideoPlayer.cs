﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.IO;

public class LVideoPlayer : MonoBehaviour
{

	protected static LVideoPlayer instance = null;
	private VideoPlayer videoPlayer;
	private VideoSource videoSource;
	private AudioSource audioSource;

	public string[] fileNames = null;
	private FileInfo[] fInfo = null;
	private string basePath = "d:/Videos/";

	public RenderTexture previewMonitor;


	public static LVideoPlayer Instance
	{
		get
		{
			return instance;
		}
	}


	void Start()
	{
		instance = this;
		videoPlayer = gameObject.AddComponent<VideoPlayer>();
		audioSource = gameObject.AddComponent<AudioSource>();

		videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraFarPlane;
		videoPlayer.targetCamera = Camera.main;
		videoPlayer.aspectRatio = VideoAspectRatio.FitInside;

		videoPlayer.playOnAwake = false;
		audioSource.playOnAwake = false;
		//GetListOfVideos();
	}

	public void SetBasePath(string path)
	{
		basePath = path;
	}


	public string[] GetListOfVideos()
	{
		if (fInfo == null)
		{
			DirectoryInfo dir = new DirectoryInfo(basePath);
			fInfo = dir.GetFiles("*.mp4");
		}

		fileNames = new string[fInfo.Length];
		int fileIndex = 0;
		foreach (FileInfo f in fInfo)
		{
			fileNames[fileIndex] = Path.GetFileNameWithoutExtension(f.Name);
			fileIndex++;
		}

		return fileNames;
	}

	public void PlayVideo(int index)
	{

		videoPlayer.source = VideoSource.Url;
		string localPath = fInfo[index].FullName;

		videoPlayer.url = localPath;


		//Set Audio Output to AudioSource
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

		//Assign the Audio from Video to AudioSource to be played
		videoPlayer.EnableAudioTrack(0, true);
		videoPlayer.SetTargetAudioSource(0, audioSource);
		videoPlayer.aspectRatio = VideoAspectRatio.FitInside;

		videoPlayer.Prepare();


		videoPlayer.Play();
		audioSource.Play();

	}

	public void ResumeVideo()
	{
		videoPlayer.Play();
	}

	public void StopVideo()
	{
		videoPlayer.Stop();
	}

	public void PlayAudio(int index)
	{

		Debug.Log(fInfo[index].FullName);
		string localPath = fInfo[index].FullName;

		WWW www = new WWW(localPath);
		audioSource.clip = www.GetAudioClip(false, true,AudioType.MPEG);

		audioSource.Play();
	}

	public void PauseVideo(int index)
	{
			videoPlayer.Pause();
	}
}
