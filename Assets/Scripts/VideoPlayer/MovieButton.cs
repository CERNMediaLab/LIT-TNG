using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovieButton : MonoBehaviour
{

	public int index = 0;

	void Start ()
	{
		Button btn = gameObject.GetComponent<Button>();
		btn.onClick.AddListener(SelectVideoFile);
		//default select of video file
		if(index==0)
			SelectVideoFile();
	}

	public void SetText(string txt, int newIndex)
	{
		gameObject.GetComponentInChildren<Text>().text = txt;
		index = newIndex;
	}

	void SelectVideoFile()
	{
			//disable previous selected button
			if(MoviePlayerManager.selectedButton!=null)
				MoviePlayerManager.selectedButton.GetComponent<Image>().enabled = false;
			//enable current selected button
			MoviePlayerManager.fileIndex = index;
			MoviePlayerManager.selectedButton = this;
			this.GetComponent<Image>().enabled = true;

			MoviePlayerManager.Instance.SetVideoInfoText((index+1).ToString());
	}



	public void ClickAction ()
	{
		LVideoPlayer.Instance.PlayVideo(index);

	}
}
