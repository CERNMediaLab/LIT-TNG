﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class MovieGrid : MonoBehaviour
{
	public GameObject movieButton;
	private bool didInitializeMenu = false;	

	void Start ()
	{
		//KinectChassis.Instance.SetActive(false);
	}

	private void CreateListOfVideos()
	{
		int i = 0;
		foreach (string f in LVideoPlayer.Instance.GetListOfVideos())
		{
			CreateMovieButton(f, i);
			i++;
		}
	}

	private void CreateMovieButton(string bName, int index)
	{
		GameObject createdbutton = (GameObject)Instantiate(movieButton);
		createdbutton.transform.SetParent (this.gameObject.transform);
		MovieButton mb = createdbutton.GetComponent("MovieButton") as MovieButton;
		mb.SetText(bName, index);
	}

	void Update()
	{
		if (!didInitializeMenu)
		{
			if (LVideoPlayer.Instance != null)
			{
				CreateListOfVideos();
				didInitializeMenu = true;
			}
		}
	}

}
