﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowHideCanvas : MonoBehaviour
{
	public GameObject canvasObject;
	private bool isVisible = false;

	void Start ()
	{
		SetAlpha(0f);
		//Toggle();
	}

	void CrossFadeAlpha (float target, float animationTime)
	{
		foreach (Graphic graphic in canvasObject.GetComponentsInChildren<Graphic>())
		{
			graphic.CrossFadeAlpha(target, animationTime, false);
		}
	}

	void CrossFadeAlpha (GameObject gObject, float target, float animationTime)
	{
		bool justSetActive = false;
		if (target == 0f)
		{
			StartCoroutine(DeactivateDelayed(gObject, animationTime));
		}
		else
		{
			gObject.SetActive(true);
			justSetActive = true;
		}

		foreach (Graphic graphic in gObject.GetComponentsInChildren<Graphic>())
		{
			if (justSetActive)
			{
				graphic.canvasRenderer.SetAlpha(0.0f);
			}
			graphic.CrossFadeAlpha(target, animationTime, false);
		}
	}

	IEnumerator DeactivateDelayed(GameObject go, float timeSeconds)
	{
		yield return new WaitForSeconds(timeSeconds);
		go.SetActive(false);
	}

	void SetAlpha(float target)
	{
		foreach (Graphic graphic in canvasObject.GetComponentsInChildren<Graphic>())
		{
			graphic.canvasRenderer.SetAlpha(target);
		}
	}

	void Toggle()
	{
		float target = 1f;
		if (isVisible)
		{
			target = 0f;
		}
		isVisible = !isVisible;
		CrossFadeAlpha (canvasObject, target, 0.5f);
	}


	void Update ()
	{
		if (Input.GetKeyDown("escape"))
		{
			Toggle();
		}

	}
}
