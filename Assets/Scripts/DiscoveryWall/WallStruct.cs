﻿using System;

[Serializable]
public class WallStruct
{
    public string wallName;
    public string wallImagePath;
    public float[] timelineOriginPositionXY;
    public float[] timelineTargetPositionXY;
    public float[] wallBackgroundPositionXY;
    public string introPath;
    public string[] timelinePaths;
}
