﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text textToChange;
    public Image imageToChange;
    private float timer = 0;
    [HideInInspector]
    public float localDelay;
    private bool update;

    public void Start()
    {
        update = true;
        textToChange.enabled = false;
        imageToChange.enabled = false;
    }

    public void SetTimerValue(float delay)
    {
        localDelay = delay;
        update = true;
        timer = delay;
        textToChange.enabled = true;
        imageToChange.enabled = true;
    }

    public void StopThisTimer()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (update == true)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
                textToChange.text = ((int)timer).ToString();
                imageToChange.fillAmount = -((timer - localDelay) / localDelay);
            }
            else if (timer <= 0)
            {
                update = false;
                timer = 0;
                textToChange.enabled = false;
                imageToChange.enabled = false;
            }
        }
    }
}
