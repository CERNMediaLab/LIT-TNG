﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{

    private Animator spAnimator;

    private void Start()
    {
        spAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (spAnimator.GetCurrentAnimatorStateInfo(0).IsName("SpinnerAnimation") == false)
            {
                TurnSpinnerOn();
            }
            else
            {
                TurnSpinnerOff();
            }
        }

    }


    void TurnSpinnerOn()
    {
        spAnimator.Play("SpinnerAnimation");
    }

    void TurnSpinnerOff()
    {
        spAnimator.Play("Idle");
    }
}
