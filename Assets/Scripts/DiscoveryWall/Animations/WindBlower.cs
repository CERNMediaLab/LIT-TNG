﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindBlower : MonoBehaviour
{

    private bool isAnimationPlaying;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            TurnLeft();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            TurnRight();
        }
    }

    public void TurnRight()
    {
        if(!isAnimationPlaying)
        StartCoroutine(RotateOnce(false, 1));
    }

    public void TurnLeft()
    {
        if(!isAnimationPlaying)
        StartCoroutine(RotateOnce(true, 1));
    }

    public IEnumerator RotateOnce(bool isDirectionLeft, float speed)
    {
        isAnimationPlaying = true;

        float originalRotation = transform.eulerAngles.z;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime * speed)
        {
            if (isDirectionLeft)
            {
                float newRotation = Mathf.SmoothStep(originalRotation, originalRotation + 90, t);
                transform.rotation = Quaternion.Euler(0, 0, newRotation);
            }
            else if (!isDirectionLeft)
            {
                float newRotation = Mathf.SmoothStep(originalRotation, originalRotation - 90, t);
                transform.rotation = Quaternion.Euler(0, 0, newRotation);
            }
            yield return null;
        }

        if (isDirectionLeft)
            transform.rotation = Quaternion.Euler(0, 0, originalRotation + 90);
        else if (!isDirectionLeft)
            transform.rotation = Quaternion.Euler(0, 0, originalRotation - 90);
        isAnimationPlaying = false;
        yield return null;
    }
}
