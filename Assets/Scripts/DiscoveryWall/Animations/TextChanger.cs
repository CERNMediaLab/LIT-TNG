﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextChanger : MonoBehaviour
{
    public Text textToChange;
    [TextArea]
    public string[] textToApply;
    public float interval;           //time you have to wait for the next text;
    private float elapsedTime;
    private int currentText = 0;

    

    void Update()
    {
        if (interval > 0)
        {
            if (elapsedTime > interval)
            {
                if (currentText + 1 < textToApply.Length)
                {
                    currentText += 1;
                    textToChange.text = textToApply[currentText];
                }
                else
                {
                    currentText = 0;
                    textToChange.text = textToApply[currentText];
                }
                elapsedTime = 0;
            }
            elapsedTime += Time.deltaTime;
        }
    }
}
