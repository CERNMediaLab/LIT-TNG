﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.IO;

public class MediaElement : MonoBehaviour
{

    public RawImage rawTexture;
    public RawImage rawFrame;
    public Image frameImage;
    public SciFiText sfText;

    private RenderTexture rTexture;
    private VideoPlayer videoPlayer;
    private VideoSource videoSource;
    private int mediaType = 0;
    private string localPath = "";
    private RectTransform m_RectTransform;
    public RectTransform f_RectTransform;
    private Vector2 size = Vector2.zero;
    private Vector2 nativeSize = Vector2.zero;
    private float delay;
    private float duration;
    private bool isReady = false;
    private bool isStarted = false;
    private bool isDelayDone = false;




    public void SetPosSize(Vector2 _pos, Vector2 _size)
    {
        m_RectTransform = GetComponent<RectTransform>();
        size = _size;
        m_RectTransform.anchoredPosition = _pos;
        m_RectTransform.sizeDelta = Vector2.zero;
        f_RectTransform.sizeDelta = Vector2.zero;
        m_RectTransform.localScale = new Vector3(1,1,1);
        f_RectTransform.localScale = new Vector3(1,1,1);
    }

    public void SetDelayDuration(float del, float dur)
    {
        delay = del;
        duration = dur;

        if (delay > 0f)
        {
            Invoke("DelayDone", delay);

        }
        else
        {
            isDelayDone = true;
        }

    }

    private void DelayDone()
    {
        isDelayDone = true;
    }

    IEnumerator GrowTo(Vector2 newSize, float aTime)
    {
        Vector2 currentSize = m_RectTransform.sizeDelta;

        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            if (videoPlayer != null && videoPlayer.texture != null)
            {
                nativeSize = new Vector2(videoPlayer.texture.width, videoPlayer.texture.height);
                if (nativeSize != Vector2.zero)
                {
                    float ratio = nativeSize.x / nativeSize.y;
                    newSize = new Vector2(size.x, (size.x) / ratio);
                }
            }
            float sizeX = Mathf.SmoothStep(currentSize.x, newSize.x, t);
            float sizeY = Mathf.SmoothStep(currentSize.y, newSize.y, t);
            //m_RectTransform.sizeDelta =  Vector2.Lerp(m_RectTransform.sizeDelta , newSize , t);
            m_RectTransform.sizeDelta = new Vector2(sizeX, sizeY);
            f_RectTransform.sizeDelta = new Vector2(sizeX, sizeY);
            yield return null;
        }
        m_RectTransform.sizeDelta = newSize;
        f_RectTransform.sizeDelta = newSize;
        size = newSize;
    }


    IEnumerator ScaleToZeroAndKill(float aTime, float dTime)
    {
        yield return new WaitForSeconds(dTime);
        Vector3 currentScale = transform.localScale;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            transform.localScale = Vector3.Lerp(currentScale, Vector3.zero, t);
            yield return null;
        }
        transform.localScale = Vector3.zero;
        Invoke("DestroyMedia", 0.2f);
    }

    public void DestroyMedia()
    {
        Destroy(this.gameObject);
    }

    public void Kill(float randomTime)
    {
        float dTime = Random.Range(0f, randomTime);
        StartCoroutine(ScaleToZeroAndKill(0.2f, dTime));

    }

    public void Kill()
    {
        StartCoroutine(ScaleToZeroAndKill(0.2f, 0f));
    }

    private void SetVideo()
    {
        rTexture = new RenderTexture(512, 512, 16, RenderTextureFormat.ARGB32);
        rTexture.Create();
        rawTexture.texture = rTexture;

        videoPlayer = gameObject.AddComponent<VideoPlayer>();
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.RenderTexture;
        videoPlayer.targetTexture = rTexture;
        videoPlayer.aspectRatio = VideoAspectRatio.FitInside;
        videoPlayer.playOnAwake = false;
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = localPath;
        videoPlayer.aspectRatio = VideoAspectRatio.Stretch;
        videoPlayer.Prepare();

    }



    private void SetImage()
    {
        byte[] imageBytes = File.ReadAllBytes(localPath);
        Texture2D tex = new Texture2D(4, 4);
        tex.LoadImage(imageBytes);
        rawTexture.texture = tex;
        //isReady = true;

    }


    private void SetText()
    {
        string newText = File.ReadAllText(localPath);
        sfText.SetSize(size);
        sfText.SetNewText(newText, true, delay + 0.5f);
        isReady = true;

    }

    public void SetMediaPath(string path, int type) // type 1 = image, 2 = video, 3 = text
    {
        localPath = path;
        mediaType = type;
        ExecuteMedia();
    }

    public void ExecuteMedia()
    {
        if (File.Exists(localPath))
        {
            switch (mediaType)
            {
                case 1:
                    SetImage();
                    break;
                case 2:
                    SetVideo();
                    break;
                case 3:
                    SetText();
                    break;
            }
        }
        else
        {
            DisplayBugWarning();
        }
    }

    public void SetFontSize(int fs)
    {
        sfText.SetNewFontSize(fs);
    }

    public void SetFontColor(Color color)
    {
        sfText.SetColor(color);
    }

    public void SetFrame(int frm)
    {
        if (frm == 0)
        {
            frameImage.enabled = false;

            if (mediaType == 3)
            {
                rawTexture.enabled = false;
            }
        }
        else
        {
            frameImage.enabled = true;
        }

    }

    public void PlayVideo()
    {
        videoPlayer.Play();
        //videoPlayer.time = 0f;
        //nativeSize = new Vector2(videoPlayer.clip.width, videoPlayer.clip.height);
    }

    public void ResumeVideo()
    {
        videoPlayer.Play();
    }

    public void StopVideo()
    {
        videoPlayer.Stop();
    }

    public void SetLoop(int lp)
    {
        if (mediaType == 2 && videoPlayer != null)
        {
            if (lp == 0)
                videoPlayer.isLooping = false;
            else
                videoPlayer.isLooping = true;
        }
    }

    public void DisplayBugWarning()
    {
        Debug.Log("something went wrong, check your path");
        DWManager.Instance.bugWarning.SetActive(true);
    }


    void Update()
    {
        if (Input.GetKeyDown("0"))
        {
            Kill(0.5f);
        }

        if (!isReady)
        {
            if (mediaType == 1)
            {

                nativeSize = new Vector2(rawTexture.texture.width, rawTexture.texture.height);
                //Debug.Log(rawTexture.texture.width + " x " + rawTexture.texture.height);
                if (nativeSize != Vector2.zero)
                {
                    float ratio = nativeSize.x / nativeSize.y;
                    size = new Vector2(size.x, (size.x) / ratio);
                }
                isReady = true;
            }

            if (mediaType == 2 && videoPlayer != null && videoPlayer.isPrepared) // added videoPlayer != null in case there is an error on path in the json file
            {
                Texture vidTex = videoPlayer.texture;
                nativeSize = new Vector2(vidTex.width, vidTex.height);
                //Debug.Log(vidTex.width + " x " + vidTex.height);
                if (nativeSize != Vector2.zero)
                {
                    float ratio = nativeSize.x / nativeSize.y;
                    size = new Vector2(size.x, (size.x) / ratio);
                }
                isReady = true;
            }


        }

        if (isReady && isDelayDone && !isStarted)
        {
            if (mediaType == 2)
            {
                PlayVideo();
            }
            StartCoroutine(GrowTo(size, 0.3f));

            isStarted = true;
            if (duration > 0f)
            {
                Invoke("Kill", duration);
            }
        }



    }
}
