﻿using System;

[Serializable]
public class StationStruct
{
	public string stationName;
    public string stationDate;
	public int category;
    public int replay;
	public MediaStruct[] mediaStruct;


}
[Serializable]
public struct MediaStruct
{
	public string path;
	public int type;
	public float posX;
	public float posY;
	public int width;
	public int height;
	public int fontSize;
	public float delay;	
	public float duration;
    public int frame;
    public int loop;
}

	
