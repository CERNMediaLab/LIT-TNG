﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marker : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.GetFloat("floor_marker_localscale") != 0)
        {
            gameObject.transform.position = new Vector3(PlayerPrefs.GetFloat("floor_marker_pos_x"), PlayerPrefs.GetFloat("floor_marker_pos_y"), PlayerPrefs.GetFloat("floor_marker_pos_z"));
            gameObject.transform.localScale = new Vector3(PlayerPrefs.GetFloat("floor_marker_localscale"), PlayerPrefs.GetFloat("floor_marker_localscale"), PlayerPrefs.GetFloat("floor_marker_localscale"));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 0.1f, gameObject.transform.position.z);
        }

        if (!Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.S))
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.1f, gameObject.transform.position.z);
        }

        if (Input.GetKey(KeyCode.A))
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x - 0.1f, gameObject.transform.position.y, gameObject.transform.position.z);
        }

        if (Input.GetKey(KeyCode.D))
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x + 0.1f, gameObject.transform.position.y, gameObject.transform.position.z);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            gameObject.transform.localScale = gameObject.transform.localScale + new Vector3(0.001f, 0.001f, 0.001f);
        }

        if (Input.GetKey(KeyCode.E))
        {
            gameObject.transform.localScale = gameObject.transform.localScale - new Vector3(0.001f, 0.001f, 0.001f);
        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.S))
        {
            PlayerPrefs.SetFloat("floor_marker_pos_x", gameObject.transform.position.x);
            PlayerPrefs.SetFloat("floor_marker_pos_y", gameObject.transform.position.y);
            PlayerPrefs.SetFloat("floor_marker_pos_z", gameObject.transform.position.z);
            PlayerPrefs.SetFloat("floor_marker_localscale", gameObject.transform.localScale.x);

        }

    }
}
