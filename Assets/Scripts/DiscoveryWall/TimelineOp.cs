﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TimelineOp
{
    private TimelineStruct timelineStruct;
    private List<StationOp> stations;
    private string timelinePath;
    public Texture newTimelineImgTexture;
    public Vector2 newTimelineImgSize = Vector2.zero;
    public string timelineName;
    public string timelineTextValue;
    public float[] timelineTextPosition;
    public int timelineTextFontSize;

    public void SetTimelinePath(string newPath)
    {
        timelinePath = newPath;
        ReadTimeline();
    }

    private void ReadTimeline()
    {
        if (File.Exists(timelinePath))
        {
            string timelineJson = File.ReadAllText(timelinePath);
            timelineStruct = JsonUtility.FromJson<TimelineStruct>(timelineJson);
        }
        else
        {
            DWManager.Instance.BugWarning("We had trouble reading the timeline. Please make sure your timelinePath is correct");
        }
    }


    public void SetTimelineName()
    {
        if (timelineStruct.timelineName != null)
            timelineName = timelineStruct.timelineName;
    }

    public void SetTimelineTextValue()
    {
        if (timelineStruct.timelineTextValue != null)
            timelineTextValue = timelineStruct.timelineTextValue;

    }

    public void SetTimelineTextPosition()
    {
        if (timelineStruct.timelineTextPositionXY != null && timelineStruct.timelineTextPositionXY.Length > 0)
        {
            timelineTextPosition = new float[timelineStruct.timelineTextPositionXY.Length];
            for (int i = 0; i < timelineStruct.timelineTextPositionXY.Length; i++)
            {
                if (timelineStruct.timelineTextPositionXY != null)
                {
                    timelineTextPosition[i] = timelineStruct.timelineTextPositionXY[i];
                }
            }
        }
    }

    public void SetTimelineTextFontSize()
    {
        if (timelineStruct.timelineTextFontSize != null)
        {
            timelineTextFontSize = timelineStruct.timelineTextFontSize;
        }
    }

    public void SetTimelineOpImg()                                                                // Maybe replace this by a Media Element
    {
        if (File.Exists(timelineStruct.timelineImgPath))
        {
            byte[] imageBytes = File.ReadAllBytes(timelineStruct.timelineImgPath);
            Texture2D tex = new Texture2D(4, 4);
            tex.LoadImage(imageBytes);
            newTimelineImgSize = new Vector2(tex.width, tex.height);
            newTimelineImgTexture = tex;
            DWManager.Instance.SetTimelineImg();
        }
    }

    public void SetInstructions()
    {
        if (timelineStruct.instructionsText.Length > 0)
        {
            DWManager.Instance.instructionText.textToApply = new string[timelineStruct.instructionsText.Length];
            for (int i = 0; i < timelineStruct.instructionsText.Length; i++)
            {
                DWManager.Instance.instructionText.textToApply[i] = timelineStruct.instructionsText[i];
            }
            DWManager.Instance.instructionText.textToChange.text = timelineStruct.instructionsText[0];
        }
    }

    public void CreateStations()
    {
        DWManager.Instance.numStations = timelineStruct.stationPaths.Length;
        DWManager.Instance.stationDates = new SciFiText[(int)DWManager.Instance.numStations];
        stations = new List<StationOp>();
        DWManager.Instance.stationList = new List<StationOp>();
        foreach (string st in timelineStruct.stationPaths)
        {
            if (File.Exists(st))
            {
                StationOp newStation = new StationOp();
                newStation.SetStationPath(st);
                stations.Add(newStation);
                DWManager.Instance.stationList.Add(newStation);
            }
            else
            {
                DWManager.Instance.BugWarning("Something went wrong when creation stations. make sure your StationPath is correctly set");
            }
        }
        DWManager.Instance.StartTimeline();
    }


}
