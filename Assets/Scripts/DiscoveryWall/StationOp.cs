﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class StationOp
{
    private StationStruct stationStruct;
    private List<MediaElement> elements;
    private string stationPath;
    private float replayTimer;


    public void SetStationPath(string newPath)
    {
        stationPath = newPath;
        ReadStation();
    }

    public void ReloadStation()
    {
        DestroyMediaElements();
        ReadStation();
        CreateMediaElements();
        Resources.UnloadUnusedAssets();
    }

    private void ReadStation()
    {
        if (File.Exists(stationPath))
        {
            string stationJson = File.ReadAllText(stationPath);
            stationStruct = JsonUtility.FromJson<StationStruct>(stationJson);
        }
    }

    public void SetStationTitle()
    {
        DWManager.Instance.SetStationTitle(stationStruct.stationName);
        DWManager.Instance.PlayCategoryAnimation(stationStruct.category);
    }

    public string GetStationDates()
    {
        return stationStruct.stationDate;
    }

    public int GetReplayValue()
    {
        return stationStruct.replay;
    }

    public void CreateMediaElements() //enter station
    {
        
            elements = new List<MediaElement>();
        if (elements.Count < stationStruct.mediaStruct.Length)
        {
            foreach (MediaStruct ms in stationStruct.mediaStruct)
            {
                MediaElement me = DWManager.Instance.CreateMediaElement(ms.path, ms.type, new Vector2(ms.posX, ms.posY), new Vector2(ms.width, ms.height), ms.fontSize, Color.white, ms.delay, ms.duration, ms.frame, ms.loop);
                elements.Add(me);
            }
            Debug.Log("created " + elements.Count + " elements");
        }
    }

    public void WaitForReplay()
    {
        if (GetReplayValue() > 0)
        {
            if (replayTimer > GetReplayValue())
            {
                replayTimer = 0;
                ReloadStation();
            }
            replayTimer += Time.deltaTime;
        }
    }

    public void DestroyMediaElements() //leave station
    {
        replayTimer = 0;
        if (elements != null)
        {
            foreach (MediaElement me in elements)
            {
                if (me != null) me.Kill(0.5f);
            }
            Debug.Log("destroyed " + elements.Count + " elements");
            elements.Clear();
            Resources.UnloadUnusedAssets();
        }
    }

}
