﻿using System;

[Serializable]
public class TimelineStruct
{
    public string timelineTextValue;
    public float[] timelineTextPositionXY;
    public int timelineTextFontSize;
    public string timelineImgPath;
	public string timelineName;
    public string[] instructionsText;
    public string[] stationPaths;
}
