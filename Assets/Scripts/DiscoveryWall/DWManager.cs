﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.EventSystems;

public class DWManager : MonoBehaviour
{

    public Text mainTitleText;
    [Header("Wall Data")]
    private string wallPath = "C:/DW/wall.json";
    private WallStruct wall;

    [Header("Wall Generator")]
    public Transform timelineOrigin;
    public Transform timelineTarget;
    private Transform[] timelineRefs;
    [Space(10)]
    private Button[] timelineButtons;
    public GameObject timelinePrefab;
    public Transform timelineHolder;
    public RawImage timelineBackground;


    private int numTimelines;
    private Vector3[] timelinePos;


    [Header("Timeline Data")]
    private List<TimelineOp> timelineList;
    private int chosenTimeline;
    private bool canCreateStations = true;

    private Timer[] timers;

    [Header("Station Data")]
    public SciFiText stationTitle;
    public SciFiText[] stationDates;
    public List<StationOp> stationList;

    [Header("Station Generator")]
    public Transform stationOrigin;
    public Transform stationTarget;

    public GameObject stationPrefab;
    public Transform stationHolder;

    [HideInInspector]
    public int? numStations = null;
    private Vector3[] stationsPos;

    [Header("Station Navigation")]
    public Transform stationMarker;
    public GameObject WindBlowerRef;
    private WindBlower WndBlwrRefScript;
    private Transform[] stationRefs;

    private bool isMovingToStation = false;
    private bool isCategoryAnimationPlaying;

    private float trainTravelSpeed = 1f;
    private int currentStation;

    [Header("Media Element Variables")]
    public GameObject mediaElement;
    public GameObject parentUI;

    [Header("Kinect/Player detection")]
    private Vector3 position0 = Vector3.zero;
    private float zoneRadius = 26f;


    private bool isSide = false;

    protected static DWManager instance = null;

    public static DWManager Instance
    {
        get
        {
            return instance;
        }
    }

    [Header("Category Selection")]
    public Animator CategoryController;
    private int chosenNumber;

    private int currentCategory;

    [Header("Inactivity")]
    private float inactiveSince;
    private float maxInactivity = 10;
    private bool activityDetected;
    private bool isInactivityEnabled = true;

    [Header("Instructions")]
    private float instructionsTimer;
    private bool canPlayInstructions;
    public TextChanger instructionText;

    [Header("Dev_0nly")]
    public Image grid;
    public GameObject bugWarning;

    private StationOp introStation;
    private bool isIntro = false;
    private bool isInZone = false;
    private bool previousIsInZone = false;
    private bool isSelectingTimeline = false;
    private int currentTimelineButton = -1;
    private Coroutine co = null;

    private int stationToLoad = -1;
    private bool canLoadStation = true;



    void Start()
    {
        //timelineHolder.gameObject.SetActive(true);
        grid.enabled = false;
        instance = this;
        isMovingToStation = false;
        stationTitle.SetNewFontSize(77);
        WndBlwrRefScript = WindBlowerRef.GetComponent<WindBlower>();
        //Cursor.visible = false;
        StartKinect();

        ReadWall();
        StartWall();

        Cursor.visible = false;
    }

    private void StartKinect()
    {
        if (CEO.Instance != null)
        {
            if (!CEO.Instance.GetIsSide())
            {
                KinectChassis.Instance.SetActive(false);
                KinectChassis.Instance.ShowFeetMarkers(false);
                KinectChassis.Instance.ShowHandMarkers(true);
                KinectChassis.Instance.SetStreamSelection(new int[] { 0, 0, 0, 0, 0 });
                isSide = false;
                //Debug.Log("isSide = " + isSide);
            }
            else
            {
                KinectChassis.Instance.SetActive(true);
                KinectChassis.Instance.ShowFeetMarkers(false);
                KinectChassis.Instance.ShowHandMarkers(true);
                KinectChassis.Instance.SetDetectClap(true);
                KinectChassis.Instance.dataManager.RequestJoints();

                KinectChassis.Instance.SetFeetVisibility(false);
                KinectChassis.Instance.SetStreamSelection(new int[] { 0, 0, 0, 0, 1 });
                HandPairManager.Instance.SetIsVertical(true);
                KinectChassis.Instance.SetDetectSwipe(true);
                isSide = true;
                //Debug.Log("isSide = " + isSide);

            }
        }
        else
        {
            Debug.Log("No CEO.Instance! " + isSide);
        }

        if (LPlayer.Instance != null)
        {
            if (CEO.Instance.GetIsFloor())
            {
                LPlayer.Instance.CmdSetFloorLevel(5);
            }
            else
            {
                LPlayer.Instance.levelSide = 5;
            }
        }

    }

    private void LoadMediaElements()
    {
        if (stationToLoad != -1 && canLoadStation)
        {
            EnableStation(stationToLoad);
            stationToLoad = -1;
            canLoadStation = false;

        }
    }

    private void Update()
    {
        Hotkeys();
        DetectionActions();
        InactivityFunction();
        LoadMediaElements();
        RunStationReplayTimer();
        PlayInstructions();

    }

    private void GetValuesFromSettings()
    {
        HandPairManager.Instance.SetZeroPointHands(new Vector3(SaveLoad.savedSettings.zeroPointHandsX, SaveLoad.savedSettings.zeroPointHandsY, SaveLoad.savedSettings.zeroPointHandsZ));


        position0.x = SaveLoad.savedSettings.pos0X;
        position0.y = SaveLoad.savedSettings.pos0Y;
        position0.z = SaveLoad.savedSettings.pos0Z;
    }

    public void SetUserPosition0(Vector3 pos0)
    {
        position0 = pos0;
        SaveLoad.savedSettings.pos0X = position0.x;
        SaveLoad.savedSettings.pos0Y = position0.y;
        SaveLoad.savedSettings.pos0Z = position0.z;
    }



    private void DetectionActions()
    {
        if (HandPairManager.Instance != null)
        {
            int sw = HandPairManager.Instance.GetSwipeAtPosition(position0, zoneRadius);
            isSelectingTimeline = timelineHolder.gameObject.activeSelf;



            switch (sw)
            {
                case -10:
                    isInZone = false;
                    break;
                case 0:
                    isInZone = true;
                    if (numTimelines == 1 && isIntro)
                    {
                        HighlightButton(0);
                        SelectButton(0);
                    }
                    ResetInactivity();
                    break;
                case -1:
                    isInZone = true;
                    ResetInactivity();
                    if (isSelectingTimeline && numTimelines > 1)
                    {
                        int previousTB = currentTimelineButton;
                        currentTimelineButton--;
                        currentTimelineButton = Mathf.Clamp(currentTimelineButton, 0, timelineButtons.Length - 1);

                        HighlightButton(currentTimelineButton);
                        try
                        {
                            StopCoroutine(co);
                            timers[previousTB].StopThisTimer();
                        }
                        catch { }
                        co = StartCoroutine(SelectLanguageDelayed(currentTimelineButton, 3f));
                    }
                    else if (!isIntro && !isMovingToStation && !isCategoryAnimationPlaying)
                    {
                        PreviousStation();
                    }
                    break;
                case 1:
                    isInZone = true;
                    ResetInactivity();

                    if (isSelectingTimeline)
                    {
                        int previousTB = currentTimelineButton;
                        currentTimelineButton++;
                        currentTimelineButton = Mathf.Clamp(currentTimelineButton, 0, timelineButtons.Length - 1);

                        HighlightButton(currentTimelineButton);
                        try
                        {
                            StopCoroutine(co);
                            timers[previousTB].StopThisTimer();
                        }
                        catch { }
                        co = StartCoroutine(SelectLanguageDelayed(currentTimelineButton, 3f));

                    }
                    else
                    {
                        if (!isIntro && !isMovingToStation && !isCategoryAnimationPlaying)
                            NextStation();
                    }
                    break;
            }

            if (isInZone && !previousIsInZone && isIntro)
            {
                timelineHolder.gameObject.SetActive(true);
                HighlightButton(-1);
            }

            if (!isInZone && previousIsInZone)
            {
                HighlightButton(-1);
                currentTimelineButton = -1;
                try
                {
                    StopCoroutine(co);
                }
                catch { }
                timelineHolder.gameObject.SetActive(false);
            }

            previousIsInZone = isInZone;
        }

    }

    //-------------------------------------------------------\\
    //********** Generate Wall (Timeline Selection) **********\\
    //---------------------------------------------------------\\



    private void ReadWall()
    {
        Debug.Log("ReadingWall");
        if (File.Exists(wallPath))
        {
            string wallJson = File.ReadAllText(wallPath);
            wall = JsonUtility.FromJson<WallStruct>(wallJson);
            numTimelines = wall.timelinePaths.Length;
            mainTitleText.text = wall.wallName;

            CreateIntro();
            CreateTimelines();

        }
        else
        {
            BugWarning("wall.json was not found at C:/DW");
        }
    }

    public void CreateIntro()
    {

        string pathIntro = wall.introPath;
        Debug.Log("started intro");

        if (File.Exists(pathIntro))
        {
            introStation = new StationOp();
            introStation.SetStationPath(pathIntro);
        }
        else
        {
            DWManager.Instance.BugWarning("Can't load Intro");
        }
        StartIntro();

    }

    public void StartIntro()
    {
        ResetTimeline();
        introStation.ReloadStation();
        introStation.SetStationTitle();
        isIntro = true;
        currentStation = 0;
    }

    public void KillIntro()
    {
        isIntro = false;
        introStation.DestroyMediaElements();
    }

    private void CreateTimelines()
    {
        timelineList = new List<TimelineOp>();
        foreach (string st in wall.timelinePaths)
        {
            if (File.Exists(st))
            {
                TimelineOp newTimeline = new TimelineOp();
                newTimeline.SetTimelinePath(st);
                timelineList.Add(newTimeline);
            }
            else
            {
                BugWarning("One or more timelines was not loaded. Please make sure the path is correct, or delete-it");
            }
        }
    }

    void StartWall()
    {
        timelineRefs = new Transform[numTimelines];
        timelineButtons = new Button[numTimelines];
        timelineRefs[0] = stationOrigin;
        GenerateWall();
    }

    void GenerateWall()
    {
        timelineOrigin.localPosition = new Vector2(wall.timelineOriginPositionXY[0], wall.timelineOriginPositionXY[1]);
        timelineTarget.localPosition = new Vector2(wall.timelineTargetPositionXY[0], wall.timelineTargetPositionXY[1]);


        timelinePos = new Vector3[numTimelines];
        timelinePos[0] = timelineOrigin.position;
        timelinePos[numTimelines - 1] = timelineTarget.position;
        timers = new Timer[numTimelines];

        int tmlns = numTimelines - 1;

        Vector3 separation;
        if (tmlns > 0)
            separation = ((timelineTarget.localPosition - timelineOrigin.localPosition) / tmlns);
        else
            separation = (timelineTarget.localPosition - timelineOrigin.localPosition);

        for (int t = 0; t < numTimelines; t++)
        {
            Vector3 vectorPosition = (separation * t) + timelineOrigin.localPosition;
            timelinePos[t] = vectorPosition;
            GameObject newTimeline = Instantiate(timelinePrefab, vectorPosition, Quaternion.identity, timelineHolder) as GameObject;
            newTimeline.name = "Timeline" + t.ToString();
            newTimeline.transform.localPosition = vectorPosition;

            RectTransform nt_RectTransform = newTimeline.GetComponent<RectTransform>();
            nt_RectTransform.anchoredPosition = vectorPosition;


            timelineRefs[t] = newTimeline.transform;
            timelineButtons[t] = newTimeline.GetComponent<Button>();
            timelineButtons[t].onClick.AddListener(SetChosenTimeline);
            newTimeline.GetComponent<LanguageButtonStruct>().timelineNo = t;
            timers[t] = newTimeline.GetComponent<Timer>();

        }

        SetWallImg();

        for (int i = 0; i < timelineList.Count; i++)
        {
            timelineList[i].SetTimelineOpImg();
            timelineList[i].SetTimelineTextFontSize();
            timelineList[i].SetTimelineTextPosition();
            timelineList[i].SetTimelineTextValue();
            timelineList[i].SetTimelineName();
        }
        SetTimelineTxt();
        timelineTarget.transform.SetAsLastSibling();
    }

    public void SetWallImg()
    {
        if (File.Exists(wall.wallImagePath))
        {
            byte[] imageBytes = File.ReadAllBytes(wall.wallImagePath);
            Texture2D tex = new Texture2D(4, 4);
            tex.LoadImage(imageBytes);
            timelineBackground.texture = tex;
            timelineBackground.transform.localPosition = new Vector2(wall.wallBackgroundPositionXY[0], wall.wallBackgroundPositionXY[1]);
            timelineBackground.enabled = true;
        }
    }


    public void SetTimelineImg()
    {
        for (int i = 0; i < timelineList.Count; i++)
        {
            RectTransform m_RectTransform = timelineRefs[i].gameObject.GetComponent<RectTransform>();
            RawImage newRawImage = timelineRefs[i].gameObject.GetComponent<RawImage>();
            m_RectTransform.sizeDelta = new Vector2(timelineList[i].newTimelineImgSize.x, timelineList[i].newTimelineImgSize.y);
            timelineRefs[i].gameObject.GetComponent<RawImage>().texture = timelineList[i].newTimelineImgTexture;
        }
    }

    public void SetTimelineTxt()
    {
        for (int i = 0; i < timelineList.Count; i++)
        {
            if (timelineList[i].timelineTextPosition != null)
            {
                Text newText = timelineRefs[i].transform.GetChild(0).GetComponent<Text>();
                newText.text = timelineList[i].timelineTextValue;
                newText.rectTransform.sizeDelta = new Vector2((timelineList[i].newTimelineImgSize.x) * 0.5f, (timelineList[i].newTimelineImgSize.y) * 0.5f);
                newText.transform.localPosition = new Vector2(timelineList[i].timelineTextPosition[0], timelineList[i].timelineTextPosition[1]);
                newText.fontSize = timelineList[i].timelineTextFontSize;
            }
        }
    }

    public void HighlightButton(int t)
    {
        EventSystem.current.SetSelectedGameObject(null);
        try
        {
            timelineButtons[t].Select();
        }
        catch { }
    }

    public void SelectButton(int t)
    {
        try
        {
            timelineButtons[t].onClick.Invoke();
        }
        catch { }
    }


    //-----------------------------------------------------------\\
    //********** Load & Run Timeline (Station Creation) **********\\
    //-------------------------------------------------------------\\



    private void ResetTimeline()
    {
        if (numStations != null && currentStation >= 0 && stationRefs.Length > 0)
        {
            canCreateStations = true;
            stationMarker.position = stationOrigin.position;

            stationList[currentStation].DestroyMediaElements();
            currentStation = -1;
            foreach (Transform stn in stationRefs)
            {
                Destroy(stn.gameObject);
            }
            Debug.Log("Destroyed " + stationRefs.Length + " stations");

        }
    }

    public void SetChosenTimeline()
    {
        SetActivityDetected();
        ResetTimeline();
        if (canCreateStations)
        {
            chosenTimeline = EventSystem.current.currentSelectedGameObject.GetComponent<LanguageButtonStruct>().timelineNo;
            timelineList[chosenTimeline].CreateStations();
            mainTitleText.text = timelineList[chosenTimeline].timelineName;
            timelineHolder.gameObject.SetActive(false);
            canCreateStations = false;
        }
    }

    public void SetTimelineTimer(int currentTimeline, float delay)
    {
        timers[currentTimeline].SetTimerValue(delay);
    }

    public void StartTimeline()
    {
        KillIntro();
        timelineList[chosenTimeline].SetInstructions();
        stationRefs = new Transform[(int)numStations];
        //stationRefs[0] = stationOrigin;
        GenerateTimeline();
        currentStation = -1;
        stationMarker.transform.position = stationOrigin.position;

        if (numTimelines == 1)
        {
            canPlayInstructions = true;
        }
    }

    public void GenerateTimeline()
    {
        stationsPos = new Vector3[(int)numStations];
        stationsPos[0] = stationOrigin.position;
        stationsPos[(int)numStations - 1] = stationTarget.position;

        int stns = (int)numStations - 1;
        Vector3 separation = ((stationTarget.position - stationOrigin.position) / stns);


        for (int s = 0; s < numStations; s++)
        {
            Vector3 vectorPosition = (separation * s) + stationOrigin.position;
            stationsPos[s] = vectorPosition;

            GameObject newStation = Instantiate(stationPrefab, vectorPosition, Quaternion.identity, stationHolder) as GameObject;
            newStation.name = "Station" + s.ToString();
            newStation.transform.position = vectorPosition;
            stationRefs[s] = newStation.transform;
        }
        stationTarget.transform.SetAsLastSibling();
        stationMarker.transform.SetAsLastSibling();

        SetStationDates();

        Invoke("NextStation", 0.5f);
    }

    public void SetStationTitle(string newText)
    {
        stationTitle.SetNewText(newText, true, 0.5f);
    }

    public void SetStationDates()
    {
        for (int i = 0; i < stationList.Count; i++)
        {
            stationDates[i] = stationRefs[i].gameObject.transform.GetChild(1).GetComponent<SciFiText>();
            stationDates[i].SetNewFontSize(50);
            stationDates[i].SetNewText(stationList[i].GetStationDates(), true, 0.5f);
        }
    }



    //---------------------------------------\\
    //********** Station Navigation **********\\
    //-----------------------------------------\\



    public void TravelToChosenStation(int chosenStation)
    {
        if (!isMovingToStation)
        {
            if (currentStation >= 0)
            {
                stationList[currentStation].DestroyMediaElements();
            }

            if (chosenStation != currentStation)
            {
                canLoadStation = false;
                stationList[chosenStation].SetStationTitle();
                StartCoroutine(MoveToChosenStation(chosenStation));
                stationToLoad = chosenStation;
            }
        }
    }

    public void EnableStation(int stNumber)
    {
        stationList[currentStation].CreateMediaElements();
        isCategoryAnimationPlaying = true;
        StartCoroutine(CheckIfCategoryAnimationEnded());
    }

    public void PlayInstructions()
    {
        if (canPlayInstructions)
        {
            timelineHolder.gameObject.SetActive(true);
            foreach (Transform tr in timelineRefs)
            {
                tr.gameObject.SetActive(false);
            }

            if (instructionsTimer > 12f)
            {
                foreach (Transform tr in timelineRefs)
                {
                    tr.gameObject.SetActive(true);
                }
                timelineHolder.gameObject.SetActive(false);
                canPlayInstructions = false;
                instructionsTimer = 0;
            }

            instructionsTimer += Time.deltaTime;
        }

    }

    IEnumerator MoveToChosenStation(int chosenStation)
    {
        if (bugWarning.activeSelf == true && chosenStation > 0)
            bugWarning.SetActive(false);

        isMovingToStation = true;

        float originalPosition = stationMarker.transform.position.x;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime * trainTravelSpeed)
        {
            if (stationRefs != null)
            {
                float newPosition = Mathf.Lerp(originalPosition, stationRefs[chosenStation].transform.position.x, t);
                stationMarker.transform.position = new Vector3(newPosition, stationMarker.transform.position.y, stationMarker.transform.position.z);
            }
            else
                break;
            yield return null;
        }
        stationMarker.transform.position = new Vector3(stationRefs[chosenStation].transform.position.x, stationMarker.transform.position.y, stationMarker.transform.position.z);

        currentStation = chosenStation;
        isMovingToStation = false;
        canLoadStation = true;

        yield return null;
    }



    void NextStation()
    {
        if (currentStation + 1 < stationRefs.Length)
        {
            TravelToChosenStation(currentStation + 1);
            WndBlwrRefScript.TurnRight();
        }
        SetActivityDetected();
    }

    void PreviousStation()
    {
        if (currentStation - 1 > -1)
        {
            TravelToChosenStation(currentStation - 1);
            WndBlwrRefScript.TurnLeft();
        }
        SetActivityDetected();
    }

    void ReloadCurrentStation()
    {
        stationList[currentStation].ReloadStation();
        if (bugWarning.activeSelf == true)
            bugWarning.SetActive(false);
    }

    void RunStationReplayTimer()
    {
        if (!isIntro && stationList.Count > 0 && currentStation >= 0)
            stationList[currentStation].WaitForReplay();
        else if (isIntro && introStation != null)
            introStation.WaitForReplay();
    }


    //-----------------------------------\\
    //********** Media Creation **********\\
    //-------------------------------------\\



    public MediaElement CreateMediaElement(string path, int type, Vector2 pos, Vector2 size, int fontSize, Color color, float delay, float duration, int frame, int loop)
    {
        MediaElement me = Instantiate(mediaElement, Vector3.zero, Quaternion.identity).GetComponent(typeof(MediaElement)) as MediaElement;
        me.gameObject.transform.SetParent(parentUI.transform);

        me.SetPosSize(pos, size);
        me.SetDelayDuration(delay, duration);

        me.SetMediaPath(path, type);
        me.SetFontSize(fontSize);
        me.SetFontColor(color);
        me.SetFrame(frame);
        me.SetLoop(loop);

        return me;
    }

    public MediaElement CreateMediaElement(string path, int type, Vector2 pos, Vector2 size, float delay, float duration, int frame, int loop)
    {
        return CreateMediaElement(path, type, pos, size, 45, Color.blue, delay, duration, frame, loop);
    }



    //---------------------------------------\\
    //********** Category Animation **********\\
    //-----------------------------------------\\



    public void PlayCategoryAnimation(int AnimationNo)
    {
        if ((currentCategory != AnimationNo || currentCategory == 0) && CategoryController.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            switch (AnimationNo)
            {
                case 1:
                    if (currentCategory == 0)
                        CategoryController.Play("Level1RingAnimation");

                    else
                    {
                        CategoryController.Play("ResetLevel" + currentCategory.ToString() + "Animation");
                        CategoryController.SetInteger("QueuedPlay", 1);
                    }
                    break;



                case 2:
                    if (currentCategory == 0)
                        CategoryController.Play("Level2RingAnimation");

                    else
                    {
                        CategoryController.Play("ResetLevel" + currentCategory.ToString() + "Animation");
                        CategoryController.SetInteger("QueuedPlay", 2);
                    }
                    break;



                case 3:
                    if (currentCategory == 0)
                        CategoryController.Play("Level3RingAnimation");

                    else
                    {
                        CategoryController.Play("ResetLevel" + currentCategory.ToString() + "Animation");
                        CategoryController.SetInteger("QueuedPlay", 3);
                    }
                    break;



                case 4:
                    if (currentCategory == 0)
                        CategoryController.Play("Level4RingAnimation");

                    else
                    {
                        CategoryController.Play("ResetLevel" + currentCategory.ToString() + "Animation");
                        CategoryController.SetInteger("QueuedPlay", 4);
                    }
                    break;
            }
            currentCategory = AnimationNo;
        }
    }

    IEnumerator CheckIfCategoryAnimationEnded()
    {
        if (CategoryController.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            isCategoryAnimationPlaying = false;
        }
        else
        {
            yield return new WaitForSeconds(0.25f);
            StartCoroutine(CheckIfCategoryAnimationEnded());
        }
    }



    //----------------------------\\
    //********** Hotkeys **********\\
    //------------------------------\\



    private void Hotkeys()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            Cursor.visible = !Cursor.visible;
            Debug.Log("toggled cursor visibility");
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            if (!isMovingToStation && !isCategoryAnimationPlaying)
                NextStation();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!isMovingToStation && !isCategoryAnimationPlaying)
                PreviousStation();
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            isInactivityEnabled = !isInactivityEnabled;
            Debug.Log("Inactivity set to " + isInactivityEnabled);
        }

        /*if (Input.GetKeyDown(KeyCode.W) && Input.GetKey(KeyCode.LeftShift))
        {
            MakeTestWall();
        }

        if (Input.GetKeyDown(KeyCode.T) && Input.GetKey(KeyCode.LeftShift))
        {
            MakeTestTimeline();
        }

        if (Input.GetKeyDown(KeyCode.S) && Input.GetKey(KeyCode.LeftShift))
        {
            MakeTestStation();
        }*/

        if (Input.GetKeyDown(KeyCode.R) && Input.GetKey(KeyCode.LeftShift))
        {
            ReloadCurrentStation();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            timelineHolder.gameObject.SetActive(!timelineHolder.gameObject.activeSelf);
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            grid.enabled = !grid.enabled;
        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (HandPairManager.Instance.GetNumHandPairs() > 0)
            {
                Vector3 pos0 = HandPairManager.Instance.GetHandPairs(0).GetJointIdPosition(0);
                SetUserPosition0(pos0);
                Debug.Log(pos0);
            }
        }

        /*if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            HighlightButton(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            HighlightButton(1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            HighlightButton(-1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            SelectButton(0);
        }*/
    }


    //----------------------------------------\\
    //********** LangSelect Functions *********\\
    //------------------------------------------\\

    private IEnumerator SelectLanguageDelayed(int lang, float delay)
    {
        SetTimelineTimer(lang, delay);
        yield return new WaitForSeconds(delay);
        try
        {
            SelectButton(lang);
        }
        catch { }
    }


    //----------------------------------------\\
    //********** Inactivity Function **********\\
    //------------------------------------------\\



    void InactivityFunction()
    {
        if (isInactivityEnabled)
        {
            if (!timelineHolder.gameObject.activeSelf)
            {
                inactiveSince += Time.deltaTime;
                if (activityDetected)
                {
                    inactiveSince = 0;
                    activityDetected = false;
                }

                if (inactiveSince >= maxInactivity)
                {
                    if (!isIntro)
                        StartIntro();
                }

            }
        }
    }

    private void ResetInactivity()
    {
        activityDetected = true;
    }


    void SetActivityDetected()
    {
        activityDetected = true;
    }



    //--------------------------------\\
    //********** Bug Warning **********\\
    //----------------------------------\\



    public void BugWarning(string newMessage)
    {
        bugWarning.SetActive(true);
        Text newText = bugWarning.GetComponent<Text>();
        newText.text = newMessage;
    }



    //----------------------------------\\
    //********** Make Test (X) **********\\
    //------------------------------------\\



    private void MakeTestWall()
    {
        wall = new WallStruct();
        wall.wallName = "WALLS";
        wall.wallImagePath = "";
        wall.introPath = "C:/DW/introStation/introStation.json";
        wall.timelinePaths = new string[2];
        wall.timelinePaths[0] = "C:/DW/timelineEN.json";
        wall.timelinePaths[1] = "C:/DW/timelineFR.json";
        wall.timelineOriginPositionXY = new float[2];
        wall.timelineOriginPositionXY[0] = -600;
        wall.timelineOriginPositionXY[1] = 0;
        wall.timelineTargetPositionXY = new float[2];
        wall.timelineTargetPositionXY[0] = 600;
        wall.timelineTargetPositionXY[1] = 0;
        wall.wallBackgroundPositionXY = new float[2];
        wall.wallBackgroundPositionXY[0] = 400;
        wall.wallBackgroundPositionXY[1] = -400;

        string json = JsonUtility.ToJson(wall);

        File.WriteAllText("C:/DW/wall.json", json);
    }

    private void MakeTestTimeline()
    {
        TimelineStruct timeline = new TimelineStruct();
        timeline.timelineName = "DISCOVERIES";
        timeline.timelineTextValue = "ENGLISH";
        timeline.timelineTextPositionXY = new float[2];
        timeline.timelineTextPositionXY[0] = 30;
        timeline.timelineTextPositionXY[1] = 40;
        timeline.timelineTextFontSize = 50;
        timeline.timelineImgPath = "C:/DW/LIT_DW_Lan-EN.png";
        timeline.stationPaths = new string[3];
        timeline.stationPaths[0] = "C:/DW/st01.json";
        timeline.stationPaths[1] = "C:/DW/st02.json";
        timeline.stationPaths[2] = "C:/DW/st03.json";

        string json = JsonUtility.ToJson(timeline);

        File.WriteAllText("C:/DW/timeline.json", json);
    }

    private void MakeTestStation()
    {
        StationStruct station = new StationStruct();
        station.stationName = "Paris Gare de Lyon";
        station.stationDate = "1967";
        station.replay = 0;
        station.category = 1;
        station.mediaStruct = new MediaStruct[3];
        station.mediaStruct[0].path = "C:/DW/img01.jpg";
        station.mediaStruct[0].type = 1;
        station.mediaStruct[0].width = 500;
        station.mediaStruct[0].height = 300;
        station.mediaStruct[0].posX = -500;
        station.mediaStruct[0].posY = 350;
        station.mediaStruct[0].duration = 5f;
        station.mediaStruct[0].delay = 0f;
        station.mediaStruct[0].frame = 1;
        station.mediaStruct[0].loop = 0;

        station.mediaStruct[1].path = "C:/DW/text01.txt";
        station.mediaStruct[1].type = 3;
        station.mediaStruct[1].width = 500;
        station.mediaStruct[1].height = 300;
        station.mediaStruct[1].posX = 500;
        station.mediaStruct[1].posY = -350;
        station.mediaStruct[1].fontSize = 30;
        station.mediaStruct[1].duration = 0f;
        station.mediaStruct[1].delay = 5f;
        station.mediaStruct[0].frame = 1;
        station.mediaStruct[0].loop = 0;


        station.mediaStruct[2].path = "C:/DW/video01.mp4";
        station.mediaStruct[2].type = 2;
        station.mediaStruct[2].width = 500;
        station.mediaStruct[2].height = 300;
        station.mediaStruct[2].posX = 1600;
        station.mediaStruct[2].posY = 550;
        station.mediaStruct[2].duration = 0f;
        station.mediaStruct[2].delay = 10f;
        station.mediaStruct[0].frame = 0;
        station.mediaStruct[0].loop = 0;


        string json = JsonUtility.ToJson(station);

        File.WriteAllText("C:/DW/st01.json", json);
    }

}
