﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotorMatrix : MonoBehaviour {

	public GameObject[] cells;
	public Sprite blue,yellow,red;
	private static int i = 0;
	private int length;
	private GameObject previous1,previous2;
	private GameObject[] _cells;

	// Use this for initialization
	void Start () {

			length = cells.Length;
			_cells = GameObject.FindGameObjectsWithTag("cell");

			//Invoke("RotationPattern",1f);
	}

	// Update is called once per frame
	void Update () {



	}

	public void callRotationPattern()
	{
		Invoke("RotationPattern",0f);
	}

	void RotationPattern()
	{		
		foreach(GameObject gameObj in _cells)
		{
				gameObj.GetComponent<Image>().sprite = yellow;
		}

		cells[i].GetComponent<Image>().sprite = red;
		cells[length - 1 - i].GetComponent<Image>().sprite = red;

		i++;
		if(i==5)
			i = 0;
	}





}
