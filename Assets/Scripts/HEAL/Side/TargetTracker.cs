﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTracker : MonoBehaviour
{

    public GameObject targetTracker;
    public int maxRandomsBeforeAcquiring;
    public bool isLeft;
    private float spinSpeed;
    private Quaternion TTrotAngle;
    private int detectionState;
    private bool isTracking;
    private bool targetLocated;
    private Vector3[] newPos;
    private GameObject[] targetTrackerRings;

    //--------- AUDIO ---------//
    public AudioClip[] targetTrackerSounds;


    void Start()
    {
        TTrotAngle = targetTracker.transform.rotation;
        newPos = new Vector3[maxRandomsBeforeAcquiring + 1];                            // initializing newpos array;
        targetTrackerRings = new GameObject[targetTracker.transform.childCount];        // intialising targetTrackerRings (targetTracker childrens) array; you can add objects as children of the main target tracker object, it will give no error. however nothing will happen unless you add it to the script.
        targetTracker.SetActive(false);                                                 // before animation ends you don't want to be moving the targetTracker

        for (int i = 0; i < 5; i++)                                                     // setting the values for our array.
        {
            targetTrackerRings[i] = targetTracker.transform.GetChild(i).gameObject;     // sets as many objets as needed for the array. Take care of the order on the Hierarchy
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (HealManager.Instance.GetIsTumorGenerated() && HealManager.Instance.brain != null)           // I had to seperate this if, for some reason it was making the other ones fail.
        {
            if (!HealManager.Instance.GetIsTargetAcquired() && !isTracking && HealManager.Instance.GetIsStartAnimationEnded())
            {

                for (int i = 0; i < maxRandomsBeforeAcquiring; i++)         // this for loop has to be down here -instead of Start()- because we need the tumor's reference.
                {
                    if (!isLeft)
                        newPos[i] = new Vector3(Random.Range(-3f, 3f), Random.Range(-3f, 3f), HealManager.Instance.GetTumor().transform.position.z);
                    else
                        newPos[i] = new Vector3(Random.Range(-3f, 3f), Random.Range(-3f, 3f), HealManager.Instance.GetTumor().transform.position.z);
                }

                targetTracker.SetActive(true);                      // now we want our target tracker to be alive
                detectionState = 0;                                 // initial state of detection
                StartCoroutine(TargetDetection(0.15f));             // tells the tracker to move smoothly to specific positions set right above this message on the for loop.


                targetTracker.transform.position = newPos[0];       //sets initial position to the first value of the tracker.
                if (!isLeft)
                    newPos[maxRandomsBeforeAcquiring] = new Vector3(HealManager.Instance.GetTumor().transform.position.x, HealManager.Instance.GetTumor().transform.position.y, HealManager.Instance.GetTumor().transform.position.z); // the last position for the tracker must be the tumor's position.
                else
                    newPos[maxRandomsBeforeAcquiring] = new Vector3(HealManager.Instance.GetTumor().transform.position.x, HealManager.Instance.GetTumor().transform.position.y, HealManager.Instance.GetTumor().transform.position.z); // the last position for the tracker must be the tumor's position.

            }

        }
        if (isTracking)         // if Target is Tracking, then spin
        {
            spinSpeed += Time.deltaTime * 100;
            if (!isLeft)
            {
                if (!targetLocated)                                 // if it's not located, spin the blue ones only.
                {
                    targetTrackerRings[0].transform.rotation = Quaternion.Euler(TTrotAngle.x, TTrotAngle.y, spinSpeed);
                    targetTrackerRings[1].transform.rotation = Quaternion.Euler(TTrotAngle.x, TTrotAngle.y, -spinSpeed / 2);
                    targetTrackerRings[2].transform.rotation = Quaternion.Euler(TTrotAngle.x, TTrotAngle.y, spinSpeed * 4);
                    targetTrackerRings[3].transform.rotation = Quaternion.Euler(TTrotAngle.x, TTrotAngle.y, -spinSpeed / 4);
                }
                else                                                //if it is located, spin them faster, and start spinning the yellow one, even if it's not active by default.
                {
                    targetTrackerRings[0].transform.rotation = Quaternion.Euler(TTrotAngle.x, TTrotAngle.y, spinSpeed * 8);
                    targetTrackerRings[1].transform.rotation = Quaternion.Euler(TTrotAngle.x, TTrotAngle.y, -spinSpeed / 0.5f);
                    targetTrackerRings[2].transform.rotation = Quaternion.Euler(TTrotAngle.x, TTrotAngle.y, spinSpeed * 8);
                    targetTrackerRings[3].transform.rotation = Quaternion.Euler(TTrotAngle.x, TTrotAngle.y, -spinSpeed / 1);
                    targetTrackerRings[4].transform.rotation = Quaternion.Euler(TTrotAngle.x, TTrotAngle.y, spinSpeed * 8);
                }
            }
            else
            {
                if (!targetLocated)                                 // if it's not located, spin the blue ones only.
                {
                    targetTrackerRings[0].transform.rotation = Quaternion.Euler(0, -90, spinSpeed);
                    targetTrackerRings[1].transform.rotation = Quaternion.Euler(0, -90, -spinSpeed / 2);
                    targetTrackerRings[2].transform.rotation = Quaternion.Euler(0, -90, spinSpeed * 4);
                    targetTrackerRings[3].transform.rotation = Quaternion.Euler(0, -90, -spinSpeed / 4);
                }
                else                                                //if it is located, spin them faster, and start spinning the yellow one, even if it's not active by default.
                {
                    targetTrackerRings[0].transform.rotation = Quaternion.Euler(0, -90, spinSpeed * 8);
                    targetTrackerRings[1].transform.rotation = Quaternion.Euler(0, -90, -spinSpeed / 0.5f);
                    targetTrackerRings[2].transform.rotation = Quaternion.Euler(0, -90, spinSpeed * 8);
                    targetTrackerRings[3].transform.rotation = Quaternion.Euler(0, -90, -spinSpeed / 1);
                    targetTrackerRings[4].transform.rotation = Quaternion.Euler(0, -90, spinSpeed * 8);
                }
            }

        }

    }

    /*IEnumerator TargetDetection(float aTime)                    //moves the targetTracker to positions.
    {
        if (detectionState < maxRandomsBeforeAcquiring)
        {
            isTracking = true;
            HealManager.Instance.SetIsTargetTracking(true);
            StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserDynamic, targetTrackerSounds[0], 0.1f, 1, 1));
            float OldPos1 = targetTracker.transform.position.x;
            float OldPos2 = targetTracker.transform.position.y;

            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
            {
                float NewPos1 = Mathf.Lerp(OldPos1, newPos[detectionState + 1].x, t);
                float NewPos2 = Mathf.Lerp(OldPos2, newPos[detectionState + 1].y, t);
                targetTracker.transform.position = new Vector3(NewPos1, NewPos2, targetTracker.transform.position.z);
                yield return null;
            }
            StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserDynamic, targetTrackerSounds[1], 0.1f, 1, 1));
            yield return new WaitForSeconds(.65f);
            detectionState++;
            StartCoroutine(TargetDetection(0.25f));
        }*/
    IEnumerator TargetDetection(float aTime)                    //moves the targetTracker to positions.
    {
        if (detectionState < maxRandomsBeforeAcquiring)
        {
            isTracking = true;
            HealManager.Instance.SetIsTargetTracking(true);
            StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserDynamic, targetTrackerSounds[0], 0.1f, .75f, 1));
            float OldPos1;
            float OldPos2;
            if (!isLeft)
            {
                OldPos1 = targetTracker.transform.position.x;
                OldPos2 = targetTracker.transform.position.y;
            }
            else
            {
                OldPos1 = targetTracker.transform.position.z;
                OldPos2 = targetTracker.transform.position.y;
            }
            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
            {
                float NewPos1;
                float NewPos2;
                if (!isLeft)
                {
                    NewPos1 = Mathf.Lerp(OldPos1, newPos[detectionState + 1].x, t);
                    NewPos2 = Mathf.Lerp(OldPos2, newPos[detectionState + 1].y, t);
                }
                else
                {
                    NewPos1 = Mathf.Lerp(OldPos1, newPos[detectionState + 1].z, t);
                    NewPos2 = Mathf.Lerp(OldPos2, newPos[detectionState + 1].y, t);
                }
                if (!isLeft)
                {
                    targetTracker.transform.position = new Vector3(NewPos1, NewPos2, targetTracker.transform.position.z);
                }
                else
                {
                    targetTracker.transform.position = new Vector3(targetTracker.transform.position.x, NewPos2, NewPos1);
                }
                yield return null;
            }
            StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserDynamic, targetTrackerSounds[1], 0.1f, .75f, 1));
            yield return new WaitForSeconds(.65f);
            detectionState++;
            StartCoroutine(TargetDetection(0.25f));
        }

        //-------------------------------------------------------------------------------------------------------------

        else if (detectionState == maxRandomsBeforeAcquiring && !targetLocated)
        {
            float OldPos1;
            float OldPos2;
            if (!isLeft)
            {
                OldPos1 = targetTracker.transform.position.x;
                OldPos2 = targetTracker.transform.position.y;
            }
            else
            {
                OldPos1 = targetTracker.transform.position.z;
                OldPos2 = targetTracker.transform.position.y;
            }
            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
            {
                float NewPos1;
                float NewPos2;
                if (!isLeft)
                {
                    NewPos1 = Mathf.Lerp(OldPos1, HealManager.Instance.GetTumor().transform.position.x, t);
                    NewPos2 = Mathf.Lerp(OldPos2, HealManager.Instance.GetTumor().transform.position.y, t);
                }
                else
                {
                    NewPos1 = Mathf.Lerp(OldPos1, HealManager.Instance.GetTumor().transform.position.z, t);
                    NewPos2 = Mathf.Lerp(OldPos2, HealManager.Instance.GetTumor().transform.position.y, t);
                }
                if (!isLeft)
                {
                    targetTracker.transform.position = new Vector3(NewPos1, NewPos2, targetTracker.transform.position.z);
                }
                else
                {
                    targetTracker.transform.position = new Vector3(targetTracker.transform.position.x, NewPos2, NewPos1);
                }
                yield return null;
            }

            if (!isLeft)
            {
                targetTracker.transform.position = new Vector3(HealManager.Instance.GetTumor().transform.position.x, HealManager.Instance.GetTumor().transform.position.y, HealManager.Instance.GetTumor().transform.position.z);
            }
            else
            {
                targetTracker.transform.position = new Vector3(HealManager.Instance.GetTumor().transform.position.x, HealManager.Instance.GetTumor().transform.position.y, HealManager.Instance.GetTumor().transform.position.z);
            }

            targetLocated = true;
            StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserDynamic, targetTrackerSounds[2], 0.1f, .75f, 1));
            yield return new WaitForSeconds(1);
            StartCoroutine(Blink());
            targetTrackerRings[0].gameObject.SetActive(false);
            targetTrackerRings[1].gameObject.SetActive(false);
            targetTrackerRings[2].gameObject.SetActive(false);
            targetTrackerRings[3].gameObject.SetActive(false);
        }

    }


    IEnumerator Blink()
    {
        StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserDynamic, targetTrackerSounds[3], 0.4f, .75f, 1));
        for (int blinkState = 0; blinkState < 10; blinkState++)     //blinks blinkState/2 times;
        {
            yield return new WaitForSeconds(.05f);
            //Debug.Log(blinkState + gameObject.name);
            targetTrackerRings[4].SetActive(!targetTrackerRings[4].activeSelf);
        }
        
        ResetLocalChanges();
        AllowTumorToAppear();
    }

    void ResetLocalChanges()
    {
        isTracking = false;
        targetTracker.SetActive(false);
        targetLocated = false;
        targetTrackerRings[0].gameObject.SetActive(true);
        targetTrackerRings[1].gameObject.SetActive(true);
        targetTrackerRings[2].gameObject.SetActive(true);
        targetTrackerRings[3].gameObject.SetActive(true);
        targetTrackerRings[4].gameObject.SetActive(false);
        detectionState = 0;
    }

    void AllowTumorToAppear()
    {
        HealManager.Instance.GetTumor().SetActive(true);
        HealManager.Instance.SetIsTargetTracking(false);
        HealManager.Instance.SetIsTargetAcquired(true);
    }
}
