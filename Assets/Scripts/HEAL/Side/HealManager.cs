﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.Networking;

public class HealManager : MonoBehaviour
{

    public GantryOperations gantryOps;
    public ProtonManager protonMng;

    //---------- Manager Variables ----------//
    public AudioSource audioDiffuserDynamic;
    public AudioSource audioDiffuserPlayer1;
    public AudioSource audioDiffuserPlayer2;
    public AudioSource audioDiffuserAmbient;
    public AudioSource audioDiffuserPlayerPos1;
    public AudioSource audioDiffuserPlayerPos2;
    public AudioSource audioDiffuserPlayerPos3;
    private bool spawnedPlayer = false;
    protected static HealManager instance = null;

    //---------- Tumor Variables ----------//
    public GameObject brain;
    public GameObject tumorPrefab;
    private GameObject tumor;
    private bool isTumorGenerated;


    private bool isStartAnimationEnded;
    private bool isTargetTracking;
    private bool isTargetAcquired;
    private bool isAuthToSendProtons;
    private bool isStartSendingProtons;

    private bool hasClapped;
    private bool isFirstProtonSent;

    private bool isP1InTheZone;
    private bool isP2InTheZone;
    private bool isP3InTheZone;
    public AudioClip playerPositionSound;

    private bool isSide = true;

    public RotorMatrix rotrMtr;
    public CameraHeadMovement tableCamera, gantryCamera;
    public bool CameraHeadMovementIsOn = true;

    private Vector3 userPosition01 = Vector3.zero;
    private Vector3 userPosition02 = Vector3.zero;
    private Vector3 userPosition03 = Vector3.zero;
    private float zoneRadius = 20f;


    private bool isGameEnded;

    //---------- Inactivity Function ----------//
    private float inactiveSince;
    private float maxInactivity = 300f;

    private bool isResettingOnClick = false;

    public static HealManager Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }


    void Start()
    {
        Debug.Log("Starting HealManager");
        if (CEO.Instance != null)
        {
            if (!CEO.Instance.GetIsSide())
            {
                KinectChassis.Instance.SetActive(false);
                KinectChassis.Instance.ShowFeetMarkers(false);
                KinectChassis.Instance.ShowHandMarkers(false);
                KinectChassis.Instance.SetStreamSelection(new int[] { 0, 0, 0, 0, 0 });
                isSide = false;
                Debug.Log("isSide = " + isSide);
            }
            else
            {
                KinectChassis.Instance.SetActive(true);
                KinectChassis.Instance.ShowFeetMarkers(false);
                KinectChassis.Instance.ShowHandMarkers(true);
                KinectChassis.Instance.SetDetectClap(true);
                KinectChassis.Instance.dataManager.RequestJoints();

                KinectChassis.Instance.SetFeetVisibility(false);
                KinectChassis.Instance.SetStreamSelection(new int[] { 0, 0, 0, 0, 1 });
                isSide = true;
                Debug.Log("isSide = " + isSide);

            }
        }
        else
        {
            Debug.Log("No CEO.Instance! " + isSide);
        }

        if (LPlayer.Instance != null)
        {
            if (CEO.Instance.GetIsFloor())
            {
                LPlayer.Instance.CmdSetFloorLevel(4);
            }
            else
            {
                LPlayer.Instance.levelSide = 4;
            }
        }

        GetValuesFromSettings();

    }

    private void GetValuesFromSettings()
    {
        HandPairManager.Instance.SetZeroPointHands(new Vector3(SaveLoad.savedSettings.zeroPointHandsX, SaveLoad.savedSettings.zeroPointHandsY, SaveLoad.savedSettings.zeroPointHandsZ));


        userPosition01.x = SaveLoad.savedSettings.pos1X;
        userPosition01.y = SaveLoad.savedSettings.pos1Y;
        userPosition01.z = SaveLoad.savedSettings.pos1Z;

        userPosition02.x = SaveLoad.savedSettings.pos2X;
        userPosition02.y = SaveLoad.savedSettings.pos2Y;
        userPosition02.z = SaveLoad.savedSettings.pos2Z;

        userPosition03.x = SaveLoad.savedSettings.pos3X;
        userPosition03.y = SaveLoad.savedSettings.pos3Y;
        userPosition03.z = SaveLoad.savedSettings.pos3Z;
    }

    public bool GetIsTumorGenerated()
    {
        return isTumorGenerated;
    }

    public bool GetIsStartAnimationEnded()
    {
        return isStartAnimationEnded;
    }

    public bool GetIsTargetAcquired()
    {
        return isTargetAcquired;
    }
    public bool GetIsAuthToSendProtons()
    {
        return isAuthToSendProtons;
    }

    public bool GetIsStartSendingProtons()
    {
        return isStartSendingProtons;
    }

    public bool GetHasClapped()
    {
        return hasClapped;
    }

    public GameObject GetTumor()
    {
        return tumor;
    }

    public bool GetIsTargetTracking()
    {
        return isTargetTracking;
    }

    public bool GetIsP1InTheZone()
    {
        return isP1InTheZone;
    }

    public bool GetIsP2InTheZone()
    {
        return isP2InTheZone;
    }

    public bool GetIsP3InTheZone()
    {
        return isP3InTheZone;
    }

    public bool GetIsFirstProtonSent()
    {
        return isFirstProtonSent;
    }

    public bool GetIsGameEnded()
    {
        return isGameEnded;
    }

    // Is player in the zone variables Assign ispXitzSet to change

    public void SetPlayerInZone(int zone, bool inzone)
    {
        switch (zone)
        {
        case 1:
            SetIsP1InTheZone(inzone);
            break;
        case 2:
            SetIsP2InTheZone(inzone);
            break;
        case 3:
            SetIsP3InTheZone(inzone);
            break;
        }
    }





    public void ToggleIsP1InTheZone()
    {
        isP1InTheZone = !isP1InTheZone;
        if (CEO.Instance.GetIsSide())
        {
            LPlayerHEAL.Instance.RpcSetIsP1InTheZone(isP1InTheZone);
        }
    }

    public void ToggleIsP2InTheZone()
    {
        isP2InTheZone = !isP2InTheZone;
        if (CEO.Instance.GetIsSide())
        {
            LPlayerHEAL.Instance.RpcSetIsP2InTheZone(isP2InTheZone);
        }
    }

    public void ToggleIsP3InTheZone()
    {
        isP3InTheZone = !isP3InTheZone;
        if (CEO.Instance.GetIsSide() && LPlayerHEAL.Instance != null)
        {
            LPlayerHEAL.Instance.RpcSetIsP3InTheZone(isP3InTheZone);
        }
    }

    public void SetIsP1InTheZone(bool ip1itzSet)
    {
        if (ip1itzSet != isP1InTheZone)
        {
            isP1InTheZone = ip1itzSet;
            if (isP1InTheZone)
            { StartCoroutine(PlaySound(audioDiffuserPlayerPos1, playerPositionSound, 0, .5f, 1)); }
            if (CEO.Instance.GetIsSide() && LPlayerHEAL.Instance != null)
            {
                LPlayerHEAL.Instance.RpcSetIsP1InTheZone(isP1InTheZone);
            }
        }
    }

    public void SetIsP2InTheZone(bool ip2itzSet)
    {
        if (isP2InTheZone != ip2itzSet)
        {
            isP2InTheZone = ip2itzSet;
            if (isP2InTheZone)
            { StartCoroutine(PlaySound(audioDiffuserPlayerPos2, playerPositionSound, 0, .5f, 1.2f)); }
            if (CEO.Instance.GetIsSide() && LPlayerHEAL.Instance != null)
            {
                LPlayerHEAL.Instance.RpcSetIsP2InTheZone(isP2InTheZone);
            }
        }
    }

    public void SetIsP3InTheZone(bool ip3itzSet)
    {
        if (isP3InTheZone = ip3itzSet)
        {
            isP3InTheZone = ip3itzSet;
            if (isP3InTheZone)
            { StartCoroutine(PlaySound(audioDiffuserPlayerPos2, playerPositionSound, 0, .5f, 1.4f)); }
            if (CEO.Instance.GetIsSide() && LPlayerHEAL.Instance != null)
            {
                LPlayerHEAL.Instance.RpcSetIsP3InTheZone(isP3InTheZone);
            }
        }
    }

    public void SetIsTargetAcquired(bool ita)
    {
        isTargetAcquired = ita;
    }

    public void SetIsStartSendingProtons(bool issp)
    {
        isStartSendingProtons = issp;
    }
    public void SetIsAuthToSendProtons(bool iatsp)
    {
        isAuthToSendProtons = iatsp;
    }

    public void SetHasClapped(bool shc)
    {
        hasClapped = shc;
    }

    public void SetIsGameEnded(bool ige)
    {
        isGameEnded = ige;
    }

    public void SetIsTumorGenerated(bool itg)
    {
        isTumorGenerated = itg;
    }

    public void SetIsTargetTracking(bool sitt)
    {
        isTargetTracking = sitt;
    }

    public void SetTumor(GameObject tumorSet)
    {
        tumor = tumorSet;
    }


    public void SetStartAnimationEnded(bool sae)
    {
        isStartAnimationEnded = sae;
    }

    public void SetIsFirstProtonSent(bool sifps)
    {
        isFirstProtonSent = sifps;
    }

    public void TriggerS_AuthorizeProtons()
    {
        LPlayerHEAL.Instance.RpcAuthorizeProtons();
    }

    public void ActionF_AuthorizeProtonsFloor()
    {
        if (CEO.Instance.GetIsFloor())
        {
            SetIsAuthToSendProtons(!GetIsAuthToSendProtons());
        }
    }

    public void TriggerS_SendProtonFloor(bool ext)
    {
        if (CEO.Instance.GetIsSide() && LPlayerHEAL.Instance != null)
        {
            if (!ext && GetIsAuthToSendProtons())
            {
                LPlayerHEAL.Instance.RpcSendprotonFloor();
            }

            if (ext)
            {
                LPlayerHEAL.Instance.RpcSendprotonFloor();
            }
        }
    }

    public void ActionF_SendprotonFloor()
    {
        protonMng.InstantiateNewProtonFloor(true);
    }

    public void TriggerF_StartSendingBeamSide()
    {
        if (CEO.Instance.GetIsFloor())
        {
            LPlayerHEAL.Instance.CmdStartSendingBeamSide();
        }
    }

    public void ActionS_StartSendingBeamSide()
    {
        gantryOps.SendNewProtonSide(true);
    }


    public void SpawnPlayer()
    {
        if (LPlayer.Instance != null)
        {
            LPlayer.Instance.CreateLevelPlayer(4);
            spawnedPlayer = true;
        }
    }

    public void SetUserPosition01(Vector3 pos1)
    {
        userPosition01 = pos1;
        SaveLoad.savedSettings.pos1X = userPosition01.x;
        SaveLoad.savedSettings.pos1Y = userPosition01.y;
        SaveLoad.savedSettings.pos1Z = userPosition01.z;
    }

    public void SetUserPosition02(Vector3 pos2)
    {
        userPosition02 = pos2;
        SaveLoad.savedSettings.pos2X = userPosition02.x;
        SaveLoad.savedSettings.pos2Y = userPosition02.y;
        SaveLoad.savedSettings.pos2Z = userPosition02.z;

    }

    public void SetUserPosition03(Vector3 pos3)
    {
        userPosition03 = pos3;
        SaveLoad.savedSettings.pos3X = userPosition03.x;
        SaveLoad.savedSettings.pos3Y = userPosition03.y;
        SaveLoad.savedSettings.pos3Z = userPosition03.z;
    }

    public Vector3 GetUserPosition01()
    {
        return userPosition01;
    }

    public Vector3 GetUserPosition02()
    {
        return userPosition02;
    }

    public Vector3 GetUserPosition03()
    {
        return userPosition03;
    }

    public float GetZoneRadius()
    {
        return zoneRadius;
    }


    public void CalibrateMinEnergy()
    {
        SaveLoad.savedSettings.minEnergyPosition = (float)HandPairManager.Instance.GetEnergyFromHandsAtPos(userPosition03, zoneRadius);
    }

    public void CalibrateMaxEnergy()
    {
        SaveLoad.savedSettings.maxEnergyPosition = (float)HandPairManager.Instance.GetEnergyFromHandsAtPos(userPosition03, zoneRadius);
    }

    public float GetAdjustedEnergy(float en)
    {

        float adEnergy = 10f * (en - SaveLoad.savedSettings.minEnergyPosition) / (SaveLoad.savedSettings.maxEnergyPosition - SaveLoad.savedSettings.minEnergyPosition);

        return adEnergy >= 0f ? adEnergy : 0f;
    }

    private void LockReset()
    {
        isResettingOnClick = false;
    }

    public void Reset()
    {
        if (LPlayerHEAL.Instance != null)
        {
            if (CEO.Instance.GetIsSide())
            {
                gantryOps.ResetSide(true);
                LPlayerHEAL.Instance.RpcReset();
            }
            if (CEO.Instance.GetIsFloor())
            {
                protonMng.ResetFloor(true);
                LPlayerHEAL.Instance.CmdReset();
            }
        }
    }

    public void ResetfromRemote()
    {
        if (CEO.Instance.GetIsSide())
        {
            gantryOps.ResetSide(true);
        }
        if (CEO.Instance.GetIsFloor())
        {
            protonMng.ResetFloor(true);
        }

    }

    private void UpdateHands()
    {

    }



    void Update()
    {
        InactivityFunction();

        if (isSide)
        {
            float? displacement = HandPairManager.Instance.GetDisplacementFromHandsAtPos(userPosition01, zoneRadius);
            SetIsP1InTheZone(displacement != null ? true : false);

            float? angle = HandPairManager.Instance.GetAngleFromHandsAtPos(userPosition02, zoneRadius);
            SetIsP2InTheZone(angle != null ? true : false);

            float? energy = HandPairManager.Instance.GetEnergyFromHandsAtPos(userPosition03, zoneRadius);
            SetIsP3InTheZone(energy != null ? true : false);


            if (isTumorGenerated && isTargetAcquired)
            {


                //Displacement
                if (displacement != null && gantryOps.GetIsTableSnappingOrSnapped()) gantryOps.SetTableTranslation((float)displacement);


                //For angle setting test
                if (angle != null)
                {
                    gantryOps.SetGantryAngle((float)angle);
                    rotrMtr.callRotationPattern();
                }

                if (energy != null && gantryOps.GetBeamIsSnappingOrSnapped()) gantryOps.SetBeamDistance(GetAdjustedEnergy((float)energy));
            }

            else if (!isTumorGenerated || !isTargetAcquired)
            {
                inactiveSince = 0;
            }

            ///////////////

            //if (GetHasClapped() == false && GetIsP3InTheZone() && GetIsAuthToSendProtons())
            if (GetIsP3InTheZone())
            {
                if (HandPairManager.Instance.GetDidClapAtPosition(userPosition03, zoneRadius));
                {                    
                    SetHasClapped(true);
                    TriggerS_SendProtonFloor(false);
                }
            }
            //////////


            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Alpha1))
            {
                if (HandPairManager.Instance.GetNumHandPairs() > 0)
                {
                    Vector3 pos1 = HandPairManager.Instance.GetHandPairs(0).GetJointIdPosition(0);
                    SetUserPosition01(pos1);
                }
            }

            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (HandPairManager.Instance.GetNumHandPairs() > 0)
                {
                    Vector3 pos2 = HandPairManager.Instance.GetHandPairs(0).GetJointIdPosition(0);
                    SetUserPosition02(pos2);
                }
            }

            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Alpha3))
            {
                if (HandPairManager.Instance.GetNumHandPairs() > 0)
                {
                    Vector3 pos3 = HandPairManager.Instance.GetHandPairs(0).GetJointIdPosition(0);
                    SetUserPosition03(pos3);
                }
            }

            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.N))
            {
                CalibrateMinEnergy();
            }

            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.M))
            {
                CalibrateMaxEnergy();
            }



            /*if(CameraHeadMovementIsOn) //THANOS
            {
              Vector3 headPosP1 = KinectChassis.Instance.GetHeadMovement(1);
              Vector3 headPosP2 = KinectChassis.Instance.GetHeadMovement(2);

              tableCamera.cameraTranslation = -10f*(headPosP1.z - Mathf.Abs(zeroHeadPosition1.z)); //THANOS
              gantryCamera.cameraTranslation = -10f*(headPosP2.z - Mathf.Abs(zeroHeadPosition2.z));
            } */

        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (GetIsTargetAcquired())
            {
                if (isResettingOnClick)
                {
                    isResettingOnClick = false;
                    Reset();
                }
                else
                {
                    isResettingOnClick = true;
                    Invoke("LockReset", 0.5f);
                }
            }
        }

        if (!spawnedPlayer && LPlayer.Instance != null && !CEO.Instance.GetIsSide())
        {
            if (LPlayer.Instance != null && LPlayer.Instance.levelSide == 4)
            {
                SpawnPlayer();
            }
        }

        //if (!isSide)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                ToggleIsP1InTheZone();
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                ToggleIsP2InTheZone();
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                ToggleIsP3InTheZone();
            }
        }
    }

    public IEnumerator PlaySound(AudioSource diffuser, AudioClip soundToBePlayed, float delayTime, float soundVolume, float soundPitch)     //DEFAULTS -> delay 0 | vol 1 | pitch 1
    {
        yield return new WaitForSeconds(delayTime);
        diffuser.clip = soundToBePlayed;
        diffuser.volume = soundVolume;
        diffuser.pitch = soundPitch;
        diffuser.Play();
    }

    void InactivityFunction()
    {
        if (CEO.Instance.GetIsSide())
        {
            inactiveSince += Time.deltaTime;
            if (inactiveSince >= maxInactivity)
            {
                Debug.Log("Reseting HEAL due to inactivity");
                Reset();

                inactiveSince = 0;
            }

            if (isP1InTheZone || isP2InTheZone || isP3InTheZone)
            {
                inactiveSince = 0;
            }
        }
    }

}
