﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TumorGenerator : MonoBehaviour {
    
	
	// Update is called once per frame
	void Update ()
    {
        if (!HealManager.Instance.GetIsTumorGenerated())
        {
            GameObject newTumor = Instantiate(HealManager.Instance.tumorPrefab, new Vector3(HealManager.Instance.brain.transform.position.x + Random.Range(-2f, 2f), HealManager.Instance.brain.transform.position.y + Random.Range(-2f, 2f), HealManager.Instance.brain.transform.position.z + Random.Range(-1.2f, 3.5f)), Quaternion.identity, HealManager.Instance.brain.transform);
            if ((newTumor.transform.position.x < -.35 || newTumor.transform.position.x > .65) && (newTumor.transform.position.y < -0.35 || newTumor.transform.position.y > 0.65) && (newTumor.transform.position.z < -1 || newTumor.transform.position.z > 2.5))
            {
                newTumor.name = "Tumor";
                HealManager.Instance.SetTumor(newTumor);
                HealManager.Instance.SetIsTumorGenerated(true);
                HealManager.Instance.GetTumor().SetActive(false);
            }
            else
                Destroy(newTumor.gameObject);
        }
    }
}
