﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GantryClone : MonoBehaviour
{
    public GameObject gantryClone;
    public GameObject gantryRef;
    public GameObject tableClone;
    public GameObject tableRef;

    // Update is called once per frame
    void Update ()
    {
        gantryClone.transform.rotation = Quaternion.Inverse(gantryRef.transform.rotation);
        tableClone.transform.position = new Vector3(tableClone.transform.position.x, tableClone.transform.position.y, tableRef.transform.position.z/50+ 1198);
    }
}
