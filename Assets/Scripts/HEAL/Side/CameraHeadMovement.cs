﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHeadMovement : MonoBehaviour {

	public Transform cameraTarget;
	public bool isTableCamera = false;
	[Range(-10, 10)]
	public float cameraTranslation = 0;
	private float constantPosX,constantPosY,constantPosZ;
	Vector3 newPos;

	// Use this for initialization
	void Start () {

		if(isTableCamera)
		{
			constantPosX = transform.position.x;
			constantPosY = transform.position.y;
	  }
		else
		{
			constantPosY = transform.position.y;
			constantPosZ = transform.position.z;
		}
	}

	// Update is called once per frame
	void Update () {

			if(HealManager.Instance.CameraHeadMovementIsOn)
				moveCamera(cameraTranslation);		

		}

	void moveCamera(float cameraTranslation)
	{
		if(isTableCamera)
			newPos = new Vector3(transform.position.x,transform.position.y,cameraTranslation);
		else
			newPos = new Vector3(cameraTranslation,transform.position.y,transform.position.z);

		transform.position = Vector3.Lerp(transform.position,newPos,1f);
		transform.LookAt(cameraTarget);
	}
}
