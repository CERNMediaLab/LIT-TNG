﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GantryOperations : MonoBehaviour
{
    //---------- VARIABLES START -----------//

    //---------- The Major Objects Variables ----------//
    public Transform laserSender;           //the gantry laser moving machine sender
    public Transform laserReceiver;         //this is a GameObject that is at the exact opposite position from the gantry laser sender, relative to his parent G-O, the script holder empty.
    public GameObject patientTableCamera;
    public GameObject patientGantryCamera;
    private bool spin;
    public GameObject[] movingObjects;
    private float spinSpeed = 0;
    private int detectionState = 0;

    //---------- Gantry Rotation Variables ----------//
    public GameObject gantry;
    public float rotationSpeed = 50f;
    public float gantrySpeedFactor = 300f;
    [Range(-90, 90)]
    public float angle;
    private int wasAngle;
    private float _angle;
    private float angleDiff;
    private Quaternion currentTarget;

    //---------- Gantry Angle Detection && Snap Variables ----------//
    public int tumorDetectionMarginError;
    public bool doNotSnap;
    private bool gantryIsSnapping;           //If the ray is aligned for more than 2 seconds with the tumor, this bool triggers, and the ray will start aligning to the exact position of the tumor's center;
    private bool gantryIsSnapped;               //If the ray is aligned with the tumor's center, this triggers and any movement is futile.
    private float currentAngle;              //current angle the gantry has, relative to the brain
    private float targetAngle;               //the angle the tumor has, relative to the brain.
    private float gantryAlignementCounter;       //a counter that counts how much time the ray has been inside the tumor.
    private LineRenderer raycastLineRenderer;//the gantry laser line renderer
    private Material raycastLineRendererMat; //the gantry line renderer material (to change colors easily)
    private Ray ray;

    //---------- Table Translation Variables ----------//
    public GameObject table;
    [Range(-5, 5)]
    public float tableTranslation;
    public float tableSpeedFactor = 50f;
    private float tableTranslationSpeed = 30f;
    private float tablePositionDiff;
    private float tablePosXTarget;
    private float tableConstantPosX, tableConstantPosY;
    private float tableAlignmentCounter;
    private bool tableIsSnapping;
    private bool tableIsSnapped;

    //---------- Beam Related Variables ----------//
    public GameObject beamMarkersPrefab;           //this is the G-Os i spawn at the hitpoint from the laser on the skull.
    public GameObject protonParticlePrefab;
    [Range(-.5f, .5f)]
    private float beamDistance = 0f;
    private float protonParticleSpeed = 0.2f;
    private GameObject protonParticle;
    private GameObject beamStartPos;           //the first G-O I spawn. The static one that will not move from the hitpoint of the skull.  --- probably we don't really need it, but I'll see optimisation later.
    private GameObject beamEndPos;             //the seconde G-O I spawn. This one moves to ajust the beam distance. It is what makes the purple beam ray "shrink" or "grow"
    private LineRenderer beamRenderer;      //the purple beam renderer
    private float beamAlignmentCounter;
    private bool beamIsSnapping;
    private bool beamIsSnapped;
    private bool protonParticleMoving;

    //---------- Laser Color Variables ----------//
    public Color startLaserColor;
    public Color targetDetectedColor;
    public Color snapTargetColor;
    public Color targetLockedColor;

    //---------- Texts -------------------------//
    public Text beamPoint;
    public Text tablePosition;
    public Text gantryAngle;
    public Text gantryRotation;
    public Text energyText;
    public Text userLocatedP1, userLocatedP2, userLocatedP3;
    public GameObject tableLockedPopup, gantryLockedPopup, energyLockedPopup;
    public GameObject tableInfoPopup, gantryInfoPopup, energyInfoPopup1, energyInfoPopup2;
    public GameObject endLevelPanel;
    public BlinkPositionLocated tableUserLocated, gantryUserLocated, energyUserLocated;
    public GameObject tableTranslationAnimation, gantryRotationAnimation, energyAdjustmentAnimation;
    public RotorMatrix rotrMtr;
    public float energy = 0;
    public Image energyBarFilled;

    //---------- SOUNDS ---------//
    public AudioClip[] gantryOpsSounds;
    public AudioClip[] gantrySounds;
    private bool startPlayingGantrySound;
    private float gantrySoundStopTimer;
    private float setWasAngleTimer;
    private bool startPlayingBeamSound;
    private float beamPitch;

    //---------- VARIABLES END ----------//

    //---------- GENERAL VOIDS & RESET ----------//

    void Start()
    {
        tableTranslation = table.transform.position.z;

        foreach (GameObject go in movingObjects)
        {
            go.transform.position = new Vector3(go.transform.position.x, go.transform.position.y + 8, go.transform.position.z);
            StartCoroutine(StartAnimation(go, go.transform.position.y, -8, 1));
            HealManager.Instance.SetStartAnimationEnded(false);
        }

        tableConstantPosX = table.transform.position.x;
        tableConstantPosY = table.transform.position.y;

        raycastLineRenderer = GetComponent<LineRenderer>();         //simple variable attribution.
        raycastLineRendererMat = raycastLineRenderer.material;

        //Invoke("ShowInitialInstructions", 10f);
    }
    //----------- UPDATE ----------//
    void Update()
    {
        if (HealManager.Instance.GetIsTumorGenerated())
        {
            RotateGantry();
            TableTranslation();
            CheckRotation();

            TestControls();
            UpdateTexts();
            LockedPopups();
        }
        if (HealManager.Instance.GetIsTargetAcquired())
        {
            CheckAlignment();
            BeamManager();
            ResetSide(false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            HealManager.Instance.ActionS_StartSendingBeamSide();
            Debug.Log(HealManager.Instance.GetIsGameEnded());
        }
    }

    public void ResetSide(bool resetNow)
    {
        if (Input.GetKeyDown(KeyCode.U) || resetNow)
        {
            HealManager.Instance.SetIsGameEnded(false);
            StopAllCoroutines();
            gantryIsSnapping = false;
            tableIsSnapping = false;
            beamIsSnapping = false;
            gantryIsSnapped = false;
            tableIsSnapped = false;
            beamIsSnapped = false;
            gantryAlignementCounter = 0f;
            tableAlignmentCounter = 0f;
            beamAlignmentCounter = 0f;
            doNotSnap = true;
            Invoke("DoNotSnap", 1f);
            Destroy(beamEndPos);
            Destroy(beamStartPos);
            angle = 0;
            wasAngle = 0;
            tableTranslation = 0;
            beamDistance = 0;
            HealManager.Instance.SetIsTargetAcquired(false);
            HealManager.Instance.SetIsTargetTracking(false);
            HealManager.Instance.SetIsAuthToSendProtons(false);
            HealManager.Instance.SetIsStartSendingProtons(false);

            protonParticleMoving = false;
            HealManager.Instance.SetIsFirstProtonSent(false);
            HealManager.Instance.SetHasClapped(false);

            HealManager.Instance.audioDiffuserDynamic.Stop();
            HealManager.Instance.audioDiffuserPlayer1.Stop();
            HealManager.Instance.audioDiffuserPlayer2.Stop();
            HealManager.Instance.audioDiffuserAmbient.Stop();
            startPlayingBeamSound = false;

            //HealManager.Instance.GetComponent<TargetTracker>(). = true;

            HealManager.Instance.SetIsTumorGenerated(false);
            Destroy(HealManager.Instance.GetTumor());
            //HealManager.Instance.GetTumor().SetActive(true);
            //HealManager.Instance.GetTumor().GetComponent<MeshRenderer>().material.SetFloat("_Alpha", .4f);

            foreach (GameObject go in movingObjects)
            {
                go.transform.position = new Vector3(go.transform.position.x, go.transform.position.y + 8, go.transform.position.z);
                StartCoroutine(StartAnimation(go, go.transform.position.y, -8, 1));
                HealManager.Instance.SetStartAnimationEnded(false);
            }

            if (HealManager.Instance.audioDiffuserAmbient.isPlaying)
                HealManager.Instance.audioDiffuserAmbient.Stop();

            HideInfoPopups();
            Invoke("ShowInitialInstructions", 10f);
            resetNow = false;
        }
    }

    //----------  GANTRY OPERATIONS VOIDS  ----------//

    void LockedPopups()
    {
        if (tableIsSnapped)
        {
            tableLockedPopup.SetActive(true);
            disableTableInfoPopup();
        }
        else
            tableLockedPopup.SetActive(false);

        if (gantryIsSnapped)
        {
            gantryLockedPopup.SetActive(true);
            disableGantryInfoPopup();
        }
        else
            gantryLockedPopup.SetActive(false);

        if (beamIsSnapped)
        {
            energyLockedPopup.SetActive(true);
            disableEnergyInfoPopup1();
            if (!HealManager.Instance.GetIsFirstProtonSent())
            {
                enableEnergyInfoPopup2();
            }
            else
            {
                disableEnergyInfoPopup2();
            }

        }
        else
            energyLockedPopup.SetActive(false);
    }


    //---------- TEST CONTROLS ----------//
    void TestControls()
    {
        if (HealManager.Instance.GetIsTargetAcquired())
        {
            if (Input.GetKey(KeyCode.LeftArrow) && !gantryIsSnapped && !gantryIsSnapping)
            {
                _angle -= 1;
                SetGantryAngle(_angle);
                rotrMtr.callRotationPattern();

            }
            if (Input.GetKey(KeyCode.RightArrow) && !gantryIsSnapped && !gantryIsSnapping)
            {
                _angle += 1;
                SetGantryAngle(_angle);
                rotrMtr.callRotationPattern();
            }
            if (Input.GetKey(KeyCode.DownArrow) && !tableIsSnapped)
                tableTranslation -= 0.1f;
            if (Input.GetKey(KeyCode.UpArrow) && !tableIsSnapped)
                tableTranslation += 0.1f;

            if (Input.GetKey(KeyCode.V))
            {
                if (energy >= 1f) energy -= 1f;
                energyBarFilled.fillAmount = energy / 100f;// / Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.B))
            {
                if (energy <= 99f) energy += 1f;
                energyBarFilled.fillAmount = energy / 100f; // / Time.deltaTime;
            }
        }
    }


    //---------- UPDATE TEXTS ----------//
    void UpdateTexts()
    {
        //set Beam Point
        beamPoint.text = (HealManager.Instance.GetTumor().transform.position.z - laserSender.transform.position.z).ToString();
        tablePosition.text = (HealManager.Instance.GetTumor().transform.position.z - laserSender.transform.position.z).ToString();
        //set Gantry Angle
        if (!gantryIsSnapped)
        {
            gantryAngle.text = ((int)angle).ToString();
            gantryRotation.text = angle.ToString();
        }
        //set 'ON/OFF' for players' located
        if (HealManager.Instance.GetIsP1InTheZone())
        {
            userLocatedP1.text = "ON";
            tableUserLocated.isBlinking = true;
            //deactivate info
            disableTableInfoPopup();
            tableTranslationAnimation.SetActive(false);
        }
        else
        {
            userLocatedP1.text = "OFF";
            tableUserLocated.isBlinking = false;

            if (!tableIsSnapped && HealManager.Instance.GetIsTargetAcquired())
            {
                enableTableInfoPopup();
                tableTranslationAnimation.SetActive(true);
            }
        }
        if (HealManager.Instance.GetIsP2InTheZone())
        {
            userLocatedP2.text = "ON";
            gantryUserLocated.isBlinking = true;
            //deactivate info
            disableGantryInfoPopup();
            gantryRotationAnimation.SetActive(false);
        }
        else
        {
            userLocatedP2.text = "OFF";
            gantryUserLocated.isBlinking = false;

            if (!gantryIsSnapped && HealManager.Instance.GetIsTargetAcquired())
            {
                enableGantryInfoPopup();
                gantryRotationAnimation.SetActive(true);
            }
        }

        if (HealManager.Instance.GetIsP3InTheZone())
        {
            userLocatedP3.text = "ON";
            energyUserLocated.isBlinking = true;
            //deactivate info
            disableEnergyInfoPopup1();
            energyAdjustmentAnimation.SetActive(false);
        }
        else
        {
            userLocatedP3.text = "OFF";
            energyUserLocated.isBlinking = false;

            if (tableIsSnapped && gantryIsSnapped)
            {
                enableEnergyInfoPopup1();
                energyAdjustmentAnimation.SetActive(true);
            }
            else
            {
                disableEnergyInfoPopup1();
                energyAdjustmentAnimation.SetActive(false);
            }

            if (beamIsSnapped)
            {
                disableEnergyInfoPopup1();
                energyAdjustmentAnimation.SetActive(false);
            }
        }
        //set energy
        energyText.text = ((int)(((beamDistance * 10) * (230 / 100)) + 70)).ToString();
        energyBarFilled.fillAmount = beamDistance / 10f;

        if (HealManager.Instance.GetIsGameEnded())
            endLevelPanel.SetActive(true);
        else
            endLevelPanel.SetActive(false);
        //endLevelPanel.GetComponent<Graphic>().CrossFadeAlpha(1f,2f,false);

    }

    void ShowInitialInstructions()
    {
        if (!HealManager.Instance.GetIsP1InTheZone())
        {
            enableTableInfoPopup();
            tableTranslationAnimation.SetActive(true);
        }
        if (!HealManager.Instance.GetIsP2InTheZone())
        {
            enableGantryInfoPopup();
            gantryRotationAnimation.SetActive(true);
        }
    }

    void HideInfoPopups()
    {
        disableTableInfoPopup();
        disableGantryInfoPopup();
        disableEnergyInfoPopup1();
        disableEnergyInfoPopup2();

        tableTranslationAnimation.SetActive(false);
        gantryRotationAnimation.SetActive(false);
        energyAdjustmentAnimation.SetActive(false);
    }

    void enableTableInfoPopup()
    {
        tableInfoPopup.SetActive(true);
        tableInfoPopup.GetComponent<SwapLanguageTexts>().SetIsBlinking(true);
    }

    void disableTableInfoPopup()
    {
        tableInfoPopup.GetComponent<SwapLanguageTexts>().SetIsBlinking(false);
        tableInfoPopup.GetComponent<SwapLanguageTexts>().SetPreviousIsBlinking(false);
        tableInfoPopup.SetActive(false);
    }

    void enableGantryInfoPopup()
    {
        gantryInfoPopup.SetActive(true);
        gantryInfoPopup.GetComponent<SwapLanguageTexts>().SetIsBlinking(true);
    }
    void disableGantryInfoPopup()
    {
        gantryInfoPopup.GetComponent<SwapLanguageTexts>().SetIsBlinking(false);
        gantryInfoPopup.GetComponent<SwapLanguageTexts>().SetPreviousIsBlinking(false);
        gantryInfoPopup.SetActive(false);
    }

    void enableEnergyInfoPopup1()
    {
        energyInfoPopup1.SetActive(true);
        energyInfoPopup1.GetComponent<SwapLanguageTexts>().SetIsBlinking(true);
    }

    void disableEnergyInfoPopup1()
    {
        energyInfoPopup1.GetComponent<SwapLanguageTexts>().SetIsBlinking(false);
        energyInfoPopup1.GetComponent<SwapLanguageTexts>().SetPreviousIsBlinking(false);
        energyInfoPopup1.SetActive(false);
    }

    void enableEnergyInfoPopup2()
    {
        energyInfoPopup2.SetActive(true);
        energyInfoPopup2.GetComponent<SwapLanguageTexts>().SetIsBlinking(true);
    }

    void disableEnergyInfoPopup2()
    {
        energyInfoPopup2.GetComponent<SwapLanguageTexts>().SetIsBlinking(false);
        energyInfoPopup2.GetComponent<SwapLanguageTexts>().SetIsBlinking(false);
        energyInfoPopup2.SetActive(false);
    }

    void DoNotSnap()
    {
        doNotSnap = false;
    }

    public void SetGantryAngle(float an)
    {
        angle = an;

    }

    public void SetTableTranslation(float tt)
    {
        tableTranslation = -tt * 0.3f;
    }

    public void SetBeamDistance(float bd)
    {
        beamDistance = bd;
    }


    //----------- CHECK ROTATION ----------//
    void CheckRotation()
    {
        Vector3 targetDir = HealManager.Instance.brain.transform.position - laserSender.position; //used for the raycast only as debug;

        Vector3 gantryToBrain = laserSender.position - HealManager.Instance.brain.transform.position;
        Vector3 tumorToBrain = HealManager.Instance.GetTumor().transform.position - HealManager.Instance.brain.transform.position;
        currentAngle = Mathf.Atan2(gantryToBrain.y, gantryToBrain.x) * Mathf.Rad2Deg;         //returns the gantry's angle from the brain
        targetAngle = Mathf.Atan2(tumorToBrain.y, tumorToBrain.x) * Mathf.Rad2Deg;              //returns the tumor's angle from the brain

        raycastLineRenderer.SetPosition(0, laserSender.position);                                       //laser start
        raycastLineRenderer.SetPosition(1, laserReceiver.position);                                     //laser end
        raycastLineRenderer.enabled = true;                                                             //turn laser on

        if (!gantryIsSnapped)
        {
            if (gantryIsSnapping)
            {
                if (((currentAngle >= (targetAngle - .1f) && currentAngle <= (targetAngle + .1f)) || currentAngle == targetAngle)                           //first case
                    || ((currentAngle >= (targetAngle - 180 - .1f) && currentAngle <= (targetAngle - 180 + .1f)) || currentAngle == targetAngle - 180)      //second case
                    || ((currentAngle >= (targetAngle + 180 - .1f) && currentAngle <= (targetAngle + 180 + .1f)) || currentAngle == targetAngle + 180))     //third case
                {
                    raycastLineRendererMat.SetColor("_TintColor", targetLockedColor);
                    //Debug.DrawRay(laserSender.position, targetRayObject.position - laserSender.position, Color.green);   //used for the raycast only as debug;
                    if (!doNotSnap)
                    {
                        gantryIsSnapped = true;
                        gantryIsSnapping = false;
                        StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserPlayer2, gantryOpsSounds[1], 0, 1, 1));
                    }
                }
                if (HealManager.Instance.audioDiffuserAmbient.isPlaying)
                    HealManager.Instance.audioDiffuserAmbient.Stop();
            }
            else if (((currentAngle >= (targetAngle - tumorDetectionMarginError) && currentAngle < (targetAngle - .1f)) || ((currentAngle <= (targetAngle + tumorDetectionMarginError) && currentAngle > (targetAngle + .1f))))                             //first case
                || ((currentAngle >= (targetAngle - 180 - tumorDetectionMarginError) && currentAngle < (targetAngle - 180 - .1f)) || ((currentAngle <= (targetAngle - 180 + tumorDetectionMarginError) && currentAngle > (targetAngle - 180 + .1f))))       //second case
                || ((currentAngle >= (targetAngle + 180 - tumorDetectionMarginError) && currentAngle < (targetAngle + 180 - .1f)) || ((currentAngle <= (targetAngle + 180 + tumorDetectionMarginError) && currentAngle > (targetAngle + 180 + .1f)))))      //third case
            {

                gantryAlignementCounter += Time.deltaTime;
                if (gantryAlignementCounter < 2f)
                {
                    raycastLineRendererMat.SetColor("_TintColor", targetDetectedColor);
                    //Debug.DrawRay(laserSender.position, targetRayObject.position - laserSender.position, Color.yellow);         //used for the raycast only as debug;
                }

                if (gantryAlignementCounter > 2f)
                {
                    raycastLineRendererMat.SetColor("_TintColor", snapTargetColor);
                    //Debug.DrawRay(laserSender.position, targetRayObject.position - laserSender.position, Color.blue); */     //used for the raycast only as debug;
                }

                if (!gantryIsSnapped && !gantryIsSnapping && gantryAlignementCounter >= 2f)
                {

                    if ((currentAngle > 0 && targetAngle > 0)
                        || (currentAngle < 0 && targetAngle < 0))
                    {
                        StartCoroutine(UniversalSnapper("gantry", currentAngle, targetAngle, "gantryIsSnapping", "gantryIsSnapped", 1f));
                        //StartCoroutine(SnapAngleToZero(targetAngle, 1f));
                    }
                    if (currentAngle < 0 && targetAngle > 0)
                    {
                        StartCoroutine(UniversalSnapper("gantry", currentAngle, targetAngle - 180, "gantryIsSnapping", "gantryIsSnapped", 1f));
                        //StartCoroutine(SnapAngleToZero(targetAngle - 180, 1f));
                    }
                    if (currentAngle > 0 && targetAngle < 0)
                    {
                        StartCoroutine(UniversalSnapper("gantry", currentAngle, targetAngle + 180, "gantryIsSnapping", "gantryIsSnapped", 1f));
                        //StartCoroutine(SnapAngleToZero(targetAngle + 180, 1f));
                    }
                }
            }

            else
            {
                raycastLineRendererMat.SetColor("_TintColor", startLaserColor);
                gantryIsSnapping = false;
                gantryIsSnapped = false;
                gantryAlignementCounter = 0;
                //Debug.DrawRay(laserSender.position, targetRayObject.position - laserSender.position, Color.red);            //used for the raycast only as debug;

            }
        }
    }

    void CheckAlignment()               //check alignment with deviation and then automaticly aligns it to the center of the tumor if it's been inside the zone for enough time
    {
        if (!tableIsSnapped)
        {
            if (tableIsSnapping)
            {
                if ((HealManager.Instance.GetTumor().transform.position.z == laserSender.position.z)
                    || ((HealManager.Instance.GetTumor().transform.position.z >= (laserSender.position.z - .01)) && (HealManager.Instance.GetTumor().transform.position.z <= (laserSender.position.z + .01))))
                {
                    tableIsSnapped = true;
                    tableIsSnapping = false;
                    StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserPlayer1, gantryOpsSounds[1], 0, 1, 1));
                }
            }

            else if (((HealManager.Instance.GetTumor().transform.position.z >= (laserSender.position.z - .5)) && (HealManager.Instance.GetTumor().transform.position.z <= (laserSender.position.z + .5)))
                || (HealManager.Instance.GetTumor().transform.position.z == laserSender.position.z))
            {
                tableAlignmentCounter += Time.deltaTime;
                if (!doNotSnap && !tableIsSnapped && !tableIsSnapping && tableAlignmentCounter > 2f)
                {
                    StartCoroutine(UniversalSnapper("table", table.transform.position.z, table.transform.position.z - HealManager.Instance.GetTumor().transform.position.z + laserSender.position.z, "tableIsSnapping", "tableIsSnapped", 1f));
                }

            }

            else
            { tableAlignmentCounter = 0; tableIsSnapping = false; }
        }
    }

    //----------- BEAM MANAGER ----------//
    void BeamManager() //I guess this function should be somewhere else
    {
        ray = new Ray(laserSender.position, laserReceiver.position - laserSender.position);       //used for the raycast
        RaycastHit hit;                                                                                 //used for the raycast
        if (Physics.Raycast(ray, out hit) && gantryIsSnapped && tableIsSnapped)
        {
            if (beamStartPos == null && beamEndPos == null)                //instantiate the beam objects;
            {
                beamStartPos = Instantiate(beamMarkersPrefab, hit.point, Quaternion.identity, gantry.transform);
                beamStartPos.name = "beamStart";
                beamRenderer = beamStartPos.GetComponent<LineRenderer>();

                beamEndPos = Instantiate(beamMarkersPrefab, hit.point, Quaternion.identity, gantry.transform);
                beamEndPos.transform.position = (beamStartPos.transform.position - laserReceiver.transform.position).normalized * beamDistance;
                beamDistance = 0.1f;
                beamEndPos.name = "beamEnd";
                StartCoroutine(CallTwoProtonsOnAccelerator());
            }

            if (beamRenderer != null)
            {
                beamRenderer.enabled = true;
                beamRenderer.SetPosition(0, beamStartPos.transform.position);
                beamRenderer.SetPosition(1, beamEndPos.transform.position);
            }
        }


        if (beamEndPos != null)
        {
            if (!beamIsSnapping && !beamIsSnapped)
            {
                HealManager.Instance.audioDiffuserAmbient.pitch = beamPitch;
                beamEndPos.transform.position = ((laserReceiver.transform.position - beamStartPos.transform.position).normalized * beamDistance) + beamStartPos.transform.position;
                if (Input.GetKey(KeyCode.Z) && beamDistance - Time.deltaTime > 0)
                {
                    beamDistance -= Time.deltaTime;
                }

                if (Input.GetKey(KeyCode.X))
                {
                    beamDistance += Time.deltaTime;
                }
            }
            else { HealManager.Instance.audioDiffuserAmbient.Stop(); }

            if (!beamIsSnapped && !doNotSnap)
            {
                //adjusting beam distance
                if (beamDistance > 10f)
                    beamDistance = 10f;
                else if (beamDistance < 0f)
                    beamDistance = 0f;

                beamPitch = 0.5f + (beamDistance / 10);
                if (!startPlayingBeamSound)
                {
                    StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserAmbient, gantryOpsSounds[2], 0, 0.5f, .5f + beamDistance / 10));
                    startPlayingBeamSound = true;
                }
                if ((HealManager.Instance.GetTumor().transform.position.x == beamEndPos.transform.position.x
                || ((HealManager.Instance.GetTumor().transform.position.x >= (beamEndPos.transform.position.x - .5)) && (HealManager.Instance.GetTumor().transform.position.x <= (beamEndPos.transform.position.x + .5)))) && (HealManager.Instance.GetTumor().transform.position.y == beamEndPos.transform.position.y)
                || ((HealManager.Instance.GetTumor().transform.position.y >= (beamEndPos.transform.position.y - .5)) && (HealManager.Instance.GetTumor().transform.position.y <= (beamEndPos.transform.position.y + .5))))
                {
                    beamAlignmentCounter += Time.deltaTime;
                    if (beamAlignmentCounter > 2f && !beamIsSnapping && !beamIsSnapped)
                    {
                        StartCoroutine(UniversalSnapper("beam.transform.position.x", beamEndPos.transform.position.x, HealManager.Instance.GetTumor().transform.position.x, "beamIsSnapping", "beamIsSnapped", 1f));
                        StartCoroutine(UniversalSnapper("beam.transform.position.y", beamEndPos.transform.position.y, HealManager.Instance.GetTumor().transform.position.y, "beamIsSnapping", "beamIsSnapped", 1f));
                    }
                }
                else
                {
                    beamAlignmentCounter = 0f;
                }
            }
            if (!doNotSnap)
            {
                if (beamIsSnapping && !beamIsSnapped)
                {
                    if ((HealManager.Instance.GetTumor().transform.position.x == beamEndPos.transform.position.x
                    || ((HealManager.Instance.GetTumor().transform.position.x >= (beamEndPos.transform.position.x - .05)) && (HealManager.Instance.GetTumor().transform.position.x <= (beamEndPos.transform.position.x + .05)))) && (HealManager.Instance.GetTumor().transform.position.y == beamEndPos.transform.position.y)
                    || ((HealManager.Instance.GetTumor().transform.position.y >= (beamEndPos.transform.position.y - .05)) && (HealManager.Instance.GetTumor().transform.position.y <= (beamEndPos.transform.position.y + .05))))
                    {
                        beamIsSnapped = true;
                        beamIsSnapping = false;
                        StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserPlayer2, gantryOpsSounds[6], 0, 1, 1));
                        beamEndPos.transform.position = HealManager.Instance.GetTumor().transform.position;
                        HealManager.Instance.TriggerS_AuthorizeProtons();
                        HealManager.Instance.SetIsAuthToSendProtons(true);
                    }
                }
            }
        }

        else if (beamEndPos == null && protonParticle != null)
        {
            StopCoroutine("UniversalSnapper");
            Destroy(protonParticle);
            protonParticleMoving = false;
        }

        if (protonParticle != null)
        {
            if (!HealManager.Instance.GetIsFirstProtonSent())
                HealManager.Instance.SetIsFirstProtonSent(true);

            if (!protonParticleMoving)
            {
                StartCoroutine(UniversalSnapper("protonParticle.transform.position.x", laserSender.transform.position.x, HealManager.Instance.GetTumor().transform.position.x, "protonParticleMoving", "protonParticle", protonParticleSpeed));
                StartCoroutine(UniversalSnapper("protonParticle.transform.position.y", laserSender.transform.position.y, HealManager.Instance.GetTumor().transform.position.y, "protonParticleMoving", "protonParticle", protonParticleSpeed));
            }

            if ((HealManager.Instance.GetTumor().transform.position.x == protonParticle.transform.position.x || ((HealManager.Instance.GetTumor().transform.position.x >= (protonParticle.transform.position.x - .25)) && (HealManager.Instance.GetTumor().transform.position.x <= (protonParticle.transform.position.x + .25)))) && (HealManager.Instance.GetTumor().transform.position.y == protonParticle.transform.position.y || ((HealManager.Instance.GetTumor().transform.position.y >= (protonParticle.transform.position.y - .25)) && (HealManager.Instance.GetTumor().transform.position.y <= (protonParticle.transform.position.y + .25)))))
            {
                Destroy(protonParticle);
                protonParticleMoving = false;
                Material tumorMat = HealManager.Instance.GetTumor().GetComponent<MeshRenderer>().material;
                tumorMat.SetFloat("_Alpha", tumorMat.GetFloat("_Alpha") - 0.025f);
                if (HealManager.Instance.GetTumor().transform.localScale.x > 0f)
                HealManager.Instance.GetTumor().transform.localScale += new Vector3(-0.000005f, -0.000005f, -0.000005f);

                if (tumorMat.GetFloat("_Alpha") <= 0.10f)
                {
                    //HealManager.Instance.GetTumor().SetActive(false);
                    StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserDynamic, gantryOpsSounds[4], 0, 1, 1));
                    StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserPlayer2, gantryOpsSounds[5], 0, 1, 1));
                    Debug.Log("target is now under control, good job !");
                    HealManager.Instance.TriggerS_AuthorizeProtons();
                    HealManager.Instance.SetIsAuthToSendProtons(false);
                    HealManager.Instance.SetIsGameEnded(true);
                }
            }
        }
    }

    IEnumerator CallTwoProtonsOnAccelerator()
    {
        HealManager.Instance.SetIsAuthToSendProtons(true);
        yield return new WaitForSeconds(.1f);
        HealManager.Instance.TriggerS_SendProtonFloor(true);
        yield return new WaitForSeconds(1);
        HealManager.Instance.TriggerS_SendProtonFloor(true);
        yield return new WaitForSeconds(.1f);
        HealManager.Instance.SetIsAuthToSendProtons(false);
    }

    public void SendNewProtonSide(bool authorizeNewProton)
    {
        if (protonParticle == null && HealManager.Instance.GetTumor().transform.gameObject.activeSelf == true && beamIsSnapped && authorizeNewProton && HealManager.Instance.GetTumor().GetComponent<MeshRenderer>().material.GetFloat("_Alpha") >= 0.10f)
        {
            protonParticleMoving = false;
            protonParticle = Instantiate(protonParticlePrefab, laserSender.transform.position, Quaternion.identity, gantry.transform);
            StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserPlayer2, gantryOpsSounds[3], 0, 1f, 1.5f));
        }
    }


    public bool GetBeamIsSnappingOrSnapped()
    {
        return !beamIsSnapping && !beamIsSnapped;

    }

    public bool GetIsTableSnappingOrSnapped()
    {
        return !tableIsSnapping && !tableIsSnapped;
    }

    //----------- ROTATE GANTRY ----------//
    void RotateGantry()
    {
        if (!gantryIsSnapped && !gantryIsSnapping)                                          //If the tumor has not ben acquired yet
        {
            currentTarget = Quaternion.Euler(new Vector3(0, 0, angle));
            angleDiff = Mathf.DeltaAngle(angle, gantry.transform.rotation.eulerAngles.z);
            rotationSpeed = Mathf.Clamp(angleDiff * angleDiff, -tableSpeedFactor, tableSpeedFactor);
            gantry.transform.rotation = Quaternion.RotateTowards(gantry.transform.rotation, currentTarget, rotationSpeed * Time.deltaTime);

            if (wasAngle <= angle - 1 || wasAngle >= angle + 1)
            {
                if (!startPlayingGantrySound)
                {
                    StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserAmbient, gantrySounds[0], 0, 1f, 1));
                    startPlayingGantrySound = true;
                    gantrySoundStopTimer = 0;
                }
            }
            else
            {
                gantrySoundStopTimer += Time.deltaTime;
                if (startPlayingGantrySound && gantrySoundStopTimer > 1f)
                {
                    HealManager.Instance.audioDiffuserAmbient.Stop();
                    StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserPlayer2, gantrySounds[1], 0f, 1, 1));
                    startPlayingGantrySound = false;
                }

            }
            setWasAngleTimer += Time.deltaTime;
            if (setWasAngleTimer > .25f)
            {
                wasAngle = (int)angle;
            }
        }
    }

    //----------- TABLE TRANSLATION ----------//
    void TableTranslation()
    {
        if (!tableIsSnapped && !tableIsSnapping)
        {
            tableTranslationSpeed = 5f;
            tablePosXTarget = Mathf.Lerp(table.transform.position.z, tableTranslation, tableTranslationSpeed * Time.deltaTime);
            table.transform.position = new Vector3(tableConstantPosX, tableConstantPosY, tablePosXTarget);
        }
    }

    IEnumerator StartAnimation(GameObject go, float initialPos, float tgtYPos, float aTime)
    {
        yield return new WaitForSeconds(2);
        float oldPosY = go.transform.position.y;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            float NewPosY = Mathf.Lerp(oldPosY, (oldPosY + tgtYPos), t);
            go.transform.position = new Vector3(go.transform.position.x, NewPosY, go.transform.position.z);
            yield return null;
        }
        go.transform.position = new Vector3(go.transform.position.x, oldPosY - 8f, go.transform.position.z);
        HealManager.Instance.SetStartAnimationEnded(true);
    }


    //I am trying to make a snapper to rule them all
    IEnumerator UniversalSnapper(string objectAffected, float currentValue, float targetValue, string isSnapping, string isSnapped, float aTime)
    {
        if (!doNotSnap)
        {

            //First switch is Snapping to true
            switch (isSnapping)
            {
                case "gantryIsSnapping":
                    gantryIsSnapping = true;
                    StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserPlayer2, gantryOpsSounds[0], 0, 1, 1));
                    break;
                case "tableIsSnapping":
                    StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserPlayer1, gantryOpsSounds[0], 0, 1, 1));
                    tableIsSnapping = true;
                    break;
                case "beamIsSnapping":
                    //StartCoroutine(HealManager.Instance.PlaySound(HealManager.Instance.audioDiffuserPlayer2, gantryOpsSounds[0], 0, 1, 1));
                    beamIsSnapping = true;
                    break;
                case "protonParticleMoving":
                    protonParticleMoving = true;
                    break;
                default:
                    Debug.Log(isSnapping + " doesnt't mean anyting, check typo or cases on Gantry Operations's void UniversalSnapper");
                    break;

            }

            //Second switch is Snapped to true
            switch (isSnapped)
            {
                case "gantryIsSnapped":
                    gantryIsSnapped = false;
                    break;
                case "tableIsSnapped":
                    tableIsSnapped = false;
                    break;
                case "beamIsSnapped":
                    beamIsSnapped = false;
                    break;
                case "protonParticle":
                    break;
                default:
                    Debug.Log(isSnapping + " doesnt't mean anyting, check typo or cases on Gantry Operations's void UniversalSnapper");
                    break;

            }


            float originalValue = currentValue;
            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
            {
                float newValue = Mathf.Lerp(originalValue, targetValue, t);
                currentValue = newValue;
                switch (objectAffected)
                {
                    case "gantry":
                        gantry.transform.rotation = Quaternion.Euler(0, 0, newValue - 90f);
                        break;
                    case "table":
                        table.transform.position = new Vector3(table.transform.position.x, table.transform.position.y, newValue);
                        break;
                    case "beam.transform.position.x":
                        beamEndPos.transform.position = new Vector3(newValue, beamEndPos.transform.position.y, beamEndPos.transform.position.z);
                        break;
                    case "beam.transform.position.y":
                        beamEndPos.transform.position = new Vector3(beamEndPos.transform.position.x, newValue, beamEndPos.transform.position.z);
                        break;
                    case "protonParticle.transform.position.x":
                        if (protonParticle != null)
                            protonParticle.transform.position = new Vector3(newValue, protonParticle.transform.position.y, protonParticle.transform.position.z);
                        break;
                    case "protonParticle.transform.position.y":
                        if (protonParticle != null)
                            protonParticle.transform.position = new Vector3(protonParticle.transform.position.x, newValue, protonParticle.transform.position.z);
                        break;
                    default:
                        Debug.Log(objectAffected + " doesnt't mean anyting, check typo on Gantry Operations's void UniversalSnapper");
                        break;

                }
                yield return null;
            }
            switch (objectAffected)
            {
                case "gantry":
                    gantry.transform.rotation = Quaternion.Euler(0, 0, targetValue - 90f); // if you remove this line, the snapped position will be pretty inaccurate, resulting in the program not finding it close enough to be snapped. DON'T.
                    break;
                case "table":
                    table.transform.position = new Vector3(table.transform.position.x, table.transform.position.y, targetValue);
                    break;
                case "beam.transform.position.x":
                    beamEndPos.transform.position = new Vector3(targetValue, beamEndPos.transform.position.y, beamEndPos.transform.position.z);
                    break;
                case "beam.transform.position.y":
                    beamEndPos.transform.position = new Vector3(beamEndPos.transform.position.x, targetValue, beamEndPos.transform.position.z);
                    break;
                case "protonParticle.transform.position.x":
                    if (protonParticle != null)
                        protonParticle.transform.position = new Vector3(targetValue, protonParticle.transform.position.y, protonParticle.transform.position.z);
                    break;
                case "protonParticle.transform.position.y":
                    if (protonParticle != null)
                        protonParticle.transform.position = new Vector3(protonParticle.transform.position.x, targetValue, protonParticle.transform.position.z);
                    break;
                default:
                    Debug.Log(objectAffected + " doesnt't mean anyting, check typo on Gantry Operations's void UniversalSnapper");
                    break;

            }
        }
        else
        {
            yield return null;
        }
    }
}
