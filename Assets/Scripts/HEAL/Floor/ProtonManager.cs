﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtonManager : MonoBehaviour
{
    public Transform[] pathDots;
    private int loopStart = 4;
    private int firstCurve = 8;
    private int sendNextProton = 10;
    private int denySendingProton = 21;
    private int loopEnd = 34;
    private int acceleratorEnd = 44;
    [Range(0, 5)]
    public float speed;
    public GameObject protonPrefab;
    public List<GameObject> activeProtons;
    private bool allowNewProton;


    public void Update()
    {
        InstantiateNewProtonFloor(false);
        ResetFloor(false);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log(HealManager.Instance.GetIsAuthToSendProtons());
        }

        if (activeProtons.Count != 0)
        {
            if (activeProtons[0])
            { activeProtons[0].GetComponent<ProtonPath>().allowNewProton = true; }

            foreach (GameObject currentProton in activeProtons)
            {
                if (activeProtons.Count > 1 && activeProtons[activeProtons.Count - 2].GetComponent<ProtonPath>().i == sendNextProton && activeProtons[0].GetComponent<ProtonPath>().i <= denySendingProton)
                {
                    activeProtons[activeProtons.Count - 1].GetComponent<ProtonPath>().allowNewProton = true;
                }

                if (currentProton.GetComponent<ProtonPath>().i + 1 == loopEnd)                  //to start a new loop
                {
                    currentProton.GetComponent<ProtonPath>().i = loopStart;
                }

                if (currentProton.GetComponent<ProtonPath>().isLeaving && currentProton.GetComponent<ProtonPath>().i == firstCurve)
                {
                    currentProton.GetComponent<ProtonPath>().i = loopEnd;
                }

                if (currentProton.GetComponent<ProtonPath>().isLeaving && currentProton.GetComponent<ProtonPath>().i == acceleratorEnd)
                {
                    Destroy(activeProtons[0]);
                    activeProtons.RemoveAt(0);
                    HealManager.Instance.TriggerF_StartSendingBeamSide();
                    Invoke("PrepareNextProton", 0.4f);
                    break;

                }

                else if (currentProton.transform.position == pathDots[currentProton.GetComponent<ProtonPath>().i + 1].position)
                {
                    currentProton.GetComponent<ProtonPath>().i++;
                }

                if (currentProton.GetComponent<ProtonPath>().allowNewProton == true)
                    currentProton.transform.position = Vector3.MoveTowards(currentProton.transform.position, pathDots[currentProton.GetComponent<ProtonPath>().i + 1].position, speed);

            }
        }

    }

    public void ResetFloor(bool resetNow)
    {
        if (Input.GetKeyDown(KeyCode.U) || resetNow)
        {
            HealManager.Instance.SetIsAuthToSendProtons(false);
            HealManager.Instance.SetIsStartSendingProtons(false);
            for (int i = activeProtons.Count-1; i > -1; i--)
            {
                if (activeProtons[i] != null)
                {
                    Destroy(activeProtons[i]);
                    activeProtons.RemoveAt(i);
                }
            }
        }
    }

    public void InstantiateNewProtonFloor(bool extValidation)            // this is the void for the manager. it manages wether or not you can send a proton.
    {
        if (Input.GetKeyDown(KeyCode.P) || HealManager.Instance.GetIsStartSendingProtons() || extValidation)
        {
            if (activeProtons.Count <= 1)
            {
                InstantiateOneProton();

            }

            else if (activeProtons.Count == 2)
            {
                InstantiateOneProton();
                activeProtons[0].GetComponent<ProtonPath>().isLeaving = true;
            }
        }
    }

    void PrepareNextProton()
    {
        if (HealManager.Instance.GetIsAuthToSendProtons())
        {
            HealManager.Instance.SetIsStartSendingProtons(true);
        }
    }

    void InstantiateOneProton()
    {

        GameObject newProton = Instantiate(protonPrefab, pathDots[0].position, Quaternion.identity, transform) as GameObject;
        activeProtons.Add(newProton);

        newProton.name = "Proton" + activeProtons.Count.ToString();
        HealManager.Instance.SetIsStartSendingProtons(false);
    }
}
