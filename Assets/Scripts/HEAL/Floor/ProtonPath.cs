﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtonPath : MonoBehaviour
{
    public int i;
    public bool allowNewProton;
    public bool isLeaving;

    public void Start()
    {
        i = 0;
        allowNewProton = false;
        isLeaving = false;
    }
}
