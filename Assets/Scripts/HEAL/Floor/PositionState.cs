﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PositionState : MonoBehaviour
{

    public GameObject Circles;
    private float spinSpeed;
    private bool isP1Blinking;
    private bool isP2Blinking;
    private bool isP3Blinking;
    //---------- First Position Arrow Images ----------// 
    public Image hepta1;
    public Image penta1;
    public Image tri1;
    public Image mono1;
    //---------- Second Position Arrow Images ----------// 
    public Image hepta2;
    public Image penta2;
    public Image tri2;
    public Image mono2;
    //---------- Third Position Arrow Images ----------// 
    public Image hepta3;
    public Image penta3;
    public Image tri3;
    public Image mono3;
    //---------- First Position Ring Images ----------// 
    public Image middleCircle1;
    public Image centerRing1;
    public Image outterRing1;
    //---------- First Position Ring Images ----------// 
    public Image middleCircle2;
    public Image centerRing2;
    public Image outterRing2;
    //---------- First Position Ring Images ----------// 
    public Image middleCircle3;
    public Image centerRing3;
    public Image outterRing3;


    public IEnumerator BlinkImage(Image im, float blinkTime)
    {
        im.CrossFadeAlpha(1f, blinkTime, true);
        yield return new WaitForSeconds(blinkTime);
        im.CrossFadeAlpha(0f, blinkTime, true);
    }

    public void FadeOutImage(Image im, float blinkTime)
    {
        im.CrossFadeAlpha(0f, blinkTime, true);
    }

    public void FadeInImage(Image im, float blinkTime)
    {
        im.CrossFadeAlpha(1f, blinkTime, true);
    }

    public IEnumerator BlinkArrowsSequence(Image hepta, Image penta, Image tri, Image mono, float timeBetween, float blinkTime, int isBlinkingNO)
    {

        StartCoroutine(BlinkImage(hepta, blinkTime));
        yield return new WaitForSeconds(timeBetween);
        StartCoroutine(BlinkImage(penta, blinkTime));
        yield return new WaitForSeconds(timeBetween);
        StartCoroutine(BlinkImage(tri, blinkTime));
        yield return new WaitForSeconds(timeBetween);
        StartCoroutine(BlinkImage(mono, blinkTime));
        yield return new WaitForSeconds(timeBetween * 3f);
        if (isBlinkingNO==1) { isP1Blinking = false; }
        else if (isBlinkingNO==2) { isP2Blinking = false; }
        else if (isBlinkingNO==3) { isP3Blinking = false; }
    }

    public void FadeOutRingSequence(Image middle, Image center, Image outter, float fadeTime)
    {
        FadeOutImage(middle, fadeTime);
        FadeOutImage(center, fadeTime);
        FadeOutImage(outter, fadeTime);
    }

    public void FadeInRingSequence(Image middle, Image center, Image outter, float fadeTime)
    {
        FadeInImage(middle, fadeTime);
        FadeInImage(center, fadeTime);
        FadeInImage(outter, fadeTime);
    }

    private void Update()
    {
        spinSpeed += Time.deltaTime * 100;
        if (HealManager.Instance.GetIsP1InTheZone())
        {
            FadeInRingSequence(middleCircle1, centerRing1, outterRing1, .5f);
            isP1Blinking = false;
            Circles.transform.GetChild(0).GetChild(0).transform.rotation = Quaternion.Euler(0, 0, spinSpeed);
            Circles.transform.GetChild(0).GetChild(1).transform.rotation = Quaternion.Euler(0, 0, -spinSpeed);
        }
        if (HealManager.Instance.GetIsP2InTheZone())
        {
            FadeInRingSequence(middleCircle2, centerRing2, outterRing2, .5f);
            isP2Blinking = false;
            Circles.transform.GetChild(1).GetChild(0).transform.rotation = Quaternion.Euler(0, 0, spinSpeed);
            Circles.transform.GetChild(1).GetChild(1).transform.rotation = Quaternion.Euler(0, 0, -spinSpeed);
        }
        if (HealManager.Instance.GetIsP3InTheZone())
        {
            FadeInRingSequence(middleCircle3, centerRing3, outterRing3, .5f);
            isP3Blinking = false;
            Circles.transform.GetChild(2).GetChild(0).transform.rotation = Quaternion.Euler(0, 0, spinSpeed);
            Circles.transform.GetChild(2).GetChild(1).transform.rotation = Quaternion.Euler(0, 0, -spinSpeed);
        }

        if (!HealManager.Instance.GetIsP1InTheZone())
        {
            if (!isP1Blinking)
            {
                FadeOutRingSequence(middleCircle1, centerRing1, outterRing1, .5f);
                StartCoroutine(BlinkArrowsSequence(hepta1, penta1, tri1, mono1, 0.15f, .2f, 1));
                isP1Blinking = true;
            }
        }

        if (!HealManager.Instance.GetIsP2InTheZone())
        {
            if (!isP2Blinking)
            {
                FadeOutRingSequence(middleCircle2, centerRing2, outterRing2, .5f);
                StartCoroutine(BlinkArrowsSequence(hepta2, penta2, tri2, mono2, 0.15f, .2f, 2));
                isP2Blinking = true;
            }
        }

        if (!HealManager.Instance.GetIsP3InTheZone())
        {
            if (!isP3Blinking)
            {
                FadeOutRingSequence(middleCircle3, centerRing3, outterRing3, .5f);
                StartCoroutine(BlinkArrowsSequence(hepta3, penta3, tri3, mono3, 0.15f, .2f, 3));
                isP3Blinking = true;
            }
        }

    }

}
