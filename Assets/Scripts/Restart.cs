﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour 
{
	
	void Start () 
	{
		Invoke("ResetActions1", 0.2f);	
		Invoke("ResetActions2", 2.5f);
	}

	void ResetActions1()
	{		
		
		KinectChassis.Instance.Disable();
		NetworkManager.Shutdown();
		StopAllCoroutines();	
	}

	void ResetActions2()
	{
		if (CEO.Instance != null)
		{
			CEO.Instance.Kill();
			NetworkManager.Shutdown();			
		}

		if (LPlayer.Instance != null)
		{
			LPlayer.Instance.Kill();
		}

		Invoke("GoToIntro", 2f);
	}
	
	
	void GoToIntro()
	{
		SceneManager.LoadSceneAsync("Init", LoadSceneMode.Single);
	}
	
}
