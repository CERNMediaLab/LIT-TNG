﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelManager : MonoBehaviour
{

	public GameObject panelTest;
	private bool isMenuActive = false;
	void Start ()
	{

	}

	void CrossFadeAlpha (GameObject gObject, float target, float animationTime)
	{
		bool justSetActive = false;
		if (target == 0f)
		{
			StartCoroutine(DeactivateDelayed(gObject, animationTime));
		}
		else
		{
			gObject.SetActive(true);
			justSetActive = true;
		}

		foreach (Graphic graphic in gObject.GetComponentsInChildren<Graphic>())
		{
			if (justSetActive)
			{
				graphic.canvasRenderer.SetAlpha(0.0f);
			}
			graphic.CrossFadeAlpha(target, animationTime, false);
		}
	}

	IEnumerator DeactivateDelayed(GameObject go, float timeSeconds) 
	{		
		yield return new WaitForSeconds(timeSeconds);
		go.SetActive(false);
	}


	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.F3)) 
		{
			isMenuActive = !isMenuActive;
			float fadeTo = isMenuActive? 1f : 0f;
			CrossFadeAlpha(panelTest, fadeTo, 0.1f);

		}

	}
}
