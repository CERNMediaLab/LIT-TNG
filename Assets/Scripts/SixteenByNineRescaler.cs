﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SixteenByNineRescaler : MonoBehaviour
{
    public float targetaspect = 16.0f / 9.0f;
    private float currentWindowAspect = (float)Screen.width / (float)Screen.height;
    private float scaleheight;

    public Camera cameraToAjust;

    private void Start()
    {
        Rescale();
    }


    void Rescale()
    {
        currentWindowAspect = (float)Screen.width / (float)Screen.height;
        scaleheight = currentWindowAspect / targetaspect;
        Debug.Log("rescaling");

        if (scaleheight < 1.0f)
        {
            Rect rect = cameraToAjust.rect;

            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;

            cameraToAjust.rect = rect;
        }
        else // add pillarbox
        {
            Rect rect = cameraToAjust.rect;
            rect.width = 1.0f;
            rect.height = 1.0f;
            rect.x = 0;
            rect.y = 0;
            
            
            /*float scalewidth = 1.0f / scaleheight;                    //Add Letterbox

            Rect rect = cameraCamCameraCameraMacaroni.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;*/

            cameraToAjust.rect = rect;
        }
    }

    private void Update()
    {
        if (currentWindowAspect != (float)Screen.width / (float)Screen.height)
        {
            Rescale();
        }
    }
}