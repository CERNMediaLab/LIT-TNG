﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;



public class PointCloudLocal : MonoBehaviour
{

	private bool _useShuriken = true;
	private GameObject _pcObject;
	private ParticleSystem _pointCloud;
	private ParticleSystem.Particle[] _pcPoints;
	private Texture2D _lblTex;
	private KinectInterop.SensorData sensorData = null;


	private const int _DownsampleSize = 2;
	private const double _DepthScale = 0.3f;

	private bool _isShurikenCreated = false;
	private bool _usePointCloud = false;
	private bool isFlat = false;




	void CreatePointCloud()
	{
		_lblTex = KinectManager.Instance.GetUsersLblTex2D();
		CreatePointCloud(_lblTex.width / _DownsampleSize, _lblTex.height / _DownsampleSize);

	}

	void CreatePointCloud(int width, int height)
	{
		Debug.Log("Local width: " + width + " height: " + height + " _DownsampleSize: " + _DownsampleSize);

		_pointCloud = (ParticleSystem)this.GetComponent("ParticleSystem");
		_pcPoints = new ParticleSystem.Particle[width * height];

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int index = (y * width) + x;
				_pcPoints[index].position = new Vector3(x * _DownsampleSize, -y * _DownsampleSize, 0);
				_pcPoints[index].color = Color.red;
				_pcPoints[index].size = 3f;
			}
		}

		_isShurikenCreated = true;
		Debug.Log("Created PC");
	}

	private void KillAllParticles()
	{
		if (_pcPoints != null)
		{
			for (int i = 0; i < _pcPoints.Length; i++)
			{
				_pcPoints[i].remainingLifetime = 0f; //kill this particle
			}
		}

		_pcPoints = new ParticleSystem.Particle[0];

		_pointCloud.SetParticles(_pcPoints, _pcPoints.Length);
		_isShurikenCreated = false;
	}

	public void EnablePointCloud (bool enPC)
	{
		_usePointCloud = enPC;
	}



	void Update()
	{


		if (KinectManager.Instance.IsInitialized())
		{


			if (!_isShurikenCreated && _usePointCloud)
			{
				CreatePointCloud();
				sensorData = KinectManager.Instance.GetSensorData();
			}

			if ( _isShurikenCreated && _usePointCloud)
			{
				RefreshPointCloud(KinectManager.Instance.GetRawDepthMap());
			}

			if ( _isShurikenCreated && !_usePointCloud)
			{
				KillAllParticles();
			}

			/*if (Input.GetKeyUp("f"))
				isFlat = !isFlat;

			if (Input.GetKeyUp("f"))
				EnablePointCloud(true);

			if (Input.GetKeyUp("g"))
				EnablePointCloud(false);
			*/

		}


	}

	private void RefreshPointCloud(ushort[] depthData)
	{
		Vector3 tempPoint = new Vector3();


		//int lineWidth = _lblTex.width / _DownsampleSize;
		_lblTex = KinectManager.Instance.GetUsersLblTex2D();



		for (int y = 0; y < _lblTex.height; y += _DownsampleSize)
		{
			for (int x = 0; x < _lblTex.width; x += _DownsampleSize)
			{
				int indexX = x / _DownsampleSize;
				int indexY = y / _DownsampleSize;
				int smallIndex = (indexY * (_lblTex.width / _DownsampleSize)) + indexX;



				tempPoint = _pcPoints[smallIndex].position;
				int fullIndex = (y * _lblTex.width) + x;
				bool isUser = false;

				if (!isFlat && _lblTex.GetPixel(x, y).a == 1)
				{
					tempPoint.z = (float)(depthData[fullIndex] * _DepthScale);
					isUser = true;
				}
				else
				{
					tempPoint.z = 0f;
				}
				_pcPoints[smallIndex].position  = tempPoint;

				_pcPoints[smallIndex].remainingLifetime = 1f; //keep particle alive


				float DepthIndex = depthData[fullIndex] / 4200f;
				//float DepthIndex2 = (DepthIndex % 256f)/300f;

				//if (DepthIndex2 < 0.1f || DepthIndex2 > 1f)
				if (!isFlat)
				{
					if (DepthIndex < 0.1f || DepthIndex > 1f || !isUser)
					{
						_pcPoints[smallIndex].color = new Color(0, 0, 0, 0);
					}
					else
					{
						_pcPoints[smallIndex].color = new Color(DepthIndex * DepthIndex + 0.2f, DepthIndex, 1f - DepthIndex);
						//_pcPoints[smallIndex].color = new Color(DepthIndex2, 1f - DepthIndex2, DepthIndex2+0.2f);
						//_pcPoints[smallIndex].color = new Color(DepthIndex2, 0, 1f - DepthIndex2);
					}
				}
				else
				{
					_pcPoints[smallIndex].color = Color.green;
				}

			}
		}

		_pointCloud.SetParticles(_pcPoints, _pcPoints.Length);

	}

	private void KillParticles()
	{

	}

	/*void OnGUI()
	{
		//KinectManager.Instance.GetUsersLblTex()
		//sensorData.bodyIndexTexture
		//GUI.DrawTexture(new Rect(30, 30, 120, 80), KinectManager.Instance.GetUsersLblTex());
	}
	*/



}
