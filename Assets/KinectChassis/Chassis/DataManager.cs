using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Windows;
using System.IO;


public class DataManager : MonoBehaviour
{
	public GameObject UDPObject;
	public PointCloud pc;
	public Text text;
	public Text debugText1;	
	//private UDPReceive udpRec;
	private UDPSend udpSend;
	public Texture2D receivedTex;
	
	string ip = "127.0.0.1";
	private DataType remoteDataType = DataType.None;
	private DataType localDataType = DataType.None;
	private string stringData;
	private string lastStringData = "";
	
	private BodyJointsLocal jointsLocal = null;
	private BodyJointsRemote jointsRemote = null;
	private bool isBodyJointsRemoteAvailable = false;
	private int emptyDataTimeout = 6;

	void Start ()
	{		
		//udpRec = UDPObject.GetComponent<UDPReceive> ();
		udpSend = UDPObject.GetComponent<UDPSend> ();
		receivedTex = new Texture2D(510, 424, TextureFormat.RGBA32, false, true);
		receivedTex.filterMode = FilterMode.Point;
		jointsLocal = new BodyJointsLocal();
		jointsRemote = new BodyJointsRemote();
		
		
		ip = CEO.Instance.GetIP();
		Debug.Log(ip);

	}

	public enum DataType
	{
		None = 0,
		Depth,
		Color_RGB,
		Skeleton,
		Depth_People,
		Depth_Hands
	}

	public BodyJointsLocal GetBodyJointsLocal()
	{
		return jointsLocal;
	}

	public BodyJointsRemote GetBodyJointsRemote()
	{
		return jointsRemote;
	}

	public void SetKaptIP(string kip)
	{
		udpSend.Setup(kip, 7777);
		//Debug.Log("Set Kapt IP: " + kip);
	}

	public void RequestColor()
	{
		udpSend.sendString(ip + " " + "color");
		remoteDataType = DataType.Color_RGB;
		Debug.Log("Sent Color Request to " + ip);
	}

	public void RequestDepth()
	{
		udpSend.sendString(ip + " " + "depth");
		remoteDataType = DataType.Depth;
		Debug.Log("Sent Depth Request to " + ip);
	}

	public void RequestPeople()
	{
		udpSend.sendString(ip + " " + "people");
		remoteDataType = DataType.Depth_People;
		Debug.Log("Sent People Request to " + ip);
	}

	public void RequestStop()
	{
		udpSend.sendString(ip + " " + "stop");
		remoteDataType = DataType.None;
		Debug.Log("Sent Stop Request");
	}

	public void RequestJoints()
	{
		//udpSend.sendString(ip + " " + "joints");
		remoteDataType = DataType.Skeleton;
		//Debug.Log("Sent Joints Request to " + ip);
	}

	public Texture2D LoadPNG(string filePath)
	{

		Texture2D tex = null;
		byte[] fileData;

		if (System.IO.File.Exists(filePath))     {
			fileData = System.IO.File.ReadAllBytes(filePath);
			//tex = new Texture2D(2, 2, TextureFormat.RGBA32, false, true);
			tex.LoadImage(fileData);
		}
		return tex;
	}

	public bool IsBodyJointsRemoteAvailable()
	{
		return isBodyJointsRemoteAvailable;
	}

	public string GetStringData()
	{
		return stringData;
	}	
	

	public void ReceiveData()
	{
		byte[] data = UDPReceive.Instance.getLatestUDPPacket();		
		

		//if (remoteDataType == DataType.Skeleton && data != null)
		if (data != null)
		{
			stringData = null;
			try
			{
				this.stringData = Encoding.UTF8.GetString(data);
				
				bool isEmptyArray = false;
				bool nobodyThere = false;
				if (stringData == "[]")
				{					
					isEmptyArray = true;
					emptyDataTimeout--;
					if (emptyDataTimeout <=0)
					{
						nobodyThere = true;
						isEmptyArray = false;
						emptyDataTimeout = -3;
					}
				}
				else
				{
					emptyDataTimeout = 6;
				}

				

				if (stringData != null && stringData != lastStringData && !isEmptyArray)
				{
					isBodyJointsRemoteAvailable = jointsRemote.SetJsonData(stringData);	
					//Debug.Log(stringData);						
				}

				lastStringData = stringData;
			}
			catch (Exception e)
			{
				Debug.Log( e.ToString());
			}
		}
		else if (remoteDataType != DataType.None)
		{
			receivedTex.LoadImage(data);
		}

		if (receivedTex != null && (remoteDataType == DataType.Depth || remoteDataType == DataType.Depth_People)) pc.SetDepthTex(receivedTex);

		
	}

	void Update ()
	{

	}

	/*void OnGUI()
	{
		if (CEO.Instance.showReceivedTexture)
		GUI.DrawTexture(new Rect(30, 30, 120, 80), receivedTex);
	}
	*/
}
