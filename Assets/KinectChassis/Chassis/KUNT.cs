using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//KAPT Universal Node Translator
public class KUNT
{
	private Vector3 zeroPos = new Vector3(0f, 0f, 0f);
	private Vector3 displacement = new Vector3(0f, 0f, 0f);
	private float worldScale = 50f;
	private float alpha = 0f;
	private float phi = 0f;
	private float side = 1f;


	//skeleton indexes
	private int indexLeftHand = 7;
	private int indexRightHand = 11;
	private int indexLeftFoot = 14;
	private int indexRightFoot = 18;
	//private int indexLeftFoot = 7;
	//private int indexRightFoot = 11;
	private float zSide = 1f;
	private float rotate180 = 1f;

	private bool isFloor = false;
	private bool isSide = false;
	private bool areValuesAssigned = false;

	public void SetIsLocal(bool _isLocal)
	{
		isFloor = _isLocal;
	}

	public void SetIsSide(bool _isSide)
	{
		isSide = _isSide;
	}

	public void AssignSavedValues()
	{
		zeroPos = new Vector3(SaveLoad.savedSettings.zeroPosX, SaveLoad.savedSettings.zeroPosY, SaveLoad.savedSettings.zeroPosZ);
		displacement = new Vector3(SaveLoad.savedSettings.displacementX, SaveLoad.savedSettings.displacementY, SaveLoad.savedSettings.displacementZ);
		worldScale = SaveLoad.savedSettings.worldScale;
		alpha = SaveLoad.savedSettings.alpha;
		phi = SaveLoad.savedSettings.phi;
		side = SaveLoad.savedSettings.side;
		zSide = SaveLoad.savedSettings.zSide;
		rotate180 = SaveLoad.savedSettings.rotate180;

		if (isSide)
		{
			AssignSavedValuesSide();
		}

		areValuesAssigned = true;
	}

	public void AssignSavedValuesSide()
	{
		zeroPos = new Vector3(SaveLoad.savedSettings.zeroPosXS, SaveLoad.savedSettings.zeroPosYS, SaveLoad.savedSettings.zeroPosZS);
		displacement = new Vector3(SaveLoad.savedSettings.displacementXS, SaveLoad.savedSettings.displacementYS, SaveLoad.savedSettings.displacementZS);
		worldScale = SaveLoad.savedSettings.worldScaleS;
		alpha = SaveLoad.savedSettings.alphaS;
		phi = SaveLoad.savedSettings.phiS;
		side = SaveLoad.savedSettings.sideS;
		zSide = SaveLoad.savedSettings.zSideS;
		rotate180 = SaveLoad.savedSettings.rotate180S;
	}

	public void SetSettings()
	{
		if (isSide)
		{
			SetSettingsSide();
		}
		else if (isFloor)
		{
			SaveLoad.savedSettings.zeroPosX = zeroPos.x;
			SaveLoad.savedSettings.zeroPosY = zeroPos.y;
			SaveLoad.savedSettings.zeroPosZ = zeroPos.z;
			SaveLoad.savedSettings.displacementX = displacement.x;
			SaveLoad.savedSettings.displacementY = displacement.y;
			SaveLoad.savedSettings.displacementZ = displacement.z;
			SaveLoad.savedSettings.worldScale = worldScale;
			SaveLoad.savedSettings.alpha = alpha;
			SaveLoad.savedSettings.phi = phi;
			SaveLoad.savedSettings.side = side;
			SaveLoad.savedSettings.zSide = zSide;
			SaveLoad.savedSettings.rotate180 = rotate180;
		}
	}

	public void SetSettingsSide()
	{
		SaveLoad.savedSettings.zeroPosXS = zeroPos.x;
		SaveLoad.savedSettings.zeroPosYS = zeroPos.y;
		SaveLoad.savedSettings.zeroPosZS = zeroPos.z;
		SaveLoad.savedSettings.displacementXS = displacement.x;
		SaveLoad.savedSettings.displacementYS = displacement.y;
		SaveLoad.savedSettings.displacementZS = displacement.z;
		SaveLoad.savedSettings.worldScaleS = worldScale;
		SaveLoad.savedSettings.alphaS = alpha;
		SaveLoad.savedSettings.phiS = phi;
		SaveLoad.savedSettings.sideS = side;
		SaveLoad.savedSettings.zSideS = zSide;
		SaveLoad.savedSettings.rotate180S = rotate180;
	}



	public void SetZSide(float zside)
	{
		zSide = zside;
	}

	public void Flip180()
	{
		rotate180 = -rotate180;
	}

	public float GetRotate180()
	{
		return rotate180;
	}

	public void Calibrate(Vector3[] skeletonJoints, int KINOId)
	{
		//determine closer hand
		bool isLeftCloser = false;
		if (skeletonJoints[indexLeftHand].z < skeletonJoints[indexRightHand].z)
		{
			isLeftCloser = true;
		}

		Vector3 ZAxle = new Vector3 (0f, 0f, 1f);
		Vector3 AlphaVector = new Vector3(0f, (skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]).y, (skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]).z);
		alpha = Vector3.Angle(AlphaVector, ZAxle);

		Vector3 PhiVector = new Vector3((skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]).x, 0f, (skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]).z);
		phi = Vector3.Angle(PhiVector, ZAxle);

		side = -1;
		if (!isLeftCloser)
		{
			alpha = -alpha;
			phi = -phi;

			side = 1;
		}

		//zeroPos = (skeletonJoints[indexLeftFoot] - skeletonJoints[indexRightFoot]) / 2f + skeletonJoints[indexRightFoot];
		//zeroPos = (skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]) / 2f + skeletonJoints[indexRightHand];
		zeroPos = skeletonJoints[0];

		/*if(KINOId == 0)
		{
			//then it's the floor
			SaveLoadKapt.savedCallibration.floorAlpha = alpha;
			SaveLoadKapt.savedCallibration.floorPhi = phi;
			SaveLoadKapt.savedCallibration.floorZeroPosX = zeroPos.x;
			SaveLoadKapt.savedCallibration.floorZeroPosY = zeroPos.y;
			SaveLoadKapt.savedCallibration.floorZeroPosZ = zeroPos.z;
		}

		SaveLoadKapt.Save();*/
	}

	public void CalibrateSide(Vector3[] skeletonJoints, int KINOId)
	{
		//determine closer hand
		bool isLeftCloser = false;
		if (skeletonJoints[indexLeftHand].z < skeletonJoints[indexRightHand].z)
		{
			isLeftCloser = true;
		}

		Vector3 ZAxle = new Vector3 (0f, 0f, 1f);
		Vector3 AlphaVector = new Vector3(0f, (skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]).y, (skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]).z);
		alpha = Vector3.Angle(AlphaVector, ZAxle);

		Vector3 PhiVector = new Vector3((skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]).x, 0f, (skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]).z);
		phi = Vector3.Angle(PhiVector, ZAxle);

		side = -1;
		if (!isLeftCloser)
		{
			alpha = -alpha;
			phi = -phi;

			side = 1;
		}

		//zeroPos = (skeletonJoints[indexLeftFoot] - skeletonJoints[indexRightFoot]) / 2f + skeletonJoints[indexRightFoot];
		//zeroPos = (skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]) / 2f + skeletonJoints[indexRightHand];
		zeroPos = skeletonJoints[0];
		SetSettings();


	}

	public void CalibrateFeet(Vector3[] skeletonJoints, Vector3 LeftFootPos, Vector3 RightFootPos)
	{
		//determine closer foot
		bool isLeftCloser = false;
		if (skeletonJoints[indexLeftFoot].z < skeletonJoints[indexRightFoot].z)
		{
			isLeftCloser = true;
		}

		worldScale = (RightFootPos - LeftFootPos).magnitude / (skeletonJoints[indexLeftFoot] - skeletonJoints[indexRightFoot]).magnitude;
		//Debug.Log("worldScale: " + worldScale);

		Vector3 ZAxle = new Vector3 (0f, 0f, 1f);
		Vector3 AlphaVector = new Vector3(0f, (skeletonJoints[indexLeftFoot] - skeletonJoints[indexRightFoot]).y, (skeletonJoints[indexLeftFoot] - skeletonJoints[indexRightFoot]).z);
		alpha = Vector3.Angle(AlphaVector, ZAxle);

		Vector3 PhiVector = new Vector3((skeletonJoints[indexLeftFoot] - skeletonJoints[indexRightFoot]).x, 0f, (skeletonJoints[indexLeftFoot] - skeletonJoints[indexRightFoot]).z);
		phi = Vector3.Angle(PhiVector, ZAxle) - 90f  + 180f * (rotate180 / 2f + 0.5f);

		side = 1;
		if (!isLeftCloser)
		{
			alpha = -alpha;
			phi = -phi;

			side = -1;
		}

		//zeroPos = (skeletonJoints[indexLeftFoot] - skeletonJoints[indexRightFoot]) / 2f + skeletonJoints[indexRightFoot];
		//zeroPos = (skeletonJoints[indexLeftHand] - skeletonJoints[indexRightHand]) / 2f + skeletonJoints[indexRightHand];
		zeroPos = (skeletonJoints[indexLeftFoot] + skeletonJoints[indexRightFoot]) / 2f;
		displacement = (RightFootPos + LeftFootPos) / 2f;

		if (isFloor) SetSettings();

	}

	public void SetSide(float sd)
	{
		side = sd;
	}

	public Vector3[] Kino2World(Vector3[] kinoCoords, int KINOId)
	{
		if (kinoCoords != null) {
			int numOfCoords = kinoCoords.Length;
			Vector3[] worldCoords = new Vector3[numOfCoords];
			for (int i = 0; i < numOfCoords; i ++) {
				worldCoords [i] = Kino2World (kinoCoords [i], KINOId);
			}
			return worldCoords;
		}
		return null;
	}

	public Vector3 Kino2World(Vector3 kinoCoords, int KINOId)
	{
		if (!areValuesAssigned)
		{
			AssignSavedValues();
			//if (isSide) AssignSavedValuesSide();
		}
		/*if(KINOId == 0)
		{
			//then it's the floor
			alpha = SaveLoadKapt.savedCallibration.floorAlpha;
			phi = SaveLoadKapt.savedCallibration.floorPhi;
			zeroPos = new Vector3(SaveLoadKapt.savedCallibration.floorZeroPosX, SaveLoadKapt.savedCallibration.floorZeroPosY, SaveLoadKapt.savedCallibration.floorZeroPosZ);
		}*/

		kinoCoords -= zeroPos;
		float x = kinoCoords.x;
		float y = kinoCoords.y;
		float z = kinoCoords.z;

		//Calculation of the trigonometric components of the rotation

		//phi
		float cp = Mathf.Cos(phi * Mathf.Deg2Rad);
		float sp = Mathf.Sin(phi * Mathf.Deg2Rad);

		//alpha
		float ca = Mathf.Cos(alpha * Mathf.Deg2Rad);
		float sa = Mathf.Sin(alpha * Mathf.Deg2Rad);

		float x2 = x * cp + y * sa * sp - z * ca * sp;
		float y2 = y * ca + z * sa;
		float z2 = x * sp - y * sa * cp + z * ca * cp;


		Vector3 worldCoords = new Vector3(-x2 * side , y2 , z2 * side * zSide);
		return worldCoords * worldScale + displacement;
	}
}