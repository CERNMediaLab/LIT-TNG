using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Windows;
using System.IO;
using Newtonsoft.Json;

public class BodyJointsRemote
{
	private List<Vector3[]> deserializedData;
	private int posUserId = 25;
	private string jsonData;


	public void Init()
	{
		deserializedData = new List<Vector3[]>();
	}

	public bool SetJsonData(string newJsonData)
	{
		jsonData = newJsonData;
		bool isDataSerialized = false;

		//string jt = "No joints to show";
		try
		{

			deserializedData = deserializeSkeletonData();

			/*
			 jt = "Number of joints: " + deserializedData.Count;

			for (int i = 0; i < deserializedData.Count; i ++)
			{
				jt +=  "    " + deserializedData[i][7] + "    "  + deserializedData[i][11] ;
			}

			debugText.text = jt;
			*/

		}
		catch (Exception err)
		{
			deserializedData.Clear();
		}
		//Debug.Log("deserialized"); 
		
		if (deserializedData != null)
		{
			isDataSerialized = true;
		}

		return isDataSerialized;
	}

	public int GetNumberOfSkeletons()
	{
		if (deserializedData != null)
		{
			return deserializedData.Count;
		}
		return -1;
	}

	public List<int> GetSkeletonIds()
	{
		List<int> ids = new List<int>();
		if (deserializedData != null)
		{
			for (int i = 0; i < deserializedData.Count; i ++)
			{
				ids.Add(Convert.ToInt32(((Vector3) deserializedData[i][posUserId]).x));
			}
		}
		return ids;
	}

	public List<Vector3> GetAllUsersHands()
	{
		List<Vector3> allUsersHands = new List<Vector3>();

		for (int i = 0; i < deserializedData.Count; i ++)
		{			
			Vector3 posLeftHand = deserializedData[i][7];
			Vector3 posRightHand = deserializedData[i][11];
			allUsersHands.Add(posLeftHand);
			allUsersHands.Add(posRightHand);
		}
		

		return allUsersHands;
	}

	public List<Vector3> GetAllUsersFeet()
	{
		List<Vector3> allUsersFeet = new List<Vector3>();

		for (int i = 0; i < deserializedData.Count; i ++)
		{			
			Vector3 posLeftFoot = deserializedData[i][14];
			Vector3 posRightFoot = deserializedData[i][18];
			allUsersFeet.Add(posLeftFoot);
			allUsersFeet.Add(posRightFoot);
		}

		return allUsersFeet;
	}

	public Vector2 GetHandStatesForPerson(int personId)
	{
		if (deserializedData != null)
		{
			//find person with personId
			for (int i = 0; i < deserializedData.Count; i ++)
			{
				if ((((Vector3) deserializedData[i][posUserId]).x) == personId)
				{
					return new Vector2 (deserializedData[i][posUserId].y, deserializedData[i][posUserId].z) ;
				}
			}
		}
		return new Vector3(-1, -1, -1);
	}

	public List<Vector3[]> GetAllSkeletonData()
	{
		return deserializedData;
	}

	public Vector3[] GetPersonSkeletonData(int personId)
	{
		if (deserializedData != null)
		{
			//find person with personId
			for (int i = 0; i < deserializedData.Count; i ++)
			{
				if ((((Vector3) deserializedData[i][posUserId]).x) == personId)
				{
					return deserializedData[i];
				}
			}
		}
		return null;
	}

	public Vector3 GetPersonSkeletonData(int personId, int jointID)
	{
		if (deserializedData != null)
		{
			//find person with personId
			for (int i = 0; i < deserializedData.Count; i ++)
			{
				if ((((Vector3) deserializedData[i][posUserId]).x) == personId)
				{
					return deserializedData[i][jointID];
				}
			}
		}
		return new Vector3(-1, -1, -1);
	}

	public List<Vector3[]> deserializeSkeletonData()
	{
		if (jsonData != null)
		{
			//deserialize
			JsonSerializerSettings settings = new JsonSerializerSettings
			{
				Formatting = Formatting.None,
				FloatParseHandling = FloatParseHandling.Double
			};
			return JsonConvert.DeserializeObject<List<Vector3[]>>(jsonData, settings);
		}
		return null;
	}

}
