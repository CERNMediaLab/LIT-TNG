﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Windows.Kinect;
using System.Linq;
using Newtonsoft.Json;

public class JointEx
{
	public Windows.Kinect.Joint Joint
	{
		get;
		set;
	}
	public ColorSpacePoint ColorSpacePos
	{
		get;
		set;
	}
};

public class KinOps : MonoBehaviour
{

	protected static KinOps instance = null;

	private KinectSensor _Sensor;
	private MultiSourceFrameReader multiReader = null;
	private MultiSourceFrame frameMulti = null;
	private ushort[] _Data;
	private byte[] _RawData;
	private Texture2D _Texture;
	private string previousCapture = "";

	static Body[] bodies;
	static List<Body> trackedBodies = new List<Body>();
	static List<Dictionary<JointType, JointEx>> mappedJoints = new List<Dictionary<JointType, JointEx>>();
	static Dictionary<ulong, int> idDictionary = new Dictionary<ulong, int>();
	private int currentID = 0;
	private int bodyCount = 0;
	private Byte[] jpgPeople;



	//KAPT
	const int NumJoints = 25;
	//static float [] depthTempPoints = new float [NumJoints];
	static List<CameraSpacePoint[]> bodyTransferData = new List<CameraSpacePoint[]>();


	public string lastBodyData = "";

	void Awake()
	{
		instance = this;
	}

	public static KinOps Instance
	{
		get
		{
			return instance;
		}
	}

	void Start ()
	{
		_Sensor = KinectSensor.GetDefault();
		if (_Sensor != null)
		{
			multiReader = _Sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body | FrameSourceTypes.BodyIndex);



			if (!_Sensor.IsOpen)
			{
				_Sensor.Open();
			}
		}

	}

	public Texture2D GetTexture_IR()
	{
		if (previousCapture != "IR")
		{
			var frameDesc = _Sensor.InfraredFrameSource.FrameDescription;
			_Data = new ushort[frameDesc.LengthInPixels];
			_RawData = new byte[frameDesc.LengthInPixels * 4];
			_Texture = new Texture2D(frameDesc.Width, frameDesc.Height, TextureFormat.BGRA32, false);
			previousCapture = "IR";
		}

		if (multiReader != null)
		{
			//frameMulti = multiReader.AcquireLatestFrame();
			if (frameMulti != null)
			{
				var frame = frameMulti.InfraredFrameReference.AcquireFrame();
				if (frame != null)
				{
					frame.CopyFrameDataToArray(_Data);

					int index = 0;
					foreach (var ir in _Data)
					{
						byte intensity = (byte)(ir >> 8);
						_RawData[index++] = intensity;
						_RawData[index++] = intensity;
						_RawData[index++] = intensity;
						_RawData[index++] = 255; // Alpha
					}

					_Texture.LoadRawTextureData(_RawData);
					_Texture.Apply();

					frame.Dispose();
					frame = null;
				}
			}

		}
		return _Texture;
	}

	public Texture2D GetTexture_Color()
	{
		if (previousCapture != "Color")
		{
			var frameDesc = _Sensor.ColorFrameSource.FrameDescription;
			_Data = new ushort[frameDesc.LengthInPixels];
			_RawData = new byte[frameDesc.LengthInPixels * 4];
			_Texture = new Texture2D(frameDesc.Width, frameDesc.Height, TextureFormat.RGBA32, false);
			previousCapture = "Color";
		}

		if (multiReader != null)
		{
			//var frameMulti = multiReader.AcquireLatestFrame();
			if (frameMulti != null)
			{
				var frame = frameMulti.ColorFrameReference.AcquireFrame();
				if (frame != null)
				{
					frame.CopyConvertedFrameDataToArray(_RawData, ColorImageFormat.Rgba);


					_Texture.LoadRawTextureData(_RawData);
					_Texture.Apply();

					frame.Dispose();
					frame = null;
				}
			}
		}

		return _Texture;
	}

	public Texture2D GetTexture_Depth()
	{
		if (previousCapture != "Depth")
		{
			var frameDesc = _Sensor.DepthFrameSource.FrameDescription;
			_Data = new ushort[frameDesc.LengthInPixels];
			_RawData = new byte[frameDesc.LengthInPixels * 4];
			_Texture = new Texture2D(frameDesc.Width, frameDesc.Height, TextureFormat.RGBA32, false);
			previousCapture = "Depth";
		}

		if (multiReader != null)
		{
			//var frameMulti = multiReader.AcquireLatestFrame();
			if (frameMulti != null)
			{
				var frame = frameMulti.DepthFrameReference.AcquireFrame();
				if (frame != null)
				{
					SetDepthPixels(frame);

					_Texture.LoadRawTextureData(_RawData);
					_Texture.Apply();

					frame.Dispose();
					frame = null;
				}
			}
		}

		return _Texture;
	}



	public void SetDepthPixels(DepthFrame frame)
	{
		int MapDepthToByte = 4000 / 256;
		int width = frame.FrameDescription.Width;
		int height = frame.FrameDescription.Height;


		//ushort minDepth = frame.DepthMinReliableDistance;
		//ushort maxDepth = frame.DepthMaxReliableDistance;

		int minDepth = 500;
		int maxDepth = 1000;

		ushort[] pixelData = new ushort[width * height];
		//byte[] pixels = new byte[width * height * 3];

		frame.CopyFrameDataToArray(pixelData);

		int colorIndex = 0;
		for (int depthIndex = 0; depthIndex < pixelData.Length; ++depthIndex)
		{
			ushort depth = pixelData[depthIndex];


			byte Blue = (byte)(depth >> 8);
			byte Green = (byte)(depth);
			byte Red = (byte)(0);



			_RawData[colorIndex++] = Blue; // Blue
			_RawData[colorIndex++] = Green; // Green
			_RawData[colorIndex++] = Red; // Red
			_RawData[colorIndex++] = 255; // Alpha

			//++colorIndex;
		}

		//return pixels;
	}

	public Texture2D GetTexture_People(bool displaySkeletonLines)
	{
		if (previousCapture != "People")
		{
			previousCapture = "People";
		}

		_Texture = KinectManager.Instance.GetUsersLblTex2D();
		if (displaySkeletonLines)
		{
			for (int i = 0; i < KinectManager.Instance.GetUsersCount(); i++)
			{
				Int64 liUserId = KinectManager.Instance.GetUserIdByIndex(i);
				KinectInterop.BodyData bdata = KinectManager.Instance.GetUserBodyData(liUserId);

				DrawSkeleton(_Texture, ref bdata);
			}
		}

		_Texture.Apply();

		return _Texture;
	}

	public byte[] GetJpegPeople()
	{
		jpgPeople = GetTexture_People(false).EncodeToJPG(20);
		//jpgPeople = null;
		//Debug.Log(jpgPeople.Length);
		return jpgPeople;
	}

	private void DrawSkeleton(Texture2D aTexture, ref KinectInterop.BodyData bodyData)
	{
		try
		{
			int jointsCount = KinectManager.Instance.GetJointCount();

			if (jointsCount != null)
			{
				for (int i = 0; i < jointsCount; i++)
				{
					int parent = (int)KinectManager.Instance.GetParentJoint((KinectInterop.JointType)i);

					if (parent != null && bodyData.joint[i].trackingState != null && bodyData.joint[i].trackingState != KinectInterop.TrackingState.NotTracked &&
					        bodyData.joint[parent].trackingState != KinectInterop.TrackingState.NotTracked)
					{
						Vector2 posParent = KinectManager.Instance.MapSpacePointToDepthCoords(bodyData.joint[parent].kinectPos);
						Vector2 posJoint = KinectManager.Instance.MapSpacePointToDepthCoords(bodyData.joint[i].kinectPos);

						if (posParent != Vector2.zero && posJoint != Vector2.zero && aTexture != null)
						{
							//Color lineColor = playerJointsTracked[i] && playerJointsTracked[parent] ? Color.red : Color.yellow;
							KinectInterop.DrawLine(aTexture, (int)posParent.x, (int)posParent.y, (int)posJoint.x, (int)posJoint.y, Color.white);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			Debug.Log("DrawSkeleton error: " + e);
		}

	}

	public string GetSkeletons2()
	{
		string bodyData = null;
		if (previousCapture != "Skeleton")
		{
			previousCapture = "Skeleton";
		}

		List<KinectInterop.BodyData> allBodyData = new List<KinectInterop.BodyData>();

		for (int i = 0; i < KinectManager.Instance.GetUsersCount(); i++)
		{
			Int64 liUserId = KinectManager.Instance.GetUserIdByIndex(i);
			KinectInterop.BodyData bdata = KinectManager.Instance.GetUserBodyData(liUserId);

			allBodyData.Add(bdata);

		}

		bodyData = SerialiseBodyData2(allBodyData);

		return bodyData;
	}

	private string SerialiseBodyData2(List<KinectInterop.BodyData> bodyData)
	{

		if (KinectManager.Instance.GetUsersCount() < 1)
			return null;

		bodyTransferData.Clear();
		foreach (var body in bodyData)
		{

			int jointsCount = KinectManager.Instance.GetJointCount();
			//List<Vector3> list = new List<Vector3>();
			CameraSpacePoint[] posJoint = new CameraSpacePoint[jointsCount + 1];

			for (int i = 0; i < jointsCount; i++)
			{
				posJoint[i].X = body.joint[i].kinectPos.x;
				posJoint[i].Y = body.joint[i].kinectPos.y;
				posJoint[i].Z = body.joint[i].kinectPos.z;



			}

			CameraSpacePoint idLeftRight = new CameraSpacePoint();
			idLeftRight.X = (float)body.liTrackingID;
			idLeftRight.Y = (float)body.leftHandState;
			idLeftRight.Z = (float)body.rightHandState;


			posJoint[jointsCount] = idLeftRight;


			//Vector3[] array = list.ToArray();

			bodyTransferData.Add(posJoint);


		}

		//var str = JsonConvert.SerializeObject(bodyTransferData);

		var str = JsonConvert.SerializeObject(bodyTransferData, Formatting.None,
		                                      new JsonSerializerSettings()
		{
			ReferenceLoopHandling = ReferenceLoopHandling.Ignore
		});

		return str;
	}



	public string GetSkeletons()
	{
		string bodyData = null;
		if (previousCapture != "Skeleton")
		{
			previousCapture = "Skeleton";
		}

		if (multiReader != null)
		{
			//var frameMulti = multiReader.AcquireLatestFrame();
			if (frameMulti != null)
			{
				var bodyFrame = frameMulti.BodyFrameReference.AcquireFrame();
				if (bodyFrame != null)
				{
					int newBodyCount = bodyFrame.BodyFrameSource.BodyCount;
					if (newBodyCount != bodyCount)
					{
						bodies = new Body[bodyFrame.BodyFrameSource.BodyCount];
					}

					bodyCount = newBodyCount;

					bodyFrame.GetAndRefreshBodyData(bodies);



					bodyData = SerialiseBodyData();

					bodyFrame.Dispose();
					bodyFrame = null;
				}
			}
		}

		return bodyData;
	}

	private string SerialiseBodyData()
	{
		trackedBodies = bodies.Where(b => b.IsTracked == true).ToList();
		if (trackedBodies.Count() < 1)
			return null;

		bodyTransferData.Clear();
		foreach (var body in trackedBodies)
		{
			List<CameraSpacePoint> list = (List<CameraSpacePoint>)(body.Joints.Select(j => j.Value).Select(p => p.Position).ToList());
			CameraSpacePoint idLeftRight = new CameraSpacePoint();
			float bodyId = GetUserID(body.TrackingId);
			//this.StatusText = (bodyId).ToString();
			idLeftRight.X = bodyId;
			idLeftRight.Y = (float)body.HandLeftState;
			idLeftRight.Z = (float)body.HandRightState;

			list.Add(idLeftRight);

			CameraSpacePoint bodyPosition = new CameraSpacePoint();


			CameraSpacePoint[] array = list.ToArray();
			//this.kinectSensor.CoordinateMapper.MapCameraPointsToDepthSpace (list, depthTempPoints);
			bodyTransferData.Add(array);

		}

		var str = JsonConvert.SerializeObject(bodyTransferData);
		return str;
	}

	public int GetUserID(ulong uid)
	{
		int gotid = -1;
		if (idDictionary.ContainsKey(uid))
		{
			gotid = idDictionary[uid];
		}
		else
		{
			currentID++;
			idDictionary.Add(uid, currentID);
			gotid = currentID;
		}

		return gotid;

	}


	void Update ()
	{
		frameMulti = multiReader.AcquireLatestFrame();

	}

	public void OnApplicationQuit()
	{

		if (multiReader != null)
		{
			multiReader.Dispose();
			multiReader = null;
		}

		if (_Sensor != null)
		{
			if (_Sensor.IsOpen)
			{
				_Sensor.Close();
			}

			_Sensor = null;
		}
	}
}
