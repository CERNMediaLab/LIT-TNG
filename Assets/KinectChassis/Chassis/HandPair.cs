﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandPair : MonoBehaviour
{

	public GameObject handMarker;
	public GameObject clapMarker;
	private Hand[] hands;

	private LineRenderer line;
	private GUIStyle guiStyle = new GUIStyle();
	private bool showAngle = false;
	private float angle;
	private float distanceSpeed = 0f;
	private float handDistance = 0f;
	private float handDistance1 = 0f;
	private float handDistance2 = 0f;
	private float startCollapseDistance = 0f;
	private float speedTrend = 0f;
	private long userID = -1;
	bool clap = false;
	bool clapLock = false;
	private float clapSensitivity = 1.5f;
	
	private bool canSwipe = true;
	private float swipeSpeedThereshold = 55f;
	private bool swipeRight = false;
	private bool swipeLeft = false;
	private float lockTimer = 0.8f;
	private bool showDebugInfo = false;



	void Start ()
	{
		GameObject leftMarker = Instantiate(handMarker, Vector3.zero, Quaternion.identity);
		Hand leftHand = leftMarker.GetComponent( typeof(Hand) ) as Hand;
		GameObject rightMarker = Instantiate(handMarker, Vector3.zero, Quaternion.identity);
		Hand rightHand = rightMarker.GetComponent( typeof(Hand) ) as Hand;

		leftMarker.transform.parent = this.transform;
		rightMarker.transform.parent = this.transform;


		hands = new Hand[2];
		hands[0] = leftHand;
		hands[1] = rightHand;

		hands[0].SetIsVertical(HandPairManager.Instance.GetIsVertical());
		hands[1].SetIsVertical(HandPairManager.Instance.GetIsVertical());


		line = this.gameObject.AddComponent<LineRenderer>();
		line.SetWidth(0.5F, 0.5F);
		line.SetVertexCount(2);
		line.material = new Material(Shader.Find("Particles/Additive"));
		line.SetColors(Color.green, Color.green);

		Debug.Log("Created Hand Pair" + "  " + HandPairManager.Instance.GetIsVertical());

	}

	public void UpdateHands(HandParams[] handData)
	{
		try
		{
			userID = handData[0].UID;
			hands[0].SetParameters(handData[0]);
			hands[1].SetParameters(handData[1]);
		}
		catch
		{
			Debug.Log("Failed to update hands");
		}
	}


	public void SetUserID(long uid)
	{
		userID = uid;
		Debug.Log("Setting ID: " + uid);
	}

	public long GetUserID()
	{
		return userID;
	}

	public Vector3 GetMiddlePoint()
	{
		return ((hands[0].GetPosition() + hands[1].GetPosition()) / 2f);
	}

	public Vector3 GetMiddlePoint3D()
	{
		return ((hands[0].GetPosition3D() + hands[1].GetPosition3D()) / 2f);
	}

	public Vector3 GetMiddlePointAdjusted()
	{
		return ((hands[0].GetAdjustedPosition() + hands[1].GetAdjustedPosition()) / 2f);
	}

	public float GetAngle()
	{
		return ( -(Vector3.Angle((hands[0].GetPosition() - hands[1].GetPosition()) , Vector3.left ) - 90f));
	}

	public float GetHandDistance3D()
	{
		return Vector3.Distance(hands[0].GetPosition3D(), hands[1].GetPosition3D());
	}

	public void CalculateSpeed()
	{
		float timedLerp = Time.deltaTime / 0.3f;

		float dif = (handDistance - handDistance1) * 100;

		speedTrend = Mathf.Lerp(speedTrend, dif, timedLerp);

		float newSpeed = speedTrend - dif;
		distanceSpeed = Mathf.SmoothStep(distanceSpeed, newSpeed, Time.deltaTime / 0.05f );
	}

	private void DetectClap()
	{
		if (handDistance < 5f * clapSensitivity && distanceSpeed < -35f / clapSensitivity && !clapLock)
		{
			clapLock = true;
			clap = true;
			//Debug.Log("CLAP");
			GameObject clapObject = Instantiate(clapMarker, GetMiddlePoint(), Quaternion.identity);

			Invoke("UnlockClap", 0.5f);
		}
	}

	public bool GetDidClap()
	{
		return clap;
	}

	public void ResetClap()
	{
		clap = false;
	}

	private void UnlockClap()
	{
		clapLock = false;
	}

	private void DetectSwipe()
	{
		if (canSwipe)
		{
			DetectSwipeLeftHand();
		}

		if(canSwipe)
		{
			DetectSwipeRightHand();
		}
	}

	private float GetForearmAngleLeft()
	{
		Vector3 handElbow = GetJointIdPosition(7) - GetJointIdPosition(5);
		float pointingAngle = Vector3.Angle(handElbow, new Vector3(1, 0, 0));
		return pointingAngle;
	}

	private float GetForearmAngleRight()
	{
		Vector3 handElbow = GetJointIdPosition(11) - GetJointIdPosition(9);
		float pointingAngle = Vector3.Angle(handElbow, new Vector3(1, 0, 0));
		return pointingAngle;
	}

	private void DetectSwipeLeftHand()
	{		
		float vertSpeedAbsolute = Mathf.Abs(hands[0].GetDirection().y);		
		if ( GetForearmAngleLeft() > 85f && vertSpeedAbsolute < swipeSpeedThereshold/4f)
		{			
			if (hands[0].GetDirection().x > swipeSpeedThereshold)
			{
				DidSwipeRight();
			}

			if(hands[0].GetDirection().x < -swipeSpeedThereshold)
			{
				DidSwipeLeft();
			}
		}
	}

	
	private void DetectSwipeRightHand()
	{
		float vertSpeedAbsolute = Mathf.Abs(hands[1].GetDirection().y);	
		if ( GetForearmAngleRight() >85f && vertSpeedAbsolute < swipeSpeedThereshold/4f)
		{			
			if (hands[1].GetDirection().x > swipeSpeedThereshold)
			{
				DidSwipeRight();
			}

			if(hands[1].GetDirection().x < -swipeSpeedThereshold)
			{
				DidSwipeLeft();
			}
		}
	}

	public bool GetSwipeRight()
	{
		return swipeRight;
	}

	public bool GetSwipeLeft()
	{
		return swipeLeft;
	}

	private void DidSwipeLeft()
	{
		//Debug.Log("Swipe Left");
		canSwipe = false;
		swipeLeft = true;
		Invoke("UnlockSwipe", lockTimer);
	}

	private void DidSwipeRight()
	{
		//Debug.Log("Swipe Right");
		canSwipe = false;
		swipeRight =true;
		Invoke("UnlockSwipe", lockTimer);
	}

	private void UnlockSwipe()
	{
		canSwipe = true;
		swipeLeft = false;
		swipeRight = false;
	}

	public Vector3 GetJointIdPosition(int jointID)
	{
		Vector3 origPosition = KinectManager.Instance.GetJointPosition(userID, jointID);
		Vector3 position =  KinectChassis.Instance.GetKuntSide().Kino2World(origPosition, 1);

		Vector3 adjustedPosition3D = new Vector3(position.y, position.x, -position.z);
		Vector3 position3D = adjustedPosition3D - HandPairManager.Instance.GetZeroPointHands();
		return position3D;
	}

	public float GetHeadParalaxLR()
	{
		Vector3 headPos =  GetJointIdPosition(3);
		Vector3 basePos =  GetJointIdPosition(0);
		//Debug.Log("ID: "+ userID + " Head: " + headPos + " base: "+ basePos);

		return (headPos - basePos).z;
	}

	public float GetHandEnergyLeft()
	{
		float energy = -1f;
		Vector3 handShoulder = GetJointIdPosition(4) - GetJointIdPosition(7);
		float pointingAngle = Vector3.Angle(handShoulder, new Vector3(0, 1, 0));
		if ( Mathf.Abs(handShoulder.x) < 5f)
		{
			energy = Mathf.Abs(handShoulder.y);
		}

		return energy;
	}

	public float GetHandEnergyRight()
	{
		float energy = -1f;
		Vector3 handShoulder = GetJointIdPosition(8) - GetJointIdPosition(11);
		float pointingAngle = Vector3.Angle(handShoulder, new Vector3(0, 1, 0));
		if ( Mathf.Abs(handShoulder.x) < 5f)
		{
			energy = Mathf.Abs(handShoulder.y);
		}

		return energy;
	}

	public Vector3 GetHandEnergyVectorRight()
	{
		return (GetJointIdPosition(8) - GetJointIdPosition(11));
	}

	public Vector3 GetHandEnergyVectorLeft()
	{

		return (GetJointIdPosition(4) - GetJointIdPosition(7));
	}

	public Vector3 GetMidPointDisplacement()
	{
		Vector3 midPoint = GetMiddlePoint();
		Vector3 basePos =  GetJointIdPosition(0);
		//Debug.Log("ID: "+ userID + " Head: " + headPos + " base: "+ basePos);

		return (midPoint - basePos);
	}





	void Update()
	{

		if (!clapLock) clap = false;


		handDistance = GetHandDistance3D();
		CalculateSpeed();
		if (KinectChassis.Instance.GetDetectClap())	DetectClap();

		if (KinectChassis.Instance.GetDetectSwipe()) DetectSwipe();

		handDistance1 = handDistance;
		if (hands[0].GetState() == 3 || (hands[1].GetState() == 3 || HandPairManager.Instance.GetDebugInfo()))
		{
			if (HandPairManager.Instance.GetRenderHandMarkers())
			{
				line.SetPosition(0, hands[0].GetPosition());
				line.SetPosition(1, hands[1].GetPosition());
				showAngle = true;
			}
		}
		else
		{
			line.SetPosition(0, hands[0].GetPosition());
			line.SetPosition(1, hands[0].GetPosition());
			showAngle = false;
		}

		if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            showDebugInfo = !showDebugInfo;
        }
	}

	public void Kill()
	{
		hands[0].Kill();
		hands[1].Kill();
		Destroy(this.gameObject);

	}

	void OnGUI()
	{
		//var position = Camera.main.WorldToScreenPoint(GetMiddlePoint());
		var position = new Vector2(800,800);
		guiStyle.fontSize = 60;
		guiStyle.normal.textColor = Color.yellow;

		if (showDebugInfo)
		{
			//GUI.Label(new Rect(position.x, Screen.height - position.y, 400, 200), "Angle: " + GetAngle().ToString("n2") + "\n" + " Parallax: " +  GetHeadParalaxLR().ToString("n2") + "\n" + " Pos: " +  GetJointIdPosition(0) + "\nMidPointDisplacement " + GetMidPointDisplacement() + "\n" + " EnergyLeft: " +  GetHandEnergyLeft().ToString("n2") + "\n" + " EnergyRight: " +  GetHandEnergyRight().ToString("n2"), guiStyle);
			GUI.Label(new Rect(position.x, Screen.height - position.y, 400, 200), "AngleFALeft: " + GetForearmAngleLeft().ToString("n2") + "\n" + "AngleFARight: " +  GetForearmAngleRight().ToString("n2"), guiStyle);
		}
		/*else
		{
			GUI.Label(new Rect(position.x, Screen.height - position.y, 400, 200), "Distance: " + handDistance.ToString("n2") + " Speed: " + distanceSpeed.ToString("n2"), guiStyle);

		}*/


	}
}
