﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{

	private Vector3 lastPosition = Vector3.zero;
	private Vector3 position = Vector3.zero;
	private Vector3 adjustedPosition = Vector3.zero;
	private Vector3 position3D = Vector3.zero;
	private Vector3 adjustedPosition3D = Vector3.zero;
	private Vector3 bias = Vector3.zero;
	private Vector3 origin = Vector3.zero;
	public Vector2 direction = Vector2.zero;
	public Vector2 normDirection = Vector2.zero;
	private float speed = 0.0f;

	public GameObject arrow;
	private GameObject arrowInstance;

	private bool isFlashing = false;
	private float lastTime = 0f;

	private int state = 0;
	private HandParams handParams;
	private bool isVertical = false;


	void Start()
	{
		/*if (ProtonFootballManager.Instance != null && ProtonFootballManager.Instance.GetShowArrow())
		{
			arrowInstance = Instantiate(arrow, this.transform.position, Quaternion.identity);
			arrowInstance.transform.parent = this.transform;
		}*/
		lastTime = Time.time;

		//this.transform.localRotation = Quaternion.Euler(90,0,0);
	}

	public enum HandState
	{
		Unknown = 0,
		NotTracked = 1,
		Open = 2,
		Closed = 3,
		Lasso = 4
	}

	public void SetIsVertical(bool iv)
	{
		isVertical = iv;
		if (isVertical) this.transform.localRotation = Quaternion.Euler(90, 0, 0);
	}

	public void SetParameters(HandParams newParams)
	{
		handParams = newParams;
		float thisTime = Time.time;
		float deltaTime = thisTime - lastTime;
		state = (int)newParams.handState;
		if (state == 3)
		{
			GetComponent<Renderer>().material.SetColor("_TintColor", Color.yellow);
		}
		else
		{
			GetComponent<Renderer>().material.SetColor("_TintColor", Color.cyan);
		}
		position =  KinectChassis.Instance.GetKuntSide().Kino2World(newParams.position, 1);


		if (isVertical)
		{
			adjustedPosition = new Vector3(-position.z, -position.y, -100);
			adjustedPosition3D = new Vector3(-position.z, -position.y, position.x);
		}
		else
		{
			adjustedPosition = new Vector3(0, position.y, -position.z);
			adjustedPosition3D = new Vector3(position.y, position.x, -position.z);
		}


		position = 0.5f*adjustedPosition - HandPairManager.Instance.GetZeroPointHands();
		position3D = 0.5f*adjustedPosition3D - HandPairManager.Instance.GetZeroPointHands();

		this.transform.position = position;

		float lerpDirection = deltaTime / 0.1f;

		Vector3 dif = (position - lastPosition) * 100;
		Vector2 newDirection = new Vector2(dif.x, dif.z);

		direction = Vector2.Lerp(direction, newDirection, lerpDirection);
		normDirection = direction.normalized;

		float newSpeed = Vector2.Distance(direction, newDirection);

		//speed = Mathf.Lerp(speed, Vector2.Distance(direction, newDirection), 0.5f);
		speed = Mathf.SmoothStep(speed, newSpeed, deltaTime / 0.05f );

		lastPosition = position;
		lastTime = thisTime;
		if (!HandPairManager.Instance.GetRenderHandMarkers())
		{
			this.transform.localScale = new Vector3(0, 0, 0);
		}
		else
		{
			this.transform.localScale = new Vector3(5, 0, 5);
		}

	}

	public Vector3 GetPosition()
	{
		return position;
	}

	public Vector3 GetPosition3D()
	{
		return position3D;
	}

	public Vector3 GetAdjustedPosition()
	{
		return adjustedPosition;
	}

	public Vector3 GetAdjustedPosition3D()
	{
		return adjustedPosition3D;
	}

	public float GetState()
	{
		return state;
	}

	public float GetSpeed()
	{
		return speed;
	}

	public Vector3 GetDirection()
	{
		return speed * normDirection;
	}

	public void SetScale(float sc)
	{
		gameObject.transform.localScale = new Vector3(sc, 0f, sc);
	}

	private void UpdateArrow()
	{
		arrowInstance.transform.LookAt(position + new Vector3(direction.x, 0, direction.y));
		arrowInstance.transform.localScale = new Vector3(speed, speed, speed) * 0.03f;
	}

	public void FlashArrow()
	{
		//showArrow = true;
		if (!isFlashing)
		{
			arrowInstance = Instantiate(arrow, this.transform.position, Quaternion.identity);
			UpdateArrow();
			Invoke("KillArrow", 1f);

			isFlashing = true;
		}
	}

	public void KillArrow()
	{

		//arrowInstance.transform.localScale = new Vector3(0, 0, 0);
		Destroy(arrowInstance);
		isFlashing = false;
	}



	void Update ()
	{
		//SetParameters();

		/*if (ProtonFootballManager.Instance != null && ProtonFootballManager.Instance.GetShowArrow())
		{
			UpdateArrow();
		}*/

	}

	public void Kill()
	{
		KillArrow();
		//Debug.Log("Kill" + this.gameObject);
		Destroy(this.gameObject);

	}
}
