﻿using UnityEngine;
using System.Collections;

public class PointCloud : MonoBehaviour
{


	private GameObject _pcObject;
	private ParticleSystem _pointCloud;
	private ParticleSystem.Particle[] _pcPoints;
	private Texture2D _depthTex;

	// Only works at 4 right now
	private const float _scaling = 1f;
	private const int _DepthWidth = (int)(512 * _scaling);
	private const int _DepthHeight = (int)(424 * _scaling);
	private const int _OriginalDownsample = 2;
	private const int _DownsampleSize = 1;
	private const double _DepthScale = 0.3f;
	private const int _Speed = 50;
	private bool _isMeshCreated = false;
	private bool _isShurikenCreated = false;
	private bool _usePointCloud = false;
	private int testDepth = 0;
	private Color testColor = Color.red;
	private Color32 testColor32 = new Color32(0, 0, 0, 0);
	private int _totalParticles = 0;
	private bool flip = true;




	void CreatePointCloud()
	{
		CreatePointCloud(_DepthWidth / _OriginalDownsample, _DepthHeight / _OriginalDownsample);
		//Debug.Log("Remote width: "+ _DepthWidth + " height: " + _DepthHeight + " _DownsampleSize: "+ _DownsampleSize);
	}

	void CreatePointCloud(int width, int height)
	{

		Debug.Log("Remote width: " + width + " height: " + height + " _DownsampleSize: " + _DownsampleSize);
		_pointCloud = (ParticleSystem)this.GetComponent("ParticleSystem");
		_pcPoints = new ParticleSystem.Particle[width * height];
		_totalParticles = width * height;

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int index = (y * width) + x;
				_pcPoints[index].position = new Vector3(x * _OriginalDownsample, -y * _OriginalDownsample, 0);
				_pcPoints[index].color = Color.red;
				_pcPoints[index].size = 3f;
			}
		}

		_isShurikenCreated = true;
	}

	private void KillAllParticles()
	{
		if (_pcPoints != null)
		{
			for (int i = 0; i < _pcPoints.Length; i++)
			{
				_pcPoints[i].remainingLifetime = 0f; //kill this particle
			}
		}

		_pcPoints = new ParticleSystem.Particle[0];

		_pointCloud.SetParticles(_pcPoints, _pcPoints.Length);
		_isShurikenCreated = false;
	}

	public void EnablePointCloud (bool enPC)
	{
		_usePointCloud = enPC;
	}

	public void SetDepthTex(Texture2D dt)
	{
		_depthTex = dt;
	}

	public void SetRotation(float rot)
	{
		this.transform.eulerAngles = new Vector3(0, rot, 0);
	}

	public void SetTranslationZ(float posz)
	{
		Vector3 pos = this.transform.position;
		this.transform.position = new Vector3(pos.x, pos.y, posz);
	}


	void Update()
	{
		if (_usePointCloud && !_isShurikenCreated)
		{
			CreatePointCloud();
		}

		if (!_usePointCloud && _isShurikenCreated)
		{
			KillAllParticles();
		}

		if (_depthTex != null && _usePointCloud && _isShurikenCreated)
		{
			RefreshPointCloud(_depthTex);
		}

		/*if (Input.GetKeyUp("space"))
		{
			Debug.Log("TestDepth: " + testDepth + " Color:" + testColor32 + " B:" + testColor32.b + " G: " + testColor32.g + " Blue rounded: " + Mathf.Round(testColor32.b / 10f));
		}


		if (Input.GetKeyUp("f"))
			EnablePointCloud(true);

		if (Input.GetKeyUp("g"))
			EnablePointCloud(false);
			*/


	}

	private Color32 ColorGammaTransform(Color colorOrig, float power)
	{
		Color correctedColor = new Color(Mathf.Pow(colorOrig.r, power), Mathf.Pow(colorOrig.g, power), Mathf.Pow(colorOrig.b, power), 1f);
		return (Color32)correctedColor;
	}

	private void RefreshPointCloud(Texture2D depthTex)
	{
		Vector3 tempPoint = new Vector3();

		//lineWidth = _depthTex.width / _DownsampleSize;
		float pow = 1f / 1.1f;

		testColor32 = ColorGammaTransform(depthTex.GetPixel(32, 32), pow);
		testDepth = GetDepthFromColor32(testColor32);



		for (int y = 0; y < _depthTex.height; y += _DownsampleSize)
		{
			for (int x = 0; x < _depthTex.width; x += _DownsampleSize)
			{
				int indexX = x / _DownsampleSize;
				int indexY = y / _DownsampleSize;
				int smallIndex = (indexY * (_depthTex.width / _DownsampleSize)) + indexX;
				int smallIndexreversed = _totalParticles - 1 - smallIndex;


				ushort depthAtPoint = flip ? GetDepthFromColor32(ColorGammaTransform(depthTex.GetPixel(_depthTex.width - x, y), pow)) : GetDepthFromColor32(ColorGammaTransform(depthTex.GetPixel(x, y), pow));



				tempPoint = _pcPoints[smallIndexreversed].position;
				tempPoint.z = - (float)(depthAtPoint * _DepthScale);
				_pcPoints[smallIndexreversed].position  = tempPoint;

				float DepthIndex = depthAtPoint / 6000f;

				if (DepthIndex < 0.01f || DepthIndex > 1f)
				{
					_pcPoints[smallIndexreversed].color = new Color(0, 0, 0, 0);
				}
				else
				{
					_pcPoints[smallIndexreversed].color = new Color(DepthIndex * DepthIndex / 0.7f + 0.7f, 0.5f + DepthIndex / 0.5f, 1f - DepthIndex);
					//_pcPoints[smallIndex].color = new Color(1,1,0,1);

				}

			}
		}

		_pointCloud.SetParticles(_pcPoints, _pcPoints.Length);

	}

	private void RefreshPointCloud32(Texture2D depthTex)
	{
		Vector3 tempPoint = new Vector3();

		int lineWidth = _depthTex.width / _DownsampleSize;



		Color32[] pixels = depthTex.GetPixels32();
		testDepth = GetDepthFromColor32(pixels[32]);
		testColor32 = pixels[32];


		for (int y = 0; y < _depthTex.height; y += _DownsampleSize)
		{
			for (int x = 0; x < _depthTex.width; x += _DownsampleSize)
			{
				int indexX = x / _DownsampleSize;
				int indexY = y / _DownsampleSize;
				int smallIndex = (indexY * (_depthTex.width / _DownsampleSize)) + indexX;


				ushort depthAtPoint = GetDepthFromColor32(pixels[y * (int)lineWidth + x]);



				tempPoint = _pcPoints[smallIndex].position;
				tempPoint.z = (float)(depthAtPoint * _DepthScale);
				_pcPoints[smallIndex].position  = tempPoint;

				float DepthIndex = depthAtPoint / 6000f;

				if (DepthIndex < 0.01f || DepthIndex > 1f)
				{
					_pcPoints[smallIndex].color = new Color(0, 0, 0, 0);
				}
				else
				{
					_pcPoints[smallIndex].color = new Color(DepthIndex * DepthIndex / 0.7f + 0.7f, 0.5f + DepthIndex / 0.5f, 1f - DepthIndex);
					//_pcPoints[smallIndex].color = new Color(1,1,0,1);

				}

			}
		}

		_pointCloud.SetParticles(_pcPoints, _pcPoints.Length);

	}

	private ushort GetDepthFromColor(Color clr)
	{

		//ushort depthLong = (ushort)(adjustmentLongScale * ( (25.5f *  (Mathf.FloorToInt(clr.b *255f)))));
		ushort depthLong = (ushort)((clr.b) * 6553.5);

		ushort depthShort = (ushort)(255 * (clr.g ));
		return (ushort)( depthLong + depthShort);
	}

	private ushort GetDepthFromColor32(Color32 clr)
	{

		//ushort depthLong = (ushort)(adjustmentLongScale * ( (25.5f *  (Mathf.FloorToInt(clr.b *255f)))));
		ushort depthLong = (ushort)(Mathf.Round(clr.b / 10f) * 256f);

		ushort depthShort = (ushort)((clr.g ));
		return (ushort)( depthLong + depthShort);
	}



	private void RefreshPointCloud(ushort[] depthData)
	{
		Vector3 tempPoint = new Vector3();


		//lineWidth = _depthTex.width / _DownsampleSize;


		for (int y = 0; y < _depthTex.height; y += _DownsampleSize)
		{
			for (int x = 0; x < _depthTex.width; x += _DownsampleSize)
			{
				int indexX = x / _DownsampleSize;
				int indexY = y / _DownsampleSize;
				int smallIndex = (indexY * (_depthTex.width / _DownsampleSize)) + indexX;



				tempPoint = _pcPoints[smallIndex].position;
				int fullIndex = (y * _depthTex.width) + x;
				tempPoint.z = (float)(depthData[fullIndex] * _DepthScale);
				_pcPoints[smallIndex].position  = tempPoint;

				float DepthIndex = depthData[fullIndex] / 1200f;
				//float DepthIndex2 = (DepthIndex % 256f)/300f;

				//if (DepthIndex2 < 0.1f || DepthIndex2 > 1f)
				if (DepthIndex < 0.1f || DepthIndex > 1f)
				{
					//_pcPoints[smallIndex].color = new Color(0, 0, 0, 0);
				}
				else
				{
					_pcPoints[smallIndex].color = new Color(DepthIndex * DepthIndex + 0.2f, DepthIndex, 1f - DepthIndex);
					//_pcPoints[smallIndex].color = new Color(DepthIndex2, 1f - DepthIndex2, DepthIndex2+0.2f);
					//_pcPoints[smallIndex].color = new Color(DepthIndex2, 0, 1f - DepthIndex2);
				}

			}
		}

		_pointCloud.SetParticles(_pcPoints, _pcPoints.Length);

	}

}
