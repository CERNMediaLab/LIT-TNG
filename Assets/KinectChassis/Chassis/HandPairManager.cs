﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandPairManager : MonoBehaviour
{


	public GameObject handPairPrefab;

	private List<HandPair> handPairs = null;
	protected static HandPairManager instance = null;


	private int axisMode = 2;
	private Vector3 zeroPointHands = Vector3.zero;

	private int numHandPairs = 0;
	private bool showDebugInfo = false;
	private bool renderHandMarkers = false;
	private bool isVertical = false;
	private bool swipeRight = false;
	private bool swipeLeft = false;
	private bool canSwipe = true;


	public static HandPairManager Instance
	{
		get
		{
			return instance;
		}
	}

	void Start ()
	{
		instance = this;
		handPairs = new List<HandPair>();
		LoadSavedValues();
	}

	private void LoadSavedValues()
	{
		zeroPointHands.x = SaveLoad.savedSettings.zeroPointHandsX;
		zeroPointHands.y = SaveLoad.savedSettings.zeroPointHandsY;
		zeroPointHands.z = SaveLoad.savedSettings.zeroPointHandsZ;

	}

	private void AssignSettingsValues()
	{
		SaveLoad.savedSettings.zeroPointHandsX = zeroPointHands.x;
		SaveLoad.savedSettings.zeroPointHandsY = zeroPointHands.y;
		SaveLoad.savedSettings.zeroPointHandsZ = zeroPointHands.z;

	}

	public bool GetRenderHandMarkers()
	{
		return renderHandMarkers;
	}

	public void SetRenderHandMarkers(bool rhm)
	{
		renderHandMarkers = rhm;
	}

	public void SetIsVertical(bool iv)
	{
		isVertical = iv;
		Debug.Log("SetIsVertical " + isVertical);
	}

	public bool GetIsVertical()
	{
		return isVertical;
	}

	public void ToggleRenderHandMarkers()
	{
		renderHandMarkers = !renderHandMarkers;
	}

	public void SetDebugInfo(bool di)
	{
		showDebugInfo = di;
	}

	public bool GetDebugInfo()
	{
		return showDebugInfo;
	}

	private void UpdateHandPairs()
	{
		numHandPairs = 0;
		if (KinectManager.Instance.IsUserDetected())
		{
			float scale = 1f;
			foreach (HandParams[] handParams in KinectChassis.Instance.GetAllUsersHandsInPairs())
			{
				UpdateHandPair(handParams, ++numHandPairs, false);
			}

			TrimHandPairs(numHandPairs);
		}
	}

	private void UpdateHandPair(HandParams[] pairParams, int currentNumber, bool isLast)
	{

		if (currentNumber > handPairs.Count) //need to create a new marker
		{
			GameObject newPairPrefab = Instantiate(handPairPrefab, Vector3.zero, Quaternion.identity);
			HandPair newHandPair = newPairPrefab.GetComponent( typeof(HandPair) ) as HandPair;
			newHandPair.SetUserID((long)KinectManager.Instance.GetAllUserIds()[currentNumber - 1]);
			handPairs.Add(newHandPair);
		}

		if (handPairs[currentNumber - 1] != null) handPairs[currentNumber - 1].UpdateHands(pairParams);
	}

	private void TrimHandPairs(int numPairs)
	{
		if (numPairs < handPairs.Count)
		{
			while (numPairs < handPairs.Count)
			{
				if (handPairs[handPairs.Count - 1] != null)	handPairs[handPairs.Count - 1].Kill();
				handPairs.RemoveAt(handPairs.Count - 1);
				//Debug.Log("Cleaned " + handMarkers.Count);
			}
		}
	}

	public void SetZeroPointHands()
	{
		if (handPairs.Count > 0)
		{
			Vector3 camPos = Camera.main.transform.position;
			zeroPointHands = handPairs[0].GetMiddlePointAdjusted() + new Vector3(camPos.x, 0, camPos.z);
			AssignSettingsValues();
		}
	}

	public void SetZeroPointHands(Vector3 zph)
	{
		zeroPointHands = zph;
	}

	public float GetNumHandPairs()
	{
		return handPairs.Count;
	}

	public HandPair GetHandPairs(int index)
	{
		return handPairs[index];
	}

	public Vector3 GetZeroPointHands()
	{
		return zeroPointHands;
	}

	public int GetAxisMode()
	{
		return axisMode;
	}

	private void UnlockSwipe()
	{
		swipeRight = false;
		swipeLeft = false;
		canSwipe = true;
	}

	
	public int GetSwipeAtPosition(Vector3 pos, float radius)
	{
		int swipe = -10; //-10 = nobody in pos, -1 = left, 0 = none, 1 = right

		foreach (HandPair hp in handPairs)
		{
			if ((hp.GetJointIdPosition(0) - pos).magnitude <= radius)
			{
				swipe = 0;
				if (canSwipe)
				{
					if (hp.GetSwipeRight())
					{
						swipeRight = true;
						canSwipe = false;
						swipe = 1;
						Invoke("UnlockSwipe", 1f);
						break;
					}

					if (hp.GetSwipeLeft())
					{
						swipeLeft = true;
						canSwipe = false;
						swipe = -1;
						Invoke("UnlockSwipe", 1f);
						break;
					}
				}
			}
		}
		return swipe;
	}

	public bool GetDidClapAtPosition(Vector3 pos, float radius)
	{
		bool didClap = false;
		//Debug.Log("Trying");
		foreach (HandPair hp in handPairs)
		{
			if ((hp.GetJointIdPosition(0) - pos).magnitude <= radius && hp.GetDidClap())
			{
				didClap = true;
				//Debug.Log("YeahClap");
				hp.ResetClap();
				break;
			}
		}
		return didClap;
	}

	public bool GetIsUserAtPosition(Vector3 pos, float radius)
	{
		bool isInPosition = false;
		foreach (HandPair hp in handPairs)
		{
			if ((hp.GetJointIdPosition(0) - pos).magnitude <= radius)
			{
				isInPosition = true;
				break;
			}
		}
		return isInPosition;
	}

	public float? GetAngleFromHandsAtPos(Vector3 pos, float radius)
	{
		float? angle = null;
		foreach (HandPair hp in handPairs)
		{
			if ((hp.GetJointIdPosition(0) - pos).magnitude <= radius)
			{
				angle = hp.GetAngle();
				break;
			}
		}
		return angle;
	}

	public float? GetDisplacementFromHandsAtPos(Vector3 pos, float radius)
	{
		float? displacement = null;
		foreach (HandPair hp in handPairs)
		{
			if ((hp.GetJointIdPosition(0) - pos).magnitude <= radius)
			{
				displacement = hp.GetMidPointDisplacement().z;
				break;
			}
		}
		return displacement;
	}

	public float? GetEnergyFromHandsAtPos(Vector3 pos, float radius)
	{
		float? energy = null;
		foreach (HandPair hp in handPairs)
		{
			if ((hp.GetJointIdPosition(0) - pos).magnitude <= radius)
			{
				float energyMax = Mathf.Max( hp.GetHandEnergyLeft(),  hp.GetHandEnergyRight());
				if (energyMax > 0f) energy = energyMax;
				break;
			}
		}
		return energy;
	}



	void Update ()
	{
		if (KinectChassis.Instance.GetShowHandMarkers())
		{
			UpdateHandPairs();
		}
		else if (handPairs.Count > 0)
		{
			TrimHandPairs(0);
		}

		if (Input.GetKeyDown("0"))
		{
			Invoke("SetZeroPointHands", 2f);
		}

		if (Input.GetKeyDown("9"))
		{
			SetDebugInfo(!showDebugInfo);
		}

		if (Input.GetKeyDown("6"))
		{
			ToggleRenderHandMarkers();
		}

	}
}
