﻿
using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class UDPReceive : MonoBehaviour
{

    
    Thread receiveThread;    
    UdpClient client;
    
    private int port; // define > init

    // infos
    private byte[] lastReceivedUDPPacket = null;
    //public string allReceivedUDPPackets = ""; // clean up this from time to time!
    //
    protected static UDPReceive instance = null;



    public static UDPReceive Instance
    {
        get
        {
            return instance;
        }
    }
    
    public void Start()
    {
        instance = this;
        init();        
    }

    
    private void init()
    {

        port = 11000;

        receiveThread = new Thread(new ThreadStart(ReceiveData));
        receiveThread.IsBackground = true;
        receiveThread.Start();
        Debug.Log(" UDP Receiver INITIALIZED "); 

    }
    
    private  void ReceiveData()
    {

        client = new UdpClient(port);
        IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
        //while (Thread.CurrentThread.IsAlive)
        while (Thread.CurrentThread.IsAlive)
        {

            //Debug.Log("Receive Thread");
            try
            {
                
                byte[] data = client.Receive(ref anyIP);

                // Bytes encoded in UTF8 in text format
                //string text = Encoding.UTF8.GetString(data);
                //string text = Encoding.ASCII.GetString(data);
               

                if (data != null)
                {
                    lastReceivedUDPPacket = data;  
                    KinectChassis.Instance.ReceiveData();
                }
                else
                {
                    
                }
                
                //allReceivedUDPPackets = allReceivedUDPPackets + text;
                //bool isThereData = data != null;
                //Debug.Log(isThereData + " size: "+ lastReceivedUDPPacket.Length);

            }
            catch (Exception err)
            {
                Debug.Log(err.ToString());
            }

        }
    }
    

    void Update()
    {
        //Debug.Log(receiveThread.IsAlive);

    }

    void OnApplicationQuit ()
    {

        if (receiveThread != null)
        {
            receiveThread.Abort();
        }

        if (client!=null) client.Close();

    }

    
    public byte[] getLatestUDPPacket()
    {
        //allReceivedUDPPackets = "";
        return lastReceivedUDPPacket;
    }
}
