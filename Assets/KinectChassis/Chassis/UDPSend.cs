﻿
using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class UDPSend : MonoBehaviour
{
    protected static UDPSend instance = null;

    private static int localPort;


    private string IP = "0.0.0.0";
    private int port = 11000;


    IPEndPoint remoteEndPoint;
    UdpClient client = null;


    string strMessage = "";


    public static UDPSend Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        // set the singleton instance
        instance = this;
    }

    public void Start()
    {
        //Setup("127.0.0.1", port);
    }



    public void Setup(string newIp, int newPort)
    {

        if (newIp != IP)
        {
            if (client != null)
            {
                Kill();
            }
            client = new UdpClient(newIp, newPort);
            IP = newIp;
            port = newPort;
            Debug.Log(newIp);
            //remoteEndPoint = new IPEndPoint(IPAddress.Parse(newIp), newPort);
        }

    }

    public int sendString(string message)
    {
        Byte[] sendBytes = Encoding.ASCII.GetBytes(message);
        if(client != null) 
        {
            client.Send(sendBytes, sendBytes.Length);
        }
        

        return sendBytes.Length;
    }

    /*public void sendString(string message)
    {
        if(message != null)
        {
            byte[] data = Encoding.UTF8.GetBytes(message);
            client.Send(data, data.Length, remoteEndPoint);
            //Debug.Log("Sent " + message + " to " + remoteEndPoint);
        }
        catch (Exception err)
        {
            //Debug.Log("Error");
        }
    }*/

    public void Kill()
    {
        client.Close();
        client = null;
    }

}

