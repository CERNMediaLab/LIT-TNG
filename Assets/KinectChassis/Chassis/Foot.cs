using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foot : MonoBehaviour
{

	private Vector3 lastPosition = Vector3.zero;
	private Vector3 position = Vector3.zero;
	private Vector2 position2D = Vector2.zero;
	private Vector3 bias = Vector3.zero;
	private Vector3 origin = Vector3.zero;
	public Vector2 direction = Vector2.zero;
	public Vector2 normDirection = Vector2.zero;
	private float speed = 0.0f;
	private float lastSpeed = 0.0f;

	public GameObject arrow;
	private GameObject arrowInstance;

	private bool isFlashing = false;
	private float lastTime = 0f;


	void Start()
	{
		if (ProtonFootballManager.Instance != null && ProtonFootballManager.Instance.GetShowArrow())
		{
			arrowInstance = Instantiate(arrow, this.transform.position, Quaternion.identity);
			arrowInstance.transform.parent = this.transform;
		}
		lastTime = Time.time;
	}

	private void SetParameters()
	{
		float trendSpeed = 13f; // was 10
		if(CEO.Instance.GetIsFloor())
		{
			trendSpeed = 20f;
		}
		float thisTime = Time.time;
		float deltaTime = thisTime - lastTime;

		position = this.transform.position;

		position2D.Set(position.x, position.z);

		//float lerpDirection = deltaTime / 0.1f;

		Vector3 dif = (position - lastPosition) * 170f;
		Vector2 newDirection = new Vector2(dif.x, dif.z);

		direction = Vector2.Lerp(direction, newDirection, deltaTime * trendSpeed);
		normDirection = direction.normalized;

		//float newSpeed = Vector2.Distance(direction, newDirection);
		float newSpeed = newDirection.magnitude;

		//speed = Mathf.Lerp(speed, Vector2.Distance(direction, newDirection), 0.5f);
		float speedTemp = Mathf.SmoothStep(speed, newSpeed, deltaTime * trendSpeed * 2f );
		if (speedTemp > 0) speed = speedTemp;

		lastTime = thisTime;
		lastPosition = position;
		lastSpeed = speed;
	}

	private void SetParametersOld()
	{
		float thisTime = Time.time;
		float deltaTime = thisTime - lastTime;
		position = this.transform.position;
		position2D.Set(position.x, position.z);

		float lerpDirection = deltaTime / 0.1f;

		Vector3 dif = (position - lastPosition) * 100;
		Vector2 newDirection = new Vector2(dif.x, dif.z);

		direction = Vector2.Lerp(direction, newDirection, lerpDirection);
		normDirection = direction.normalized;

		float newSpeed = Vector2.Distance(direction, newDirection);

		//speed = Mathf.Lerp(speed, Vector2.Distance(direction, newDirection), 0.5f);
		float speedTemp = Mathf.SmoothStep(speed, newSpeed, deltaTime / 0.05f );
		if (speedTemp > 0) speed = speedTemp;

		lastPosition = position;
		lastTime = thisTime;
	}

	

	public Vector3 GetPosition()
	{
		return position;
	}

	public float GetSpeed()
	{
		return speed;
	}

	public Vector3 GetDirection()
	{
		return speed * normDirection;
	}

	public void SetScale(float sc)
	{
		gameObject.transform.localScale = new Vector3(sc, 0f, sc);
	}

	private void UpdateArrow()
	{
		arrowInstance.transform.LookAt(position + new Vector3(direction.x, 0, direction.y));
		arrowInstance.transform.localScale = new Vector3(speed, speed, speed) * 0.03f;
	}

	public void FlashArrow()
	{
		//showArrow = true;
		if (!isFlashing)
		{
			arrowInstance = Instantiate(arrow, this.transform.position, Quaternion.identity);
			UpdateArrow();
			Invoke("KillArrow", 1f);

			isFlashing = true;
		}
	}

	public void KillArrow()
	{

		//arrowInstance.transform.localScale = new Vector3(0, 0, 0);
		Destroy(arrowInstance);
		isFlashing = false;
	}



	void Update ()
	{
		SetParameters();

		if (ProtonFootballManager.Instance != null && ProtonFootballManager.Instance.GetShowArrow())
		{
			//UpdateArrow();
		}

	}

	public void Kill()
	{
		KillArrow();
		//Debug.Log("Kill" + this.gameObject);
		Destroy(this.gameObject);

	}
}
