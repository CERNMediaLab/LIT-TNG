using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct HandParams
{
	public Vector3 position;
	public float handState;
	public long UID;

}

public class BodyJointsLocal
{
	public Text debugText;

	List<Vector3[]> allSkeletons = null;

	public List<Vector3> GetAllLocalUsersSelectedJoints()
	{
		List<Vector3> allLocalUsersFeet = new List<Vector3>();

		List<long> userIDs = KinectManager.Instance.GetAllUserIds();

		foreach (long userID in userIDs)
		{
			Vector3 posLeftFoot = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.HandLeft);
			Vector3 posRightFoot = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.HandRight);

			allLocalUsersFeet.Add(posLeftFoot);
			allLocalUsersFeet.Add(posRightFoot);
		}

		return allLocalUsersFeet;
	}

	public List<Vector3> GetAllUsersHands()
	{
		List<Vector3> allUsersHands = new List<Vector3>();

		List<long> userIDs = KinectManager.Instance.GetAllUserIds();

		foreach (long userID in userIDs)
		{
			Vector3 posLeftHand = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.HandLeft);
			Vector3 posRightHand = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.HandRight);

			allUsersHands.Add(posLeftHand);
			allUsersHands.Add(posRightHand);
		}

		return allUsersHands;
	}

	public List<HandParams[]> GetAllUsersHandsInPairs()
	{
		List<HandParams[]> allUsersHands = new List<HandParams[]>();

		List<long> userIDs = KinectManager.Instance.GetAllUserIds();

		foreach (long userID in userIDs)
		{
			HandParams[] pairOfHands = new HandParams[2];
			pairOfHands[0].position = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.HandLeft);
			pairOfHands[1].position = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.HandRight);

			pairOfHands[0].handState = (float)KinectManager.Instance.GetLeftHandState(userID);
			pairOfHands[1].handState = (float)KinectManager.Instance.GetRightHandState(userID);

			pairOfHands[0].UID = userID;
			pairOfHands[1].UID = userID;


			allUsersHands.Add(pairOfHands);
		}

		return allUsersHands;
	}


	public List<Vector4[]> GetAllUsersHandsInPairsV4()
	{
		List<Vector4[]> allUsersHands = new List<Vector4[]>();

		List<long> userIDs = KinectManager.Instance.GetAllUserIds();

		foreach (long userID in userIDs)
		{
			Vector4[] pairOfHands = new Vector4[2];
			pairOfHands[0] = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.HandLeft);
			pairOfHands[1] = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.HandRight);

			pairOfHands[0].w = (float)KinectManager.Instance.GetLeftHandState(userID);
			pairOfHands[1].w = (float)KinectManager.Instance.GetRightHandState(userID);


			allUsersHands.Add(pairOfHands);
		}

		return allUsersHands;
	}


	public List<Vector3> GetAllUsersFeet()
	{
		List<Vector3> allUsersFeet = new List<Vector3>();

		List<long> userIDs = KinectManager.Instance.GetAllUserIds();

		foreach (long userID in userIDs)
		{
			Vector3 posLeftFoot = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.AnkleLeft);
			Vector3 posRightFoot = KinectManager.Instance.GetJointPosition(userID, (int)KinectInterop.JointType.AnkleRight);

			allUsersFeet.Add(posLeftFoot);
			allUsersFeet.Add(posRightFoot);
		}

		return allUsersFeet;
	}

	public List<Vector3[]> GetAllSkeletonData()
	{
		allSkeletons = new List<Vector3[]>();

		List<long> userIDs = KinectManager.Instance.GetAllUserIds();

		foreach (long userID in userIDs)
		{
			Vector3[] allJoints = new Vector3[Enum.GetNames(typeof(KinectInterop.JointType)).Length];

			for (int i = 0; i <= Enum.GetNames(typeof(KinectInterop.JointType)).Length - 1; i++)
			{
				allJoints[i] = KinectManager.Instance.GetJointPosition(userID, i);
				//Debug.Log(i);
			}

			allSkeletons.Add(allJoints);

		}

		return allSkeletons;
	}

	public void DisplayUserSelectedJoints()
	{
		string jt = "No joints to show";
		List<Vector3> AllUsersJoints = GetAllLocalUsersSelectedJoints();
		if (AllUsersJoints.Count > 0)
		{
			jt = "Number of joints: " + AllUsersJoints.Count;
			foreach (Vector3 userJoint in AllUsersJoints)
			{
				jt +=  "    " + userJoint;
			}

		}

		debugText.text = jt;

	}

}
