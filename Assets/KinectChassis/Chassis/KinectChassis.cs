using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Windows.Kinect;

public class KinectChassis : MonoBehaviour
{
	public GameObject localPointCloud;
	public GameObject remotePointCloud;
	public DataManager dataManager;

	public GameObject FootMarker;


	public GameObject CalibrationIndicator01;
	public GameObject CalibrationIndicator02;

	public GameObject CalibrationIndicatorL01;
	public GameObject CalibrationIndicatorL02;

	private bool useLocalCloud = false;
	private bool useLocalSkeleton = true;
	private bool useRemoteCloud = false;
	private bool useRemoteSkeleton = true;

	private bool isActive = false;

	private bool showFeetMarkers = false;
	private bool renderFeetMarkers = true;

	private bool showHandMarkers = false;
	private bool renderHandMarkers = true;

	private bool checkKick = true;

	private int numFeet = 0;
	private int numFeetRemote = 0;

	//private KUNT kuntRemote = null;
	private KUNT kuntLocal = null;
	private KUNT kuntSide = null;

	private List<Foot> feetMarkers = null;

	private List<Vector3> protonPositions = null;

	private float sideSwap = -1f;

	private float topSpeed = 0f;
	private float lastTopSpeed = 0f;
	private float instantSpeedInterval = 0.3f;

	private bool detectClap = false;
	private bool detectSwipe = false;
	private int[] streamSelection = new int[5]; //color, depth, IR, people, joints


	// The singleton instance of KinectChassis
	protected static KinectChassis instance = null;





	void Awake()
	{
		// set the singleton instance
		instance = this;
	}

	public static KinectChassis Instance
	{
		get
		{
			return instance;
		}
	}

	void Start ()
	{
		//kuntRemote = new KUNT();
		kuntLocal = new KUNT();
		kuntSide = new KUNT();
		//DontDestroyOnLoad(gameObject);

		//kuntRemote.SetSide(-1);
		feetMarkers = new List<Foot>();


		kuntLocal.SetIsLocal(true);
		kuntSide.SetIsSide(true);
		SetStreamSelection(new int[] {0,1,0,1,1});

		ResetSpeedCycle();
		Invoke("SetAssignedValues", 1f);
	}

	public void SetAssignedValues()
	{
		kuntLocal.AssignSavedValues();
		kuntSide.AssignSavedValues();
		if(CEO.Instance.GetIsSide())
		{
			sideSwap = 1f;
		}
		kuntLocal.SetZSide(1 * sideSwap);
	}

	public bool GetStreamSelection(int s)
	{
		bool ss = false;
		if (streamSelection[s] == 1) ss = true;

		return ss;
	}

	public void SetStreamSelection(int[] ss)
	{
		streamSelection = ss;
	}

	public Texture2D GetTexture_IR()
	{
		return KinOps.Instance.GetTexture_IR();
	}

	public Texture2D GetTexture_Color()
	{
		return KinOps.Instance.GetTexture_Color();
	}

	public Texture2D GetTexture_Depth()
	{
		return KinOps.Instance.GetTexture_Depth();
	}

	public Texture2D GetTexture_People(bool displaySkeletonLines)
	{
		return KinOps.Instance.GetTexture_People(displaySkeletonLines);
	}

	public byte[] GetJpegPeople()
	{
		return KinOps.Instance.GetJpegPeople();
	}

	public KUNT GetKuntLocal()
	{
		return kuntLocal;
	}

	public KUNT GetKuntSide()
	{
		return kuntSide;
	}



	public void SetActive(bool active)
	{
		isActive = active;
	}

	public void ShowFeetMarkers(bool sfm)
	{
		showFeetMarkers = sfm;
	}

	public void ShowHandMarkers(bool shm)
	{
		showHandMarkers = shm;
	}

	public bool GetShowHandMarkers()
	{
		return showHandMarkers;
	}

	public void SetDetectClap(bool dc)
	{
		detectClap = dc;
	}

	public bool GetDetectClap()
	{
		return detectClap;
	}

	public void SetDetectSwipe(bool ds)
	{
		detectSwipe = ds;
	}

	public bool GetDetectSwipe()
	{
		return detectSwipe;
	}

	private void CalibrateSide()
	{
		if (dataManager.GetBodyJointsLocal().GetAllSkeletonData().Count == 1)
		{
			kuntSide.CalibrateSide(dataManager.GetBodyJointsLocal().GetAllSkeletonData()[0], 1);
			Debug.Log("Calibrating Side");

			//resetCalibrationFactors();
			//SaveLoad.Save();

			//gui.updateVariablesInSettings();
		}
	}

	private void CalibrateLocal()
	{
		if (dataManager.GetBodyJointsLocal().GetAllSkeletonData().Count == 1)
		{
			kuntLocal.Calibrate(dataManager.GetBodyJointsLocal().GetAllSkeletonData()[0], 1);
			Debug.Log("Calibrating Local");

			//resetCalibrationFactors();
			//SaveLoad.Save();

			//gui.updateVariablesInSettings();
		}
	}

	private void CalibrateRemote()
	{
		/*if (dataManager.GetBodyJointsRemote().GetAllSkeletonData().Count == 1)
		{
			kuntRemote.Calibrate(dataManager.GetBodyJointsRemote().GetAllSkeletonData()[0], 1);
			Debug.Log("Calibrating Remote");

			//resetCalibrationFactors();
			//SaveLoad.Save();

			//gui.updateVariablesInSettings();

		}*/
	}


	public void SetCalibrationIndicatorPosition(float indPos)
	{
		CalibrationIndicator01.transform.position = new Vector3(CalibrationIndicator01.transform.position.x, CalibrationIndicator01.transform.position.y, indPos);
		CalibrationIndicator02.transform.position = new Vector3(CalibrationIndicator02.transform.position.x, CalibrationIndicator02.transform.position.y, indPos);
		CalibrationIndicatorL01.transform.position = new Vector3(CalibrationIndicatorL01.transform.position.x, CalibrationIndicatorL01.transform.position.y, -indPos);
		CalibrationIndicatorL02.transform.position = new Vector3(CalibrationIndicatorL02.transform.position.x, CalibrationIndicatorL02.transform.position.y, -indPos);
	}

	public Vector3 GetCalibrationIndicatorPosition(int ind)
	{
		Vector3 posCalInd = Vector3.zero;

		switch (ind)
		{
		case 1:
			posCalInd = CalibrationIndicator01.transform.position;
			break;
		case 2:
			posCalInd = CalibrationIndicator02.transform.position;
			break;
		case 3:
			posCalInd = CalibrationIndicatorL01.transform.position;
			break;
		case 4:
			posCalInd = CalibrationIndicatorL02.transform.position;
			break;
		}
		return posCalInd;
	}

	public void CalibrateLocalFeet(float delayTime)
	{
		if (CEO.Instance.GetIsPlayerLeft())
		{
			FadeLeftFeetTo(1f, 0.5f);
		}
		else
		{
			FadeRightFeetTo(1f, 0.5f);
		}

		Invoke("CalibrateLocalFeet", delayTime);
	}

	public void CalibrateLocalFeet()
	{
		if (dataManager.GetBodyJointsLocal().GetAllSkeletonData().Count == 1)
		{
			GameObject foot01 = null;
			GameObject foot02 = null;
			if (CEO.Instance.GetIsPlayerLeft())
			{
				foot01 = CalibrationIndicatorL01;
				foot02 = CalibrationIndicatorL02;
			}
			else
			{
				foot01 = CalibrationIndicator01;
				foot02 = CalibrationIndicator02;
			}


			kuntLocal.CalibrateFeet(dataManager.GetBodyJointsLocal().GetAllSkeletonData()[0], foot01.transform.position, foot02.transform.position);
			kuntLocal.SetZSide(1 * sideSwap);
		}
		FadeLeftFeetTo(0f, 0.5f);
		FadeRightFeetTo(0f, 0.5f);
	}


	public void CalibrateRemoteFeet(float delayTime)
	{

		if (!CEO.Instance.GetIsPlayerLeft())
		{
			FadeLeftFeetTo(1f, 0.5f);
		}
		else
		{
			FadeRightFeetTo(1f, 0.5f);
		}
		Invoke("CalibrateRemoteFeet", delayTime);
	}

	public void CalibrateRemoteFeet()
	{
		bool asLocal = true;

		if (asLocal || (dataManager.GetBodyJointsRemote() != null && dataManager.GetBodyJointsRemote().GetAllSkeletonData() != null && dataManager.GetBodyJointsRemote().GetAllSkeletonData().Count == 1))
		{

			GameObject foot01 = null;
			GameObject foot02 = null;
			if (!CEO.Instance.GetIsPlayerLeft())
			{
				foot01 = CalibrationIndicatorL01;
				foot02 = CalibrationIndicatorL02;
			}
			else
			{
				foot01 = CalibrationIndicator01;
				foot02 = CalibrationIndicator02;
			}

			if (asLocal)
			{
				LPlayerPF.Instance.CmdCalibrateRemoteAsLocal(foot01.transform.position, foot02.transform.position, -sideSwap, SaveLoad.savedSettings.protonDistance);

			}
			/*else
			{

				kuntRemote.CalibrateFeet(dataManager.GetBodyJointsRemote().GetAllSkeletonData()[0], foot01.transform.position, foot02.transform.position);

				kuntRemote.SetZSide(-1 * sideSwap);
			}*/
		}

		FadeLeftFeetTo(0f, 0.5f);
		FadeRightFeetTo(0f, 0.5f);
	}

	public void CalibrateRemoteFeetAsLocal()
	{

	}

	public void CalibrateRemoteAsLocal(Vector3 leftFootPos, Vector3 rightFootPos, float _sideSwap)
	{
		if (dataManager.GetBodyJointsLocal().GetAllSkeletonData().Count == 1)
		{
			kuntLocal.CalibrateFeet(dataManager.GetBodyJointsLocal().GetAllSkeletonData()[0], leftFootPos, rightFootPos);
			//kuntLocal.SetZSide(1 * _sideSwap);
			kuntLocal.SetZSide(1);
		}


	}

	private void UpdateFeetMarkers(Vector3 markerPos, int currentNumber, bool isLast)
	{
		//Debug.Log(currentNumber);
		if (currentNumber > feetMarkers.Count) //need to create a new marker
		{
			GameObject newMarker = Instantiate(FootMarker, markerPos, Quaternion.identity);
			Foot newFoot = newMarker.GetComponent( typeof(Foot) ) as Foot;
			feetMarkers.Add(newFoot);

			if (!renderFeetMarkers)
			{
				newMarker.transform.localScale = new Vector3(0, 0, 0);
				//newMarker.GetComponent<Renderer>().enabled = false;
			}
		}

		if (feetMarkers[currentNumber - 1] != null) feetMarkers[currentNumber - 1].transform.position = markerPos;
	}


	public void SetFeetVisibility(bool fv)
	{
		float scale = 0f;
		if (fv) scale = 5f;

		foreach (Foot foot in feetMarkers)
		{
			if(foot != null) foot.SetScale(scale);
		}

		renderFeetMarkers = fv;
	}

	public void ToggleFeetVisibility()
	{
		renderFeetMarkers = !renderFeetMarkers;
		SetFeetVisibility(renderFeetMarkers);
	}

	private void TrimFeetMarkers(int numFeet)
	{
		if (numFeet < feetMarkers.Count)
		{
			while (numFeet < feetMarkers.Count)
			{
				if (feetMarkers[feetMarkers.Count - 1] != null)	feetMarkers[feetMarkers.Count - 1].Kill();
				feetMarkers.RemoveAt(feetMarkers.Count - 1);
				Debug.Log("Cleaned " + feetMarkers.Count);
			}
		}
	}

	public List<Foot> GetFeet()
	{
		return feetMarkers;
	}


	public string GetFeetString()
	{
		string feetStrg = "";

		foreach (Foot foot in feetMarkers)
		{
			string FootPos = foot.GetPosition().ToString("F1");
			feetStrg += FootPos + "   ";
		}

		return feetStrg;
	}

	public List<HandParams[]> GetAllUsersHandsInPairs()
	{
		return dataManager.GetBodyJointsLocal().GetAllUsersHandsInPairs();
	}

	private void ResetSpeedCycle()
	{
		topSpeed = lastTopSpeed;
		Invoke("ResetSpeedCycle", instantSpeedInterval);
	}

	public float GetTopSpeed()
	{
		//Debug.Log(topSpeed);
		return topSpeed;
	}

	public Vector3 CheckKick(Vector3 tPosition, float tRadius)
	{
		Vector3 kickVector = Vector3.zero;
		lastTopSpeed = 0f;

		foreach (Foot foot in feetMarkers)
		{
			if (foot.GetSpeed() > lastTopSpeed) lastTopSpeed = foot.GetSpeed();
			if (lastTopSpeed > topSpeed) topSpeed = lastTopSpeed;

			Vector3 diffPos = foot.GetPosition() - tPosition;
			if (diffPos.magnitude < tRadius)
			{
				kickVector = foot.GetDirection();
				//foot.FlashArrow();
				break;
			}
		}

		return kickVector;
	}

	public Vector3 GetFirstFootPosition()
	{
		return feetMarkers[0].GetPosition();
	}

	public void FadeLeftFeetTo(float alpha, float time)
	{
		StartCoroutine(FadeObjectTo(CalibrationIndicatorL01, alpha, time));
		StartCoroutine(FadeObjectTo(CalibrationIndicatorL02, alpha, time));
	}

	public void FadeRightFeetTo(float alpha, float time)
	{
		StartCoroutine(FadeObjectTo(CalibrationIndicator01, alpha, time));
		StartCoroutine(FadeObjectTo(CalibrationIndicator02, alpha, time));
	}

	IEnumerator FadeObjectTo(GameObject fObject, float aValue, float aTime)
	{
		if (aValue != 0f)
		{
			fObject.SetActive(true);
		}
		Material objMat = fObject.GetComponent<Renderer>().material;
		Color objColor = objMat.GetColor("_TintColor");
		float alpha = objColor.a;
		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
		{
			Color newColor = new Color(objColor.r, objColor.g, objColor.b, Mathf.Lerp(alpha, aValue, t));
			objMat.SetColor("_TintColor", newColor);
			yield return null;
		}
		objMat.SetColor("_TintColor", new Color(objColor.r, objColor.g, objColor.b, aValue));
		if (aValue == 0f)
		{
			fObject.SetActive(false);
		}
	}

	public Texture2D GetReceivedTex()
	{
		return dataManager.receivedTex;
	}

	public void SideSwap()
	{
		sideSwap = -sideSwap;
	}

	public float GetSideSwap()
	{
		return sideSwap;
	}

	public void Rotate180()
	{
		kuntLocal.Flip180();
	}

	public float GetRotate180()
	{
		return kuntLocal.GetRotate180();
	}

	public string GetStringData()
	{
		return dataManager.GetStringData();
	}

	public void ReceiveData()
	{
		dataManager.ReceiveData();
		//UpdateRemote ();
	}

	/*public void UpdateRemote ()
	{

		if (useRemoteSkeleton && dataManager.IsBodyJointsRemoteAvailable())
		{
			numFeetRemote = 0;
			float scale = 1f;

			if (showFeetMarkers)
			{
				foreach (Vector3 footPos in dataManager.GetBodyJointsRemote().GetAllUsersFeet())
				{
					Vector3 kaptFootPos = scale * kuntRemote.Kino2World(footPos, 1);
					UpdateFeetMarkers(new Vector3(kaptFootPos.x, 0, kaptFootPos.z), ++numFeetRemote, false);
				}
			}
		}

	}*/



	public void Disable()
	{
		KinectManager.Instance.OnApplicationQuit();
		KinOps.Instance.OnApplicationQuit();
	}


	void Update ()
	{

		if (isActive)
		{
			numFeet = 0;
			if (useLocalSkeleton && KinectManager.Instance.IsUserDetected())
			{
				float scale = 1f;

				if (showFeetMarkers)
				{

					foreach (Vector3 footPos in dataManager.GetBodyJointsLocal().GetAllUsersFeet())
					{
						Vector3 kaptFootPos = scale * kuntLocal.Kino2World(footPos, 1);
						UpdateFeetMarkers(new Vector3(kaptFootPos.x, 0, kaptFootPos.z), ++numFeet, false);
					}
				}
			}

			//UpdateRemote ();
			//
			/*if (useRemoteSkeleton && dataManager.IsBodyJointsRemoteAvailable())
			{
				float scale = 1f;

				if (showFeetMarkers)
				{
					foreach (Vector3 footPos in dataManager.GetBodyJointsRemote().GetAllUsersFeet())
					{
						Vector3 kaptFootPos = scale * kuntRemote.Kino2World(footPos, 1);
						UpdateFeetMarkers(new Vector3(kaptFootPos.x, 0, kaptFootPos.z), ++numFeet, false);
					}
				}
			}*/

			if (showFeetMarkers) TrimFeetMarkers(numFeet);

			//CheckKick (new Vector3(0,0,0), 18f);

			if (Input.GetKeyUp("space"))
			{
				SideSwap();
			}

			if (Input.GetKeyUp("v"))
			{
				Rotate180();
			}

			if (Input.GetKeyUp("x"))
			{
				CalibrateLocalFeet(2f);
			}

			if (Input.GetKeyUp("c"))
			{
				CalibrateRemoteFeet(2f);
			}

			if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.X))

			{
				Invoke("CalibrateSide", 2f);
			}
		}

	}
}
