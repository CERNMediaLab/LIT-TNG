﻿using UnityEngine;
using System.Collections;

public class KillShuriken : MonoBehaviour
{

    private ParticleSystem shuriken;
    private float secondsToDie = 4f;
    void Start ()
    {
    	shuriken = this.transform.GetComponent<ParticleSystem>();
    	shuriken.playbackSpeed = 6f;
    }

    
    void Update ()
    {
    	shuriken.playbackSpeed = 4f;
        secondsToDie -= Time.deltaTime;
    	if(secondsToDie <=0)
    	{
    		Kill();
    	}

    }
    
    private void Kill()
    {
    	//Debug.Log("dead");
    	Destroy(this.gameObject);
    }
}
