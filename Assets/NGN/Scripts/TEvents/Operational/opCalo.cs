﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class opCalo
{

    private TEventAgent agent;
    private TStructCalo structCalo;

    private GameObject ParentEvent;
    private MeshCollider meshCollider;
    private Mesh mesh;
    private Material trackMaterial;

    private Color colorNormal;
    private Color colorSelected;
    private Color colorHover;
    private Color colorEnabled;
    private GameObject towerMesh;

    private float innerRadius;
    private float outerRadius;
    public int side;

    private bool meshExists = false;
    public float meshScale = 0.55f;
    private bool isTowerFullyGrown;
    private float maxAlpha = 135.0f / 255f;

    public Vector3 p1n;
    public Vector3 p2n;
    public Vector3 p3n;
    public Vector3 p4n;

    public void SetStructCalo(ref TStructCalo scalo)
    {
        structCalo = scalo;
        colorNormal = new Color(1f, 1f, 55.0f / 255f, maxAlpha);
        colorNormal.a = maxAlpha;
        //meshScale *= agent.eventScaleFactor;
        //colorSelected = new Color(1f, 27.0f / 255f, 15.0f / 255f, 135.0f / 255f);
        //colorHover = new Color(0f, 1f, 120f / 255f, 135.0f / 255f);
        //colorEnabled = new Color(1f, 1f, 120f / 255f, 135.0f / 255f);
        
        colorNormal = agent.caloMaterial.GetColor("_TintColor");
        maxAlpha = colorNormal.a;

        innerRadius = calculateMidPointDistance(structCalo.p1, structCalo.p2, structCalo.p3, structCalo.p4);
        outerRadius = calculateMidPointDistance(structCalo.p1u, structCalo.p2u, structCalo.p3u, structCalo.p4u);
        //Debug.Log("innerRadius "+innerRadius+" outerRadius "+outerRadius);
        isTowerFullyGrown = false;
    }

    private void DestroyTower()
    {
        if (towerMesh)
        {
            GameObject.Destroy(towerMesh);
        }
        meshExists = false;
    }

    public void Clamp(float radius)
    {
        float clampingRadius = radius / meshScale;
        //Debug.Log(clampingRadius + " "+innerRadius);

        if (clampingRadius <= innerRadius) //don't build the geometry
        {
            p1n = structCalo.p1;
            p2n = structCalo.p2;
            p3n = structCalo.p3;
            p4n = structCalo.p4;
            DestroyTower();
        }

        if (clampingRadius >= outerRadius && !isTowerFullyGrown) //build the whole geometry
        {

            if (!meshExists)
            {
                BuildCaloGeometry();
            }
            p1n = structCalo.p1u;
            p2n = structCalo.p2u;
            p3n = structCalo.p3u;
            p4n = structCalo.p4u;

            isTowerFullyGrown = true;
        }

        if (clampingRadius > innerRadius && clampingRadius < outerRadius) //clamp!
        {
            if (!meshExists)
            {
                BuildCaloGeometry();
            }

            float fraction = (clampingRadius - innerRadius) / (outerRadius - innerRadius);

            p1n = Vector3.Lerp(structCalo.p1, structCalo.p1u, fraction);
            p2n = Vector3.Lerp(structCalo.p2, structCalo.p2u, fraction);
            p3n = Vector3.Lerp(structCalo.p3, structCalo.p3u, fraction);
            p4n = Vector3.Lerp(structCalo.p4, structCalo.p4u, fraction);

            isTowerFullyGrown = false;

        }

        //towerMesh.renderer.material.SetColor ("_Color", colorNormal);
        //towerMesh.renderer.material.SetColor ("_Emission", colorNormal);
        updateMeshVertices();

    }

    /**calculates distance to the point in the middle*/
    /**of a surface defined by four vertexes*/
    private float calculateMidPointDistance(Vector3 point1, Vector3 point2, Vector3 point3, Vector3 point4)
    {
        Vector3 midpoint = (point1 + point2 + point3 + point4) / 4.0f;
        return midpoint.magnitude;
    }

    private void updateMeshVertices()
    {

        if (meshExists)
        {

            Vector3[] vertices = mesh.vertices;
            vertices[4] = p1n * meshScale;
            vertices[5] = p2n * meshScale;
            vertices[6] = p3n * meshScale;
            vertices[7] = p4n * meshScale;
            vertices[10] = p2n * meshScale;
            vertices[11] = p1n * meshScale;
            vertices[14] = p4n * meshScale;
            vertices[15] = p3n * meshScale;
            vertices[17] = p1n * meshScale;
            vertices[18] = p4n * meshScale;
            vertices[21] = p2n * meshScale;
            vertices[22] = p3n * meshScale;

            mesh.vertices = vertices;

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            ;
        }
    }

    public void BuildCaloGeometry()
    {
        if (!meshExists)
        {
            towerMesh = new GameObject("CaloTower");

            towerMesh.gameObject.AddComponent<MeshFilter>();
            towerMesh.gameObject.AddComponent(typeof(MeshRenderer));
            mesh = new Mesh ();
            towerMesh.GetComponent<MeshFilter>().mesh = mesh;

            towerMesh.transform.RotateAround(Vector3.zero, Vector3.up, agent.displaceRotation);
            towerMesh.transform.parent = agent.ParentEvent.transform;
            towerMesh.transform.position = agent.displacePosition;// + agent.ParentEvent.transform.position;



            Vector3[] vertices = new Vector3[24];
            Vector2[] uvs = new Vector2[24];

            vertices[0] = structCalo.p1 * meshScale;
            vertices[1] = structCalo.p2 * meshScale;
            vertices[2] = structCalo.p3 * meshScale;
            vertices[3] = structCalo.p4 * meshScale;

            vertices[4] = p1n;
            vertices[5] = p2n;
            vertices[6] = p3n;
            vertices[7] = p4n;

            vertices[8] = structCalo.p1 * meshScale;
            vertices[9] = structCalo.p2 * meshScale;
            vertices[10] = p2n;
            vertices[11] = p1n;

            vertices[12] = structCalo.p3 * meshScale;
            vertices[13] = structCalo.p4 * meshScale;
            vertices[14] = p4n;
            vertices[15] = p3n;

            vertices[16] = structCalo.p1 * meshScale;
            vertices[17] = p1n;
            vertices[18] = p4n;
            vertices[19] = structCalo.p4 * meshScale;

            vertices[20] = structCalo.p2 * meshScale;
            vertices[21] = p2n;
            vertices[22] = p3n;
            vertices[23] = structCalo.p3 * meshScale;

            for (int t  = 0; t < 6; t++)
            {
                uvs[t * 4 + 0] = Vector2.zero;
                uvs[t * 4 + 1] = Vector2.right;
                uvs[t * 4 + 2] = Vector2.one;
                uvs[t * 4 + 3] = Vector2.up;
            }

            mesh.vertices = vertices;

            if (side == -1)
            {
                mesh.triangles = new int[]
                {
                    0, 1, 2,
                    0, 2, 3,
                    4, 6, 5,
                    4, 7, 6,
                    8, 10, 9,
                    8, 11, 10,
                    12, 14, 13,
                    12, 15, 14,
                    16, 18, 17,
                    16, 19, 18,
                    20, 21, 22,
                    20, 22, 23
                };
            }
            else
            {
                mesh.triangles = new int[]
                {
                    0, 2, 1,
                    0, 3, 2,
                    4, 5, 6,
                    4, 6, 7,
                    8, 9, 10,
                    8, 10, 11,
                    12, 13, 14,
                    12, 14, 15,
                    16, 17, 18,
                    16, 18, 19,
                    20, 22, 21,
                    20, 23, 22
                };
            }


            mesh.uv = uvs;

            meshExists = true;

            //towerMesh.renderer.material.shader = Shader.Find("Transparent/VertexLit");
            towerMesh.GetComponent<Renderer>().material = agent.caloMaterial;

            //towerMesh.renderer.material.SetColor ("_Emission", colorNormal);

            towerMesh.gameObject.GetComponent<Renderer>().enabled = true;
            towerMesh.gameObject.layer = 30;

            Vector3 rotPhi = new Vector3(0, 0, 0);

            towerMesh.transform.Rotate(rotPhi, Space.World);

        }


        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        ;
        towerMesh.GetComponent<MeshFilter>().mesh = mesh;

    }

    private void destroyEverything()
    {
        GameObject.Destroy(towerMesh);
        ParentEvent = null;
        agent = null;
        meshCollider = null;
        mesh = null;
    }


    public void SetAgent(TEventAgent ag)
    {
        agent = ag;
        meshScale *= agent.eventScaleFactor;
    }

    public GameObject getTowerMesh()
    {
        return towerMesh;
    }

    public void FadeTo(float fadeTo)
    {
        if (towerMesh != null && towerMesh.GetComponent<Renderer>() != null)
        {
            colorNormal = towerMesh.GetComponent<Renderer>().material.GetColor("_TintColor");
            colorNormal.a = fadeTo;
            towerMesh.GetComponent<Renderer>().material.SetColor ("_TintColor", colorNormal);
            towerMesh.GetComponent<Renderer>().material.SetColor ("_Emission", colorNormal);
        }
    }

    public void FadeToRelative(float fadeTo)
    {
        FadeTo(fadeTo*maxAlpha);
    }

    private void destroyMeshCollider()
    {
        if (towerMesh.gameObject.GetComponent("MeshCollider") == null)
        {
            GameObject.Destroy(towerMesh.gameObject.GetComponent("MeshCollider"));
        }
    }

    private void rebuildMeshCollider()
    {
        if (towerMesh.gameObject.GetComponent("MeshCollider") == null)
        {
            towerMesh.gameObject.AddComponent<MeshCollider>();
        }
    }

    private void destroyGeometry()
    {
        //destroyMeshCollider();
        if (towerMesh)
        {
            GameObject.Destroy(towerMesh);
        }

    }

}
