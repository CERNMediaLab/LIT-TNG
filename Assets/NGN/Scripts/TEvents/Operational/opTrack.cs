using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

using System.Text;

public class opTrack
{
    private TEventAgent agent;
    private TStructTrack structTrack;
    private float[] segmentLengths;
    private float totalLength = 0f;
    private Vector3[] clampedPolyline;
    public VectorLine trackLine;
    private Material trackMaterial;
    private float lineThickness = 7f;
    public GameObject trackParticles;
    private ParticleSystem shuriken;
    private Color retColor = Color.white;

    private bool isBeingHovered = false;
    float[] widths;
    
    public void SetStructTrack(ref TStructTrack st)
    {
        widths =  new float[2];
        widths[0] = 1f;
        widths[1] = lineThickness;
        structTrack = st;
        
        if (!agent.strEvent.hasPolylines)
        {
            // if polyline was not specified, we need to calculate it ourselves
            calculateReconstructedTrack();
        }
        
        
        CalculateSegments();
        if (agent.displayShuriken)
        {
            trackParticles = (GameObject)GameObject.Instantiate(agent.trackParticles, structTrack.polyline[0] * agent.eventScaleFactor + agent.displacePosition  + agent.ParentEvent.transform.position, agent.ParentEvent.transform.rotation);
            trackParticles.transform.parent = agent.ParentEvent.transform;
            shuriken = trackParticles.transform.GetComponent<ParticleSystem>();
        }
        SetColor();
        st = structTrack;
        
    }
    
    public void SetAgent(TEventAgent ag)
    {
        agent = ag;
    }
    
    public void calculateReconstructedTrack()
    {
        if(TEventManager.Instance.eventToHire.experiment=="ATLAS")
        {
            float _cottheta2 = 1.0f / Mathf.Tan(structTrack.theta*Mathf.Deg2Rad/2f);
            float eta =(Mathf.Log(_cottheta2));
            

           // Debug.Log(_cottheta2 + " " + eta + " " + structTrack.theta + " " + structTrack.pt + " " + structTrack.phi );
            THelix helix = new THelix(structTrack.d0, structTrack.z0, structTrack.phi, eta, structTrack.pt);
            TDHelix dhelix = new TDHelix(helix);
            structTrack.eta = helix.eta;
            structTrack.polyline = dhelix.generatePolyline();
        }
        else if(TEventManager.Instance.eventToHire.experiment=="ALICE")
        {
            TDHelix dhelix = new TDHelix(structTrack.pv_x, structTrack.pv_y,structTrack.pv_z, structTrack.phi,structTrack.theta, structTrack.pt,structTrack.C);
            structTrack.eta = -Mathf.Log(Mathf.Tan(structTrack.theta/2f));
            structTrack.polyline = dhelix.generatePolyline();
        }
    }
    
    public void SetMaterial(Material mat)
    {
        trackMaterial = mat;
        //Debug.Log(trackMaterial);
    }
    
    public void SetIsBeingHovered(bool ibh)
    {
        isBeingHovered = ibh;
    }
    
    public void OnHoverEnter()
    {
        trackLine.lineWidth = lineThickness * 4f;
        ShowTrackInfo();
    }
    
    public void OnHoverExit()
    {
        if (trackLine != null)
        {
            trackLine.lineWidth = lineThickness;
        }
        HideTrackInfo();
    }
    
    private void ShowTrackInfo()
    {
        TEventManager.Instance.trackInfoTextObject.GetComponent<GUIText>().text = GetTStructTrackInfo().ToString();
        TEventManager.Instance.trackInfoTextObject.GetComponent<GUIText>().pixelOffset = new Vector2 (10, 150);
        Vector3 mousePos = Input.mousePosition;
        TEventManager.Instance.trackInfoObject.transform.position = new Vector3(mousePos.x / Screen.width, mousePos.y / Screen.height, mousePos.z);
        TEventManager.Instance.trackInfoObject.GetComponent<GUITexture>().pixelInset = new Rect (0, 0, 200, 160);
        TEventManager.Instance.trackInfoObject.active = true;
    }
    
    private void HideTrackInfo()
    {
        TEventManager.Instance.trackInfoObject.active = false;
    }
    
    public void CalculateSegments()
    {
        /** make the segmentLengths array **/
        int numPoints = structTrack.polyline.Length;
        if(structTrack.polyline==null || numPoints==0)
        {
            totalLength=0;
            return;
        }
        
        segmentLengths = new float[numPoints];
        for (int seg = 0; seg < numPoints; seg++)
        {
            if (seg == 0 )
            {
                segmentLengths[seg] = structTrack.polyline[seg].magnitude;
            }
            
            else
            {
                segmentLengths[seg] = segmentLengths[seg - 1] + (structTrack.polyline[seg] - structTrack.polyline[seg - 1]).magnitude;
            }
        }
        totalLength = segmentLengths[numPoints - 1];
    }
    
    
    
    public int CalculateLastFullPolylineIndexOfDistance(float distance)
    {
        int index = 0;
        float sumDist = 0F;
        
        for (int i = 0; i < structTrack.polyline.Length; i++)
        {
            
            if (sumDist < distance)
            {
                sumDist = segmentLengths[i];
                index = i;
            }
            
            else
                break;
        }
        
        index--;
        
        //sumDist = index >= 0F ?  segmentLengths[index] : 0F;
        if (index < 0)
        {
            index = 0;
        }
        
        return index;
    }
    
    public Vector3 CalculateXplodingPoint(Vector3 pointA, Vector3 pointB, float lengthDiff)
    {
        Vector3 XplodPoint = pointA + Vector3.ClampMagnitude(pointB - pointA, lengthDiff);
        return XplodPoint;
    }
    
    public Vector3 CalculateXplodingPoint(float distance)
    {
        Vector3 XplodPoint;
        if (distance >= totalLength && structTrack.polyline.Length >= 1)
        {
            XplodPoint = structTrack.polyline[structTrack.polyline.Length - 1];
        }
        else
        {
            int lastIndex = CalculateLastFullPolylineIndexOfDistance(distance);
            
            Vector3 pointA = structTrack.polyline[lastIndex];
            Vector3 pointB = structTrack.polyline[lastIndex + 1];
            float lengthDiff = distance - segmentLengths[lastIndex];
            XplodPoint = pointA + Vector3.ClampMagnitude(pointB - pointA, lengthDiff);
            
            
        }
        return XplodPoint * agent.eventScaleFactor;
    }
    
    public void ZeroTrack()
    {

        clampedPolyline = new Vector3[2];
        clampedPolyline[0] = new Vector3(0, 0, 0);
        clampedPolyline[1] = new Vector3(0, 0, 0);


        //trackLine.ZeroPoints();
        
    }
    
    public void CalculateClampedTrack(float length)
    {
        if (length < totalLength && (agent.displayTrack))
            
        {
            int index = CalculateLastFullPolylineIndexOfDistance(length);
            
            
            //Debug.Log(index);
            if (index == -1)
            {
                ZeroTrack();
                Debug.Log("zero");
            }
            
            else
            {
                float lengthAtPoints = segmentLengths[index];
                int numpoints = (length - lengthAtPoints) > 0 ? index + 2 : index + 1;
                
                
                clampedPolyline = new Vector3[numpoints];
                
                for (int i = 0; i <= index; i++)
                {
                    clampedPolyline[i] = structTrack.polyline[i];
                    //Debug.Log(clampedPolyline[i]);
                }
                
                if (numpoints > index)
                {
                    clampedPolyline[numpoints - 1] = CalculateXplodingPoint(structTrack.polyline[index], structTrack.polyline[index + 1], length - lengthAtPoints);
                }
                
            }
        }
        else
        {
            clampedPolyline = structTrack.polyline;
        }
    }
    
    public void Clamp(float unscaledLength)
    {
        Clamp(unscaledLength, false);
    }

    public void Clamp(float unscaledLength, bool fast)
    {

        //unscaledLength = totalLength/4f;
        float length = unscaledLength / agent.eventScaleFactor;

        //Debug.Log(length + "  " + totalLength );
        if (agent.displayTrack)
        {
            if (length <= totalLength * 1.1f)
            {
                CalculateClampedTrack(length);
            }
            
            //trackLine.Resize(clampedPolyline);
            if (clampedPolyline.Length > 1)
            {
                trackLine.Resize(clampedPolyline.Length);

                if (fast)
                {
                    trackLine.points3[clampedPolyline.Length-1] = clampedPolyline[clampedPolyline.Length-1];
                }
                else {

                    for (int w = 0; w < clampedPolyline.Length; w++)
                    {
                        trackLine.points3[w] = clampedPolyline[w];
                    }
                }
                
            }
        }
        
        if (agent.displayShuriken)
        {
            trackParticles.transform.position = CalculateXplodingPoint(length)  + agent.ParentEvent.transform.position;
            trackParticles.transform.RotateAround(Vector3.zero, Vector3.up, agent.displaceRotation);
            trackParticles.transform.position += agent.displacePosition;
            
            if (length >= totalLength)
            {
                shuriken.playbackSpeed = 0;
            }
        }
    }
    
    public void CalculateLine()
    {
        if (agent.displayTrack)
        {
            CalculateClampedTrack(agent.maxExplosionLength / agent.eventScaleFactor);
            VectorLine.Destroy(ref trackLine);
            //VectorLine.SetEndCap("Rounded")
            ZeroTrack();
             trackLine = new VectorLine("TrackLine", clampedPolyline, retColor, trackMaterial, lineThickness, LineType.Continuous, Joins.Weld);
            //trackLine.SetWidths(widths);
            trackLine.drawTransform = agent.ParentEvent.transform;
            //trackLine.gameObject.transform.parent = agent.ParentEvent.transform;
        }
        
    }
    
    public void SetColor()
    {
        if(TEventManager.Instance.eventToHire.experiment=="ALICE")
        {
            
            retColor = Color.black;
            if (structTrack.pname == "pi+")
                retColor = Color.red;
            else if (structTrack.pname == "pi-")
                retColor = Color.red;
            else if (structTrack.pname == "K+")
                retColor = Color.yellow;
            else if (structTrack.pname == "K-")
                retColor = Color.yellow;
            else if (structTrack.pname == "D0")
                retColor = Color.red;
            else if (structTrack.pname == "mi-")
                retColor = Color.green;
            else if (structTrack.pname == "mi+")
                retColor = Color.green;
            else if (structTrack.pname == "p+")
                retColor = new Color(1.0f,0.5f,0.0f,1.0f);
            else if (structTrack.pname == "p-")
                retColor = new Color(1.0f,0.5f,0.0f,1.0f);
            else if (structTrack.pname == "deuteron")
                retColor = new Color(1.0f,0.5f,0.0f,1.0f);
            else if (structTrack.pname == "e-")
                retColor = Color.blue;
            else if (structTrack.pname == "e+")
                retColor = Color.blue;
            else if (structTrack.pname == "v0")
                retColor = new Color(1.0f,0.0f,1.0f,1.0f);
            else if (structTrack.pname == "kink")
                retColor = new Color(0.0f,1.0f,1.0f,1.0f);
            else if (structTrack.pname == "cascade")
                retColor = Color.white;
        }
        else
        {
            
            retColor = Color.white;
            
            float perc = Mathf.Abs(structTrack.pt / agent.strEvent.maxPt);
            
            if (perc <= 0.25f)
            {
                retColor = Color.red;
                //retColor = new Color32(203,124,124,255);
            }
            else if (perc <= 0.5f)
            {
                
                retColor = Color.yellow;
                //retColor = new Color32(200,203,124,255);
            }
            else if (perc <= 0.75f)
            {
                retColor = Color.green;
                //retColor = new Color32(124,203,144,255);
            }
            else
            {
                retColor = Color.white;
                //retColor = new Color32(239,239,239,255);
                //lhcb, ana 16.04.
                if (structTrack.pname == "pi+")
                    retColor = Color.cyan;
                else if (structTrack.pname == "pi-")
                    retColor = Color.yellow;
                else if (structTrack.pname == "K+")
                    retColor = Color.blue;
                else if (structTrack.pname == "K-")
                    retColor = Color.green;
                else if (structTrack.pname == "D0")
                    retColor = Color.red;
                //end
            }
        }
        if (agent.displayShuriken)
        {
            shuriken.startColor = retColor;
        }
        
    }
    
    public void Draw()
    {
        if (agent.displayTrack)
        {
            if (trackLine != null && trackLine.points3.Length > 1)
            {
                //Debug.Log(trackLine.GetLength());
                trackLine.active = true;
                trackLine.SetColor(retColor);
                if (agent.Draw3D)
                {
                    //trackLine.Draw3D();
                    trackLine.Draw();
                }
                else
                {
                    trackLine.Draw();
                }
            }
        }
    }
    
    public bool IsReady()
    {
        return (trackLine != null);
    }
    
    public void FadeTo(float fadeTo)
    {
        
        retColor.a = fadeTo;
        if (agent.displayTrack)
        {
            trackLine.SetColor(retColor);
        }
    }
    public void Zero()
    {
        trackLine.ZeroPoints();
    }
    public void DestroyShuriken()
    {
        if (agent.displayShuriken)
        {
            trackParticles.AddComponent<KillShuriken>();
        }
        
    }
    
    public void DestroyTrack()
    {
        if (agent.displayTrack)
        {
            VectorLine.Destroy(ref trackLine);
        }
        
    }
    
    public void DestroyMeshCollider()
    {
        if (trackLine != null)
        {
            trackLine.collider = false;
        }
    }
    
    public void RebuildMeshCollider()
    {
        if (trackLine != null)
        {
            trackLine.collider = true;
        }
    }
    
    public StringBuilder GetTStructTrackInfo()
    {
        //StringBuilder trackInfo = new StringBuilder("Track Information\n");
        //trackInfo.Append("PName: " + structTrack.pname + "\n");
        //trackInfo.Append("E: " + structTrack.E + "\n");
        StringBuilder trackInfo = new StringBuilder("E: " + structTrack.E + "\n");
        //trackInfo.Append("Px: " + structTrack.px + "\n");
        //trackInfo.Append("Py: " + structTrack.py + "\n");
        //trackInfo.Append("Pz: " + structTrack.pz + "\n");
        trackInfo.Append("Pt: " + structTrack.pt + "\n");
        trackInfo.Append("q: " + structTrack.q + "\n");
        trackInfo.Append("z0: " + structTrack.z0 + "\n");
        trackInfo.Append("d0: " + structTrack.d0 + "\n");
        trackInfo.Append("m: " + structTrack.m + "\n");
        trackInfo.Append("chi2: " + structTrack.chi2 + "\n");
        trackInfo.Append("eta: " + structTrack.eta + "\n");
        trackInfo.Append("cottheta: " + structTrack.cottheta + "\n");
        trackInfo.Append("phi: " + structTrack.phi + "\n");
        trackInfo.Append("mass: " + structTrack.mass + "\n");
        
        trackInfo.Append("ip: " + structTrack.ip + "\n");
        //trackInfo.Append("pv_z: " + structTrack.pv_z + "\n");
        //trackInfo.Append("pv_x: " + structTrack.pv_x + "\n");
        //trackInfo.Append("pv_y: " + structTrack.pv_y + "\n");
        trackInfo.Append("ipchi2: " + structTrack.ipchi2 + "\n");
        //trackInfo.Append("zFirstMeasurement: " + structTrack.zFirstMeasurement + "\n");
        
        return trackInfo;
    }
    
}
