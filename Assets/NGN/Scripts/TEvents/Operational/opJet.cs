﻿using UnityEngine;
using System.Collections;

public class opJet
{
    private TStructJet structJet;
    private TEventAgent agent;

    private MeshCollider meshCollider;
    private Mesh mesh;

    private Material jetMaterial;
    private bool isVisible;   
    

    private Color colorNormal;
    private Color colorSelected;
    private Color colorHover;
    private Color colorEnabled;
    private GameObject cone;

    private float scaleEt = 0; 


    public void Setup()
    {        
        colorNormal = new Color(15.0f / 255f, 35.0f / 255f, 1.0f, 135.0f / 255f);
        colorSelected = new Color(1f, 27.0f / 255f, 15.0f / 255f, 135.0f / 255f);
        colorHover = new Color(0f, 1f, 120f / 255f, 135.0f / 255f);
        colorEnabled = new Color(1f, 1f, 120f / 255f, 135.0f / 255f);
        scaleEt = 0f;
    }

    public void SetStructJet(TStructJet sjet)
    {
    	Setup();
        structJet = sjet;
        CreateConeJet();
    }

    public void SetAgent(TEventAgent ag)
    {
        agent = ag;
        //Debug.Log(agent);
    }

    public GameObject getCone()
    {
        return cone;
    }

    public void CreateConeJet()
    {
        //Debug.Log(agent.GetManager());
        cone = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/jet", typeof(GameObject)));

        cone.gameObject.GetComponent<Renderer>().enabled = false;
        //cone.gameObject.layer = 30;
        
        scaleEt = structJet.et * 0.03f;
        scaleEt = Mathf.Clamp(scaleEt, 0f, 100f);
        cone.transform.localScale = new Vector3(scaleEt,scaleEt,scaleEt);

        // eta = -ln(tg theta/2), so theta = 2*atan(exp(-eta))
        structJet.theta = 2 * Mathf.Atan(Mathf.Exp(-structJet.eta));

        float thetaDeg = Mathf.Rad2Deg * structJet.theta;
        float phiDeg = Mathf.Rad2Deg * structJet.phi;

        /*thetaDeg = 0;
        phiDeg = 0 ;
        float z:float = 22;*/

        cone.GetComponent<Renderer>().material.SetColor ("_TintColor", colorNormal);

        Vector3 rotTheta = new Vector3(0, thetaDeg +90f, 0); //set
        Vector3 rotPhi = new Vector3(phiDeg, 0, 0); //set


        cone.transform.parent = agent.ParentEvent.transform;
        cone.transform.localPosition = Vector3.zero;

        cone.transform.Rotate(rotTheta, Space.World);

        //cone.transform.Rotate(rotPhi, Space.World);

        //Debug.Log("eta: "+eta+"  phiDeg: "+phiDeg+"  thetaDeg: "+thetaDeg);
        
    }

    public void ScaleSize(float scale)
    {
        cone.transform.localScale = new Vector3(scaleEt * scale, scaleEt * scale, scaleEt * scale);        
    }

    public void Fade(float scale)
    {
        colorNormal.a = scale * 135.0f / 255f;
        cone.GetComponent<Renderer>().material.SetColor ("_TintColor", colorNormal);
    }

    public void destroyEverything()
    {
        GameObject.Destroy(cone);        
        agent = null;
        meshCollider = null;
        mesh = null;
    }



    

    public float getTl()
    {
        return (Mathf.Cos(structJet.theta));
    }

    

    bool getIsVisible()
    {
        return isVisible;
    } 
    

    public void setVisibility(bool vis)
    {
        if (vis != isVisible)
        {
            if (vis)
            {
                cone.GetComponent<Renderer>().enabled = true;

            }

            if (!vis)
            {
                cone.GetComponent<Renderer>().enabled = false;

            }

            isVisible = vis;
        }

    }


    public void destroyMeshCollider()
    {
        if (cone.gameObject.GetComponent("MeshCollider") != null)
        {
            GameObject.Destroy(cone.gameObject.GetComponent("MeshCollider"));
        }
    }

    public void rebuildMeshCollider()
    {
        if (cone.gameObject.GetComponent("MeshCollider") == null)
        {
            cone.gameObject.AddComponent<MeshCollider>();
        }
    }
    

    public void destroyGeometry()
    {
        destroyMeshCollider();
        if (cone)
        {
            GameObject.Destroy(cone);
        }

    }
}
