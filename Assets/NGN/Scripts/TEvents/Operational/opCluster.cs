using UnityEngine;
using System.Collections;
using Vectrosity;

using System.Text;

public class opCluster
{
    private TEventAgent agent;
    private TStructCluster structCluster;
    public Vector3[] points;

    
    public void SetStructCluster(ref TStructCluster st)
    {
        structCluster = st;
        points = st.points;
    }
    
    public void SetAgent(TEventAgent ag)
    {
        agent = ag;
    }
    
    public void Draw()
    {
        if(points==null)
        {
            Debug.Log("NO CLUSTERS!");
            return;
        }
        foreach (Vector3 xyz in points)
        {
            GameObject mySphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            mySphere.transform.localScale = new Vector3(3f,3f,3f);
            mySphere.transform.position = xyz;
        }
        GameObject zeroPoint = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        zeroPoint.transform.localScale = new Vector3(3f,3f,3f);
        zeroPoint.transform.position = new Vector3(0f,0f,0f);
        
        zeroPoint.GetComponent<Renderer>().material.color = Color.red;
    }
}
