using UnityEngine;
using System.Collections;
using Vectrosity;

using System.Text;

public class TEventAgent : MonoBehaviour
{
    //=== Basic Settings =======
    public bool displayTrack = true;
    public bool displayShuriken = false;
    public bool autoKill = true;
    public bool autoTeleport = true;

    public TStructEvent strEvent;
    public opTrack[] opTracks;
    public opCalo[] opCalos;
    private opJet[] opJets;
    public opCluster[] opClusters;
    public int numTracks = 0;
    public int numCalos = 0;
    private int numJets = 0;
    private int numClusters = 0;
    public GameObject ParentEvent;
    public float eventScaleFactor = 1f;
    public GameObject trackParticles;
    private float explodeTime = 3f;
    private float currentExplodeTime = 0f;
    private bool isExploding = false;
    private bool wasExploding = false;
    private float currentExplosionLength = 0f;
    public float maxExplosionLength = 500f;

    private bool hasAutoTeleported = false;

    private bool isFadingOut = false;
    private float fadingPhase = 1f;
    private bool killAfterFadeOut = false;
    private float fadeoutspeed = 1f;
    private opTrack currentHoveredTrack = null;
    private opTrack previousHoveredTrack = null;
    public bool Draw3D = true;
    private Camera activeTrackCamera = null;
    private bool isEverythingLoaded = false;
    public Material caloMaterial;
    private float maxPt = 10f;

    public bool showEventInfoBox = false;

    public Vector3 displacePosition = new Vector3(0f, 0f, 0f);
    public float displaceRotation = 0f;

    public float timeToLive = 0f;

    private GameObject proton01;
    private GameObject proton02;
    private float collidingProtonsDonePercent = 0f;
    private bool isCollidingProtons = false;


    public void SetParentEvent(GameObject pev)
    {
        ParentEvent = pev;
        ParentEvent.transform.localScale = new Vector3(eventScaleFactor, eventScaleFactor, eventScaleFactor);
        ParentEvent.transform.Rotate(new Vector3(0f, displaceRotation, 0f), Space.World);
        ParentEvent.transform.position = displacePosition;
        //Debug.Log(ParentEvent.transform.position);
    }

    public void SetStart()
    {        
        VectorLine.SetCamera();
        if (TEventManager.Instance.GetDisplayCollidingProtons())
        {
            proton01 = (GameObject) GameObject.Instantiate(Resources.Load("Prefabs/proton"));
            proton02 = (GameObject) GameObject.Instantiate(Resources.Load("Prefabs/proton"));
            proton01.gameObject.GetComponent<Renderer>().enabled = false;
            proton02.gameObject.GetComponent<Renderer>().enabled = false;

            //proton01.gameObject.layer = 31;
            //proton02.gameObject.layer = 31;
        }
    }

    public void SetExplodeTime(float xpldtime)
    {
        explodeTime = xpldtime;
    }

    public void SetExplosionLength(float xplength)
    {
        maxExplosionLength = xplength;
    }

   
    public Camera GetActiveTrackCamera()
    {
        return activeTrackCamera;
    }

    public void SetMaxPt(float mpt)
    {
        maxPt = mpt;
    }

    public void SetDisplayTrack(bool dt)
    {
        displayTrack = dt;
    }

    public void SetDisplayShuriken(bool ds)
    {
        displayShuriken = ds;
    }

    public float GetMaxPt()
    {
        return maxPt;
    }

    public void SetStructEvent(ref TStructEvent sEvent)
    {
        strEvent = sEvent;
        //strEvent.agent = this;

        /*Tracks*/
        Material trackMaterial = Resources.Load("Materials/VectorLineGlow", typeof(Material)) as Material;
        trackParticles = Resources.Load("Prefabs/Particle", typeof(GameObject)) as GameObject;
        numTracks = strEvent.tracks.Length;
        opTracks = new opTrack[numTracks];

        for (int tr = 0; tr < numTracks ; tr++)
        {

            opTrack newOpTrack = new opTrack();
            newOpTrack.SetAgent(this);
            newOpTrack.SetMaterial(trackMaterial);
            newOpTrack.SetStructTrack(ref strEvent.tracks[tr]);

            newOpTrack.CalculateLine();

            opTracks[tr] = newOpTrack;

        }

        if (strEvent.calos != null && strEvent.calos.Length > 0)
        {
            numCalos = strEvent.calos.Length;
            //Debug.Log(numCalos);
            opCalos = new opCalo[numCalos];
            caloMaterial = Resources.Load("Materials/CaloMaterial", typeof(Material)) as Material;
            //Debug.Log(eventScaleFactor);

            for (int cal = 0; cal < numCalos ; cal++)
            {

                opCalo newOpCalo = new opCalo();
                newOpCalo.SetAgent(this);
                //newOpCalo.SetMaterial(trackMaterial);
                newOpCalo.SetStructCalo(ref strEvent.calos[cal]);

                //newOpCalo.CalculateLine();

                opCalos[cal] = newOpCalo;

            }
        }

        /*Jets*/
        if (strEvent.jets != null)
        {
            numJets = strEvent.jets.Length;
            opJets = new opJet[numJets];

            for (int tr = 0; tr < numJets; tr++)
            {
                opJet newOpJet = new opJet();
                newOpJet.SetAgent(this);
                newOpJet.SetStructJet(strEvent.jets[tr]);

                opJets[tr] = newOpJet;
            }
        }

        strEvent.hasPolylines = true;
        sEvent = strEvent;

        /* Clusters */
        if(strEvent.clusters != null)
        {
            numClusters = strEvent.clusters.Length;
            opClusters = new opCluster[numClusters];
            
            for(int cl=0;cl<numClusters;cl++)
            {
                opCluster newOpCluster = new opCluster();
                newOpCluster.SetAgent(this);
                newOpCluster.SetStructCluster(ref strEvent.clusters[cl]);
                
                opClusters[cl] = newOpCluster;
            }
        }

    }

    private void SetActiveTrackCamera()
    {
        if (Draw3D)
        {
            activeTrackCamera = Camera.main;
        }
        else
        {
            if (!displayShuriken)
            {
                activeTrackCamera = GameObject.Find("VectorCam").GetComponent<Camera>();
            }
        }
    }

    private bool AreAllTracksReady()
    {
        bool ready = true;

        for (int tr = 0; tr < numTracks; tr++)
        {
            if (opTracks[tr] == null)
            {
                ready = false;
                break;
            }
            else if (!opTracks[tr].IsReady())
            {
                ready = false;
                break;
            }

        }

        return ready;
    }

    public void Explode()
    {
        if (TEventManager.Instance.GetDisplayCollidingProtons())
        {
            FadeOut();
            CreateCollidingProtons();
        }

        DestroyMeshColliders();
        isExploding = true;
        currentExplosionLength = 0f;
        currentExplodeTime = 0f;
        for (int tr = 0; tr < numTracks; tr++)
        {
            opTracks[tr].ZeroTrack();
            opTracks[tr].Draw();
        }
        for (int jt = 0; jt < numJets; jt++)
        {
            opJets[jt].ScaleSize(0f);
            opJets[jt].setVisibility(true);
        }
        for (int cl = 0; cl < numClusters; cl++)
        {
            opClusters[cl].Draw();
        }

        //FadeTo(1f);
    }

    private void ExplosionCycle()
    {
        currentExplodeTime += Time.deltaTime;


        float adjustedLength = maxExplosionLength * eventScaleFactor;
        currentExplosionLength = Mathf.Lerp(0f, adjustedLength, currentExplodeTime / explodeTime);
        //Debug.Log(currentExplosionLength);
        for (int tr = 0; tr < numTracks; tr++)
        {
            opTracks[tr].Clamp(currentExplosionLength);
            opTracks[tr].Draw();
        }

        for (int jt = 0; jt < numJets; jt++)
        {
            opJets[jt].ScaleSize(currentExplosionLength);
        }

        for (int cl = 0; cl < numCalos; cl++)
        {
            opCalos[cl].Clamp(currentExplosionLength);
        }

        if (currentExplodeTime >= explodeTime)
        {
            isExploding = false;
            Debug.Log("finished exploding");
        }

    }

    private void RunOnceReady()
    {
        Explode();
        //SetActiveTrackCamera();        
    }

    public Coroutine CreateCollidingProtons()
    {
        collidingProtonsDonePercent = 0f;
        isCollidingProtons = true;
        StopAllCoroutines();
        return StartCoroutine(CollideProtons());
    }

    private IEnumerator CollideProtons()
    {
        float distInit = 500.0f;
        float collisionSpeed = 100.0f;
        proton01.gameObject.GetComponent<Renderer>().enabled = true;
        proton02.gameObject.GetComponent<Renderer>().enabled = true;
        Material protonMaterial = proton01.GetComponent<Renderer>().material;

        while (collidingProtonsDonePercent < 100f)
        {
            collidingProtonsDonePercent += Time.deltaTime * collisionSpeed;
            collidingProtonsDonePercent = Mathf.Clamp(collidingProtonsDonePercent, 0, 100);
            //Debug.Log(collidingProtonsDonePercent);
            float dist = distInit - distInit * collidingProtonsDonePercent / 100.0f;

            proton01.transform.position = new Vector3(0, 0, dist) + ParentEvent.transform.position;
            proton02.transform.position = new Vector3(0, 0, -dist) + ParentEvent.transform.position;

            proton01.transform.LookAt(Camera.allCameras[0].transform.position, new Vector3(1, 0, 0));
            proton02.transform.LookAt(Camera.allCameras[0].transform.position, new Vector3(1, 0, 0));



            float opacity = Mathf.Clamp((collidingProtonsDonePercent - 15f) / 100f, 0.0f, 1.0f);

            Color color = new Color(1f, 0.3f, 0.3f, opacity);
            protonMaterial.SetColor ("_TintColor", color);
            proton01.GetComponent<Renderer>().material = protonMaterial;
            proton02.GetComponent<Renderer>().material = protonMaterial;
            //Debug.Log(collidingProtonsDonePercent);
            yield return 1;
        }

        if (collidingProtonsDonePercent == 100.0f)
        {
            proton01.gameObject.GetComponent<Renderer>().enabled = false;
            proton02.gameObject.GetComponent<Renderer>().enabled = false;
            isCollidingProtons = false;
        }
    }


    void Update ()
    {

        if (isEverythingLoaded && autoTeleport && !hasAutoTeleported)
        {
            TEventManager.Instance.Teleport();
            hasAutoTeleported = true;
        }

        if (!isEverythingLoaded)
        {
            if (AreAllTracksReady() || displayShuriken)
            {
                isEverythingLoaded = true;
                RunOnceReady();
            }
        }

        if (!isExploding && isEverythingLoaded)
        {
            for (int tr = 0; tr < numTracks; tr++)
            {
                opTracks[tr].Draw();
            }
        }
        else
        {
            if (!isCollidingProtons && isEverythingLoaded && isExploding)
            {
                if (currentExplosionLength == 0f) // Do this only on the first frame of the explosion
                {
                    FadeTo(1f);
                }
                ExplosionCycle();
            }
        }


        if (Input.GetKeyUp ("f"))
        {
            FadeOut();
        }

        if (Input.GetKeyUp ("k"))
        {
            FadeOutAndDestroy();
        }

        if (Input.GetKeyUp ("p"))
        {
            CreateCollidingProtons();
        }

        //==========Jenninator============

        if (autoKill && timeToLive > 0)
        {
            timeToLive -= Time.deltaTime;
            if (timeToLive <= 0.0f)
            {
                FadeOutAndDestroy();
            }
        }
        //==========/Jenninator============

        /*if (Input.GetKeyUp ("h"))
        {
            RebuildMeshColliders();
        }

        if (Input.GetKeyUp ("j"))
        {
            DestroyMeshColliders();
        }*/

        if (Input.GetKeyUp ("5"))
        {
            showEventInfoBox = !showEventInfoBox;
        }

        if (isFadingOut)
        {
            FadeOutCycle();
        }

        if (wasExploding && !isExploding)
        {
            //RebuildMeshColliders();
        }

        if (killAfterFadeOut && fadingPhase == 0)
        {
            Die();
        }

        if (isExploding && !wasExploding)
        {

        }

        wasExploding = isExploding;

        /*
        if (manager.mouseControl.startedMoving)
        {
            DestroyMeshColliders();
        }

        if (manager.mouseControl.stoppedMoving)
        {
            RebuildMeshColliders();
        }

        if (isEverythingLoaded && manager.mouseControl.isActive)
        {
            TrackSelection();
        }
        */

        if (previousHoveredTrack != currentHoveredTrack)
        {
            if (currentHoveredTrack != null)
            {
                currentHoveredTrack.OnHoverEnter();
            }

            if (previousHoveredTrack != null)
            {
                previousHoveredTrack.OnHoverExit();
            }
        }

        if (TEventManager.Instance.displayInfoBoxes)
        {
            if (showEventInfoBox)
            {
                ShowEventInfo();
            }
            else
            {
                HideEventInfo();
            }
        }

        previousHoveredTrack = currentHoveredTrack;

    }

    public void FadeOutAndDestroy()
    {
        killAfterFadeOut = true;
        FadeOut();
        DestroyShurikens();
    }

    public void FadeOut()
    {
        isFadingOut = true;
        fadingPhase = 1f;
        fadeoutspeed = 1f;
    }

    public void FadeOutCycle()
    {
        if (isFadingOut)
        {
            fadingPhase -= Time.deltaTime * fadeoutspeed;
            if (fadingPhase <= 0f)
            {
                fadingPhase = 0;
                isFadingOut = false;
            }
            //Debug.Log(fadingPhase);
            //if (displayTrack)
            {
                FadeTo(fadingPhase);
            }

        }
    }

    public void FadeTo(float fadeTo)
    {
        if (displayTrack)
        {
            for (int tr = 0; tr < numTracks; tr++)
            {
                opTracks[tr].FadeTo(fadeTo);
            }
        }

        for (int cl = 0; cl < numCalos; cl++)
        {
            opCalos[cl].FadeToRelative(fadeTo);
        }

        for (int jt = 0; jt < numJets; jt++)
        {
            opJets[jt].Fade(fadeTo);
        }

    }

    public void Die()
    {
        DestroyTracks();
        showEventInfoBox = false;
        DestroyProtons();
        TEventManager.Instance.RemoveEventAgent(this);
    }

    public void DestroyTracks()
    {
        for (int tr = 0; tr < numTracks; tr++)
        {
            opTracks[tr].DestroyTrack();
            Object.Destroy(this);
            Object.Destroy(ParentEvent);
        }

    }

    public void DestroyJets()
    {
        for (int jt = 0; jt < numJets; jt++)
        {
            opJets[jt].destroyEverything();
            opJets[jt] = null;
        }
    }

    public void DestroyProtons()
    {
        if (proton01 != null)
        {
            GameObject.Destroy(proton01);
        }
        if (proton02 != null)
        {
            GameObject.Destroy(proton02);
        }
    }

    public void DestroyShurikens()
    {
        for (int tr = 0; tr < numTracks; tr++)
        {
            opTracks[tr].DestroyShuriken();
        }

    }

    public void DestroyMeshColliders()
    {
        for (int tr = 0; tr < numTracks; tr++)
        {
            opTracks[tr].DestroyMeshCollider();
        }
    }

    public void RebuildMeshColliders()
    {
        //if (manager.mouseControl.isActive)
        {
            for (int tr = 0; tr < numTracks; tr++)
            {
                opTracks[tr].RebuildMeshCollider();
            }
        }
    }

    /*private void OnGUI()
    {
        GUI.Label(new Rect(25, 95, 480, 50), "Event Created");
    }
    */

    public StringBuilder GetTStructEventInfo()
    {
        StringBuilder eventInfo = new StringBuilder("Event Information\n");
        eventInfo.Append("Energy: " + strEvent.energy + "\n");
        eventInfo.Append("Number of Tracks: " + strEvent.tracks.Length + "\n");
        eventInfo.Append("ETMiss: " + strEvent.etmiss + "\n");
        eventInfo.Append("MaxPT: " + strEvent.maxPt + "\n");
        return eventInfo;
    }

    public void ShowEventInfo()
    {
        TEventManager.Instance.eventInfoTextObject.GetComponent<GUIText>().text = GetTStructEventInfo().ToString();
        Vector3 mousePos = Input.mousePosition;
        TEventManager.Instance.eventInfoObject.transform.position = new Vector3(0.02f, 0.02f, mousePos.z);
        TEventManager.Instance.eventInfoObject.active = true;
    }

    private void HideEventInfo()
    {
        TEventManager.Instance.eventInfoObject.active = false;
    }

    private void TrackSelection()
    {
        currentHoveredTrack = null;

        bool trackHoverChange = false;
        bool gotit = false;
        if (activeTrackCamera == null)
        {
            //SetActiveTrackCamera();
        }

        Ray ray = activeTrackCamera.ScreenPointToRay (Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        float dist = 300;

        //if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerTracks))
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            for (int tr = 0; tr < numTracks; tr++)
            {
                if (hit.collider == opTracks[tr].trackLine.collider)
                {
                    currentHoveredTrack = opTracks[tr];

                    if (Input.GetMouseButtonDown(0))
                    {
                        //selection code here
                    }

                    gotit = true;
                }


                if (gotit)
                {
                    break;
                }
            }
        }


        if (!gotit)
        {
            //get jets code
        }

    }

    //temp code just to make everything compile
    public float[] getCaloGeoLoader(string caloTypeXml, string caloName, int sampling, int region)
    {
        float[] fakeValue = new float[10];
        return fakeValue;
    }
}
