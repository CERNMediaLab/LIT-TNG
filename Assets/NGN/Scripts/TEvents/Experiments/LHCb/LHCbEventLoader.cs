﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;

public class LHCbEventLoader
{
    public IList<ParticleTrack> array;
    public TEventManager manager;

    public TStructEvent LoadFromJson(string json)
    {
        //      StreamReader r = new StreamReader ("C:/Users/atrisovi/TEV/ReferenceFiles/111797_42021447.json");
        //      json= r.ReadToEnd();
        //      Debug.Log (json);

        LHCbEvent ev = JsonConvert.DeserializeObject<LHCbEvent> (json);
        TStructEvent tse = ev.ToStructEvent ();
        if (ev.MUON != null && ev.MUON.Length > 0)
        {
            TStructCalo [] calos = new TStructCalo[ev.MUON.Length];

            TStructCalo tsc;
            for (int i = 0; i < ev.MUON.Length; i++)
            {
                tsc = new TStructCalo (ev.MUON[i][0], ev.MUON[i][1], ev.MUON[i][2], ev.MUON[i][3], ev.MUON[i][4], ev.MUON[i][5], 1.1f, 1.8f, 1f);
                //          Debug.Log(tsc.ToString());
                calos[i] = tsc;
            }

            tse.calos = calos;
        }
        //      int a = tse.calos.Count();
        //      Debug.Log ("calos count" + a.ToString());
        return tse;
    }
}
public class LHCbEvent
{
    public List<ParticleTrack> particles
    {
        get;
        set;
    }
    public int [][] MUON
    {
        get;
        set;
    }
    //Converts LHCbEvent to struct event
    public TStructEvent ToStructEvent()
    {
        int n = particles.Count();
        TStructTrack[] tracks_temp = new TStructTrack[n];
        int num = 0;
        Vector3 [] polyline_temp;
        //Debug.Log("particles: " + particles.Count());
        foreach (ParticleTrack pt in particles)
        {
            TStructTrack tst = new TStructTrack (pt.E, pt.name, pt.m, pt.trackchi2, pt.ip, pt.px, pt.py, pt.pz, pt.pv_x, pt.pv_y, pt.pv_z, pt.q, pt.zFirstMeasurement);

            //converting array to Vector3 objects
            //Debug.Log("Num Segments: " + pt.track.Length);
            polyline_temp = new Vector3[pt.track.Length];
            for (int i = 0; i < pt.track.Length; i++)
            {
                polyline_temp[i] = new Vector3(pt.track [i] [0], pt.track [i] [1], pt.track [i] [2]);
                //Debug.Log(v3);

            }
            tst.polyline = polyline_temp;
            tracks_temp [num] = tst;
            //Debug.Log(tst.polyline);
            num++;

        }

        //final result
        TStructEvent tse = new TStructEvent ("LHCb", false);
        //tse.SetExperiment("LHCb");
        tse.tracks = tracks_temp;
        /*for (int i = 0; i < tse.tracks.Length; i++)
        {
            //Debug.Log(tse.tracks[i].polyline);
        }*/
        tse.hasPolylines = true;

        return tse;
    }
}
public class ParticleTrack
{
    public float E
    {
        get;
        set;
    }
    public string name
    {
        get;
        set;
    }
    public float pz
    {
        get;
        set;
    }
    public float zFirstMeasurement
    {
        get;
        set;
    }
    public float px
    {
        get;
        set;
    }
    public float py
    {
        get;
        set;
    }
    public float m
    {
        get;
        set;
    }
    public float q
    {
        get;
        set;
    }
    public float[][] track
    {
        get;
        set;
    }
    public float trackchi2
    {
        get;
        set;
    }
    public float ip
    {
        get;
        set;
    }
    public float pv_z
    {
        get;
        set;
    }
    public float pv_y
    {
        get;
        set;
    }
    public float pv_x
    {
        get;
        set;
    }
    public float ipchi2
    {
        get;
        set;
    }
}
