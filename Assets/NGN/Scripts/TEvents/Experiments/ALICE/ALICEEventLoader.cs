using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

public class ALICEEventLoader
{
    public TStructEvent LoadFromJson(string json)
    {
        ALICEEvent ev = JsonConvert.DeserializeObject<ALICEEvent>(json);
        if(ev == null){
            Debug.Log("No ALICE Event!!!");
        }
        return ev.ToStructEvent ();
    }
}

public class ALICEEvent
{
    public string _typename { get; set; }
    public int fUniqueID { get; set; }
    public int fBits { get; set; }
    public int fEventID { get; set; }
    public double fEnergy { get; set; }
    public int fMultiplicity { get; set; }
    public string fCollidingSystem { get; set; }
    public int fTimeStamp { get; set; }
    public List<ALICETrack> fTracks { get; set; }
    public List<ALICECluster> fClusters { get; set; }
    public List<ALICECaloCluster> fCaloClusters { get; set; }
    
    private bool displayClusters = false;
    private bool displayCaloClusters = false;
    
    public TStructEvent ToStructEvent()
    {
        Dictionary<int,string> pdgDictionary = new Dictionary<int,string>(); // Map PDG codes to names
        pdgDictionary[13] = "mi-";
        pdgDictionary[1] = "mi-";
        pdgDictionary[2212] = "p+";
        pdgDictionary[321] = "K+";
        pdgDictionary[211] = "pi+";
        pdgDictionary[11] = "e-";
        pdgDictionary[1000010020] = "deuteron";
        
        float scale=0.15f;   // scaling factor (the same as the one set in Unity for geometry)
        
        TStructEvent tse = new TStructEvent ("ALICE", false); // resulting event
        
        //---------------------------------------------------------------------------------
        //      Read tracks from JSON
        //---------------------------------------------------------------------------------
        
        TStructTrack[] tracks_temp = new TStructTrack[fTracks.Count()];
        
        float B = -0.500667762756347656f; // magnetic field in main barrel
        float kB2C = 0.00299792458f;      // units conversion factor
        float C = 1f/(kB2C*B);            // helix scaling factor
        int num = 0;
        
        foreach (ALICETrack pt in fTracks)
        {
            string name = "";
            if (pdgDictionary.ContainsKey(pt.fPID)){
                name = pdgDictionary[pt.fPID];
            }
            if(pt.fType.Contains("V0"))
            {
                name = "v0";
            }
            else if(pt.fType.Contains("kink"))
            {
                name = "kink";
            }
            else if(pt.fType.Contains("cascade"))
            {
                name = "cascade";
            }

            if(name=="")
            {
                Debug.Log("no PID in dictionary found:" + pt.fPID);
            }
            
            float E = pt.fE;
            float m = pt.fMass;
            float ip = 0f;
            float pv_x = scale*pt.fStartCoordinates[0];
            float pv_y = scale*pt.fStartCoordinates[1];
            float pv_z = scale*pt.fStartCoordinates[2];
            float px = pt.fMomentum[0];
            float py = pt.fMomentum[1];
            float pz = pt.fMomentum[2];
            float q = pt.fCharge;
            float ipchi2 = 0f;
            float trackchi2 = 0f;
            float zFirstMeasurement = 0f;
            
            TStructTrack tst = new TStructTrack (E,name,m,trackchi2,ip,px,py,pz,pv_x,pv_y,pv_z,q,zFirstMeasurement);//can I put values directly here??
            
            // parameters needed to calculate helix (doesn't work yet)
            tst.phi = pt.fPhi;
            tst.theta = pt.fTheta;
            tst.pt = pt.fSignedPT;
            tst.C = Mathf.Abs(C);//85.3f;
            
            // reading polylines from JSON (then helix calculation is not required)
            int numPoints = pt.fPolyX.Count();
            Vector3 [] polyline_temp = new Vector3[numPoints];
            for (int i = 0; i < numPoints; i++){
                polyline_temp[i] = new Vector3(scale*(float)pt.fPolyX[i],scale*(float)pt.fPolyY[i],scale*(float)pt.fPolyZ[i]);
            }
            tst.polyline = polyline_temp;
            
            tracks_temp[num++] = tst;
        }
        tse.tracks = tracks_temp;
        tse.hasPolylines = true;
        
        //---------------------------------------------------------------------------------
        //      Read clusters from JSON
        //---------------------------------------------------------------------------------
        
        if(displayClusters)
        {
            TStructCluster[] clusters_temp = new TStructCluster[fClusters.Count()];
            int num2 =0;
            foreach(ALICECluster cl in fClusters)
            {
                Vector3[] tmp_vector = new Vector3[cl.fXArray.Count()];
                
                for(int i=0;i<cl.fXArray.Count();i++)
                {
                    tmp_vector[i] = new Vector3(scale*cl.fXArray[i],scale*cl.fYArray[i],scale*cl.fZArray[i]);
                    Debug.Log("cl_x:" + cl.fXArray[i] + "\tcl_y:" + cl.fYArray[i] +"\tcl_z:"+ cl.fZArray[i]);
                }
                TStructCluster tsc = new TStructCluster(tmp_vector);
                
                clusters_temp[num2] = tsc;
                num2++;
            }
            tse.clusters = clusters_temp;
        }

        if(displayCaloClusters)
        {
            float maxEnergy = 2.0f;
            float maxCaloHeight = 150.0f;
            
            List<TStructCalo> calos_tmp = new List<TStructCalo>();

            /* Fake calo towers
            float dAlpha = 10.0f;
            float r1 = 200;
            float r2 = 230;
            float dZ = 10;
            
            for(float z=-150.0f;z<=150;z+=1.5f*dZ)
            {
                for(float alpha=0.0f; alpha<=80.0f; alpha+=1.5f*dAlpha)
                {
                    float x1 = r1*Mathf.Cos(Mathf.Deg2Rad*alpha);
                    float x2 = r1*Mathf.Cos(Mathf.Deg2Rad*(alpha+dAlpha));
                    float y1 = r1*Mathf.Sin(Mathf.Deg2Rad*(alpha));
                    float y2 = r1*Mathf.Sin(Mathf.Deg2Rad*(alpha+dAlpha));

                    float x1u = r2*Mathf.Cos(Mathf.Deg2Rad*(alpha));
                    float x2u = r2*Mathf.Cos(Mathf.Deg2Rad*(alpha+dAlpha));
                    float y1u = r2*Mathf.Sin(Mathf.Deg2Rad*(alpha));
                    float y2u = r2*Mathf.Sin(Mathf.Deg2Rad*(alpha+dAlpha));
                    
                    TStructCalo tsc = new TStructCalo();
                    
                    tsc.p1 = new Vector3(x2,y2,z);
                    tsc.p2 = new Vector3(x1,y1,z);
                    tsc.p3 = new Vector3(x1u,y1u,z);
                    tsc.p4 = new Vector3(x2u,y2u,z);

                    tsc.p1u = new Vector3(x2,y2,z+dZ);
                    tsc.p2u = new Vector3(x1,y1,z+dZ);
                    tsc.p3u = new Vector3(x1u,y1u,z+dZ);
                    tsc.p4u = new Vector3(x2u,y2u,z+dZ);
                    
                    calos_tmp.Add(tsc);
    //                num3++;
                }
            }
            */
            
            foreach(ALICECaloCluster cl in fCaloClusters)
            {
                scale=0.4f;
                
                float dR = cl.fEnergy/maxEnergy * maxCaloHeight;

//                if(cl.fEnergy < 0.20f){
//                    dR=0;
//                }
//                else{
//                    Debug.Log("dR:" + dR + "\tE:" + cl.fEnergy);
//                }
                
                // calculate phi1 and phi2
                float phi = cl.fPhi;// - Mathf.PI;
                float phi1 = phi - cl.fPhiHalfLength;
                float phi2 = phi + cl.fPhiHalfLength;
                
                // calculate theta1 and theta2
                float theta = 2*Mathf.Atan(Mathf.Exp(-cl.fEta));
                float dtheta = Mathf.Abs( theta - 2*Mathf.Atan(Mathf.Exp(-(cl.fEta+2*cl.fEtaHalfLength))));
                float theta1 = theta - dtheta/2.0f;
                float theta2 = theta + dtheta/2.0f;
        
//                Debug.Log("theta:" + theta + "\tdTheta:" + dtheta);
                
                // calculate correction for radius
                float Rcorr  = cl.fR / Mathf.Cos(cl.fPhiHalfLength);
                float Rcorr2 = (cl.fR+dR) / Mathf.Cos(cl.fPhiHalfLength);
                
                // calculate x coordinates
                float x1 = Rcorr * Mathf.Cos(phi1);
                float x2 = Rcorr * Mathf.Cos(phi2);
                float x1u = Rcorr2 * Mathf.Cos(phi1);
                float x2u = Rcorr2 * Mathf.Cos(phi2);
                
                // calculate y coordinates
                float y1 = Rcorr * Mathf.Sin(phi1);
                float y2 = Rcorr * Mathf.Sin(phi2);
                float y1u = Rcorr2 * Mathf.Sin(phi1);
                float y2u = Rcorr2 * Mathf.Sin(phi2);
                
                // calculate z coordinates
                float z1  = cl.fR * Mathf.Cos(theta1);
                float z2  = cl.fR * Mathf.Cos(theta2);
                float z1u = (cl.fR + dR) * Mathf.Cos(theta1);
                float z2u = (cl.fR + dR) * Mathf.Cos(theta2);
                
                Debug.Log("z1:" + z1 + "\tz2:" + z2 + "\tz1u:" + z1u + "\tz2u" + z2u);
                
                TStructCalo tsc = new TStructCalo();
                
                tsc.p1  = new Vector3( scale * x2  , scale * y2  , scale * z1  );
                tsc.p2  = new Vector3( scale * x1  , scale * y1  , scale * z1  );
                tsc.p3  = new Vector3( scale * x1u , scale * y1u , scale * z1u  );
                tsc.p4  = new Vector3( scale * x2u , scale * y2u , scale * z1u  );
                
                tsc.p1u = new Vector3( scale * x2  , scale * y2  , scale * z2  );
                tsc.p2u = new Vector3( scale * x1  , scale * y1  , scale * z2  );
                tsc.p3u = new Vector3( scale * x1u , scale * y1u , scale * z2u  );
                tsc.p4u = new Vector3( scale * x2u , scale * y2u , scale * z2u  );
 
                calos_tmp.Add(tsc);
            }
            tse.calos = calos_tmp.ToArray();
        }
        return tse;
    }
}

public class ALICETrack
{
    public string _typename { get; set; }
    public int fUniqueID { get; set; }
    public int fBits { get; set; }
    public string fType { get; set; }
    public float fCharge { get; set; }
    public float fE { get; set; }
    public int fParentID { get; set; }
    public int fPID { get; set; }
    public float fSignedPT { get; set; }
    public float fMass { get; set; }
    public List<float> fMomentum { get; set; }
    public List<float> fStartCoordinates { get; set; }
    public List<float> fEndCoordinates { get; set; }
    public List<int> fChildrenIDs { get; set; }
    public float fHelixCurvature { get; set; }
    public float fTheta { get; set; } // radians
    public float fPhi { get; set; }   // radians
    public List<double> fPolyX { get; set; }
    public List<double> fPolyY { get; set; }
    public List<double> fPolyZ { get; set; }
}

public class ALICECluster
{
    public string _typename { get; set; }
    public int fUniqueID { get; set; }
    public int fBits { get; set; }
    public List<float> fXArray { get; set; }
    public List<float> fYArray { get; set; }
    public List<float> fZArray { get; set; }
    public int fd_dx { get; set; }
    public int fTrackID { get; set; }
    public string fSource { get; set; }
}

public class ALICECaloCluster
{
    public string _typename { get; set; }
    public int fUniqueID { get; set; }
    public int fBits { get; set; }
    public float fR { get; set; }
    public float fPhi { get; set; }
    public float fEta { get; set; }
    public float fPhiHalfLength { get; set; }
    public float fEtaHalfLength { get; set; }
    public float fEnergy { get; set; }
}

