using UnityEngine;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// Not sure yet where to divide between CMSEvent and CMSEventLoader

public class CMSEvent {
	// This is not used right now but contains the information of the 
	// types: the first string is the collection name, the second string
	// is the name of the variable in the collection, and the int
	// is the index of the property in the collection.
	// For now, we fetch the information "by hand" using the "known"
	// index explicitly. Potentially dangerous, but good enough
	// for testing.
	private Dictionary<string, Dictionary<string, int> > Types;

	private JObject typeObjects;
	private JObject collectionObjects;
	private JObject associationObjects;

	private Dictionary<string,object> types;
	private Dictionary<string,object> collections;
	private Dictionary<string,object> associations;

	private float maxPt; // max pt of tracks

	public float getMaxPt() {
		return maxPt;
	}
	
	private void outputKeys(Dictionary<string,object>.KeyCollection kc) {
		foreach(string k in kc) {
			Debug.Log("key: " + k);
		}
	}

	private bool HasType(string type_key) {
		return types.ContainsKey(type_key);
	}
	
	private bool HasCollection(string collection_key) {
		return collections.ContainsKey(collection_key);	
	}
	
	private bool HasAssociation(string association_key) {
		return associations.ContainsKey(association_key);
	}

	private JArray GetType(string type_key) {
		if (HasType (type_key)) {
			JArray type = (JArray)types [type_key];
			return type;
		} else {
			Debug.Log ("No type " + type_key);
			return null;
		}
	}
	
	private JArray GetCollection(string collection_key) {		
		if ( HasCollection (collection_key) ) {
			JArray collection = (JArray)collections [collection_key];
			return collection;
		} else {
			Debug.Log ("No collection " + collection_key);
			return null;
		}
	}
	
	private JArray GetAssociation(string association_key) {
		if ( HasAssociation(association_key) ) {
			return (JArray) associations [association_key];
		} else {
			Debug.Log("No association " + association_key);
			return null;
		}
	}

	public CMSEvent(Dictionary<string, object> eventObjects) {

		// Hmm. This casting doesn't "smell right". There is
		// likely a better way to handle this, but for now
		// continue this way. Must learn a bit more C# and 
		// newtonsoft API

		if (eventObjects.ContainsKey ("Types")) {
			typeObjects = (JObject) eventObjects ["Types"];
			types = typeObjects.ToObject<Dictionary<string,object> >();
		} else {
			Debug.Log ("No Types in event");
		}
		
		if (eventObjects.ContainsKey ("Collections")) {
			collectionObjects = (JObject) eventObjects ["Collections"];
			collections = collectionObjects.ToObject<Dictionary<string,object> >();
		} else {	
			Debug.Log ("No Collections in event");
		}
		
		if (eventObjects.ContainsKey ("Associations")) {
			associationObjects = (JObject) eventObjects ["Associations"];
			associations = associationObjects.ToObject<Dictionary<string,object> >();
		} else {
			Debug.Log ("No Associations in event");
		}

		maxPt = 0.0f;
	}

	private Vector3 toVector3(JArray jarray) {
		return new Vector3 ((float)jarray [0], (float)jarray [1], (float)jarray [2]);
	}

	private List<TStructCalo> toStructCalos(string collection_name, float minE) {

		List<TStructCalo> tscs = new List<TStructCalo>();
		JArray recHits = GetCollection (collection_name);
	
		if (recHits == null) {
			Debug.Log ("NullReference for " + collection_name + ". Returning empty collection");
			return tscs;
		}

		//Debug.Log ("There are " + recHits.Count + " " + collection_name);

		for (int i = 0; i < recHits.Count; i++) {

			JArray rh = (JArray) recHits[i];

			float energy = rh[0].Value<float>();
			//Debug.Log("rh energy: " + energy);

			if ( energy < minE ) {
				continue;
			}

			TStructCalo tsc = new TStructCalo();
			tsc.energy = energy;
			tsc.name = collection_name;

			tsc.p1 = toVector3(rh[5].ToObject<JArray>())*100.0f; // front1
			tsc.p2 = toVector3(rh[6].ToObject<JArray>())*100.0f; // front2
			tsc.p3 = toVector3(rh[7].ToObject<JArray>())*100.0f; // front3 
			tsc.p4 = toVector3(rh[8].ToObject<JArray>())*100.0f; // front4

			tsc.p1u = toVector3(rh[9].ToObject<JArray>())*100.0f;  // back1
			tsc.p2u = toVector3(rh[10].ToObject<JArray>())*100.0f; // back2
			tsc.p3u = toVector3(rh[11].ToObject<JArray>())*100.0f; // back3
			tsc.p4u = toVector3(rh[12].ToObject<JArray>())*100.0f; // back4

			tscs.Add(tsc);
		}

		return tscs;
	}

	private List<TStructTrack> toStructTracks(string collection_name, string extra_name, string assoc_name, int pi) {

		List<TStructTrack> tsts = new List<TStructTrack>();

		JArray tracks = GetCollection (collection_name);
		if (tracks == null) {
			Debug.Log ("NullReference for " + collection_name + ". Returning empty collection");
			return tsts;
		}

		JArray extras = GetCollection (extra_name);
		if (extras == null) {
			Debug.Log ("NullReference for " + extra_name + ". Returning empty collection");
			return tsts;
		}

		JArray assocs = GetAssociation (assoc_name);
		if (assocs == null) {
			Debug.Log ("NullReference for " + assoc_name + ". Returning empty collection");
			return tsts;
		}

		//Debug.Log ("There are " + assocs.Count + " tracks");

		for (int i = 0; i < assocs.Count; i++) {

			// WTF is going on here?
			// assocs[i][][0] is the position of the objects in the file
			// but since we got them by name all we need are the indices of the 
			// track and its associated innermost and outermost states (in the extra),
			// which we get below. It still confuses ME sometimes...
			
			JArray assoc = (JArray) assocs[i];
			
			int ti = (int) assoc[0][1];

			float pt  = (float) tracks[ti][pi];

			if ( pt < 1.0 ) {
				continue;
			}
			
			if ( pt > maxPt ) {
				maxPt = pt;
			}

			TStructTrack tst = new TStructTrack();
			tst.pname = collection_name;

			int ei = (int) assoc[1][1]; 

			// Should fix this as the same methid can be used for drawing tracks and
			// electrons but the indices are different for some properties. 
			// Should really use the schema to avoid these problems...
			//float phi = (float) tracks[ti][3];
			//float eta = (float) tracks[ti][4];
			//tst.pt = pt;
			//tst.eta = eta;
			//tst.phi = phi;
			//

			//Debug.Log("pt, eta, phi: " + pt + "," + eta + "," + phi);

			// What's all this then?
			// Well, we know the beginning and end points of the track as well
			// as the directions at each of those points. This in-principle gives 
			// us the 4 control points needed for a cubic bezier spline. 
			// The control points from the directions are determined by moving along 0.25
			// of the distance between the beginning and end points of the track. 
			// This 0.25 is nothing more than a fudge factor that reproduces closely-enough
			// the NURBS-based drawing of tracks done in iSpy. At some point it may be nice
			// to implement the NURBS-based drawing but I value my sanity.

			Vector3 ipos = toVector3(extras[ei][0].ToObject<JArray>())*100;
			Vector3 idir = toVector3(extras[ei][1].ToObject<JArray>());
			idir.Normalize();

			Vector3 opos = toVector3(extras[ei][2].ToObject<JArray>())*100;
			Vector3 odir = toVector3(extras[ei][3].ToObject<JArray>());
			odir.Normalize();

			float scale = Vector3.Distance(opos, ipos);
			scale *= 0.25f;

			Vector3[] cps = new Vector3[4] {ipos, (ipos + idir*scale), (opos - odir*scale), opos};

			CubicBezierCurve curve = new CubicBezierCurve(cps, 16);
			tst.polyline = curve.GetCurvePoints();

			// We only have innermost and outermost state. No polyline (yet).
			// For now, make a polyline with just 2 points for testing.
			//Vector3[] polyline = new Vector3[2] {ipos, opos};
			//tst.polyline = polyline;

			tsts.Add(tst);
		}

		return tsts;
	}

	public List<TStructTrack> toStructTracksFromMuons(string collection_name, string extra_name, string assoc_name) {

		List<TStructTrack> tsts = new List<TStructTrack>();

		JArray muons  = GetCollection (collection_name);
		if (muons == null) {
			Debug.Log ("NullReference for " + collection_name + ". Returning empty collection");
			return tsts;
		}

		JArray extras = GetCollection (extra_name);
		if (extras == null) {
			Debug.Log ("NullReference for " + extra_name + ". Returning empty collection");
			return tsts;
		}

		JArray assocs = GetAssociation (assoc_name);
		if (assocs == null) {
			Debug.Log ("NullReference for " + assoc_name + ". Returning empty collection");
			return tsts;
		}

		//Debug.Log ("There are " + muons.Count + " global muons");

		for (int i = 0; i < muons.Count; i++) {
			
			TStructTrack tst = new TStructTrack ();
			tst.pname = collection_name;
			List<Vector3> polyline = new List<Vector3>();

			for (int j = 0; j < assocs.Count; j++ ) {
			
				JArray assoc = (JArray) assocs[j];
				
				int mi = (int) assoc[0][1];
				int ei = (int) assoc[1][1]; 

				if ( mi == i ) { // Then we have the muon we want and now get the point data
					polyline.Add(toVector3(extras[ei][0].ToObject<JArray>())*100.0f);
				}
			}

			float pt  = (float) muons[i][0];
			float phi = (float) muons[i][3];
			float eta = (float) muons[i][4];
			
			tst.pt = pt;
			tst.eta = eta;
			tst.phi = phi;
			//Debug.Log("pt, eta, phi: " + pt + "," + eta + "," + phi);

			tst.polyline = polyline.ToArray();

			tsts.Add(tst);
		}
		
		return tsts;
	}

	public List<TStructJet> toStructJets(string collection_name, float minEt) {

		List<TStructJet> tsjs = new List<TStructJet> ();

		JArray jets = GetCollection(collection_name);
		if (jets == null) {
			Debug.Log("NullReference for " + collection_name + ". Returning empty collection");
			return tsjs;
		}

		//Debug.Log ("There are " + jets.Count + " " + collection_name);

		for (int i = 0; i < jets.Count; i++) {

			JArray jet = (JArray) jets[i];

			float et = jet[0].Value<float>();
			//Debug.Log("jet" + i + " et = " + et); 

			if ( et < minEt ) {
				continue;
			}

			TStructJet tsj = new TStructJet(); 

			tsj.et = et;
			tsj.eta = jet[1].Value<float>();
			tsj.theta = jet[2].Value<float>();
			tsj.phi = jet[3].Value<float>();

			tsjs.Add(tsj);
		}

		return tsjs;
	}

	public TStructEvent toStructEvent() {

		List<TStructCalo> ebRecHits = toStructCalos ("EBRecHits_V2", 0.5f);
		List<TStructCalo> eeRecHits = toStructCalos ("EERecHits_V2", 0.5f);
		List<TStructCalo> hbRecHits = toStructCalos ("HBRecHits_V2", 0.5f);
		List<TStructCalo> heRecHits = toStructCalos ("HERecHits_V2", 0.5f);
		List<TStructCalo> hfRecHits = toStructCalos ("HFRecHits_V2", 0.5f);
		List<TStructCalo> hoRecHits = toStructCalos ("HORecHits_V2", 0.5f); 

		List<TStructTrack> tracks = toStructTracks ("Tracks_V2", "Extras_V1", "TrackExtras_V1", 2);
		List<TStructTrack> electrons = toStructTracks ("GsfElectrons_V1", "Extras_V1", "GsfElectronExtras_V1", 0);
		List<TStructTrack> muons = toStructTracksFromMuons ("GlobalMuons_V1", "Points_V1", "MuonGlobalPoints_V1");

		List<TStructJet> jets = toStructJets ("Jets_V1", 20.0f);

		TStructEvent tsevt = new TStructEvent ("CMS", false);

		//ebRecHits.AddRange (eeRecHits);
		ebRecHits.AddRange (hbRecHits);
		//ebRecHits.AddRange (heRecHits);
		//ebRecHits.AddRange (hfRecHits);
		//ebRecHits.AddRange (hoRecHits);

		tsevt.calos = ebRecHits.ToArray ();

		tracks.AddRange (electrons);
		tracks.AddRange (muons);
		tsevt.tracks = tracks.ToArray ();

		tsevt.jets = jets.ToArray ();

		tsevt.maxPt = getMaxPt();
		tsevt.hasPolylines = true;

		return tsevt;
	}
}

public class CMSEventLoader
{	
	private Dictionary<string, object> eventObjects; // These include the Types, Collections, Associations
	
	private void outputKeys(Dictionary<string,object>.KeyCollection kc) {
		foreach(string k in kc) {
			Debug.Log("key: " + k);
		}
	}

	public TStructEvent LoadFromJson(string json)
	{           
		eventObjects = JsonConvert.DeserializeObject< Dictionary<string,object> >(json); 
		//outputKeys(eventObjects.Keys);
		CMSEvent cmsevt = new CMSEvent(eventObjects);
		return cmsevt.toStructEvent ();
	}
		
}
