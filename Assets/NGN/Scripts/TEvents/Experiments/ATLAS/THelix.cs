using UnityEngine;
using System.Collections;

public class THelix
{

    public float phiTrack = 0;
    public float d0;
    public float z0;
    public float phi0;
    public float tL;
    public float pT;
    public float startPhi;
    public float eta;

    //Circle variables
    public float rC;
    public float xC;
    public float yC;
    public float e;


    // save values for tests (wrongly drawn tracks) for debugging
    public float zVertex = 0.0f;
    public float phiVertex = 0.0f;
    public float rhoVertex = 0.0f;
    // negative end vertex means none exits
    private float rhoEndVertex = -1.0f;

    public int numHits;
    private int SIZE = 5;
    // C - constant depending on the magnetic field, constant proportional
    // to curvature in the magnetic field

    public float C;// = 166.6666667f; // =100/0.6


    /** Standard Helix constructor w/o end vertex */
    public THelix(float rhoVertex, float phiVertex, float zVertex, float pTTrack, float phiTrack, float etaTrack, int charge )
    {
        createHelix(rhoVertex, phiVertex, zVertex, pTTrack, phiTrack, etaTrack, charge, -1f);
    }


    /** Standard Helix constructor w/ end vertex */
    public THelix(float rhoVertex, float phiVertex, float zVertex, float pTTrack, float phiTrack, float etaTrack, int charge, float rhoEndVertex )
    {
        createHelix(rhoVertex, phiVertex, zVertex, pTTrack, phiTrack, etaTrack, charge, rhoEndVertex);

    }

    public void createHelix(float rhoVertex, float phiVertex, float zVertex, float pTTrack, float phiTrack, float etaTrack, int charge, float rhoEndVertex )
    {
        
        // note: this method is not called when creating helix representation
        // of reconstructed InDet tracks, but is called for simulated InDet tracks
        
        // input:
        // rhoVertex = rho of vertex from which track starts
        // phiVertex = phi of vertex
        // zVertex   =   Z of vertex
        // pTTrack = transverse track momentum > 0
        // phiTrack = phi of track seen from vertex
        // etaTrack = eta = -ln(tg(theta/2)),  theta = arctg(pTTrack/Pz) = 2*arctg( e**(-eta))
        // charge  = charge
        //
        // derived:
        // rC = radius of circle
        // xC = x postion of center of circle
        // yC = y postion of center of circle
        // E  = E**eta
        // charge=1 if icode.ge.0 charge=-1 else
        //
        // Output:
        // tL = dip of track = Pz/pTTrack =0.5 * ( E - 1/E ) : constant along the helix
        // eta = ln( Pz/pTTrack + SQRT[(Pz/pTTrack)**2 +1 ]
        // d0 = distance between circle and (0,0) at point of closest approach PCA
        // Fs = angle between (0,0) and vertex as seen from the circle center [degrees]
        // Z0 = Z of circle at PCA
        // F0 = phi0 of track at PCA [degrees]
        // Pq = charge*pTTrack
        // For helices starting at PCA: Fs=0. this is the case for reconstructed
        // helices, which are not yet associated to a secondary vertex or are
        // drawn to PCA.
        // calculation:
        // rC = C*pTTrack                            C = 100/0.6
        // rC = C*pTTrack
        // xC = rhoVertex*cos(phiVertex) + rC*sin(phiTrack)
        // yC = rhoVertex*sin(phiVertex) - rC*cos(phiTrack)
        // tl = Pz/pTTrack = 0.5 * ( E - 1/E )
        // d0 = rC - sqrt(xC*xC + yC*yC)
        // startPhi = pi/2 - phiTrack + atan2(yC,xC)    modify startPhi to: -pi/2 < startPhi < pi/2
        // z0 = zVertex - rC*startPhi*tl
        // phi0 = phiTrack + startPhi
        // pCharge = charge*pTTrack
        // Change startPhi and phi0 from radians to degrees.
        // ///
        
        this.phiTrack = Mathf.Rad2Deg * phiTrack;
        float PI2 = Mathf.PI / 2;
        float PI = Mathf.PI;
        float TPI = Mathf.PI * 2;
        
        
        if (charge > 0)
        {
            rC = C * pTTrack;
            xC = rhoVertex * Mathf.Cos(phiVertex) + rC * Mathf.Sin(phiTrack);
            yC = rhoVertex * Mathf.Sin(phiVertex) - rC * Mathf.Cos(phiTrack);
            
            e = Mathf.Exp(etaTrack);
            
            tL = 0.5f * (e - 1f / e);
            eta = etaTrack;
            
            d0 = rC - charge * Mathf.Sqrt(xC * xC + yC * yC);
            
            if (phiTrack < 0)
                phiTrack += TPI;
            
            startPhi = PI2 - phiTrack + Mathf.Atan2(yC, xC);
            
            if (startPhi <= -PI)
                startPhi += TPI;
            else if (startPhi > PI)
                startPhi -= TPI;
            
            z0 = zVertex - rhoVertex * tL;
            
            
            phi0 = Mathf.Rad2Deg * (phiTrack + startPhi);
            
            startPhi = Mathf.Rad2Deg * (startPhi);
            
            pT = charge * pTTrack;
        }
        
        else
        {
            rC = C * pTTrack;
            xC = rhoVertex * Mathf.Cos(phiVertex) + rC * Mathf.Sin(phiTrack);
            yC = rhoVertex * Mathf.Sin(phiVertex) - rC * Mathf.Cos(phiTrack);
            e = Mathf.Exp(etaTrack);
            
            tL = 0.5f * (e - 1f / e);
            eta = etaTrack;
            d0 = rC - Mathf.Sqrt(xC * xC + yC * yC);
            
            if (phiTrack < 0)
                phiTrack += TPI;
            
            float temp = Mathf.Atan2(yC, xC);
            
            if (temp < 0)
                temp += TPI;
            
            startPhi = PI2 - phiTrack + temp;
            
            if (startPhi <= -PI)
                startPhi += TPI;
            else if (startPhi > PI)
                startPhi -= TPI;
            
            z0 = zVertex - rC * startPhi * tL;
            
            phi0 = Mathf.Rad2Deg * (phiTrack + startPhi);
            
            startPhi = charge * Mathf.Rad2Deg * (startPhi);
            
            pT = charge * pTTrack;
            
        }
        
        // save values for tests (wrongly drawn tracks) for debugging
        this.zVertex = zVertex;
        
        this.phiVertex = phiVertex;
        
        this.rhoVertex = rhoVertex;
        
        // keep rhoEndVertex for simulated tracks with a daughter
        this.rhoEndVertex = rhoEndVertex;
    }
    
    public void createHelixALICE(float rhoVertex, float phiVertex, float zVertex, float pTTrack, float phiTrack, float etaTrack, int charge, float rhoEndVertex )
    {

        // note: this method is not called when creating helix representation
        // of reconstructed InDet tracks, but is called for simulated InDet tracks

        // input:
        // rhoVertex = rho of vertex from which track starts
        // phiVertex = phi of vertex
        // zVertex   =   Z of vertex
        // pTTrack = transverse track momentum > 0
        // phiTrack = phi of track seen from vertex
        // etaTrack = eta = -ln(tg(theta/2)),  theta = arctg(pTTrack/Pz) = 2*arctg( e**(-eta))
        // charge  = charge
        //
        // derived:
        // rC = radius of circle
        // xC = x postion of center of circle
        // yC = y postion of center of circle
        // E  = E**eta
        // charge=1 if icode.ge.0 charge=-1 else
        //
        // Output:
        // tL = dip of track = Pz/pTTrack =0.5 * ( E - 1/E ) : constant along the helix
        // eta = ln( Pz/pTTrack + SQRT[(Pz/pTTrack)**2 +1 ]
        // d0 = distance between circle and (0,0) at point of closest approach PCA
        // Fs = angle between (0,0) and vertex as seen from the circle center [degrees]
        // Z0 = Z of circle at PCA
        // F0 = phi0 of track at PCA [degrees]
        // Pq = charge*pTTrack
        // For helices starting at PCA: Fs=0. this is the case for reconstructed
        // helices, which are not yet associated to a secondary vertex or are
        // drawn to PCA.
        // calculation:
        // rC = C*pTTrack                            C = 100/0.6
        // rC = C*pTTrack
        // xC = rhoVertex*cos(phiVertex) + rC*sin(phiTrack)
        // yC = rhoVertex*sin(phiVertex) - rC*cos(phiTrack)
        // tl = Pz/pTTrack = 0.5 * ( E - 1/E )
        // d0 = rC - sqrt(xC*xC + yC*yC)
        // startPhi = pi/2 - phiTrack + atan2(yC,xC)    modify startPhi to: -pi/2 < startPhi < pi/2
        // z0 = zVertex - rC*startPhi*tl
        // phi0 = phiTrack + startPhi
        // pCharge = charge*pTTrack
        // Change startPhi and phi0 from radians to degrees.
        // ///

        this.phiTrack = Mathf.Rad2Deg * phiTrack;
        float PI2 = Mathf.PI / 2;
        float PI = Mathf.PI;
        float TPI = Mathf.PI * 2;

        rC = Mathf.Abs(C * pTTrack);
        xC = rhoVertex * Mathf.Cos(phiVertex) + rC * Mathf.Sin(phiTrack);//sin takes radians
        yC = rhoVertex * Mathf.Sin(phiVertex) + Mathf.Abs(rC * Mathf.Cos(phiTrack));

        e = Mathf.Exp(etaTrack);
        tL = 0.5f * (e - 1f / e);
        eta = etaTrack;
        d0 = rC - Mathf.Sqrt(xC * xC + yC * yC);
        
        if (phiTrack < 0)
            phiTrack += TPI;
        
        float temp = Mathf.Atan2(yC, xC);
        
        if (temp < 0)
            temp += TPI;
        
        startPhi = PI2 - phiTrack + temp;
        
        if (startPhi <= -PI)
            startPhi += TPI;
        else if (startPhi > PI)
            startPhi -= TPI;

//        z0 = zVertex;// - rhoVertex * tL;
        z0 = zVertex - rC * startPhi * tL;
        phi0 = Mathf.Rad2Deg * (phiTrack + startPhi);
        startPhi = - Mathf.Rad2Deg * (startPhi);
        pT = - pTTrack;

        Debug.Log("phiTrack:"+phiTrack*180f/3.14f);
        Debug.Log("phiVertex:"+phiVertex*180f/3.14f);
        Debug.Log("ch:" + charge + "\trC:" + rC + "\txC:" + xC + "\tyC:" + yC);
        Debug.Log("tL:"+tL+"\teta:"+eta+"\td0:"+d0);
        

        // save values for tests (wrongly drawn tracks) for debugging
        this.zVertex = zVertex;
        this.phiVertex = phiVertex;
        this.rhoVertex = rhoVertex;

        // keep rhoEndVertex for simulated tracks with a daughter
        this.rhoEndVertex = rhoEndVertex;
    }

    /**
     * Construct AHelix object.
     * @param d0
     * @param z0
     * @param phi0 [degrees]
     * @param tL
     * @param pT
     */
    public THelix(float d0, float z0, float phi0, float tL, float pT)
    {
        this.d0 = d0;
        this.z0 = z0;
        this.phi0 = phi0;

        if (this.phi0 < 0){
            this.phi0 += 360;
        }

        this.tL = tL;

        this.C = 166.6666667f; // =100/0.6
        
        this.eta = this.calculateEta(this.tL);
        this.pT = pT;
        startPhi = 0;
        int ch = (int)Mathf.Sign(pT);
        float pTAbs = Mathf.Abs(pT);



        createHelix(0f, 0f, 0f, pTAbs, phi0, eta, ch, -1f);
    }
    public THelix(float x, float y,float z, float phi0, float tL, float pT,float C,float theta)
    {
        // x,y,z - position of the track's vertex
        // phi0  - phi angle of track looking from it's vertex
        // tL - cot(theta)
        // pT - signed transverse momentum

//        this.phi0 = Mathf.Rad2Deg*phi0;
//        if (this.phi0 < 0){
//            this.phi0 += 360;
//        }
        
        this.tL = tL;
//        this.eta = this.calculateEta(this.tL);
        this.eta = this.calculateEtaFromTheta(theta);
        this.pT = pT;
        this.C = C;
        
        int ch = (int)Mathf.Sign(pT);
        float pTAbs = Mathf.Abs(pT);
        float rho = Mathf.Sqrt(x*x+y*y);
        float phiVertex = Mathf.Atan2(y,x); // phi of vertex looking from (0,0,0)
        
        createHelix(rho, phiVertex, z, pTAbs, phi0, eta, ch, -1f);
//        createHelixALICE(rho, phiVertex, z, pTAbs, phi0, eta, ch, -1f);
    }

    
    
    public THelix(float d0, float z0, float phi0, float tL, float pT, int numHits)
    {
        this.d0 = d0;
        this.z0 = z0;
        this.phi0 = phi0;
        this.tL = tL;
        this.eta = this.calculateEta(this.tL);
        this.pT = pT;
        this.numHits = numHits;
        this.C = 166.6666667f; // =100/0.6
    }


    public THelix(float d0, float z0, float phi0, float tL, float pT, float startPhi)
    {
        this.d0 = d0;
        this.z0 = z0;
        this.phi0 = phi0;
        this.tL = tL;
        this.eta = this.calculateEta(this.tL);
        this.pT = pT;
        this.startPhi = startPhi;
        this.C = 166.6666667f; // =100/0.6
    }

    public float getRhoEndVertex()
    {
        return rhoEndVertex;
    }




    public Vector3 getP()
    {
        float pTrans = Mathf.Abs(pT);
        float phi = Mathf.Deg2Rad * phi0;
        Vector3  P = new Vector3(pTrans * Mathf.Cos(phi), pTrans * Mathf.Sin(phi), pTrans * tL);
        return P;
    }

    // eta = -ln(tg theta/2), so eta = -ln(sqrt(ctgtheta*ctgtheta+1)-ctgtheta)
    public float calculateEta(float ctgTheta)
    {
        float eta = -Mathf.Log(Mathf.Sqrt(ctgTheta * ctgTheta + 1.0f) - ctgTheta);
        return eta;
    }
    public float calculateEtaFromTheta(float theta)
    {
        float eta = -Mathf.Log(Mathf.Tan(theta/2f));
        return eta;
    }
}
