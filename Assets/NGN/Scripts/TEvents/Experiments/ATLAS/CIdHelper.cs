using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * This class contains methods for extracting various properties from the 32 bit
 * identifiers used for hits. It works very similarly to the IdHelpers in Athena,
 * hence its name.
 *
 * Based on the similar file from Atlantis, modified to suit Camelia
 */
public class CIdHelper
{

    // These and all the other numbers in this class have been taken from Athena.
    //private var MAX_BIT:int = 0x80000000;
    private int PIXEL_MASK = 0x000000FF;
    private int[] SUBDETECTOR = new int[]{2, 4, 5, 7, 10};
    private int[] LAR_PART = new int[]{-3, -2, -1, 1, 2, 3, 4, 5};
    private int[] LAR_BARREL_ENDCAP = new int[]{-3, -2, -1, 1, 2, 3};
    private int[] LAR_POS_NEG = new int[]{-2, 2};
    private int[] TRT_BARREL_ENDCAP = new int[]{-2, -1, 1, 2};
    private AtlasCaloBuilder ccalo;

    private string caloName;
    private string caloTypeXml;

    public void setCcalo(AtlasCaloBuilder calo)
    {
        ccalo = calo;
    }


    /**
     * Returns the decoded athena identifier (id) as a string, resp. an
     * array of strings: first item is the full id expanded (decoded) and
     * particular subparts are separated by "/" in one sting, further
     * interesting subparts of the decoded id are provided in the following
     * items of the array with an explanation given.
     * So far (2009-01-20), this is implemented only for LAr and Tile, for
     * other subsystems only first array item is returned with the expanded
     * id.
     * @param id int compact identifier
     * @return String decoded identifier
     */
    public void getFullIdentifier(int id)
    {


        int sub = subDetector(id);
        bool isEvaluated = false;
        switch (sub)
        {
        case 2:
            // Inner Detector. We will not implement this here
            Debug.Log("Inner Detector. Skipping");
            break;
        case 4:
            int part = larPart(id);
            int sampling = larSampling(id);
            int eta = larEta(id);
            int phi = larPhi(id);
            int region;
            int posNeg;
            ccalo.setIsLAr(true);
            ccalo.setIsTile(false);

            if (part == 1)   // LAr barrel, LAr endcap
            {
                int barrelEndcap = larBarrelEndcap(id);
                region = larRegion(id);

                if (barrelEndcap == 1)
                {
                    //r.push("barrel A");
                    ccalo.setType(AtlasCaloBuilder.cType.EMBA);
                    ccalo.setCaloName("LAr");
                    ccalo.setCaloTypeXml("ABarrelCalorimeter");
                    ccalo.setSide(1);
                }
                else if (barrelEndcap == -1)
                {
                    //r.push("barrel C");
                    ccalo.setType(AtlasCaloBuilder.cType.EMBC);
                    ccalo.setCaloName("LAr");
                    ccalo.setCaloTypeXml("ABarrelCalorimeter");
                    ccalo.setSide(-1);
                }
                else if (barrelEndcap == 2)
                {
                    //r.push("endcap A");
                    ccalo.setType(AtlasCaloBuilder.cType.EMECA);
                    ccalo.setCaloName("LAr Outer Endcap");
                    ccalo.setCaloTypeXml("AEndcapCalorimeter");
                    ccalo.setSide(1);
                }
                else if (barrelEndcap == 3)
                {
                    //r.push("endcap A");
                    ccalo.setType(AtlasCaloBuilder.cType.EMECA);
                    ccalo.setCaloName("LAr Inner Endcap");
                    ccalo.setCaloTypeXml("AEndcapCalorimeter");
                    ccalo.setSide(1);
                }
                else if (barrelEndcap == -2)
                {
                    //r.push("endcap C");
                    ccalo.setType(AtlasCaloBuilder.cType.EMECC);
                    ccalo.setCaloName("LAr Outer Endcap");
                    ccalo.setCaloTypeXml("AEndcapCalorimeter");
                    ccalo.setSide(-1);
                }
                else if (barrelEndcap == -3)
                {
                    //r.push("endcap C");
                    ccalo.setType(AtlasCaloBuilder.cType.EMECC);
                    ccalo.setCaloName("LAr Inner Endcap");
                    ccalo.setCaloTypeXml("AEndcapCalorimeter");
                    ccalo.setSide(-1);
                }
            }
            else if (part == 2)     // HEC
            {
                posNeg = larPosNeg(id);
                region = larRegion(id);
                if (posNeg == 2)
                {
                    //r.push("A");
                    ccalo.setType(AtlasCaloBuilder.cType.HECA);
                    ccalo.setCaloName("HEC");
                    ccalo.setCaloTypeXml("AEndcapCalorimeter");
                    ccalo.setSide(1);
                }
                else if (posNeg == -2)
                {
                    //r.push("C");
                    ccalo.setType(AtlasCaloBuilder.cType.HECC);
                    ccalo.setCaloName("HEC");
                    ccalo.setCaloTypeXml("AEndcapCalorimeter");
                    ccalo.setSide(-1);
                }
            }
            else if (part == 3)     // FCAL
            {
                posNeg = larPosNeg(id);
                if (posNeg == 2)
                {
                    //r.push("A");
                    ccalo.setType(AtlasCaloBuilder.cType.FCALA);
                }
                else if (posNeg == -2)
                {
                    //r.push("C");
                    ccalo.setType(AtlasCaloBuilder.cType.FCALC);
                }
            }
            else
            {
                Debug.Log("Invalid identifier: " + id);
            }

            isEvaluated = true;
            break;

        case 5:
            // tile calorimeter (first number of full id is 5)
            /*var section:int = tileSection(id);
            var side:int = tileSide(id);
            var module:int = tileModule(id);
            var tower:int = tileTower(id);
            sampling = tileSampling(id);
            var pmt:int = tilePmt(id);
            var adc:int = tileAdc(id);

            isEvaluated = true;*/
            ccalo.setIsLAr(false);
            ccalo.setIsTile(true);
            ccalo.setType(AtlasCaloBuilder.cType.TILE);
            ccalo.setCaloTypeXml("ABarrelCalorimeter");
            ccalo.setSide(tileSide(id));

            switch (tileSection(id))
            {
            case 1:
                // TILE barrel
                ccalo.setCaloName("TILE Barrel");
                break;
            case 2:
                // TILE extended barrel
                ccalo.setCaloName("Extended TILE");
                break;
            case 3:
                // ITC gap/crack
                break;
            case 4:
                // MBTS
                break;
            default:
                break;
            }

            break;

        case 7:
            //Muon Spectrometer
            Debug.Log("Muon Spectrometer. Skipping");
            break;
        default:
            Debug.Log("Not yet implemented for identifier: " + id);
            break;

        }


    } // getFullIdentifier() ------------------------------------------------



    /**
     * Extracts the subdetector field.
     * @param id int compact identifier
     * @return int subdetector identifier
     * @throws AAtlantisException
     */
    public int subDetector(int id)
    {
        int subDetectorIndex = id >> 29 & 7;
        if (subDetectorIndex >= SUBDETECTOR.Length)
        {
            Debug.Log("Invalid subdetector field in identifier: " + id);
        }
        return SUBDETECTOR[subDetectorIndex];
    }

    public int larPart(int id)
    {
        if (subDetector(id) != 4)
        {
            Debug.Log("Not a LAr identifier: " + id);
        }
        int larPartIndex = id >> 26 & 7;
        if (larPartIndex >= LAR_PART.Length)
        {
            Debug.Log("Invalid part field in identifier: " + id);
        }
        return LAR_PART[larPartIndex];
    }

    public int larBarrelEndcap(int id)
    {
        if (subDetector(id) != 4)
        {
            Debug.Log("Not a LAr identifier: " + id);
        }
        int larBarrelEndcapIndex = id >> 23 & 7;
        if (larBarrelEndcapIndex >= LAR_BARREL_ENDCAP.Length)
        {
            Debug.Log("Invalid barrel-endcap field in identifier: " + id);
        }
        return LAR_BARREL_ENDCAP[larBarrelEndcapIndex];
    }

    public int larPosNeg(int id)
    {
        if (larPart(id) < 2)
        {
            Debug.Log("Not a LAr HEC/FCAL identifier: " + id);
        }
        int larPosNegIndex = id >> 25 & 1;
        if (larPosNegIndex >= LAR_POS_NEG.Length)
        {
            Debug.Log("Invalid pos-neg field in identifier: " + id);
        }
        return LAR_POS_NEG[larPosNegIndex];
    }

    public int larSampling(int id)
    {
        int value = -1;
        bool isEvaluated = false;
        switch (larPart(id))
        {
        case 1:
            value = id >> 21 & 3;
            isEvaluated = true;
            break;
        case 2:
            break;
        case 3:
            value = id >> 23 & 3;
            isEvaluated = true;
            break;
        default:
            Debug.Log("Not a LAr EM/HEC identifier: " + id);
            break;
        }

        return value;
    }

    public int larRegion(int id)
    {
        int value = -1;
        bool isEvaluated = false;
        switch (larPart(id))
        {
        case 1:
            value = id >> 18 & 7;
            isEvaluated = true;
            break;
        case 2:
            value = id >> 22 & 1;
            isEvaluated = true;
            break;
        default:
            Debug.Log("Not a LAr EM/HEC identifier: " + id);
            break;
        }
        return value;
    }

    public int larEta(int id)
    {
        int value = -1;
        bool isEvaluated = false;
        switch (larPart(id))
        {
        case 1:
            value = id >> 9 & 511;
            isEvaluated = true;
            break;
        case 2:
            value = id >> 18 & 15;
            isEvaluated = true;
            break;
        case 3:
            value = id >> 17 & 63;
            isEvaluated = true;
            break;
        default:
            Debug.Log("Not a LAr EM/HEC/FCAL identifier: " + id);
            break;
        }
        return value;
    }

    public int larPhi(int id)
    {
        int value = -1;
        bool isEvaluated = false;
        switch (larPart(id))
        {
        case 1:
            value = id >> 1 & 255;
            isEvaluated = true;
            break;
        case 2:
            value = id >> 12 & 63;
            isEvaluated = true;
            break;
        case 3:
            value = id >> 13 & 15;
            isEvaluated = true;
            break;
        default:
            Debug.Log("Not a LAr EM/HEC/FCAL identifier: " + id);
            break;
        }
        return value;
    }

    public int larModule(int id)
    {
        if (larPart(id) != 3)
        {
            Debug.Log("Not an FCAL identifier: " + id);
        }
        return id >> 21 & 3;
    }


    public bool larIsBarrel(int id)
    {
        bool value = false;;
        bool isEvaluated = false;
        switch (Mathf.Abs(larBarrelEndcap(id)))
        {
        case 1:
            value = true;  // yes, it's barrel
            isEvaluated = true;
            break;
        case 2:
            value = false; // is endcap
            isEvaluated = true;
            break;
        case 3:
            value = false; // is endcap
            isEvaluated = true;
            break;
        default:
            Debug.Log("Not a LAr identifier: " + id);
            break;
        }
        return value;
    }


    public int tileSection(int id)
    {
        if (subDetector(id) != 5)
        {
            Debug.Log("Not a TILE identifier: " + id);
        }
        return id >> 26 & 7;
    }

    public int tileSide(int id)
    {
        if (subDetector(id) != 5)
        {
            Debug.Log("Not a TILE identifier: " + id);
        }
        return (id >> 22 & 15) - 1;
    }

    public int tileModule(int id)
    {
        if (subDetector(id) != 5)
        {
            Debug.Log("Not a TILE identifier: " + id);
        }
        return id >> 14 & 255;
    }

    public int tileTower(int id)
    {
        if (subDetector(id) != 5)
        {
            Debug.Log("Not a TILE identifier: " + id);
        }
        return id >> 8 & 63;
    }

    public int tileSampling(int id)
    {
        if (subDetector(id) != 5)
        {
            Debug.Log("Not a TILE identifier: " + id);
        }
        return id >> 4 & 15;
    }

    public int tilePmt(int id)
    {
        if (subDetector(id) != 5)
        {
            Debug.Log("Not a TILE identifier: " + id);
        }
        return id >> 2 & 3;
    }

    public int tileAdc(int id)
    {
        if (subDetector(id) != 5)
        {
            Debug.Log("Not a TILE identifier: " + id);
        }
        return id & 3;
    }


}



