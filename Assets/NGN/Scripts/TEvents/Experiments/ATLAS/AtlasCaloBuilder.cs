using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AtlasCaloBuilder
{

    private float scaleEt = 0f;


    /** ATLAS reconstruction parameters */
    private float etaSteps;
    private float phiSteps;
    private int region;
    private int sampling;

    /**Values to be read from the calorimeter description XML*/
    private float rMin;
    private float rMax;
    private float zMin;
    private float zMax;
    private float phi0;
    private int numPhi;
    private float deltaPhi;
    private float eta0;
    private float deltaEta;
    private int numEta;
    private int minEta;


    private CIdHelper idHelper;
    private int caloID;
    private bool isTileCal;
    private bool isLAr;
    private bool isBarrel;
    private bool isEndcap;



    private string caloTypeXml;
    private string caloName;
    private bool caloExists;

    TStructCalo structCalo;
    opCalo opCal;









    public enum cType
    {
        // LAr barrel, LAr endcap
        EMBA,
        EMBC,
        EMECA,
        EMECC,
        // HEC
        HECA,
        HECC,
        // FCAL
        FCALA,
        FCALC,
        // Tilecal
        TILE
    };

    private cType caloType;


    void Setup()
    {
        setType(cType.FCALA);


        idHelper = new CIdHelper();
        idHelper.setCcalo(this);
        isTileCal = false;
        isLAr = false;
        isBarrel = false;
        isEndcap = false;
    }

    public void setCaloID(int cid)
    {
        caloID = cid;
    }


    private void calculateReconstructedCalorimeter()
    {

        reconstructCalo();

        if (caloExists)
        {

            // eta = -ln(tg theta/2), so theta = 2*atan(exp(-eta))
            /*theta = 2*Mathf.Atan(Mathf.Exp(-eta));


            /*Calculate cell surface vertexes*/

            float thetaMin = 2 * Mathf.Atan(Mathf.Exp(-getEtaMin()));
            float thetaMax = 2 * Mathf.Atan(Mathf.Exp(-getEtaMax()));

            float minX = rMin * Mathf.Cos(getPhiMin());
            float maxX = rMin * Mathf.Cos(getPhiMax());

            float minY = rMin * Mathf.Sin(getPhiMin());
            float maxY = rMin * Mathf.Sin(getPhiMax());

            float minZ = rMin * Mathf.Cos(thetaMin);
            float maxZ = rMin * Mathf.Cos(thetaMax);

            scaleEt = structCalo.energy * 0.05f;

            opCal.meshScale = 0.08f;
            float length = 1f + scaleEt;

            structCalo.p1 = opCal.meshScale * new Vector3(minX, minY, minZ);
            structCalo.p2 = opCal.meshScale * new Vector3(maxX, maxY, minZ);
            structCalo.p3 = opCal.meshScale * new Vector3(maxX, maxY, maxZ);
            structCalo.p4 = opCal.meshScale * new Vector3(minX, minY, maxZ);

            Vector3 xtraZ = opCal.meshScale * new Vector3(0, 0, zMin * opCal.side);


            structCalo.p1u = length * structCalo.p1 + xtraZ;
            structCalo.p2u = length * structCalo.p2 + xtraZ;
            structCalo.p3u = length * structCalo.p3 + xtraZ;
            structCalo.p4u = length * structCalo.p4 + xtraZ;

            structCalo.p1 += xtraZ;
            structCalo.p2 += xtraZ;
            structCalo.p3 += xtraZ;
            structCalo.p4 += xtraZ;

            /*p1n = p1;
            p2n = p2;
            p3n = p3;
            p4n = p4;*/

        }
    }







    private void reconstructCalo()
    {
        determineType();
        determineEtaSteps();
        determinePhiSteps();
        determineRegion();
        determineSampling();


        //float[] xmlValues = agent.getCaloGeoLoader().getCaloElements(caloTypeXml, caloName, sampling, region);
        float[] xmlValues = new float[10]; // fake code, temporary

        caloExists = xmlValues[8] != -1.0;

        numPhi = (int)xmlValues[0];
        phi0 = xmlValues[1];
        deltaEta = xmlValues[2];
        eta0 = xmlValues[3];
        zMax = xmlValues[4];
        zMin = xmlValues[5];
        rMax = xmlValues[6];
        rMin = xmlValues[7];
        numEta = (int)xmlValues[8];
        minEta = (int)xmlValues[9];

        //float pi = 3.1415926f;
        deltaPhi = 2 * Mathf.PI / numPhi;

        //minEta = 0.0f;

    }

    public void setCaloName(string name)
    {
        caloName = name;
    }

    public void setCaloTypeXml(string ctypexml)
    {
        caloTypeXml = ctypexml;
    }

    public void setType(cType tp)
    {
        caloType = tp;
    }

    public void setSide(int s)
    {
        opCal.side = s;
    }

    public void determineType()
    {
        idHelper.getFullIdentifier(caloID);
    }

    public void determineEtaSteps()
    {
        if (isLAr)
        {
            etaSteps = idHelper.larEta(caloID);
        }
        else if (isTileCal)
        {
            etaSteps = idHelper.tileTower(caloID);
        }
    }

    public void determineRegion()
    {
        if (isLAr)
        {
            region = idHelper.larRegion(caloID);
        }
        else if (isTileCal)
        {
            region = 0;
        }
    }

    public void determinePhiSteps()
    {
        if (isLAr)
        {
            phiSteps = idHelper.larPhi(caloID);
        }
        else if (isTileCal)
        {
            phiSteps = idHelper.tileModule(caloID);
            //phi = 5.625*mod;
        }
    }

    public void determineSampling()
    {
        if (isLAr)
        {
            sampling = idHelper.larSampling(caloID);
        }
        else if (isTileCal)
        {
            sampling = idHelper.tileSampling(caloID);
        }
    }


    /**
    * Returns the eta value of the center of a cell.
    */
    private float getEta()
    {
        if (opCal.side < 0)
        {
            return -(eta0 + (etaSteps - minEta + 0.5f) * deltaEta);
        }
        else
        {
            return (eta0 + (etaSteps - minEta + 0.5f) * deltaEta);
        }
    }

    /**
    * Returns the lowest eta value of a cell.
    */
    private float  getEtaMin()
    {
        if (opCal.side < 0)
        {
            return -(eta0 + (etaSteps - minEta) * deltaEta);
        }
        else
        {
            return (eta0 + (etaSteps - minEta) * deltaEta);
        }
    }

    /**
    * Returns the highest eta value of a cell.
    */
    private float  getEtaMax()
    {
        if (opCal.side < 0)
        {
            return -(eta0 + (etaSteps - minEta + 1f) * deltaEta);
        }
        else
        {
            return (eta0 + (etaSteps - minEta + 1f) * deltaEta);
        }
    }

    /**
    * Returns the phi value of the center of a cell.
    */
    private float getPhi()
    {
        return phi0 + (phiSteps + 0.5f) * deltaPhi;
    }

    /**
    * Returns the lowest phi value of a cell.
    */
    private float getPhiMin()
    {
        return phi0 + ((phiSteps) * deltaPhi);
    }

    /**
    * Returns the highest phi value of a cell.
    */
    private float getPhiMax()
    {
        return phi0 + ((phiSteps + 1f) * deltaPhi);
    }


    /**
    * Returns the center radius of the calorimeter part.
    */
    private float getR()
    {
        return ((rMin + rMax) / 2.0f);
    }


    private float getRMin()
    {
        return rMin;
    }

    private float getRMax()
    {
        return rMax;
    }

    /**
    * Returns the center z of the calorimeter part.
    */
    private float getZ()
    {
        return ((zMin + zMax) / 2.0f);
    }

    private float getZMin()
    {
        return zMin;
    }

    private float getZMax()
    {
        return zMax;
    }

    public void setIsLAr(bool il)
    {
        isLAr = il;
    }

    public void setIsTile(bool it)
    {
        isTileCal = it;
    }

    public cType getType()
    {
        return caloType;
    }

    public TStructCalo generateCalorimeter(float thetaMid, float phiMid, float newenergy)
    {
        rMin = 200.0f;

        //float pi = 3.1415926f;
        //float raddeg = 180f/pi;

        float L = 430f;
        float H = rMin;

        float sideSign = 1f;
        if (thetaMid >90f && thetaMid < 270f)
        {
            sideSign = -1f;            
        }
        

        thetaMid /= Mathf.Rad2Deg;

        float tAngle = Mathf.Atan(H / L);        
        float R0 = rMin;

       
        bool isSide = false;        
        
        if (Mathf.Abs(Mathf.DeltaAngle(thetaMid*Mathf.Rad2Deg,0f)) < tAngle*Mathf.Rad2Deg || Mathf.Abs(Mathf.DeltaAngle(thetaMid*Mathf.Rad2Deg,180f)) < tAngle*Mathf.Rad2Deg)
        {
            R0 = L / Mathf.Cos(thetaMid);            
            isSide = true;
        }
        else
        {
            R0 = H / Mathf.Cos(Mathf.PI / 2f - thetaMid);
        }

        /*Calculate cell surface vertexes*/

        TStructCalo newStructCalo = new TStructCalo();
        setIsLAr(false);
        setIsTile(true);


        newStructCalo.energy = newenergy;

        float deltaTheta = 0.1f + 0.1f * Mathf.Cos(thetaMid) ;
        float deltaPhi = 0.1f;

        float thetaMin =  thetaMid - deltaTheta / 2.0f;
        float thetaMax =  thetaMid + deltaTheta / 2.0f;

        float phiMin =  phiMid - deltaPhi / 2.0f;
        float phiMax =  phiMid + deltaPhi / 2.0f;

        //float thetaMin = 2*Mathf.Atan(Mathf.Exp(-getEtaMin()));
        //float thetaMax = 2*Mathf.Atan(Mathf.Exp(-getEtaMax()));

        if (!isSide)            
        {
            float minX = R0 * Mathf.Sin(thetaMid) * Mathf.Cos(phiMin);
            float maxX = R0 * Mathf.Sin(thetaMid) * Mathf.Cos(phiMax);

            float minY = R0 * Mathf.Sin(thetaMid) * Mathf.Sin(phiMin);
            float maxY = R0 * Mathf.Sin(thetaMid) * Mathf.Sin(phiMax);

            float minZ = R0 * Mathf.Cos(thetaMin);
            float maxZ = R0 * Mathf.Cos(thetaMax);


            scaleEt = newenergy * 0.05f;

            float meshScale = 1f;
            float length = 1f + scaleEt;

            float side = 1f;
            newStructCalo.p1 = meshScale * new Vector3(minX, minY, minZ);
            newStructCalo.p2 = meshScale * new Vector3(maxX, maxY, minZ);
            newStructCalo.p3 = meshScale * new Vector3(maxX, maxY, maxZ);
            newStructCalo.p4 = meshScale * new Vector3(minX, minY, maxZ);

            Vector3 xtraZ = meshScale * new Vector3(0, 0, zMin * side);


            newStructCalo.p1u = length * newStructCalo.p1 + xtraZ;
            newStructCalo.p2u = length * newStructCalo.p2 + xtraZ;
            newStructCalo.p3u = length * newStructCalo.p3 + xtraZ;
            newStructCalo.p4u = length * newStructCalo.p4 + xtraZ;

            newStructCalo.p1 += xtraZ;
            newStructCalo.p2 += xtraZ;
            newStructCalo.p3 += xtraZ;
            newStructCalo.p4 += xtraZ;

           /* GameObject cube  = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position =  new Vector3(minX, minY, minZ) * 0.6f;
            cube.transform.localScale =  new Vector3(3, 3, 3);
            cube.transform.LookAt(Vector3.zero);
            cube.renderer.material.color = Color.blue;


            GameObject textMeshPrefabObject = GameObject.Find("TextSample");
            GameObject wallTxtOb = (GameObject)GameObject.Instantiate(textMeshPrefabObject, cube.transform.position, Quaternion.identity);            
            wallTxtOb.transform.localScale =  new Vector3(6, 6, 6);
            wallTxtOb.transform.Rotate(new Vector3(0, -90, 0), Space.World);
            TextMesh wallTxt = (TextMesh)wallTxtOb.GetComponent("TextMesh");            
            wallTxt.text = (raddeg*thetaMid).ToString("F1");*/
        }
        else
        {
            float minX = R0 * Mathf.Sin(thetaMid) * Mathf.Cos(phiMin);
            float maxX = R0 * Mathf.Sin(thetaMid) * Mathf.Cos(phiMax);

            float minY = R0 * Mathf.Sin(thetaMid) * Mathf.Sin(phiMin);
            float maxY = R0 * Mathf.Sin(thetaMid) * Mathf.Sin(phiMax);

            float minZ = L*sideSign;
            float maxZ = L*sideSign;


            scaleEt = newenergy * 0.05f;

            float meshScale = 1f;
            float length = 1f + scaleEt;

            float side = 1f;
            newStructCalo.p1 = meshScale * new Vector3(minX, minY, minZ);
            newStructCalo.p2 = meshScale * new Vector3(maxX, maxY, minZ);
            newStructCalo.p3 = meshScale * new Vector3(maxX, maxY, maxZ);
            newStructCalo.p4 = meshScale * new Vector3(minX, minY, maxZ);

            Vector3 xtraZ = meshScale * new Vector3(0, 0, zMin * side);


            newStructCalo.p1u = length * newStructCalo.p1 + xtraZ;
            newStructCalo.p2u = length * newStructCalo.p2 + xtraZ;
            newStructCalo.p3u = length * newStructCalo.p3 + xtraZ;
            newStructCalo.p4u = length * newStructCalo.p4 + xtraZ;

            newStructCalo.p1 += xtraZ;
            newStructCalo.p2 += xtraZ;
            newStructCalo.p3 += xtraZ;
            newStructCalo.p4 += xtraZ;

            /*GameObject cube  = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position =  new Vector3(minX, minY, minZ) * 0.6f;
            cube.transform.localScale =  new Vector3(3, 3, 3);
            cube.transform.LookAt(Vector3.zero);
            cube.renderer.material.color = Color.red;

            GameObject textMeshPrefabObject = GameObject.Find("TextSample");
            GameObject wallTxtOb = (GameObject)GameObject.Instantiate(textMeshPrefabObject, cube.transform.position, Quaternion.identity);            
            wallTxtOb.transform.localScale =  new Vector3(6, 6, 6);
            wallTxtOb.transform.Rotate(new Vector3(0, -90, 0), Space.World);
            TextMesh wallTxt = (TextMesh)wallTxtOb.GetComponent("TextMesh");            
            wallTxt.text = (raddeg*thetaMid).ToString("F1");*/            

        }


        return newStructCalo;

    }






    /*function setEta(neweta:float) {
        eta = neweta;
        }*/










}

