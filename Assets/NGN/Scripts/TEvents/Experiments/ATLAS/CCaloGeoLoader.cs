using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System.Text.RegularExpressions;


public class CCaloGeoLoader {

    private XDocument caloGeoXml;
    


    public void LoadXml(string xmlstring) {


        /** first lets eliminate any DOCTYPE tag in the XML, otherwise Linq will refuse to parse it**/
        string adjustedXml = Regex.Replace(xmlstring, "<!DOCTYPE.+?>", string.Empty);

        /** then we parse the result**/
        this.caloGeoXml = XDocument.Parse(adjustedXml,  LoadOptions.None);

    }


    /**Get the nphi, phi0, deta, eta0, zmax, zMin,rMax and rMin from the calo descrription XML */
    /* first we test if the calorimeter exists in our XML description, for the given region and sampling.*/
    /* If not, we assign the value -1 to all the variables **/
    public  float[] getCaloElements(string type, string name, int sampling, int region) {
        float[] values;
        values = new float[10];
        IEnumerable<XElement> caloElement = from a in caloGeoXml.Element("AGeometry").Elements(type) where a.Attribute("sampling").Value == sampling.ToString() && a.Attribute("region").Value == region.ToString() && a.Attribute("n").Value == name
        select a;

        bool caloExists = caloElement.Count()>0;

        //Debug.Log(caloExists);

        if (caloExists){
            values[0] = float.Parse(caloElement.ElementAt(0).Attribute("nphi").Value);
            values[1] = float.Parse(caloElement.ElementAt(0).Attribute("phi0").Value);
            values[2] = float.Parse(caloElement.ElementAt(0).Attribute("deta").Value);
            values[3] = float.Parse(caloElement.ElementAt(0).Attribute("eta0").Value);
            values[4] = float.Parse(caloElement.ElementAt(0).Attribute("zMax").Value);
            values[5] = float.Parse(caloElement.ElementAt(0).Attribute("zMin").Value);
            values[6] = float.Parse(caloElement.ElementAt(0).Attribute("rMax").Value);
            values[7] = float.Parse(caloElement.ElementAt(0).Attribute("rMin").Value);
            values[8] = float.Parse(caloElement.ElementAt(0).Attribute("neta").Value);
            values[9] = float.Parse(caloElement.ElementAt(0).Attribute("meta").Value);
        } else {
            values[0] = -1.0f;
            values[1] = -1.0f;
            values[2] = -1.0f;
            values[3] = -1.0f;
            values[4] = -1.0f;
            values[5] = -1.0f;
            values[6] = -1.0f;
            values[7] = -1.0f;
            values[8] = -1.0f;
            values[9] = -1.0f;
        }
        

        return values;            

    }

    
    

    

}