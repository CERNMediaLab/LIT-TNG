
/**
 * Helices in a format suitable for drawing given as a circle with
 * centre point and radius etc.....
 * phi is the angle seen from the primary vertex
 * a = alpha = the angle as seen for the center of the circle
 * This class was inspired by Atlantis' ADHelix. Highly modified
 */

using UnityEngine;
using System.Collections;

public class TDHelix
{

    public THelix helix;

    // description of helix
    float rC;
    float xC;
    float yC;
    float ch;
    float a0;
    float x0;
    float y0;
    float z0;
    float bz;
    float cosA0;
    float sinA0;
    // position along helix
    float aStart;
    float aEnd;
    float aCurrent;
    // values at current position
    float x;
    float y;
    float z;
    float rho;
    float phi;
    float startPhi;    // the phi value of starting point of "v"
    bool startMark; // true when calculating start point, false otherwise

    //tracker cylinder dimensions. Must adjust later.
    static float dimensionFactor = 19f;
    float rTo = 5f * dimensionFactor;
    float zTo = 12f * dimensionFactor;

    static float phiUpper;
    static float phiLower;
    static private Vector3 point3D;
    private static int callDepthCut;

    // should be changed
    static float rhoMax = 55.0f;

    public TDHelix(THelix h)
    {

        rC = h.rC;
        ch = Mathf.Sign(h.pT);
        a0 = h.phi0 + ch * 90.0f;
        float a0Rad = Mathf.Deg2Rad * a0;
        cosA0 = Mathf.Cos(a0Rad);
        sinA0 = Mathf.Sin(a0Rad);
        z0 = h.z0;
        float d0 = h.d0;
        //int kdeb = 0;
        float signD0 = ch;


        d0 *= signD0;
        x0 = d0 * cosA0;
        y0 = d0 * sinA0;
        float SD0 = 1.0f;
        float SRC = 1.0f;
        float r0 = SD0 * d0 - SRC * rC;
        xC = r0 * cosA0;
        yC = r0 * sinA0;
        bz = rC * Mathf.Deg2Rad * (h.tL);
        //var rTo:float = rToPar.getD();

        aStart = h.startPhi;
        aCurrent = -99999999f;
        moveTo(aStart);
        //Debug.Log(" x: "+x+" y: "+y+" z: "+z);
        setPhi(180f);

        // don't draw tracks starting outside the tracking region
        /*if (Mathf.Abs(rho) > rTo || Mathf.Abs(z) > zTo) {
                aEnd = aStart;
                return;
            }*/

        //Calculate max phi angle -> no track can have more than 180 deg
        float aEndMax = Mathf.Min(aStart + 180.0f, 180.0f);

        //Also the track should not stick out of the rTo zylinder
        float aEndR = intersectWithRadialCylinder(rTo, aStart, aEndMax);
        // nor should it go beyound the max. z-extension
        float aEndZ = intersectWithZPlanes(zTo, aStart, aEndMax);        // or beyond the end-vertex radius, if given
        aEnd = Mathf.Min(aEndMax, aEndR);

        if (aEndZ > aStart)
            aEnd = Mathf.Min(aEnd, aEndZ);

        if ((aEnd - aStart) > 180.0f)
            aEnd = aStart + ch * 180.0f;

        aEnd = Mathf.Min(aEnd, aEndMax);

        //Debug.Log("rC=" +rC +" aStart="+aStart+" aEnd=" +aEnd + " aEndR="+aEndR + " charge="+ch);
    }

    public TDHelix(float x, float y,float zVertex, float hphiTrack, float theta, float pT,float C)
    {
        // x,y,z - position of the track's vertex
        // hphiTrack  - phi angle of track looking from it's vertex
        // theta  - theta angle of track looking from it's vertex
        // pT - signed transverse momentum
        // C - constant related with the magnetic field

        float PI2 = Mathf.PI / 2;
        float PI = Mathf.PI;
        float TPI = Mathf.PI * 2;
        
        if(hphiTrack < 0){hphiTrack += TPI;}
        
        float rhoVertex= Mathf.Sqrt(x*x+y*y);
        float phiVertex= Mathf.Atan2(y,x);
        
        float hrC = C * Mathf.Abs(pT);
        float hxC = rhoVertex * Mathf.Cos(phiVertex) + hrC * Mathf.Sin(hphiTrack);
        float hyC = rhoVertex * Mathf.Sin(phiVertex) - hrC * Mathf.Cos(hphiTrack);
        
        int charge = (int)Mathf.Sign(pT);
        float htL = 1.0f/Mathf.Tan(theta); // cot(theta)
        float hz0,hstartPhi;
        
        if (charge > 0)
        {
            hstartPhi = PI2 - hphiTrack + Mathf.Atan2(hyC, hxC);

            if (hstartPhi <= -PI){hstartPhi += TPI;}
            else if (hstartPhi > PI){hstartPhi -= TPI;}
            hz0 = zVertex - rhoVertex * htL; // ch > 0
        }
        else
        {
            float temp = Mathf.Atan2(hyC, hxC);
            if (temp < 0){temp += TPI;}
            hstartPhi = PI2 - hphiTrack + temp;
            
            if (hstartPhi <= -PI){hstartPhi += TPI;}
            else if (hstartPhi > PI){hstartPhi -= TPI;}
            hz0 = zVertex - hrC * startPhi * htL; // ch <= 0
        }
        this.aStart = charge * Mathf.Rad2Deg*(hstartPhi);
                
        // tdhelix
        this.rC = hrC;
        this.ch = charge;
        this.a0 = Mathf.Rad2Deg*hphiTrack + this.ch * 90.0f;
        this.cosA0 = Mathf.Cos(Mathf.Deg2Rad * this.a0);
        this.sinA0 = Mathf.Sin(Mathf.Deg2Rad * this.a0);
        this.z0 = hz0;
        this.x0 = x;
        this.y0 = y;
        this.xC = hxC;
        this.yC = hyC;
        this.bz = this.rC * Mathf.Deg2Rad * (htL);
        
        this.aCurrent = -99999999f;

        moveTo(this.aStart);
        setPhi(180f);
        
        //Calculate max phi angle -> no track can have more than 180 deg
        float aEndMax = Mathf.Min(aStart + 180.0f, 180.0f);
        
        //Also the track should not stick out of the rTo zylinder
        float aEndR = intersectWithRadialCylinder(rTo, aStart, aEndMax);
        // nor should it go beyound the max. z-extension
        float aEndZ = intersectWithZPlanes(zTo, aStart, aEndMax);        // or beyond the end-vertex radius, if given
        aEnd = Mathf.Min(aEndMax, aEndR);
        
        if (aEndZ > aStart){
            aEnd = Mathf.Min(aEnd, aEndZ);
        }
        
        if ((aEnd - aStart) > 180.0f){
            aEnd = aStart + this.ch * 180.0f;
        }
        
        aEnd = Mathf.Min(aEnd, aEndMax);
    }

    

    // calculate phi on track at intersection
    // Cylinder is infinite in Z direction
    private float intersectWithRadialCylinder(float rCyl, float aStart, float aEnd)
    {
        //if (rC <= 0) return aStart;
        float r0 = Mathf.Sqrt(xC * xC + yC * yC);
        float bb = (rCyl * rCyl - rC * rC - r0 * r0) / (2.0f * rC * r0);
        float rhoStart = getRho(aStart);
        float rhoEnd = getRho(aEnd);

        if (rCyl >= rhoStart && rCyl >= rhoEnd)
            return aEnd;

        if (rCyl <= rhoStart && rCyl <= rhoEnd)
            return aStart;

        float cc = Mathf.Rad2Deg * (Mathf.Acos(bb));

        if (Mathf.Abs(cc) > 360.0f)
            Debug.Log("error in cc in dhelix");

        float gA = Mathf.Rad2Deg * (Mathf.Atan2(yC, xC));

        float a1 = ch * (a0 - gA + cc);

        float a2 = ch * (a0 - gA - cc);

        if (a1 < 0f)
            a1 += 360.0f;

        if (a2 < 0)
            a2 += 360.0f;

        if (a1 >= 360.0f)
            a1 -= 360.0f;

        if (a2 >= 360.0f)
            a2 -= 360.0f;

        return Mathf.Min(a1, a2);
    }


    // calculate phi on track at intersection
    private float intersectWithZPlanes(float zCyl, float aStart, float aEnd)
    {
        float zMin = 20f;
        float aZ = aEnd;

        if (zCyl >= zMin && bz != 0f)
        {
            float aZPlus = (zCyl - z0) / bz;
            float aZMinus = ( -zCyl - z0) / bz;

            if (aZPlus < aZ && aZPlus > aStart)
                aZ = aZPlus;

            if (aZMinus < aZ && aZMinus > aStart)
                aZ = aZMinus;
        }

        return aZ;
    }


    // calculate phi on track at intersection
    // cylinder finite in Z
    private float intersectWithCylinder(bool useR, float rCyl, bool useZ, float zCyl)
    {
        float aStart = 0f;
        float aEndMax = 180f;
        float zMin = 20f;
        float aEndR = 0f;
        float aEndZ;

        if (useR)
            aEndR = intersectWithRadialCylinder(rCyl, aStart, aEndMax);

        aEndZ = 0f;

        if (useZ && zCyl >= zMin && bz != 0f)
        {
            aEndZ = intersectWithZPlanes(zCyl, aStart, aEndMax);
            return Mathf.Min(aEndR, aEndZ);
        }

        else
            return aEndR;
    }


    // a... angle in deg starting with 0 meaning point of closest approach
    private void moveTo(float a)
    {
        if (a != aCurrent)
        {
            float aRad = Mathf.Deg2Rad * (a0 - ch * a);
            x = x0 + rC * (Mathf.Cos(aRad) - cosA0);
            y = y0 + rC * (Mathf.Sin(aRad) - sinA0);
            z = z0 + bz * a;

            float dx = x;// - primaryVtx[0];
            float dy = y;// - primaryVtx[1];
            rho = Mathf.Sqrt(dx * dx + dy * dy);
            phi = Mathf.Rad2Deg * (Mathf.Atan2(dy, dx));

            while (phi < 0.0f || phi > 360.0f)
            {
                if (phi < 0.0f)
                    phi += 360.0f;
                else
                    phi -= 360.0f;
            }

            if (phi < phiLower)
                phi += 360.0f;
            else if (phi > phiUpper)
                phi -= 360.0f;

            aCurrent = a;
        }
    }


    // s1 and s2 are the extreme values of phi
    // used to avoid tracks being drawn with phi discontinuity
    // in phi projections
    private float setPhiStart(float s1, float s2)
    {
        setPhi(180f);
        float phi1 = getPhi(s1);
        float phi2 = getPhi(s2);
        float phiM = getPhi((s1 + s2) / 2f);
        // doesn't work for all tracks but for most

        if (Mathf.Abs(phi1 - phi2) > 180f || Mathf.Abs(phi1 - phiM) > 180f ||  Mathf.Abs(phi2 - phiM) > 180f)
        {
            if (phi1 - phiM > 180f)
                phi1 -= 360f;

            if (phi2 - phiM > 180f)
                phi2 -= 360f;

            if (phi1 - phiM < -180f)
                phi1 += 360f;

            if (phi2 - phiM < -180f)
                phi2 += 360f;

            // must be something wrong here, phiM is the same using the
            // following code, then why use if ... else ...
            if (phi1 < phiM && phiM < phi2)
                phiM = (phi1 + phi2) / 2f;
            else if (phi1 > phiM && phiM > phi2)
                phiM = (phi1 + phi2) / 2f;
            else
            {
                phiM = (phi1 + phi2) / 2f;
            }
        }

        if (phiM > 360f)
            phiM -= 360f;

        if (phiM < 0f)
            phiM += 360f;

        setPhi(phiM);

        return phiM;
    }


    private float getAStart()
    {
        return aStart;
    }


    private float getAEnd()
    {
        return aEnd;
    }


    private Vector3 getYXPoint(float a)
    {
        moveTo(a);
        return new Vector3(x, y, -1);
    }

    public Vector3[] generatePolyline()
    {
        int numSegments = 1 + (int)Mathf.CeilToInt(Mathf.Abs(aEnd - aStart) * 0.2f);
        numSegments = (int)Mathf.Min(numSegments, 30f);
        Vector3[] polylineVectors = new Vector3[numSegments + 1];

        for (int n = 0; n < numSegments + 1; n++)
        {
            float currentAngle = aStart + n * (aEnd - aStart) / numSegments;
            polylineVectors[n] = get3DPoint(currentAngle);
        }

        //Debug.Log("# segments: "+polylineVectors.length);
        return polylineVectors;
    }

    private Vector3 get3DPoint(float a)
    {
        moveTo(a);
        return new Vector3(x, y, z);
    }


    // set phi range
    public void setPhi(float phiMid)
    {
        phiLower = phiMid - 180f;
        phiUpper = phiMid + 180f;
    }


    public float getPhi(float a)
    {
        moveTo(a);
        return phi;
    }


    public float getRho(float a)
    {
        moveTo(a);
        return rho;
    }

    public float etaAbs(float z, float rho)   //returns the absolute value of eta in relation to z=0
    {
        float zrho = z / rho;
        return Mathf.Log(zrho + Mathf.Sqrt(zrho * zrho + 1f));
    }

}
