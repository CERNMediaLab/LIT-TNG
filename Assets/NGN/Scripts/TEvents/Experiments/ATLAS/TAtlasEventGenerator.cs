using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TAtlasEventGenerator
{
    float energy;
    int seed = 44;
    Material trackMaterial;
    TStructEvent generatedEvent;
    TEventManager eventManager;


    void initialSetup()
    {

        /*float eventScale = 2f;

        ParentEvent = new GameObject("ParentEvent");
        ParentEvent.transform.Rotate(new Vector3(0f, 90f, 0f));
        ParentEvent.transform.localScale = new Vector3(eventScale, eventScale, eventScale);*/

    }

    void setTrackMaterial(Material mat)
    {
        trackMaterial = mat;
    }

    public void SetEventManager(TEventManager evmgr)
    {
        eventManager = evmgr;
    }
    
    public TStructEvent generateEvent(float energy, float lrBalance)
    {
        generatedEvent = new TStructEvent("ATLAS", true);


        generateRandomTracks(energy, lrBalance);
        //generateRandomJets(energy, lrBalance);
        //generateRandomCalorimeters(energy);
        //generateSignature(energy);
        //    generatedEvent.SetIsGenerated(true);
        //    generatedEvent.SetExperiment("ATLAS");
        eventManager.HireAgent(generatedEvent);
        return generatedEvent;
    }

    public TStructEvent generateTestEvent()
    {
        generatedEvent = new TStructEvent("ATLAS", true);


        generateTestTracks();        
        
        eventManager.HireAgent(generatedEvent);
        return generatedEvent;
    }

    void generateSignature(float energy)
    {

    }

    int determineNumberOfTracks(float energy)
    {
        int numtracks = Mathf.CeilToInt(energy * 40f);
        return numtracks;
    }

    public void generateTestTracks()
    {

        float _theta = 0.001f;
        float phi0 = 0f;

        float pi = Mathf.PI;
        TStructTrack[] generatedTracks = new TStructTrack[1];
        List<TStructCalo> listCalo = new List<TStructCalo>();
        TStructTrack eventTrack = new TStructTrack();
        float _pt = 100f;
        _pt = _pt / 15.0f;
        eventTrack.pt = _pt;
        
        float _cottheta = 1.0f / Mathf.Tan(_theta);
        eventTrack.cottheta = _cottheta;
        eventTrack.theta = _theta;


        eventTrack.phi = phi0;

        float z0 = 0f;
        eventTrack.z0 = z0;

        eventTrack.d0 = 0f;

        generatedTracks[0] = eventTrack;

        AtlasCaloBuilder caloBuilder = new AtlasCaloBuilder();
        TStructCalo calo = new TStructCalo();
        float caloNrg = 5f;

        calo = caloBuilder.generateCalorimeter(_theta, phi0, caloNrg);
        //calo = caloBuilder.generateCalorimeter(0, phi0, caloNrg);
        listCalo.Add(calo);

        TStructCalo[] calosArray = new TStructCalo[1];
        calosArray[0] = listCalo[0];
        generatedEvent.calos = calosArray;
        generatedEvent.tracks = generatedTracks;
        generatedEvent.maxPt = 10f / 15f;
    }


    void generateRandomTracks(float energy, float lrBalance)
    {

        /*generic tracks*/
        int numTracks = determineNumberOfTracks(energy);       
        
        float pi = Mathf.PI;

        /* determine biased range*/
        lrBalance *= 1.35f;
        float minTheta = Mathf.Clamp(lrBalance * 90f, 0f, 180f);
        float maxTheta = Mathf.Clamp(lrBalance * 90f + 180f, 0f, 180f);
        

        //float minTheta = 0f;
        //float maxTheta = 180f;



        /**Build MTracks, assigning them random values**/

        TStructTrack[] generatedTracks = new TStructTrack[numTracks];

        List<TStructCalo> listCalo = new List<TStructCalo>();

        for (int i = 0; i < numTracks; i++)
        {
            //Random.seed = System.DateTime.Now.Ticks;


            TStructTrack eventTrack = new TStructTrack();
            float _pt = Random.Range(0f, 10f);
            _pt = _pt / 15.0f;
            eventTrack.pt = _pt;


            float _theta = Random.Range(minTheta, maxTheta);
            float _cottheta = 1.0f / Mathf.Tan(_theta);
            eventTrack.cottheta = _cottheta;
            eventTrack.theta = _theta;

            float phi0 = Random.Range(-pi, pi);
            eventTrack.phi = phi0;

            float z0 = 0f;
            eventTrack.z0 = z0;

            eventTrack.d0 = 0f;

            generatedTracks[i] = eventTrack;

            /** Calorimeters */
            if (_pt > 0.5 || _pt < -0.5 )
            {

                AtlasCaloBuilder caloBuilder = new AtlasCaloBuilder();
                TStructCalo calo = new TStructCalo();
                float caloNrg = Random.Range(0.5f, 5.0f);

                calo = caloBuilder.generateCalorimeter(_theta, phi0, caloNrg);
                //calo = caloBuilder.generateCalorimeter(0, phi0, caloNrg);
                listCalo.Add(calo);
            }
        }

        TStructCalo[] calosArray = new TStructCalo[listCalo.Count];
        for (int i = 0; i < listCalo.Count; i++)
        {
            calosArray[i] = listCalo[i];
        }

        generatedEvent.calos = calosArray;
        generatedEvent.tracks = generatedTracks;
        generatedEvent.maxPt = 10f / 15f;





        /** build a MMisEt*/
        /*MMisEt eventMisEt = new MMisEt();
        eventMisEt.setEt(Random.Range(0f, 10.0f));
        eventMisEt.setEtx(Random.Range(-10.0f, 10.0f));
        eventMisEt.setEty(Random.Range(-10.0f, 10.0f));*/

    }

    void generateRandomCalorimeters(float energy)
    {

    }

    void generateRandomJets(float energy, float lrBalance)
    {
        int numJets = (int)Mathf.Floor(Random.Range(0f, energy));
        TStructJet[] generatedJets = new TStructJet[numJets];

        lrBalance *= 1.35f;
        float minTheta = Mathf.Clamp(lrBalance * 90f, 0f, 180f);
        float maxTheta = Mathf.Clamp(lrBalance * 90f + 180f, 0f, 180f);


        for (int i = 0; i < numJets; i++)
        {
            //Debug.Log("Jet");
            TStructJet eventJet = new TStructJet();
            eventJet.et = (Random.Range(0f, energy * 1.2f));

            float thetaJet = Random.Range(minTheta, maxTheta);
            float eta = -Mathf.Log(Mathf.Tan(thetaJet / 2f));
            
            eventJet.eta = -eta;

            eventJet.phi = Random.Range(0f, 2f * Mathf.PI);
            generatedJets[i] = eventJet;
        }

        generatedEvent.jets = generatedJets;
    }

}