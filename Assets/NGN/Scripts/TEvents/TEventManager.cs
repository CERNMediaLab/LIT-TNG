using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Text;

public class TEventManager : MonoBehaviour
{

    //==== BASIC SETTINGS ======
    public bool displayTrack = true;
    public bool displayShuriken = false;
    public bool displayInfoBoxes = false;
    public bool isMouseActive = false;
    private bool autoTeleport = false;
    private bool autoKill = false;
    private bool killLastEvent = true;
    private bool displayCollidingProtons = true;
    private float explodeTime = 3f;

    private bool isInitialized = false;


    public List<TEventAgent> eventAgents;
    private GameObject parentEvent;// buffer of ParentEvent object. Necessary because of the delay in creating GameObjects
    public TStructEvent eventToHire;// buffer of events to hire
    private bool areThereEventsToHire = false;

    TAtlasEventGenerator atlasGenerator;
    private LHCbEventLoader LHCBLoader;
    private CMSEventLoader cmsEventLoader;
    private ALICEEventLoader ALICELoader;

    private WWW www;
    private string EventData = "";
    private bool eventIsLoaded = false;
    private string currentExperiment;
    private bool displayLoadProgress = false;


    public TStructEvent lastEvent;

    private Vector3 lastEventPos;
    public float lastEventScale = 1f;
    private float lastEventRotation = 0f;
    public float timeToLive;

    // The singleton instance of TEventManager
    protected static TEventManager instance = null;


    //public MouseControl mouseControl;



    public GameObject eventInfoObject;
    public GameObject eventInfoTextObject;

    public GameObject trackInfoObject;
    public GameObject trackInfoTextObject;


    public static TEventManager Instance
    {
        get
        {
            return instance;
        }
    }

    void Start ()
    {
        // set the singleton instance
        instance = this;
        Initialize();
    }
    public void SetDisplayCollidingProtons(bool dcp)
    {
        displayCollidingProtons = dcp;
    }

    public void SetAutoTeleport(bool at)
    {
        autoTeleport = at;
    }

    public void SetAutoKill(bool sak)
    {
        autoKill = sak;
    }

    public void SetRotation(float rot)
    {
        lastEventRotation = rot;
    }

    public void SetScale(float scale)
    {
        lastEventScale = scale;
    }

    public bool GetDisplayCollidingProtons()
    {
        return displayCollidingProtons;
    }



    public void Initialize()
    {
        if (!isInitialized)
        {
            isInitialized = true;

            eventAgents = new List<TEventAgent>();
            atlasGenerator = new TAtlasEventGenerator();
            atlasGenerator.SetEventManager(this);
            LHCBLoader = new LHCbEventLoader();
            cmsEventLoader = new CMSEventLoader();
            ALICELoader = new ALICEEventLoader();
            Debug.Log("START of TEventManager");
            if (ALICELoader == null) {
                Debug.Log("NO ALICE Event Loader!!!");
            }
            parentEvent = null;

            GameObject camera = GameObject.Find("Main Camera");
            //mouseControl = (MouseControl) camera.AddComponent<MouseControl>();
            //mouseControl.SetActive(isMouseActive);



            if (displayInfoBoxes)
            {
                eventInfoObject = (GameObject) GameObject.Instantiate(Resources.Load("Prefabs/InfoBox"));
                eventInfoTextObject = (GameObject) eventInfoObject.transform.Find("InfoText").gameObject;
                eventInfoObject.active = false;

                trackInfoObject = (GameObject) GameObject.Instantiate(Resources.Load("Prefabs/InfoBox"));
                trackInfoTextObject = (GameObject) trackInfoObject.transform.Find("InfoText").gameObject;
                trackInfoObject.active = false;
            }


        }
    }


    public void AddEventAgent(TEventAgent eag)
    {
        eventAgents.Add(eag);
        //eag.SetManager(this);
    }

    public void RemoveEventAgent(TEventAgent eag)
    {
        if (eventAgents.Contains(eag))
        {
            eventAgents.Remove(eag);
        }
    }


    //**** External hooks ***///

    public TStructEvent GenerateEvent(float energy, float lrBalance)
    {
        //==========Jenninator============
        lastEventPos = new Vector3(0, 0, 0);
        timeToLive = 0f;
        //lastEventScale = 1f;
        //==========/Jenninator============
        return atlasGenerator.generateEvent(energy, lrBalance);
    }

    public TStructEvent GenerateTestTrack()
    {
        return atlasGenerator.generateTestEvent();
    }

    //==========Jenninator============
    public TStructEvent GenerateEvent(float energy, float ttl, Vector3 pos, float scale, float lrBalance)
    {
        lastEventPos = pos;
        timeToLive = ttl;
        lastEventScale = scale;
        if (ttl > 0) SetAutoKill(true);
        //Debug.Log(atlasGenerator);
        return atlasGenerator.generateEvent(energy, lrBalance);
    }
    //==========/Jenninator============

    public void HireAgent(TStructEvent strEvt, Vector3 pos)
    {
        lastEventPos = pos;
        HireAgent(strEvt);
    }

    public void HireAgent(TStructEvent strEvt)
    {
        if (parentEvent != null)
        {
            Debug.Log("Only one event hired per frame");
        }
        else
        {
            parentEvent = new GameObject("ParentEvent");
            //Debug.Log("Hp1");
            eventToHire = strEvt;
            areThereEventsToHire = true;
            lastEvent = strEvt;

            //Debug.Log(lastEvent);
        }
    }

    public void HireAgentCycle()
    {
        if (areThereEventsToHire && parentEvent != null)
        {
            //Debug.Log("We're hiring");
            TEventAgent agent = (TEventAgent)parentEvent.AddComponent<TEventAgent>();


            bool generateTracks = !eventToHire.hasPolylines;

            if (eventToHire.experiment == "LHCb")
            {
                agent.eventScaleFactor = 0.0101f;
                agent.SetExplosionLength(30000f);
            }
            else if (eventToHire.experiment == "ATLAS")
            {
                agent.eventScaleFactor = 1.0f;
            }

            else if (eventToHire.experiment == "CMS")
            {
                Debug.Log("Hired a CMS event");
            }
            else if (eventToHire.experiment == "ALICE")
            {
                agent.eventScaleFactor = 1.0f;
                Debug.Log("Hired ALICE event");
            }

            agent.eventScaleFactor *= lastEventScale;
            agent.autoTeleport = autoTeleport;
            agent.autoKill = autoKill;
            agent.displacePosition = lastEventPos;
            agent.displaceRotation = lastEventRotation;
            agent.SetDisplayTrack(displayTrack);
            agent.SetDisplayShuriken(displayShuriken);
            agent.SetParentEvent(parentEvent);
            agent.SetStart();
            agent.SetExplodeTime(explodeTime);
            agent.SetStructEvent(ref eventToHire);

            AddEventAgent(agent);
            parentEvent = null;
            //    eventToHire = null;
            //    eventToHire = new TStructEvent("", false);
            areThereEventsToHire = false;


            //agent.ParentEvent.transform.localScale *= lastEventScale;

            agent.timeToLive = timeToLive;


            lastEvent = eventToHire;
        }
        else
        {
            areThereEventsToHire = false;
            Debug.Log("nope");
        }

    }

    public void SetExplodeTime(float xpldtime)
    {
        explodeTime = xpldtime;
    }

    public void ExplodeAll()
    {
        if (eventAgents.Count > 0)
        {
            for (int i = 0; i < eventAgents.Count; i++)
            {
                eventAgents[i].Explode();
            }
        }
    }


    public void LoadEventFromFile(string experiment, string filePath)
    {
        eventIsLoaded = false;
        currentExperiment = experiment;
        www = new WWW(filePath);
    }

    private void Update()
    {
        if (Input.GetKeyUp ("1"))
        {
            displayTrack = !displayTrack;
            Debug.Log("Display Track: " + displayTrack);
        }

        if (Input.GetKeyUp ("2"))
        {
            displayShuriken = !displayShuriken;
            Debug.Log("Display displayShuriken: " + displayShuriken);
        }

        /*if (Input.GetKeyUp ("b"))
        {
            Vector2 random  = Random.insideUnitCircle;
            //SetAutoKill(true);
            lastEvent = GenerateEvent(Random.value * 10, 2.0f, new Vector3(random.x * 50, random.y * 20, 0), 0.3f);

        }*/


        //Debug.Log(areThereEventsToHire);
        if (areThereEventsToHire)
        {
            //Debug.Log(areThereEventsToHire);
            //areThereEventsToHire = false;
            HireAgentCycle();

        }

        if (www != null && !eventIsLoaded)
        {
            if ( www.isDone)
            {
                EventData = www.text;
                if (currentExperiment == "LHCb")
                {
                    HireAgent(LHCBLoader.LoadFromJson(EventData));
                }

                if (currentExperiment == "CMS")
                {
                    Debug.Log("CMS event loaded. Now read in JSON");
                    HireAgent(cmsEventLoader.LoadFromJson(EventData));
                }

                if (currentExperiment == "ALICE")
                {
//                    HireAgent(ALICELoader.LoadFromXML(EventData));
                    HireAgent(ALICELoader.LoadFromJson(EventData));
                }

                www = null;
                eventIsLoaded = true;
                displayLoadProgress = false;
            }
            else
            {
                displayLoadProgress = true;

            }
        }
    }


    private void OnGUI()
    {

        if (displayLoadProgress)
        {
            string percent = System.String.Format("{0:F2} % Loaded ", 100f * www.progress);
            GUI.Label(new Rect(25, 65, 280, 50), percent + www.isDone );
        }

        /*if (EventData != "")
        {
            GUI.Label(new Rect(25, 165, 480, 50), EventData);
        }*/

    }



    public void Teleport()
    {
        if (LPlayerPF.Instance != null)
        {
            LPlayerPF.Instance.Teleport(lastEvent);
        }
    }


    //--------------------------Show information functions-------------------------------
    public string GetEventInfo(int id)
    {
        if (id < eventAgents.Count)
        {
            return eventAgents[id].GetTStructEventInfo().ToString();
        }
        else
        {
            return "There is no event to display information";
        }
    }
    //--------------------------End of show information functions-------------------------------

    public void KillLastEvent()
    {
        if (killLastEvent && (eventAgents.Count > 0))
        {
            eventAgents[eventAgents.Count - 1].FadeOutAndDestroy();
        }
    }

}
