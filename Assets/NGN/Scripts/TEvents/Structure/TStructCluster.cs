using UnityEngine;
using System.Collections;

[System.Serializable]
public struct TStructCluster
{
    
    public Vector3[] points {get; set;}
    
    public string pname {get; set;}
    public float E  {get; set;}
    
    public TStructCluster(Vector3[] _points)
    {
        points = _points;
        pname = "Cluster";
        E = float.NaN;
    }
}
