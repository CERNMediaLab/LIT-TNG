﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct TStructTeleportTrack
{

    public float[] polylineX {get; set;}
    public float[] polylineY {get; set;}
    public float[] polylineZ {get; set;}

    public string pname {get; set;}
    public float E  {get; set;}
    public float px {get; set;}
    public float py {get; set;}
    public float pz {get; set;}
    public float pt {get; set;}
    public float q {get; set;}
    public float z0 {get; set;}
    public float d0 {get; set;}
    public float m {get; set;}
    public float chi2 {get; set;}
    public float eta {get; set;}
    public float cottheta {get; set;}
    public float theta {get; set;}
    public float phi {get; set;}
    public float mass {get; set;}
    public float C {get; set;}

    

    //lhcb
    public float ip { get; set; }
    public float pv_z { get; set; }
    public float pv_y { get; set; }
    public float pv_x { get; set; }
    public float ipchi2 { get; set; }
    public float zFirstMeasurement { get; set; }

    public TStructTeleportTrack(TStructTrack tst)
    {
    	polylineX = null;
        polylineY = null;
        polylineZ = null;
        if(tst.polyline == null)
    	{
    		//Debug.Log("Teleport Track: Polyline is null");
    	}
    	else 
    	{
    		polylineX = new float[tst.polyline.Length];
	    	polylineY = new float[tst.polyline.Length];
	    	polylineZ = new float[tst.polyline.Length];
            for(int i = 0; i < tst.polyline.Length; i ++)
	    	{
            	polylineX[i] = tst.polyline[i].x;
	    		polylineY[i] = tst.polyline[i].y;
	    		polylineZ[i] = tst.polyline[i].z;
	    	}
	    }

    	pname = tst.pname;
        E = tst.E;
        px = tst.px;
        py = tst.py;
        pz = tst.pz;
        pt = tst.pt;
        q = tst.q;
        z0 = tst.z0;
        d0 = tst.d0;
        m = tst.m;
        chi2 = tst.chi2;
        eta = tst.eta;
        cottheta = tst.cottheta;
        theta = tst.theta;
        phi = tst.phi;
        mass = tst.mass;
        C = tst.C;

        ip = tst.ip;
        pv_x = tst.pv_x;
        pv_y = tst.pv_y;
        pv_z = tst.pv_z;
        ipchi2 = tst.ipchi2;
        zFirstMeasurement = tst.zFirstMeasurement;
    }
}
