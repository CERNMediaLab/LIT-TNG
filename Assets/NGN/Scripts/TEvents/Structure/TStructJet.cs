﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct TStructJet
{

    public float et {get; set;}
    public float eta {get; set;}
    public float phi {get; set;}
    public float theta {get; set;}
}
