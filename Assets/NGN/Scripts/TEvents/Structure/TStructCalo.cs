﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TStructCalo
{
    public float energy
    {
        get;
        set;
    }
    public string name
    {
        get;
        set;
    }

    /**Vertexes */
    public Vector3 p1
    {
        get;
        set;
    }
    public Vector3 p2
    {
        get;
        set;
    }
    public Vector3 p3
    {
        get;
        set;
    }
    public Vector3 p4
    {
        get;
        set;
    }

    public Vector3 p1u
    {
        get;
        set;
    }
    public Vector3 p2u
    {
        get;
        set;
    }
    public Vector3 p3u
    {
        get;
        set;
    }
    public Vector3 p4u
    {
        get;
        set;
    }

    public TStructCalo() { }



    public TStructCalo(int x, int dx, int y, int dy, int z, int dz)
    {
        p1 = new Vector3(x, y, z);
        p2 = new Vector3(x + dx, y, z);
        p3 = new Vector3(x + dx, y + dy, z);
        p4 = new Vector3(x, y + dy, z);

        p1u = new Vector3(x, y, z + dz);
        p2u = new Vector3(x + dx, y, z + dz);
        p3u = new Vector3(x + dx, y + dy, z + dz);
        p4u = new Vector3(x, y + dy, z + dz);
    }

    /*** For Horizontal Calorimeters, e.g LHCb ***/

    public TStructCalo(int x, int dx, int y, int dy, int z, int dz, float energyScale, float globalscale, float pyramidScale)
    {
        p1 = new Vector3(x, y, z)*globalscale;
        p2 = new Vector3(x + dx, y, z)*globalscale;
        p3 = new Vector3(x + dx, y + dy, z)*globalscale;
        p4 = new Vector3(x, y + dy, z)*globalscale;

        float xyscale = 1f/(energyScale) + pyramidScale*(1f - 1f/(energyScale));


        p1u = new Vector3(x*xyscale , y*xyscale , z + dz)*globalscale *energyScale;
        p2u = new Vector3((x + dx)*xyscale, y*xyscale, z + dz)*globalscale *energyScale;
        p3u = new Vector3((x + dx)*xyscale, (y + dy)*xyscale, z + dz)*globalscale *energyScale;
        p4u = new Vector3(x*xyscale, (y + dy)*xyscale, z + dz)*globalscale *energyScale;

    }


    public TStructCalo(TStructTeleportCalo tttcalo)
    {
        energy = tttcalo.energy;
        name = tttcalo.name;

        p1 = new Vector3(tttcalo.p1X, tttcalo.p1Y, tttcalo.p1Z);
        p2 = new Vector3(tttcalo.p2X, tttcalo.p2Y, tttcalo.p2Z);
        p3 = new Vector3(tttcalo.p3X, tttcalo.p3Y, tttcalo.p3Z);
        p4 = new Vector3(tttcalo.p4X, tttcalo.p4Y, tttcalo.p4Z);

        p1u = new Vector3(tttcalo.p1uX, tttcalo.p1uY, tttcalo.p1uZ);
        p2u = new Vector3(tttcalo.p2uX, tttcalo.p2uY, tttcalo.p2uZ);
        p3u = new Vector3(tttcalo.p3uX, tttcalo.p3uY, tttcalo.p3uZ);
        p4u = new Vector3(tttcalo.p4uX, tttcalo.p4uY, tttcalo.p4uZ);
    }
    /**
        ANA: ToString override - needed for checkup
     */
    public override  string ToString()
    {
        return this.p1.ToString() + " " + this.p1u.ToString() + " " + this.p2.ToString() + " " + this.p2u.ToString() + " " + this.p3.ToString() + " " + this.p3u.ToString();
    }
}
