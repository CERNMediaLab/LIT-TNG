﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct TStructTeleportEvent
{
    public string experiment {get; set;}
    public float energy {get; set;}
    public int numtracks {get; set;}
    public float etmiss {get; set;}
    public float maxPt {get; set;}

    public bool isLoaded {get; set;}
    public bool hasPolylines {get; set;}

    public bool isCaloGeometryReady {get; set;}
    //public TEventAgent agent;

    public TStructTeleportTrack[] tracks {get; set;}
    public TStructJet[] jets {get; set;}
    public TStructTeleportCalo[] calos {get; set;}
    //TMisEt misEt;

    private float eventScale {get; set;}

    public bool isLive {get; set;}
    public bool isGenerated {get; set;}

    public TStructTeleportEvent(TStructEvent tse)
    {
        experiment = tse.experiment;
        energy = tse.energy;
        numtracks = tse.numtracks;
        etmiss = tse.etmiss;
        maxPt = tse.maxPt;        

        isLoaded = tse.isLoaded;
        hasPolylines = tse.hasPolylines;

        isCaloGeometryReady = tse.isCaloGeometryReady;

        tracks = new TStructTeleportTrack[tse.tracks.Length];

        for (int i = 0; i < tse.tracks.Length; i ++)
        {
            tracks[i] = new TStructTeleportTrack(tse.tracks[i]);
        }

        jets = tse.jets;

        calos = null;

        if (tse.calos != null)
        {
            calos = new TStructTeleportCalo[tse.calos.Length];
            for (int i = 0; i < tse.calos.Length; i ++)
            {
                calos[i] = new TStructTeleportCalo(tse.calos[i]);
            }
        }

        eventScale = tse.eventScale;

        isLive = tse.isLive;
        isGenerated = tse.isGenerated;

    }

    public TStructEvent convertToTStructEvent()
    {
        TStructEvent tse = new TStructEvent();

        tse.experiment = experiment;
        tse.energy = energy;
        tse.numtracks = numtracks;
        tse.etmiss = etmiss;
        tse.maxPt = maxPt;

        tse.isLoaded = isLoaded;
        tse.hasPolylines = hasPolylines;

        tse.isCaloGeometryReady = isCaloGeometryReady;

        tse.tracks = new TStructTrack[tracks.Length];
        for (int i = 0; i < tracks.Length; i ++)
        {
            tse.tracks[i] = new TStructTrack(tracks[i]);
        }

        tse.jets = jets;

        if (calos != null)
        {
            tse.calos = new TStructCalo[calos.Length];
            for (int i = 0; i < calos.Length; i ++)
            {
                tse.calos[i] = new TStructCalo(calos[i]);
            }
        }

        tse.eventScale = eventScale;

        tse.isLive = isLive;
        tse.isGenerated = isGenerated;

        return tse;
    }
}