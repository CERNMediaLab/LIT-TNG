﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TStructTeleportCalo {

	public float energy {get; set;}
    public string name {get; set;}    

    /**Vertexes */
	public float p1X {get; set;}
	public float p1Y {get; set;}
	public float p1Z {get; set;}

    public float p2X {get; set;}
    public float p2Y {get; set;}
    public float p2Z {get; set;}

    public float p3X {get; set;}
    public float p3Y {get; set;}
    public float p3Z {get; set;}

    public float p4X {get; set;}
    public float p4Y {get; set;}
    public float p4Z {get; set;}

    public float p1uX {get; set;}
    public float p1uY {get; set;}
    public float p1uZ {get; set;}

    public float p2uX {get; set;}
    public float p2uY {get; set;}
    public float p2uZ {get; set;}

    public float p3uX {get; set;}
    public float p3uY {get; set;}
    public float p3uZ {get; set;}

    public float p4uX {get; set;}
    public float p4uY {get; set;}
    public float p4uZ {get; set;} 

	public TStructTeleportCalo(TStructCalo calo)
    {
    	energy = calo.energy;
    	name = calo.name;
    	
    	p1X = calo.p1.x;
    	p1Y = calo.p1.y;
    	p1Z = calo.p1.z;

    	p2X = calo.p2.x;
    	p2Y = calo.p2.y;
    	p2Z = calo.p2.z;

    	p3X = calo.p3.x;
    	p3Y = calo.p3.y;
    	p3Z = calo.p3.z;

    	p4X = calo.p4.x;
    	p4Y = calo.p4.y;
    	p4Z = calo.p4.z;

    	p1uX = calo.p1u.x;
    	p1uY = calo.p1u.y;
    	p1uZ = calo.p1u.z;

    	p2uX = calo.p2u.x;
    	p2uY = calo.p2u.y;
    	p2uZ = calo.p2u.z;

    	p3uX = calo.p3u.x;
    	p3uY = calo.p3u.y;
    	p3uZ = calo.p3u.z;

    	p4uX = calo.p4u.x;
    	p4uY = calo.p4u.y;
    	p4uZ = calo.p4u.z;
    }
}
