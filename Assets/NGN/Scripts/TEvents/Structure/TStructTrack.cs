using UnityEngine;
using System.Collections;

[System.Serializable]
public struct TStructTrack
{

    public Vector3[] polyline {get; set;}

    public string pname {get; set;}
    public float E  {get; set;}
    public float px {get; set;}
    public float py {get; set;}
    public float pz {get; set;}    
    public float q {get; set;}
    public float mass {get; set;}
    public float chi2 {get; set;}

    //Not in the constructor
    public float z0 {get; set;}
    public float d0 {get; set;}
    public float m {get; set;}    
    public float eta {get; set;}
    public float cottheta {get; set;}
    public float phi {get; set;}
    
    public float C {get; set;}    
    public float theta {get; set;}


    //lhcb    
    public float ip { get; set; }
    public float pv_z { get; set; }
    public float pv_y { get; set; }
    public float pv_x { get; set; }    
    public float zFirstMeasurement { get; set; }

    public float pt {get; set;}
    public float ipchi2 { get; set; }

    public TStructTrack(float _E, string _name, float _m, float _trackchi2, float _ip, float _px, float _py, float _pz, float _pvx, float _pvy, float _pvz, float _q, float _zFirstMeasurement)
    {
        this.pname = _name;
        this.E = _E;
        this.px = _px; this.py = _py; this.pz = _pz;
        this.q = _q;        
        this.mass = _m;        
        
        //LHCb
        this.ip = _ip;
        this.pv_x = _pvx; this.pv_y = _pvy; this.pv_z = _pvz;
        this.chi2 = _trackchi2;      
        this.zFirstMeasurement = _zFirstMeasurement;

        //Extra initializations; default values       
        this.pt = float.NaN;
        this.ipchi2 = float.NaN;
        this.z0 = float.NaN;
        this.d0 = float.NaN;
        this.m = float.NaN;
        this.eta = float.NaN;
        this.cottheta = float.NaN;
        this.phi = float.NaN;
        this.theta = float.NaN;
        this.C = float.NaN;        
        

        this.polyline = null;
    }

    public TStructTrack(TStructTeleportTrack tstt)
    {
        if(tstt.polylineX == null)
        {
            //Debug.Log("Track: Polyline is null");
            polyline = null;
        }
        else 
        {
            int len = tstt.polylineX.Length;
            polyline = new Vector3[len];
            for(int i = 0; i < len; i ++) 
            {
                polyline[i] = new Vector3(tstt.polylineX[i], tstt.polylineY[i], tstt.polylineZ[i]);
            }
        }
        
        pname =  tstt.pname;
        E = tstt.E;
        px = tstt.px;
        py = tstt.py;
        pz = tstt.pz;
        pt = tstt.pt;
        q = tstt.q;
        z0 = tstt.z0;
        d0 = tstt.d0;
        m = tstt.m;
        chi2 = tstt.chi2;
        eta = tstt.eta;
        cottheta = tstt.cottheta;
        phi = tstt.phi;
        mass = tstt.mass;
        C = tstt.C;
        theta = tstt.theta;
        
        ip = tstt.ip;
        pv_x = tstt.pv_x;
        pv_y = tstt.pv_y;
        pv_z = tstt.pv_z;
        ipchi2 = tstt.ipchi2;
        zFirstMeasurement = tstt.zFirstMeasurement;
    }
}
