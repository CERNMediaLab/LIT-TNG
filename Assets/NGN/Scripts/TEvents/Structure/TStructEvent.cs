using UnityEngine;
using System.Collections;

[System.Serializable]
public struct TStructEvent
{
    public string experiment {get; set;}
    public float energy {get; set;}
    public int numtracks {get; set;}
    public float etmiss {get; set;}
    public float maxPt {get; set;}

    public bool isLoaded {get; set;}
    public bool hasPolylines {get; set;}

    public bool isCaloGeometryReady {get; set;}
    //public TEventAgent agent;

    public TStructTrack[] tracks {get; set;}
    public TStructJet[] jets {get; set;}
    public TStructCalo[] calos {get; set;}
    public TStructCluster[] clusters {get; set;}
    //TMisEt misEt;

    public float eventScale {get; set;}
    public float caloScale {get; set;}


    public bool isLive {get; set;}
    public bool isGenerated {get; set;}

    public TStructEvent(string exp, bool isGen) {
    	experiment = exp;
    	hasPolylines = false;
    	isCaloGeometryReady = false;
        isLoaded = false;
        hasPolylines = false;
    	eventScale = 0.1f;
        caloScale = float.NaN;

    	isLive = false;
    	isGenerated = isGen;

        energy = 0f;
        etmiss = 0f;
        maxPt = 0f;
        numtracks = 0;

        tracks = null;
        jets = null;
        calos = null;
        clusters = null;
    }
}