﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'


/*
	Magnetic line shader
	
	@author: Tarjei S. Maalsnes, CERN Media Lab
	@email: tarjei.malsnes@cern.ch
*/

Shader "MediaLab/MagWaveShader"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_EdgePower ("Edge power", float) = 3.0
		_Alpha ("Alpha", float) = 1.0
	}
	
	SubShader
	{
		Pass
		{
       		Tags
       		{
       			"Queue"="Transparent"
       			"RenderType"="Transparent"
       		}
       		
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Always
			
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			
			uniform float4 _Color;
			uniform float _EdgePower;
			uniform float _Alpha;
			
			struct vertexIn
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};
			
			struct vertexOut
			{
				float4 pos : SV_POSITION;
				float3 normalDir : TEXCOORD0;
				float3 viewDir : TEXCOORD1;
			};
			
			vertexOut vert(vertexIn v)
			{
				float4x4 modelMatrix = unity_ObjectToWorld;
				float4x4 normalMatrix = unity_WorldToObject;
				
				vertexOut o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.normalDir = mul(float4(v.normal, 0.0), normalMatrix).xyz;
				o.viewDir = _WorldSpaceCameraPos.xyz - mul(modelMatrix, v.vertex).xyz;
				
				return o;
			}
			
			float4 frag(vertexOut v) : COLOR
			{
				float3 viewDir = normalize(v.viewDir);
				half edgeAmount = 1.0 - saturate(dot(viewDir, normalize(v.normalDir)));
				
				float4 col = _Color * pow(edgeAmount, _EdgePower);
				return col;
			}
			
			ENDCG
		}
	}
}
