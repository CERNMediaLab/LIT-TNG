// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "MediaLab/AlphaMask"
{
	Properties
	{
		_Texture ( "Texture", 2D ) = ""
		_Mask ( "Mask", 2D) = ""
	}

	SubShader
	{
		LOD 300
		Lighting Off

		Pass
		{
			Tags
       		{
       			"Queue"="Transparent"
       			"RenderType"="Transparent"
       		}

			Blend SrcAlpha OneMinusSrcAlpha
			//Cull Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform sampler2D _Texture;
			uniform sampler2D _Mask;

			float4 _Texture_ST;
			float4 _Mask_ST;

			struct vertexIn
			{
				float4 vertex : POSITION;
				float2 tex_coord : TEXCOORD0;
				float2 mask_coord : TEXCOORD1;
			};

			struct vertexOut
			{
				float4 pos : SV_POSITION;
				float2 tex_coord : TEXCOORD0;
				float2 mask_coord : TEXCOORD1;
			};

			vertexOut vert(vertexIn v)
			{
				float4x4 modelMatrix = unity_ObjectToWorld;

				vertexOut o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.tex_coord = TRANSFORM_TEX(v.tex_coord, _Texture);
				o.mask_coord = TRANSFORM_TEX(v.mask_coord, _Mask);

				return o;
			}

			float4 frag(vertexOut v) : COLOR
			{
				float4 tex = tex2D(_Texture, v.tex_coord.xy);
				float4 m = tex2D(_Mask, v.mask_coord.xy);
				float newAlpha = min((m.r + m.g + m.b)/3, tex.a);	// grayscale
				tex.a = newAlpha;

				return tex;
			}

			ENDCG
		}
	}
}
