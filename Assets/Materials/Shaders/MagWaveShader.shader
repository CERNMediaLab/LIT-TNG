﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


/*
	Magnetic line shader

	@author: Tarjei S. Maalsnes, CERN Media Lab
	@email: tarjei.malsnes@cern.ch
*/

Shader "MediaLab/MagWaveShader"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
	}

	SubShader
	{
		Pass
		{
       		Tags
       		{
       			"Queue"="Transparent"
       			"RenderType"="Transparent"
       		}

			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			uniform float4 _Color;

			struct vertexIn
			{
				float4 vertex : POSITION;
			};

			struct vertexOut
			{
				float4 pos : SV_POSITION;
			};

			vertexOut vert(vertexIn v)
			{
				vertexOut o;
				o.pos = UnityObjectToClipPos(v.vertex);

				return o;
			}

			float4 frag(vertexOut v) : COLOR
			{
				float4 col = _Color;
				return col;
			}

			ENDCG
		}
	}
}
