// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'


Shader "MediaLab/Translucent"
{
	Properties
	{
		_Alpha ( "Alpha", Range ( 0, 1 ) ) = 1.0
		_Texture ( "Texture", 2D ) = ""
	}

	SubShader
	{
		LOD 300
		Lighting Off

		Pass
		{
			Tags
       		{
       			"Queue"="Transparent"
       			"RenderType"="Transparent"
       		}

			Blend SrcAlpha OneMinusSrcAlpha
			//Cull Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			uniform sampler2D _Texture;
			uniform float _Alpha;

			struct vertexIn
			{
				float4 vertex : POSITION;
				float4 tex_coord : TEXCOORD0;
			};

			struct vertexOut
			{
				float4 pos : SV_POSITION;
				float4 tex_coord : TEXCOORD0;
			};

			vertexOut vert(vertexIn v)
			{
				float4x4 modelMatrix = unity_ObjectToWorld;

				vertexOut o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.tex_coord = v.tex_coord;

				return o;
			}

			float4 frag(vertexOut v) : COLOR
			{
				float4 tex = tex2D(_Texture, v.tex_coord.xy);
				tex.a = _Alpha;

				return tex;
			}

			ENDCG
		}
	}
}
