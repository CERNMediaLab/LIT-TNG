﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'


Shader "MediaLab/RF_MagWaveShader2"
{
	Properties
	{
		_MainTex ("Texture 1", 2D) = "red" {}
		_Tex2 ("Texture 2", 2D) = "blue" {}
		_Blend ("Texture blend", range(0.0, 1.0)) = 0.5
		_Alpha ("Alpha", range(0.0, 1.0)) = 1.0
	}
	
	SubShader
	{
		Pass
		{
       		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
       		
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			ZWrite Off
			
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			uniform sampler2D _Tex2;
			uniform float _Blend;
			uniform float _Alpha;
			
			struct vertexIn
			{
				float4 vertex : POSITION;
				float4 tex_coord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			
			struct vertexOut
			{
				float4 pos : SV_POSITION;
				float2 tex_coord : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				UNITY_VERTEX_OUTPUT_STEREO
			};

			float4 _MainTex_ST;
			
			vertexOut vert(vertexIn v)
			{
				float4x4 modelMatrix = unity_ObjectToWorld;
				float4x4 normalMatrix = unity_WorldToObject;
				
				vertexOut o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.tex_coord = TRANSFORM_TEX(v.tex_coord, _MainTex);
				
				return o;
			}
			
			float4 frag(vertexOut v) : COLOR
			{				
				float4 tex1 = tex2D(_MainTex, v.tex_coord.xy);
				float4 tex2 = tex2D(_Tex2, v.tex_coord.xy);
				float4 final = lerp(tex1, tex2, _Blend);
				
				final.a *= _Alpha;
				return final;
			}
			
			ENDCG
		}
	}
	//FallBack "Diffuse"
}
