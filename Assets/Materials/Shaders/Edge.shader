// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

/*
 * Author: Tarjei S. Maalsnes
 * tarjei.malsnes@cern.ch
 */

Shader "MediaLab/Edge"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_EdgeColor ("Edge Color", Color) = (0,0,0,1)
		_EdgePower ("Edge Power", range (0, 5)) = 3
		_Derp ("Derp", range(0, 5)) = 2
	}

	SubShader
	{
		Pass
		{
			Tags
			{
				"Queue"="Transparent"
				"RenderType"="Transparent"
				"IgnoreProjector" = "True"
			}

			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Lighting Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform float4 _Color;
			uniform float4 _EdgeColor;
			uniform float _EdgePower;
			uniform float _Derp;

			struct vertexIn
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct vertexOut
			{
				float4 pos : SV_POSITION;
				float3 normalDir : TEXCOORD0;
				float3 viewDir : TEXCOORD1;
			};

			vertexOut vert(vertexIn v)
			{
				float4x4 modelMatrix	= unity_ObjectToWorld;
				float4x4 normalMatrix	= unity_WorldToObject;

				vertexOut o;
				o.pos		= UnityObjectToClipPos(v.vertex);
				o.normalDir	= mul(float4(v.normal, 0.0), normalMatrix).xyz;
				o.viewDir	= _WorldSpaceCameraPos.xyz - mul(modelMatrix, v.vertex).xyz;

				return o;
			}

			float4 frag(vertexOut v) : COLOR
			{
				float3 viewDir		= normalize(v.viewDir);
				float3 normalDir	= normalize(v.normalDir);
				half edgeAmount		= 1 - saturate(pow(dot(viewDir, normalDir), _EdgePower));

				float4 col;
				if (edgeAmount >= _Derp)
					col = _EdgeColor;

				else
				{
					col	= _Color;
					col.a		= edgeAmount;
				}

				return col;
			}

			ENDCG
		}

	}
}
