﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


/*
 * Author: Tarjei S. Maalsnes
 * tarjei.malsnes@cern.ch
 */

Shader "MediaLab/TextureSlider"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Texture2("Texture 2", 2D) = "black" {}
		_Slider	("Slider", Range(0, 1)) = 0.5
		[MaterialToggle] _Vertical ("Vertical", Float) = 0
	}
	SubShader
	{
		Tags
		{
			"RenderType"="Opaque"
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct vertex_in
			{
				float4 vertex : POSITION;
				float2 uvA : TEXCOORD0;
				float2 uvB : TEXCOORD1;
			};

			struct fragment_in
			{
				float4 vertex : SV_POSITION;
				float2 uvA : TEXCOORD0;
				float2 uvB : TEXCOORD1;
			};

			sampler2D _MainTex;
			sampler2D _Texture2;

			float4 _MainTex_ST;
			float4 _Texture2_ST;

			float _Slider;
			float _Vertical;

			fragment_in vert (vertex_in v)
			{
				fragment_in o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uvA = TRANSFORM_TEX(v.uvA, _MainTex);
				o.uvB = TRANSFORM_TEX(v.uvB, _Texture2);

				return o;
			}

			fixed4 frag (fragment_in i) : SV_Target
			{
				if (_Vertical == 0)
				{
					if (i.uvA.y < _Slider)
						return tex2D(_MainTex, i.uvA);
					else
						return tex2D(_Texture2, i.uvB);
				}
				else
				{
					if (i.uvA.x < _Slider)
						return tex2D(_MainTex, i.uvA);
					else
						return tex2D(_Texture2, i.uvB);
				}
			}
			ENDCG
		}
	}
}
