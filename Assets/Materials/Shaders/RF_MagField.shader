﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'


Shader "MediaLab/RF_MagWaveShader"
{
	Properties
	{
		_MainTex ("Texture 1", 2D) = "red" {}
		_Tex2 ("Texture 2", 2D) = "blue" {}
		_Blend ("Texture blend", range(0.0, 1.0)) = 0.5
		_Alpha ("Alpha", range(0.0, 1.0)) = 1.0
		_Color ("Color", Color) = (1,1,1,1)
	}

	SubShader
	{
		Pass
		{
       		Tags
       		{
       			"Queue"="Transparent"
       			"RenderType"="Transparent"
       		}

			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			uniform sampler2D _MainTex;
			uniform sampler2D _Tex2;
			uniform float _Blend;
			uniform float _Alpha;
			float4 _Color;

			struct vertexIn
			{
				float4 vertex : POSITION;
				float4 tex_coord : TEXCOORD0;
			};

			struct vertexOut
			{
				float4 pos : SV_POSITION;
				float4 tex_coord : TEXCOORD2;
			};

			vertexOut vert(vertexIn v)
			{
				float4x4 modelMatrix = unity_ObjectToWorld;
				float4x4 normalMatrix = unity_WorldToObject;

				vertexOut o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.tex_coord = v.tex_coord;

				return o;
			}

			float4 frag(vertexOut v) : COLOR
			{
				float4 tex1 = tex2D(_MainTex, v.tex_coord.xy);
				float4 tex2 = tex2D(_Tex2, v.tex_coord.xy);
				float4 final = lerp(tex1, tex2, _Blend);

				final.a *= _Alpha;
				final.rgb *= _Color;
				return final;
			}

			ENDCG
		}
	}
	//FallBack "Diffuse"
}
